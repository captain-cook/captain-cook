﻿using UnityEngine;
using System.Collections;

public class BladeSpeedController : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

        private float m_oldRefresh;
        private float m_currentRefresh;
        private Vector3 m_oldPosition;
        private Vector3 m_currentPosition;

	#endregion

	#region Unity

	#endregion

	#region Custom

        public float Velocity
        {
            get 
            {
                return (m_currentPosition - m_oldPosition).magnitude / (m_currentRefresh - m_oldRefresh);
            }
        }

        public float VelocityY
        {
            get
            {
                return (m_currentPosition.y - m_oldPosition.y) / (m_currentRefresh - m_oldRefresh);
            }
        }

        public Vector3 VelocityXYZ
        {
            get
            {
                return (m_currentPosition - m_oldPosition) / (m_currentRefresh - m_oldRefresh);
            }
        }

        public Vector2 VelocityXY
        {
            get
            {
                return ((Vector2)m_currentPosition - (Vector2)m_oldPosition) / (m_currentRefresh - m_oldRefresh);
            }
        }

        public void HoldPosition()
        {
            m_oldRefresh = m_currentRefresh;
            m_currentRefresh = Time.time;
            m_oldPosition = m_currentPosition;
            m_currentPosition = transform.position;
        }

	#endregion
	
}
