﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopPanel : ReferencedMonoBehaviour {

    public Image _knife;

    public Image _halo;
    public Sprite _kinfeBought;
    public TweenPosition _tPositionPole;
    [HideInInspector]
    public bool _isHovered = false;

    public Text _gold;
    public TweenScale _tScaleGold;
    private bool m_hasBounced = false;
	public float currentGold = 250;

    private bool m_alreadyBought = false;
    private TweenPosition m_tPosition;
    private TweenScale m_tScale;


	// Use this for initialization
	void Start () {

        base.LoadReferences();

        m_tPosition = GetComponent<TweenPosition>();
        m_tScale = GetComponent<TweenScale>();

        gameObject.SetActive(false);
	}
	
	

    public void DisplayShopPanel()
    {
        gameObject.SetActive(true);

        transform.localScale = Vector3.zero;
        transform.localPosition = m_tPosition.from;

        m_tPosition.PlayForward();
        m_tScale.PlayForward();
    }

    public void HandOnHover()
    {
        if (!_isHovered)
        {
            _isHovered = true;
            _halo.enabled = true;
            _tPositionPole.transform.localPosition = _tPositionPole.from;
            _tPositionPole.PlayForward();

            references._shop.PlayKnifeAnimation();
        }        
    }

    //chan,ge the sprite of the shop item and decrease the amount of gold
    public void BuyItem()
    {
        if (!m_alreadyBought)
        {
            _tPositionPole.PlayReverse();
            m_alreadyBought = true;
            _knife.sprite = _kinfeBought;
			animation.Play("A_GoldRemoval");

            references._shop.MoveKnife();
            _tScaleGold.PlayForward();

            SdAudioManager.Trigger("UI_Shop_Buy" , gameObject);
        }        
    }

    public void EndOfBouncingGold()
    {
        if (!m_hasBounced)
        {
            m_hasBounced = true;
            _tScaleGold.PlayReverse();
        }

    }

	void Update () {
		currentGold = Mathf.Round(currentGold);
		_gold.text = currentGold.ToString();
	}
}
