﻿using UnityEngine;
using System.Collections;

public class EnemyBoat : MonoBehaviour {

    private TweenScale m_tScaleCircle;
    public Material _posterMaterial;
    public bool _isDefeated = true;

	// Use this for initialization
	void Start () {

        m_tScaleCircle = GetComponentInChildren<TweenScale>();
        m_tScaleCircle.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnHover(bool isActive)
    {
        if (isActive)
        {
            m_tScaleCircle.GetComponent<MeshRenderer>().material.color = new Color(61 / 255f, 212 / 255f, 48 / 255f);
            m_tScaleCircle.enabled = true;
            m_tScaleCircle.PlayForward();
        }
        else
        {
            m_tScaleCircle.GetComponent<MeshRenderer>().material.color = Color.white;
            m_tScaleCircle.transform.localScale = m_tScaleCircle.from;
            m_tScaleCircle.enabled = false;
        }
        
    }
}
