﻿Shader "Captain-Cook/Outline2" {
	Properties {
		_OutlineColor("Outline Color", Color) = (0.5, 0.5, 0.5, 1)
		_OutlineSize("Outline Size", Float) = 0.1
		_Barycenter("Barycenter", Vector) = (0, 0, 0, 1)
	}

	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Overlay" }
		LOD 200

		CGINCLUDE
			fixed4 _OutlineColor;
			float _OutlineSize;
			float4 _Barycenter;
			sampler2D_float _CameraDepthTexture;

			//#pragma target 3.0
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float4 color2 : COLOR1;
				float4 uv1 : TEXCOORD0;
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				/*float4 relative = float4(
					v.vertex.x - _Barycenter.x,
					v.vertex.y - _Barycenter.y,
					v.vertex.z - _Barycenter.z,
					0);

				relative *= _OutlineSize;

				o.pos = mul(UNITY_MATRIX_MVP, _Barycenter + relative);*/

				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color2 = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color2.xy /= o.color2.z;
				//o.color2.y = 1 - o.color2.y;
				o.color2.xy = (0.5 * o.color2.xy) + 0.5;
				o.color = _OutlineColor;
				return o;
			}
		ENDCG

		Pass{
			Name "REGULAR"
			Tags { "LightMode" = "Always" }
			Cull Front
			ZWrite Off

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				half4 frag(v2f i) :COLOR
				{
					return i.color;
				}
			ENDCG
		}

		Pass{
			Name "BEHIND"
			Tags { "LightMode" = "Always" }
			Cull Off
			ZWrite Off
			ZTest Always

			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				half4 frag(v2f i) :COLOR
				{
						float2 p = i.color2.xy;
						p.y = 1 - p.y;
						float d = tex2D(_CameraDepthTexture, p).r;
						return half4(i.color2.z, 0, 0, 1);
						/*if(d < i.color2.z)
						{
							return half4(0,0,0,0);
						}
						else
						{
							return half4(0,0,0,1);
							return _OutlineColor;
						}*/

				}
			ENDCG
		}
	}
}
