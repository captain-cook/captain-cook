﻿using UnityEngine;
using System.Collections;

public class HandControllerBinomial : CustomHandController {

    private HandController.HandSide m_handSide;

	// Use this for initialization
    void Awake()
    {
        base.LoadReferences();
        m_handSide = GetComponent<HandController>()._side;
	}
	
	// Update is called once per frame
	void Update () {

        ShowBinomialTastes();
	}

    void ShowBinomialTastes()
    {
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        
        //the hand is on the binomial seltion space
        if (screenPosition.x > Screen.width * 0.24f && screenPosition.x < Screen.width * 0.76f 
            && screenPosition.y > Screen.height * 0.60f)
        {
            //LEFT
            if (screenPosition.x < Screen.width * 0.41f )
            {
               references._binomialManager.CheckForUpdate(m_handSide , 1);
            }
            //RIGHT 
            else if (screenPosition.x > Screen.width * 0.59f)
            {
                references._binomialManager.CheckForUpdate(m_handSide, 3);
            }
            //CENTER
            else if (screenPosition.x >= Screen.width * 0.41f &&
                screenPosition.x <= Screen.width * 0.59f)
            {
                references._binomialManager.CheckForUpdate(m_handSide,2);       
            }
        } 
        else
        {
            references._binomialManager.HandOut(m_handSide);
        }
    }
}
