﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BezierFilter : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Transform _pointA;
        public Transform _pointB;

        public Transform _modifierA;
        public Transform _modifierB;

	#endregion

	#region Private
    
        private Bezier m_bezier;

	#endregion

	#region Unity

		void Start ()
		{
            m_bezier = new Bezier(_pointA.position, _modifierA.position, _modifierB.position, _pointB.position);
		}

		void Update ()
		{
            if(_pointA != null && _pointB != null && _modifierA != null && _modifierB != null)
            {
                if (m_bezier == null)
                {
                    m_bezier = new Bezier(_pointA.position, _modifierA.position, _modifierB.position, _pointB.position);
                }
                else if (
                    m_bezier._pointA != _pointA.position ||
                    m_bezier._pointB != _pointB.position ||
                    m_bezier._modifierA != _modifierA.position ||
                    m_bezier._modifierB != _modifierB.position)
                {
                    m_bezier._pointA = _pointA.position;
                    m_bezier._pointB = _pointB.position;
                    m_bezier._modifierA = _modifierA.position;
                    m_bezier._modifierB = _modifierB.position;
                }
            }
            else
            {
                m_bezier = new Bezier(Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero);
            }
            
		}

	#endregion

	#region Custom

        public Bezier Bezier
        {
            get { return m_bezier; }
        }

	#endregion
	
}
