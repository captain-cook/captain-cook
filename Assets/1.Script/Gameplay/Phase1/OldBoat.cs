﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OldBoat : MonoBehaviour {

    //movement variables
    private bool m_isMoving = false;

    //Navigation variables
    private NavMeshAgent m_navMeshAgent;
    public LineRenderer _lineTravel;

    //Camera following the boat
    public Camera _CamZoom;

    [HideInInspector]
    public float _chunkSize = 10;

    public List<Objectif> _objectifs;
    
	// Use this for initialization
	void Start () {

        //_targetCross.enabled = false;
        m_navMeshAgent = GetComponent<NavMeshAgent>();
        //m_navMeshAgent.enabled = false;

		_lineTravel.enabled = false;
        _objectifs = new List<Objectif>();

		UpdateCam ();
	}
	
	// Update is called once per frame
	void Update () {

        //the zoomed camera follow the boat
        if ( m_isMoving )
        {
			UpdateCam();

			//display the path of the boat
			DisplayPath ();

			//look we already arrived
			if (m_navMeshAgent.hasPath && m_navMeshAgent.remainingDistance == 0)
			{
                m_isMoving = false;
			}
        }

	}

    //compuite the path and launch the movement
    public void MoveTo(Vector3 position)
    {  
		m_isMoving = true;
        Vector3 target = position;
        target.y = transform.localPosition.y;

		//launch the travel
        m_navMeshAgent.enabled = true;
        m_navMeshAgent.SetDestination(target);
    }

	//replace the zoomedCamera focus on the boat
	private void UpdateCam()
	{
		Vector3 posCam = transform.position;
		posCam.y = _CamZoom.transform.position.y;
		_CamZoom.transform.position = posCam;
	}
	
    //display the path travel
	private void DisplayPath()
	{
		_lineTravel.enabled = true;
		//get the pathfinding to display it 
		Vector3[] path = m_navMeshAgent.path.corners;
		_lineTravel.SetVertexCount (path.Length);
		_lineTravel.SetPosition (0, transform.position);
		for (int i = 1; i < path.Length; i++) {
			_lineTravel.SetPosition(i , path[i]) ;
		}
	}

    //detect the collision with other boat and objectif
    void OnTriggerEnter(Collider other)
    {
        if (other.collider.tag == "Objectif" && m_isMoving && (other.collider.transform.position - transform.position).magnitude < 2)
        {
            Debug.Log("STOP!");
            rigidbody.velocity = Vector3.zero;
            m_navMeshAgent.Stop();
            m_navMeshAgent.ResetPath();
        }
    }

    
}
