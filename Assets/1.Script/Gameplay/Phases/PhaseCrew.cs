﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseCrew : Phase  {

    public PhaseCrew()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerCrew>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerCrew>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
        _references._labelManager.ActivateLabelsUI(true);
              
    }

    public override void Exit()
    {

    }

    public override void EarlyEnter()
    {
        Camera.main.cullingMask ^= 4096;
        _references._handCamera.enabled = true;
    }

    public override void EarlyExit()
    {
        _references._handCamera.enabled = false;
        Camera.main.cullingMask |= 4096;
    }
}
