﻿using UnityEngine;
using System.Collections;

public class Vibrator : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public float _factor;

	#endregion

	#region Private

        private Vector3 m_eulerBase;

	#endregion

	#region Unity

		void Start ()
		{
		}

        void OnEnable()
        {
            m_eulerBase = transform.localEulerAngles;
        }

        void OnDisable()
        {
            transform.localEulerAngles = m_eulerBase;
        }

		void Update ()
		{
            transform.localEulerAngles = m_eulerBase + new Vector3(Random.Range(-_factor, _factor), Random.Range(-_factor, _factor), Random.Range(-_factor, _factor));
		}

	#endregion

	#region Custom

	#endregion
	
}
