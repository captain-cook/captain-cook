﻿using UnityEngine;
using System.Collections;

public class TScroll : MonoBehaviour {

    public SORecipe _recipeData;

   
    private float m_heightMove = 0.02f;
    private float m_startHeight;
    private Tourniquet m_tourniquet;

	// Use this for initialization
	void Start () {

        m_tourniquet = transform.parent.GetComponent<Tourniquet>();
        m_startHeight = transform.position.y;
	}

    public void Init(SORecipe recipe)
    {
        _recipeData = recipe;
    }

    public void FollowHand(float newHeight)
    {
        Vector3 newPosition = transform.position;
        newPosition.y = m_startHeight + newHeight * transform.localScale.y * 0.5f;
        transform.position = newPosition;
    }

    public void Reset ()
    {
        Vector3 newPosition = transform.position;
        newPosition.y = m_startHeight;
        transform.position = newPosition;
    }

    //if the recipe is not of the good type we disable the tscroll
    public void Disable()
    {

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        
    }

    //reacctive the tscroll for a new phase
    public void Enable()
    {
        GetComponent<MeshRenderer>().enabled = true;
        GetComponent<Collider>().enabled = true;
    }
}
