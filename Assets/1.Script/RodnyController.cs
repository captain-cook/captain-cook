﻿using UnityEngine;
using System.Collections;

public class RodnyController : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public ParticleSystem _fxShoot;
        public ParticleSystem _fxDropCanon;
        public Transform _ingredient;

	#endregion

	#region Private

        private int m_lastCallPlayReadyToSend = 3;

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{

		}

	#endregion

	#region Custom

        public void AnimIdleVariantRandom()
        {
            int rand = Random.Range(0, 100);
            GetComponent<Animator>().SetInteger("IdleVariant", rand);
        }

        public void PlaySoundIngredientAnimation(int shoot = 0)
        {
            SdAudioManager.Trigger("Pirate_Rodny_SendIngredient", gameObject);
            if(shoot == 1)
            {
                SdAudioManager.Play("CannonShootDist", SdAudioManager.Audioinstance.gameObject);
            }
        }

        public void PlaySoundReadyToSendAnimation()
        {
            m_lastCallPlayReadyToSend++;
            if (m_lastCallPlayReadyToSend > 2)
            {
                int rand = Random.Range(0, 100);
                int mod = rand % 20;
                if (mod >= 10)
                {
                    SdAudioManager.Trigger("Pirate_Rodny_ReadyToSend", gameObject);
                    m_lastCallPlayReadyToSend = 0;
                }
            }
        }

        public void PlaySoundSelectedAnimation()
        {
            if (m_lastCallPlayReadyToSend > 0)
            {
                SdAudioManager.Trigger("Pirate_Rodny_PlayerSelectPirate", gameObject);
            }
        }

        public void PlayFXShoot()
        {
            _fxShoot.Play();
        }

        public void PlayFXDropCanon()
        {
            _fxDropCanon.Play();
        }

        public void PlaySoundScratchAnimation()
        {
            SdAudioManager.Trigger("Pirate_Rodny_Idle01", gameObject);
        }

        public void GrabIngredient()
        {
            /*GameObject go = GetComponent<FoodDistributor>()._recipeStep._ingredient._prefab;
            go = (GameObject.Instantiate(go) as GameObject);

            go.transform.parent = _ingredient;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            Destroy(go.rigidbody);
            Destroy(go.collider);
            Destroy(go.GetComponent<CutableMeshController>());
            Destroy(go.GetComponent<Highlighter>());*/
        }

        public void DropIngredient()
        {
            Destroy(_ingredient.GetChild(0).gameObject);
        }

	#endregion
	
}
