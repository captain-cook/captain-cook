using UnityEngine;
using System.Collections;

public class SdAmbiances: iSd
{
    protected GameObject m_parentManager = SdAudioManager.Audioinstance.gameObject;

    public enum Weather
    {
        Sunny,
        Windy,
        Rainy
    };
    public enum WeatherBehaviour
    {
        Calm,
        Moderate,
        Strong
    };

    public virtual void SetParameter(string parameter, float value, GameObject source = null) { }
    public virtual void Execute(string action, GameObject source = null) 
    {
        string[] objects = action.Split('_');
        switch (objects[0])
        {
            case "General":
                SdAudioManager.SetSwitch("WeatherBehaviour", objects[1], m_parentManager);
                if (objects.Length > 2)
                    SdAudioManager.SetSwitch("Weather", objects[2], m_parentManager);
                SdAudioManager.Play("AMB_Boat", m_parentManager);
                break;
            case "Shop":
                if (objects[1] == "Stop")
                {
                    SdAudioManager.Stop("AMB_Shop", 2000, source);
                    break;
                }
                SdAudioManager.Play("AMB_Shop", source);
                break;
            default:
                break;
        }
    }

    public static void Play()
    {
        SdAudioManager.TriggerEvent(SdAudioManager.Audioinstance.gameObject, "AMB_Boat");
    }
    public static void SetWeather(Weather weather, System.Nullable<WeatherBehaviour> density = null)
    {
        if (density != null)
        {
            SdAudioManager.TriggerEvent(SdAudioManager.Audioinstance.gameObject, "Weather_Behaviour_Switch", density.ToString());
        }
        SdAudioManager.TriggerEvent(SdAudioManager.Audioinstance.gameObject, "Weather_Switch", weather.ToString());
        //SdAudioManager.TriggerEvent(SdAudioManager.Audioinstance.gameObject, "AMB_Boat_Play");
    }
    public static void Stop()
    {
        SdAudioManager.Stop("AMB_Boat",3000,SdAudioManager.Audioinstance.gameObject);
            //.TriggerEvent(SdAudioManager.Audioinstance.gameObject, "AMB_Boat_Stop");
    }
    /*public static void SetWeather(Weather weather, System.Nullable<WeatherBehaviour> density = null)
    {
        if (density != null)
        {
            SdAudioManager.TriggerEvent(SdAudioManager.Events.SetSwitch, SdAudioManager.Audioinstance.gameObject, "Weather_Behaviour_Switch", density.ToString() );
        }
        SdAudioManager.TriggerEvent(SdAudioManager.Events.SetSwitch, SdAudioManager.Audioinstance.gameObject, "Weather", weather.ToString());
        SdAudioManager.TriggerEvent(SdAudioManager.Events.Play, SdAudioManager.Audioinstance.gameObject,"AMB_Boat_Play");
    }*/
}

