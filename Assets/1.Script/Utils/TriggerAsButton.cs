﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class TriggerAsButton
{

    public string _leftTriggerName;
    public string _rightTriggerName;

    private bool m_leftState;
    private bool m_rightState;
    private bool m_oldLeftState;
    private bool m_oldRightState;

    public TriggerAsButton(string left, string right)
    {
        _leftTriggerName = left;
        _rightTriggerName = right;
    }

    public void Update()
    {
        m_oldLeftState = m_leftState;
        m_oldRightState = m_rightState;

        m_leftState = Input.GetAxis(_leftTriggerName) > 0.5f;
        m_rightState = Input.GetAxis(_rightTriggerName) > 0.5f;
    }

    public bool LeftTriggerDown { get { return m_oldLeftState == false && m_leftState == true; } }

    public bool RightTriggerDown { get { return m_oldRightState == false && m_rightState == true; } }

    public bool SLeftTriggerUp { get { return m_oldLeftState == true && m_leftState == false; } }

    public bool RightTriggerUp { get { return m_oldRightState == true && m_rightState == false; } }

    public bool LeftTrigger { get { return m_leftState; } }

    public bool RightTrigger { get { return m_rightState; } }
}
