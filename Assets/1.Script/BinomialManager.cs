﻿using UnityEngine;
using System.Collections;

public class BinomialManager : ReferencedMonoBehaviour {

    public Binomial _binomialLeft;
    public Binomial _binomialCenter;
    public Binomial _binomialRight;
    
    public Binomial _binomialWait1;
    public Binomial _binomialWait2;

    [HideInInspector]
    public int _numBinomialActive = 0;
    public int _numPhase = 0; 

    // 0 = no hands / 1 = left hand / 2 = right hand
    private bool m_isLeftHandActive = false;
    private bool m_isRightHandActive = false;
    private float m_lastSelectionSoundTrigged = 0;
    private bool m_voiceTriggedOnce = false;
   

    [HideInInspector]
    public Binomial _binomialToDisable;
    [HideInInspector]
    public SelectionButton _buttonToDisable;

    
	// Use this for initialization
	void Start () {

        base.LoadReferences();

        _binomialLeft = transform.FindChild("BinomialLeft").GetComponent<Binomial>();
        _binomialCenter = transform.FindChild("BinomialCenter").GetComponent<Binomial>();
        _binomialRight = transform.FindChild("BinomialRight").GetComponent<Binomial>();
        
        _binomialWait1.gameObject.SetActive(false);
        _binomialWait2.gameObject.SetActive(false);

	}

    void Update()
    {
        //wait a little while before trigge pirates voices
        if (_numBinomialActive > 0)
        {
            m_lastSelectionSoundTrigged += Time.deltaTime;

            if (m_lastSelectionSoundTrigged >= 2 && !m_voiceTriggedOnce)
            {
                PlayBinomialSelectionSound(_numBinomialActive);
                m_voiceTriggedOnce = true;
                m_lastSelectionSoundTrigged = 0;
            }
            else if (m_voiceTriggedOnce && m_lastSelectionSoundTrigged >= 5)
            {
                PlayBinomialSelectionSound(_numBinomialActive);
                m_lastSelectionSoundTrigged = 0;
            }
        }
    }

    //check if the taste panel can be updated
    public void CheckForUpdate(HandController.HandSide handSide , int binomial)
    {       
        //udpate with the left hand
        if (handSide == HandController.HandSide.Left && m_isRightHandActive == false && _numBinomialActive != binomial)
        {
            if (_numBinomialActive > 0 && _numBinomialActive <= 3)
            {
                GetBinomial(_numBinomialActive).SetAnimation(0);
                GetBinomial(_numBinomialActive)._tastePanel.gameObject.SetActive(false);
            }

            //set sound
            m_lastSelectionSoundTrigged = 0;
            m_voiceTriggedOnce = false;

            GetBinomial(binomial).SetAnimation(1);
            _numBinomialActive = binomial;
            m_isLeftHandActive = true;
            GetBinomial(_numBinomialActive).SetTasteUI();
        }

        else if (handSide == HandController.HandSide.Right && m_isLeftHandActive == false && _numBinomialActive != binomial)
        {
            if (_numBinomialActive > 0 && _numBinomialActive <= 3)
            {
                GetBinomial(_numBinomialActive).SetAnimation(0);
                GetBinomial(_numBinomialActive)._tastePanel.gameObject.SetActive(false);
            }

            //set sound
            m_lastSelectionSoundTrigged = 0;
            m_voiceTriggedOnce = false;

            GetBinomial(binomial).SetAnimation(1);
            _numBinomialActive = binomial;
            m_isRightHandActive = true;
            GetBinomial(_numBinomialActive).SetTasteUI();
        }
    }

    public void ResetSetpanels()
    {        
        _binomialLeft._tastePanel.gameObject.SetActive(false);
        _binomialRight._tastePanel.gameObject.SetActive(false);
        _binomialCenter._tastePanel.gameObject.SetActive(false);
    }

    void PlayBinomialSelectionSound(int numBinomial)
    {
        switch (numBinomial)
        {
            case 1 :
                SdAudioManager.Trigger("Pirate_MingYao_PlayerPointing" , _binomialLeft._pirate1.gameObject);
                break;

            case 2 :
                SdAudioManager.Trigger("Pirate_Arnaud_PlayerPointing", _binomialCenter._pirate1.gameObject);
                break;

            case 3 :
                SdAudioManager.Trigger("Pirate_Rodny_PlayerPointing", _binomialRight._pirate1.gameObject);
                break;
        }
    }

    //notice that an hand is no longer on the binomal selection space
    public void HandOut(HandController.HandSide handSide)
    {
        if (handSide == HandController.HandSide.Left && m_isLeftHandActive)
        {
            m_isLeftHandActive = false;
        }
        else if (handSide == HandController.HandSide.Right && m_isRightHandActive)
        {
            m_isRightHandActive = false;
        }
    }

    public void SetTastes()
    {
        //Left binomial
        if (!_binomialLeft._ate)
        {
            _binomialLeft._tastePirate1._tasteTemp = references._tasteManager.GetRandomTaste();
            _binomialLeft._tastePirate2._tasteTemp = references._tasteManager.GetRandomTaste();
        }
        else
        {
            _binomialLeft.Disable();
        }
        
        //Center binomial
        if ( ! _binomialCenter._ate)
        {
            _binomialCenter._tastePirate1._tasteTemp = references._tasteManager.GetRandomTaste();
            _binomialCenter._tastePirate2._tasteTemp = references._tasteManager.GetRandomTaste();
        }
        else
        {
            _binomialCenter.Disable();
        }
       
        //Right binomial
        if (!_binomialRight._ate)
        {
            _binomialRight._tastePirate1._tasteTemp = references._tasteManager.GetRandomTaste();
            _binomialRight._tastePirate2._tasteTemp = references._tasteManager.GetRandomTaste();
        }
        else
        {
            _binomialRight.Disable();
        }
    }
        
    public SORecipe.Type GetActualPhase()
    {
        switch (_numPhase )
        {
            case 1 :
                return SORecipe.Type.Entree;

            case 2 :
                return SORecipe.Type.Meal;

            case 3 :
                return SORecipe.Type.Desert;
        }

        return SORecipe.Type.Desert;
    }
   
    //return the binomial correponding of the num
    Binomial GetBinomial(int num)
    {
        switch (num)
        {
            case 1:
                return _binomialLeft;

            case 2:
                return _binomialCenter;

            case 3:
                return _binomialRight;

            default :
                return _binomialRight;
        }
    }

    public string GetPirateName(Binomial binomial = null)
    {
        if (binomial == null)
            binomial = _binomialToDisable;

        if (binomial == _binomialLeft)
            return "MingYao";
        else if (binomial == _binomialCenter)
            return "Arnaud";
        else if (binomial == _binomialRight)
            return "Rodny";

        return "Rodny";
    }
    public string GetSbireName(Binomial binomial)
    {
        if (binomial == _binomialLeft)
            return "Sbire1";
        else if (binomial == _binomialCenter)
            return "Sbire2";
        else if (binomial == _binomialRight)
            return "Sbire3";

        return "Sbire1";
    }

    public void DisableBinomial()
    {
        _binomialLeft.Disable();
        _binomialCenter.Disable();
        _binomialRight.Disable();
    }

    //make the enemy fall on the desk at different timing
    public IEnumerator JumpEnemies()
    {
        yield return new WaitForSeconds(1);
        SdAudioManager.Trigger("Boss_Mama_AbortTaunt", gameObject);
        yield return new WaitForSeconds(3);


        float force = -500;

        _binomialCenter._pirate2.gameObject.SetActive(true);
        _binomialCenter._pirate2.rigidbody.AddForce(new Vector3(0, force, 0));


        yield return new WaitForSeconds(1);

        _binomialLeft._pirate2.gameObject.SetActive(true);
        _binomialLeft._pirate2.rigidbody.AddForce(new Vector3(0, force, 0));

        yield return new WaitForSeconds(1);

        _binomialRight._pirate2.gameObject.SetActive(true);        
        _binomialRight._pirate2.rigidbody.AddForce(new Vector3(0, force, 0));

        yield return new WaitForSeconds(0.1f);

        references._boatMama.StartCoroutine("WaitForMamaTable");
    }

    public void ActivateWaitingBinomial()
    {
        if (_numPhase == 2)
        {
            _binomialWait1.gameObject.SetActive(true);
        }
        else if (_numPhase == 3)
        {
            _binomialWait2.gameObject.SetActive(true);
        }
    }
}
