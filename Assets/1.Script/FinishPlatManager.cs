﻿using UnityEngine;
using System.Collections;

public class FinishPlatManager : ReferencedMonoBehaviour {

    [HideInInspector]
    //public bool _isLunchServed = false;

    private int m_nbHand = 0;
    private FinishPlat[] m_finishPlats;
    	
    void Start()
    {
        m_finishPlats = GetComponentsInChildren<FinishPlat>();
        base.LoadReferences();
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

        

	}

    void OnTriggerEnter(Collider other)
    {
        if (other.collider.tag == "Hand")
        {
            m_nbHand++;
            if (m_nbHand >= 2)
            {
                PushPlat();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.collider.tag == "Hand")
        {
            m_nbHand--;
        }
    }

    public void PushPlat()
    {
        m_finishPlats[0].Push();
        m_finishPlats[1].Push();

        collider.enabled = false;
        SdAudioManager.Stop("BattleMusic", 2000, SdAudioManager.Audioinstance.gameObject);
    }

    public void LunchIsReady()
    {        
       references._fightingTable.InitVerdict();        
    }

    //switch the mesh to fit with the actual recipe
    public void SetRecipePrefab()
    {        
        foreach (FinishPlat plat in m_finishPlats)
        {
            //remove the old mesh
            MeshRenderer mesh = plat.GetComponentInChildren<MeshRenderer>();
            if (mesh != null)
            {
                Destroy(mesh.gameObject);
            }

            GameObject newMesh = Instantiate(references._recipeManager._currentRecipe._prefab) as GameObject;
            newMesh.transform.parent = plat.transform;
            newMesh.transform.localScale = Vector3.one;
            newMesh.transform.localPosition = Vector3.zero;
            newMesh.transform.localRotation = Quaternion.identity;

            SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_FinalDish", references._fightingTable._pirateLeft.gameObject);

            references._mama.SetBool("Cook", false);

            plat.Init();
        }
    }

    public void RemovePrefab()
    {
        foreach (FinishPlat plat in m_finishPlats)
        {
            //remove the old mesh
            MeshRenderer mesh = plat.GetComponentInChildren<MeshRenderer>();
            if (mesh != null)
            {
                Destroy(mesh.gameObject);
            }

         }
    }

    public void ResetFinishPlats()
    {
        m_nbHand = 0;
        collider.enabled = true;

        m_finishPlats[0].Reset();
        m_finishPlats[1].Reset();
    }
}
