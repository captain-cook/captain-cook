﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseRecipe : Phase  {

   public PhaseRecipe()
    {
        _camPosition = PhaseManager.CamPosition.Right;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerRecipe>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerRecipe>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
        _references._labelManager.ActivateLabelsUI(true);
        
        
    }

    public override void Exit()
    {

    }

    public override void EarlyEnter()
    {
        Camera.main.cullingMask ^= 4096;
        _references._handCamera.enabled = true;
    }

    public override void EarlyExit()
    {
        _references._handCamera.enabled = false;
        Camera.main.cullingMask |= 4096;
    }
}
