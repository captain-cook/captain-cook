﻿using UnityEngine;
using System.Collections;

public class SdRecipeWheel : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        
        switch (action)
        {
            case "Wheel":
                //AkSoundEngine.SetRTPCValue("RecipeWheelSpeed", 0, source);
                SdAudioManager.Play("Caroussel", source);
                break;
            case "Stop":
                SdAudioManager.Stop("Caroussel",0, source);
                break;
            default:
                break;
        }
    }

    public override void SetParameter(string parameter, float value, GameObject source = null) {
        switch(parameter){
            case "RecipeWheelSpeed":
                AkSoundEngine.SetRTPCValue("RecipeWheelSpeed",value,source);
                break;
            default:
                break;
        }
    }
}


