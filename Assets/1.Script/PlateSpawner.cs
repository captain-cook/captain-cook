﻿using UnityEngine;
using System.Collections;

public class PlateSpawner : MonoBehaviour
{

	#region Enum

	#endregion

	#region Public

        public GameObject _plate;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{

		}

	#endregion

	#region Custom

        public void Spawn()
        {
            PlateController plateController = (GameObject.Instantiate(_plate, transform.position, Quaternion.identity) as GameObject).GetComponent<PlateController>();
            plateController._source = transform.FindChild("Source");
            plateController._destination = transform.FindChild("Dest");
            plateController.PlayAnimation();
        }

	#endregion
	
}
