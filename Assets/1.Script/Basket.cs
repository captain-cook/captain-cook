﻿using UnityEngine;
using System.Collections;

public class Basket : MonoBehaviour {

    public Transform _top;
    public Transform _bottom;

    public Transform[] _slots;

    //movement variables
    public AnimationCurve _inCurve;
    public AnimationCurve _outCurve;

    private Timer m_timer;
    private Vector3 m_source;
    private Vector3 m_destination;
    private AnimationCurve m_currentCurve;

    private MoveType m_moveType;
    private enum MoveType
    {
        Up,
        Down
    }

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void SetTopBottom(Transform top, Transform down)
    {
        _top = top;
        _bottom = down;
        transform.rigidbody.position = _top.position;
    }

    void FixedUpdate()
    {
        if(m_timer != null)
        {
            if(m_timer.IsElapsedLoop == false)
            {
                transform.rigidbody.position = Vector3.Lerp(m_source, m_destination, m_currentCurve.Evaluate(m_timer.Current));
            }
        }
    }

    //make the basket get in or out the scene
    public void Move(bool down)
    {
        if(down)
        {
            SdAudioManager.Trigger("CookBoard_WickerBasketOpen", gameObject);
            m_source = _top.position;
            m_destination = _bottom.position;
            m_currentCurve = _inCurve;
        }
        else
        {
            SdAudioManager.Trigger("CookBoard_WickerBasketClose", gameObject);
            m_source = _bottom.position;
            m_destination = _top.position;
            m_currentCurve = _outCurve;
        }

        transform.rigidbody.position = _top.position;
        m_timer = new Timer(m_currentCurve.keys[m_currentCurve.length - 1].time);
    }

    
}
