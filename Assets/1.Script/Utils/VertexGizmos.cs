﻿using UnityEngine;
using System.Collections;

public class VertexGizmos : MonoBehaviour {

    public int _vertexCount;

	void Start () {
	
	}
	
	void Update () {
	
	}

    void OnDrawGizmos()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.sharedMesh;

        if(mesh != null)
        {
            Gizmos.color = Color.red;
            Gizmos.matrix = transform.localToWorldMatrix;

            for (int i = 0; i < mesh.vertices.Length; i++)
            {
                Vector3 position = new Vector3(
                    mesh.vertices[i].x,
                    mesh.vertices[i].y,
                    mesh.vertices[i].z);

                Gizmos.DrawSphere(position, 0.02f);
            }

            _vertexCount = mesh.vertices.Length;
        }
    }

}
