﻿using UnityEngine;
using System.Collections;

public class Tourniquet2 : MonoBehaviour {

    public int _nbItemMax = 30;
    public float _rotateSpeed = 1;

    //link to the Tscroll element in the scene
    private TScroll[] m_tscrolls;
    
    //rotation variables
    private float m_angle;
    private float m_radius;
    private int m_indTScroll = 0;
    private bool m_isRotating = false;
    private float m_targetRotation;
    private float m_step;
    private Transform m_firstSpot;

	// Use this for initialization
	void Start () {

        m_tscrolls = GetComponentsInChildren<TScroll>();
	}
	
	// Update is called once per frame
	void Update () {

       
	}

   

    //update the position of the scroll to dispatch them aroudn the tourniquet
    void SetScrollPosition ( ref int entree , ref int meal , ref int desert , TScroll tscroll)
    {
        
       
    }

    //return the rotation the tourniquet should be rotate whith to be in front of the tscroll
    float ScrollToAngle(Transform trans)
    {
        float angle = Mathf.Rad2Deg * Mathf.Atan(trans.localPosition.z / trans.localPosition.x);
        if (trans.localPosition.x < 0)  angle -= 90;
        else angle += 90 ;

        if (angle < 0) angle += 360;

        return angle;
    }

    //rotate the tourniquet to the spécifide tscroll
    void RotateToScroll(TScroll tscroll)
    {
        m_isRotating = true;
        m_targetRotation = ScrollToAngle(tscroll.transform);
        Debug.Log("target = " + m_targetRotation);
        //transform.eulerAngles = new Vector3(0, ScrollToAngle(tscroll), 0);
    }

    
}
