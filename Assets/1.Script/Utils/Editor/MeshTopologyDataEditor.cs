﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MeshTopologyData))]
public class MeshTopologyDataEditor : Editor {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

	#endregion

	#region Unity

        void OnInspectorGUI()
        {
            DrawDefaultInspector();

            MeshTopologyData myScript = (MeshTopologyData)target;
            if (GUILayout.Button("Calculate Topology"))
            {
                myScript.CalculateTopology();
            }
        }

	#endregion

	#region Custom

	#endregion

}
