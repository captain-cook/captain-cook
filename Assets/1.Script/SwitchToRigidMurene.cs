﻿using UnityEngine;
using System.Collections;

public class SwitchToRigidMurene : ReferencedMonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public GameObject _freeMurenePrefab;
        public SkinnedMeshRenderer _skinnedMeshRenderer;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
			base.LoadReferences();
		}

		void Update ()
		{

		}

	#endregion

	#region Custom

        public void Bake()
        {
            /*Bounds bounds = _skinnedMeshRenderer.bounds;
            _skinnedMeshRenderer.transform.position = bounds.center;*/

            GameObject go = GameObject.Instantiate(_freeMurenePrefab, _skinnedMeshRenderer.transform.position, _skinnedMeshRenderer.transform.rotation) as GameObject;
            go.GetComponent<Ingredient>()._inBasket = false;
            go.GetComponent<Ingredient>()._forceInWorkspace = true;

            //Mesh mesh = new Mesh();
            //_skinnedMeshRenderer.BakeMesh(mesh);
            //go.GetComponent<MeshFilter>().sharedMesh = mesh;

            references._knife.SetObjectToSlice(go.gameObject);

            GameObject.Destroy(transform.gameObject);
        }

	#endregion

}
