﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TastePanel : MonoBehaviour {

    public Image _tasteIconLeft;
    public Image _tasteIconRight;

    void Start()
    {
        _tasteIconLeft = transform.FindChild("ImageLeft").transform.FindChild("TasteIcon").GetComponent<Image>();
        _tasteIconRight = transform.FindChild("ImageRight").transform.FindChild("TasteIcon").GetComponent<Image>();

        gameObject.SetActive(false);
    }

}
