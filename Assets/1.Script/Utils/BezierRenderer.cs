﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BezierRenderer : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Color _color = Color.white;
        public int _subdivision = 100;

        public bool _dottedLine = false;

	#endregion

	#region Private

	#endregion

	#region Unity

        void OnDrawGizmos()
        {
            BezierFilter filter = GetComponent<BezierFilter>();

            if (filter != null && _subdivision > 0)
            {
                Vector3[] points = filter.Bezier.GetCurve(_subdivision);

                Gizmos.color = _color;

                for (int i = 0; i < points.Length - 1; i++)
                {
                    if (_dottedLine == false || i % 2 == 0)
                    {
                        Gizmos.DrawLine(points[i], points[i + 1]);
                    }
                }

                Gizmos.color = Color.red;

                if (filter._pointA != null && filter._modifierA != null)
                {
                    Gizmos.DrawLine(filter._pointA.position, filter._modifierA.position);
                }
                if (filter._pointB != null && filter._modifierB != null)
                {
                    Gizmos.DrawLine(filter._pointB.position, filter._modifierB.position);
                }
            }
        }

	#endregion

	#region Custom

	#endregion
	
}
