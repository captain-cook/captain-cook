﻿using UnityEngine;
using System.Collections;

public class LabelManager : ReferencedMonoBehaviour {

    private bool m_isActive = false;

    public LabelUIPhase1 _leftIcon;
    public LabelUIPhase1 _rightIcon;


	// Use this for initialization
	void Start () {

        base.LoadReferences();
        //gameObject.SetActive(false);

	}	
	
    //display or hide the
    public void ActivateLabelsUI(bool activate)
    {
        if (activate)
        {
            gameObject.SetActive(true);
            m_isActive = true;
            _leftIcon.GetComponent<TweenAlpha>().PlayForward();
            _rightIcon.GetComponent<TweenAlpha>().PlayForward();
        }
        else
        {
            m_isActive = false;
            _leftIcon.GetComponent<TweenAlpha>().PlayReverse();
            _rightIcon.GetComponent<TweenAlpha>().PlayReverse();

        }
    }

    public void EndOfAlphaTween()
    {
        if (!m_isActive)
        {
            gameObject.SetActive(false);
        }
    }

    //check the actual phase and in which side we want to switch
    public void Switch(bool isLeft)
    {
        Phase actualPhase = references._phaseManager._actualPhase;

        if (actualPhase == references._phaseManager.GetPhase("PhaseCrew"))
        {
            // CREW -> MAP
            if (isLeft)
            {
                references._phaseManager.SwitchPhase("PhaseMap");
                SwitchIcon("IconShop", "IconCrew");
            }
            // CREW -> RECIPE
            else
            {
                references._phaseManager.SwitchPhase("PhaseRecipe");
                SwitchIcon("IconCrew", "IconShop");
            }
        }

        else if (actualPhase == references._phaseManager.GetPhase("PhaseMap"))
        {
            // MAP -> SHOP
            if (isLeft)
            {
                references._phaseManager.SwitchPhase("PhaseShop");
                SwitchIcon("IconRecipe", "IconMap");
            }
            // MAP -> CREW
            else
            {
                references._phaseManager.SwitchPhase("PhaseCrew");
                SwitchIcon("IconMap", "IconRecipe");
            }
        }

        else if (actualPhase == references._phaseManager.GetPhase("PhaseShop"))
        {
            // SHOP -> RECIPE
            if (isLeft)
            {
                references._phaseManager.SwitchPhase("PhaseRecipe");
                SwitchIcon("IconCrew", "IconShop");
            }
            // SHOP -> MAP
            else
            {
                references._phaseManager.SwitchPhase("PhaseMap");
                SwitchIcon("IconShop", "IconCrew");
            }
        }

        else if (actualPhase == references._phaseManager.GetPhase("PhaseRecipe"))
        {
            // RECIPE -> CREW
            if (isLeft)
            {
                references._phaseManager.SwitchPhase("PhaseCrew");
                SwitchIcon("IconMap", "IconRecipe");

            }
            // RECIPE -> SHOP
            else
            {
                references._phaseManager.SwitchPhase("PhaseShop");
                SwitchIcon("IconRecipe", "IconMap");

            }
        }
    }

    void SwitchIcon(string iconNameLeft , string iconNameRight)
    {
        _leftIcon.Fade(iconNameLeft);
        _rightIcon.Fade(iconNameRight);
    }
}
