﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OvenFeedBack : MonoBehaviour {

    private Image m_chrono;
    private float m_fillAmount = 1;

	// Use this for initialization
	void Start () {

        m_chrono = GetComponent<Image>();
        SetFeedBack(true);
        

        Display(false);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void IncSmoke()
    {

    }

    //increase the clock visual feedback 
    public void IncChrono(float time , float timeMax)
    {
        //good timing
        if (time <= timeMax)
        {
            m_chrono.fillAmount = 1 - time / timeMax;
        }
        //bad timing
        else
        {           
            m_chrono.fillAmount = (time - timeMax ) / (timeMax);
            m_chrono.color = Color.Lerp(Color.green, Color.red, m_chrono.fillAmount);            
        }        
    }

    public void SetFeedBack(bool goodFeedBack)
    {
        if (goodFeedBack)
        {
            m_chrono.color = Color.green;
            m_chrono.fillClockwise = false;
            m_chrono.fillAmount = 1;
        }
        else
        {
            m_chrono.fillAmount = 0;
            //m_chrono.color = Color.red;
            m_chrono.fillClockwise = true;
        }
        
    }

    public void Display(bool display)
    {
        m_chrono.enabled = display;
    }
}
