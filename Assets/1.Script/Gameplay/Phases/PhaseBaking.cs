﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseBaking : Phase  {

   public PhaseBaking()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerBaking>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerBaking>();
        
    }
	
    public override void Enter()
    {
        _references._handCamera.enabled = false;

        _references._table.GetComponent<Animation>()["Anim_Table_Plancha_FourCustom"].speed = 1;
        _references._table.GetComponent<Animation>()["Anim_Table_Plancha_FourCustom"].time = 0;
        _references._table.GetComponent<Animation>().Play();
        _references._table.GetComponent<SoundOven>()._isOpen = false;


        SdAudioManager.Trigger("CookBoard_PlanchaOpen", _references._plancha.gameObject);
        _references._basketManager.GetComponentInChildren<Collider>().enabled = false;
    }

    public override void Exit()
    {
        _references._planchaButton.gameObject.SetActive(false);
    }

    public override void EarlyEnter()
    {
        _references._planchaButton.gameObject.SetActive(true);
    }

    public override void EarlyExit()
    {
        _references._table.GetComponent<Animation>()["Anim_Table_Plancha_FourCustom"].speed = -1;
        _references._table.GetComponent<Animation>()["Anim_Table_Plancha_FourCustom"].time = 1f;
        _references._table.GetComponent<Animation>().Play();
        _references._table.GetComponent<SoundOven>()._isOpen = true;

        _references._basketManager.GetComponentInChildren<Collider>().enabled = true;
    }

    
}
