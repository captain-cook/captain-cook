﻿using UnityEngine;
using System.Collections;

public class RoundPanel : ReferencedMonoBehaviour {

    private TweenScale m_tScale;
    private bool m_isDisplayed = false;
    private bool m_isScoresIncreasing = false;

    // store round score
    private float m_scorePlayer;
    private float m_scoreMama;
    private float m_scoreLevel = 0;
    public float _scoreIncreaseSpeed = 1;

    // elements to animate
    public TweenScale _tScaleAnim;
    public UILabel _labelPlayer;
    public UILabel _labelMama;
    public UISpriteAnimation _verdictAnimation;
    public float _timeToWaitBeforeClose = 2;

    public UIAtlas _atlasWin;
    public UIAtlas _atlasLoose;

	// Use this for initialization
	void Start () {

        base.LoadReferences();
        m_tScale = GetComponent<TweenScale>();
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
        if (m_isScoresIncreasing)
        {
            m_scoreLevel += Time.deltaTime * _scoreIncreaseSpeed;

            //increase player score
            if (m_scoreLevel <= m_scorePlayer)
            {
                _labelPlayer.text = (Mathf.Round(m_scoreLevel)) + "%";
            }            

            //increase mama score
            if (m_scoreLevel <= m_scoreMama)
            {
                _labelMama.text = (Mathf.Round(m_scoreLevel)) + "%";
            }

            //check if score increasement is finish
            if (m_scoreLevel >= m_scorePlayer && m_scoreLevel > m_scoreMama)
            {
                SdAudioManager.Stop("UI_Jauge" , 0 , SdAudioManager.Audioinstance.gameObject);

                m_isScoresIncreasing = false;
                StartAnimation();
            }
        }
	}

    //start the tween to display the panel
    public void DisplayPanel()
    {
        references._verdictManager.SetRoundScore();
        gameObject.SetActive(true);
        transform.localScale = Vector3.zero;
        m_tScale.PlayForward();
        _tScaleAnim.transform.localScale = Vector3.zero;
        
        //resset the score label & verdict animation
        _labelPlayer.text = "";
        _labelMama.text = "";
        _verdictAnimation.namePrefix = "*******";
    }

    //call at the end of the tweenScale displaying the panel
    public void EndOfScale()
    {
        // panel displayed we start increment scores
        if(!m_isDisplayed)
        {            
            m_scoreLevel = 0;
            m_scorePlayer = references._verdictManager.GetRoundScore(1);
            m_scoreMama = references._verdictManager.GetMamaScore(1);
            m_isDisplayed = true;
            m_isScoresIncreasing = true;
            //SdAudioManager.Trigger("UI_Jauge");
        }
        // panel not displayed anymore we disable the gameobject
        else
        {
            m_isDisplayed = false;
            gameObject.SetActive(false);
            WhatsNext();
        }
    }

    //launch the tween and animation of the poulpe
    void StartAnimation()
    {
        _tScaleAnim.ResetToBeginning();
        _tScaleAnim.PlayForward();

        StartCoroutine("VerdictAnimation");
    }

    //launch the verdict animation wait then close then panel
    IEnumerator VerdictAnimation()
    {
        // player won
        if (m_scorePlayer >= m_scoreMama)
        {
            _verdictAnimation.GetComponent<UISprite>().atlas = _atlasWin;
            _verdictAnimation.namePrefix = "EndPageVictory_";

            int rand = Random.Range(0, 100);
            if (rand < 50)
            {
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_FeedbackPositive", references._binomialManager._binomialToDisable.gameObject);
            }
            else
            {
                SdAudioManager.Trigger("Boss_Mama_RoundLose", references._mama.gameObject);
            }
            references._mama.SetBool("Defaite", true);
        }
        //player lost
        else
        {
            _verdictAnimation.GetComponent<UISprite>().atlas = _atlasLoose;
            _verdictAnimation.namePrefix = "EndPageDefeat_";
           references._mama.SetBool("Victory", true);

            int rand = Random.Range(0, 100);
            if (rand < 50)
            {
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_FeedbackNegative", references._binomialManager._binomialToDisable.gameObject);
            }
            else
            {
                SdAudioManager.Trigger("Boss_Mama_RoundWin", references._mama.gameObject);
            }
        }

        yield return new WaitForSeconds(_timeToWaitBeforeClose);

        m_tScale.PlayReverse();
        references._mama.SetBool("Victory", false);
        references._mama.SetBool("Defaite", false);
    }

    // check if the wombat is finish and launch the proper phase 
    void WhatsNext()
    {
        //combat not finish
        if (references._binomialManager._numPhase < 3)
        {
            references._phaseManager.SwitchPhase("PhaseBinomial");            
        }
        else
        {
            if (references._verdictManager.GetNbRoundsWin() >= 2)
            {
                references._fightingTable.FeedBackWon();
            }
            else
            {
                references._fightingTable.FeedBackLost();
            }

            //display the full score panel
            references._verdictPanel.DisplayPanel();
        }
    }
}
