﻿Shader "Transparent/SkyboxMesh Background" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}

    _MainTexMulColor ("TextureMul Color", Color) = (1,1,1,0)
    _MainTexAddColor ("TextureAdd Color", Color) = (0,0,0,1)
}

SubShader {
	Tags {"Queue"="Background+1"}
	LOD 100

	ZWrite Off
	Lighting Off
	Fog {Mode Off}
	Blend SrcAlpha OneMinusSrcAlpha 

	Pass {
		SetTexture [_MainTex] { combine texture } 

        SetTexture [_MainTex] {
        	constantColor [_MainTexMulColor]
		    combine previous * constant
		}
        
        SetTexture [_MainTex] {
        	constantColor [_MainTexAddColor]
		    combine previous + constant
		}
                
        SetTexture [_MainTex] {
        	constantColor [_Color]
		    combine previous * constant
		}
	}
}
}