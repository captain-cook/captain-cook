﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class HandControllerCrew : CustomFixedHandController
{
    [HideInInspector]
    public Vector2 _screenPosition;
    [HideInInspector]
    public HandController _handController;

    private Camera m_handCamera;

    void Awake()
    {
        base.LoadReferences();
        _handController = GetComponent<HandController>();
        m_handCamera = references._handCamera;
    }

    public override void Update()
    {
         // get the coordinate of the hand on the screen
        _screenPosition = m_handCamera.WorldToScreenPoint(transform.position);

       
    }
    
}