﻿using UnityEngine;
using System.Collections;

public class IngredientLogo : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Vector3 _basePosition;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
            _basePosition = transform.position;
		}

		void Update ()
		{
            transform.position = _basePosition + (Vector3.up * Mathf.Sin(Time.time * 1.7f) * 0.2f);
		}

	#endregion

	#region Custom

	#endregion
	
}
