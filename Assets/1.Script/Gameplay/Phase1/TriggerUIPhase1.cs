﻿using UnityEngine;
using System.Collections;

public class TriggerUIPhase1 : MonoBehaviour {

    public LabelUIPhase1 m_labelUI;

    //detect if the hand is in the trigger to switch phase
    void OnTriggerStay(Collider other)
    {
        //Debug.Log("Trigger in");
    }

    //launch the feedback to tell the player is on the trigger
    void OnTriggerEnter(Collider other)
    {
        if (other.collider.tag == "Hand")
        {
            m_labelUI.LaunchTween(true);
            m_labelUI.SetHandActive(other.GetComponent<HandController>());
        }
    }

    //launch the feedback to tell the player is no longer in the trigger
    void OnTriggerExit(Collider other)
    {
        if (other.collider.tag == "Hand")
        {
            m_labelUI.LaunchTween(false);
            m_labelUI.SetHandActive(other.GetComponent<HandController>());
        }
    }
}
