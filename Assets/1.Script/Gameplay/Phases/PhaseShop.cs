﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseShop : Phase  {

   public PhaseShop()
    {
        _camPosition = PhaseManager.CamPosition.Back;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerShop>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerShop>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
        _references._labelManager.ActivateLabelsUI(true);
        _references._shop.OpenShop();
    }

    public override void Exit()
    {

    }

    public override void EarlyEnter()
    {
        Camera.main.cullingMask ^= 4096;
        _references._handCamera.enabled = true;

        
        _references._sky.skyIsStorm = true;
        SdAudioManager.Trigger("Ambiances_General_Strong");
    }

    public override void EarlyExit()
    {
        _references._handCamera.enabled = false;
        Camera.main.cullingMask |= 4096;


        _references._sky.skyIsStorm = false;
        SdAudioManager.Trigger("Ambiances_General_Calm_Sunny");
        _references._shop.CloseShop();

    }
}
