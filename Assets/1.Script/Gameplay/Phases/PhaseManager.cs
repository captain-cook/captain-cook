﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhaseManager : ReferencedMonoBehaviour {

    
    public Phase _actualPhase;
    private Dictionary<string, Phase> m_phases;

    //cameras
    private Camera m_mainCamera;

    //movement variables
    public AnimationCurve _speedCurve;
    public float _moveSpeed = 1;
    [HideInInspector]
    public bool _isSwitching = false;
    private float m_stepMove = 0;
    private Transform m_targetPosition;
    private Transform m_oldPosition;
    private Phase m_targetPhase;
    //private Bezier m_bezierMove;

    public PhaseName _phaseStart;

    public enum CamPosition
    {
        Front,
        Right,
        Left,
        Back
    }

    public enum PhaseName
    {
        PhaseMap, 
        PhaseCrew,
        PhaseBinomial,
        PhaseCooking,
        PhaseTourniquet,
        PhaseBasket,
        PhaseVerdict,
        PhaseBaking,
        PhaseShop,
        PhaseRecipe,
        PhaseArrival
    }
    

	// Use this for initialization
	void Start () {

        base.LoadReferences();

        m_mainCamera = Camera.main;

        //register all the phase
        m_phases = new Dictionary<string, Phase>();
        m_phases.Add("PhaseMap", new PhaseMap());
        m_phases.Add("PhaseCrew", new PhaseCrew());
        m_phases.Add("PhaseBinomial", new PhaseBinomial());
        m_phases.Add("PhaseCooking", new PhaseCooking());
        m_phases.Add("PhaseBasket", new PhaseBasket());
        m_phases.Add("PhaseTourniquet", new PhaseTourniquet());
        m_phases.Add("PhaseVerdict", new PhaseVerdict());
        m_phases.Add("PhaseBaking", new PhaseBaking());
        m_phases.Add("PhaseShop", new PhaseShop());
        m_phases.Add("PhaseRecipe", new PhaseRecipe());
        m_phases.Add("PhaseArrival", new PhaseArrival());
        m_phases.Add("PhaseInit", new PhaseInit());


        _actualPhase = m_phases["PhaseInit"];
        SwitchPhase(_phaseStart.ToString());
	}
	
	// Update is called once per frame
	void Update () {
	
        if (_isSwitching)
        {
            //swtching
            if (m_stepMove < 1)
            {
                //fix the angle issue when the rotation is to long                
                Vector3 targetRotation = m_targetPosition.transform.eulerAngles;
                if (m_targetPosition.transform.eulerAngles.y - m_oldPosition.transform.eulerAngles.y > 180)
                {
                    targetRotation.y -= 360;
                }
                else if (m_targetPosition.transform.eulerAngles.y - m_oldPosition.transform.eulerAngles.y < -180)
                {
                    targetRotation.y += 360;
                }

                //fix the same problem on the Z axis
                if (m_targetPosition.transform.eulerAngles.z - m_oldPosition.transform.eulerAngles.z > 180)
                {
                    targetRotation.z -= 360;
                }
                else if (m_targetPosition.transform.eulerAngles.z - m_oldPosition.transform.eulerAngles.z < -180)
                {
                    targetRotation.z += 360;
                }

                //change camera's transform   
                m_stepMove += Time.deltaTime * _moveSpeed;
                Vector3 newPosition = Vector3.Lerp(m_oldPosition.transform.position, m_targetPosition.transform.position, _speedCurve.Evaluate(m_stepMove));

                m_mainCamera.transform.position = newPosition;
                Vector3 directRotation = Vector3.Lerp(m_oldPosition.transform.eulerAngles, targetRotation, _speedCurve.Evaluate(m_stepMove));
                m_mainCamera.transform.eulerAngles = directRotation;

            }
            //switch is finish
            else
            {
                SetHandController();

                _actualPhase.Exit();
                _actualPhase = m_targetPhase;
                _actualPhase.Enter();

                _isSwitching = false;
            }
        }
	}

    public void SwitchPhase(string phaseName)
    {
        if (!_isSwitching && m_phases[phaseName] != _actualPhase)
        {
            _isSwitching = true;
            m_targetPhase = m_phases[phaseName];
            _actualPhase.EarlyExit();
            m_targetPhase.EarlyEnter();

            //need to switch the camera position before change the phase
            
            if (m_phases[phaseName]._camPosition != _actualPhase._camPosition)
            {
                m_stepMove = 0;
                m_oldPosition = GetCamPosition(_actualPhase._camPosition);
                m_targetPosition = GetCamPosition(m_targetPhase._camPosition);
            }
            else
            {
                m_stepMove = 1;
            }
        }            
    }

    //return the proper transform for a given camposition
    Transform GetCamPosition(CamPosition position)
    {
        switch (position)
        {
            case CamPosition.Front :
                return references._frontCamPosition;
            case CamPosition.Left:
                return references._leftCamPosition;
            case CamPosition.Right:
                return references._rightCamPosition;
            case CamPosition.Back:
                return references._backCamPosition;
            default :
                return null;
        }
    }

    public Phase GetPhase(string phaseName)
    {
        return m_phases[phaseName];
    }

    //enable the old hand controller et enable the new one
    void SetHandController()
    {
        //disable tall handControllers
        CustomHandController[] handControllersLeft = references._handLeft.GetComponents<CustomHandController>();
        CustomHandController[] handControllersRight = references._handRight.GetComponents<CustomHandController>(); 

        //left hand
        foreach ( CustomHandController chc in handControllersLeft)
        {
            chc.enabled = false;
        }

        //right hand
        foreach (CustomHandController chc in handControllersRight)
        {
            chc.enabled = false;
        }

        //enable the new one
        m_targetPhase._customHandControllerLeft.enabled = true;
        m_targetPhase._customHandControllerRight.enabled = true;
    }
}
