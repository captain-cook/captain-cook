﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasketManager : ReferencedMonoBehaviour {

    public float _ratioInitialize = 0.6f;

    public Transform _basketSpawn;

    private Basket m_basket;

    private GameObject[] m_ingredients;
    private bool[] m_linkedIngredients;

    public Transform _top;
    public Transform _bottom;

	// Use this for initialization
	void Awake ()
    {
        base.LoadReferences();
	}
	
	// Update is called once per frame
	void Update () {
	
        if ( Input.GetKeyUp(KeyCode.DownArrow))
        {
            Activate(true);
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Activate(false);
        }

        if(m_ingredients != null)
        {
            for (int i = 0; i < m_ingredients.Length; i++)
            {
                if (m_ingredients[i] != null && m_linkedIngredients[i] == true)
                {
                    m_ingredients[i].rigidbody.position = m_basket._slots[i].position;
                    m_ingredients[i].transform.localEulerAngles = m_basket._slots[i].localEulerAngles;
                    //m_ingredients[i].rigidbody.angularVelocity = Vector3.zero;
                    //m_ingredients[i].rigidbody.velocity = Vector3.zero;
                }
            }
        }
        
	}

    public void SpawnBasket(GameObject basket)
    {
        GameObject bask = Instantiate(basket, _basketSpawn.position, _basketSpawn.rotation) as GameObject;
        m_basket = bask.GetComponent<Basket>();

        m_basket.SetTopBottom(_top, _bottom);
        //m_basket._top = _top;
        //m_basket._bottom = _bottom;

        bask.transform.parent = transform;

        m_ingredients = new GameObject[4];
        m_linkedIngredients = new bool[4];

        for (int i = 0; i < 4; i++)
        {
            if (m_basket._slots[i].childCount > 0)
            {
                //Rigidbody[] rigidbodies = go.GetComponentsInChildren<Rigidbody>();
                Ingredient ingredient = m_basket._slots[i].GetChild(0).GetComponentInChildren<Ingredient>();
                ingredient.transform.parent = null;

                m_ingredients[i] = ingredient.gameObject;

                ingredient._indexInSteps = i;

                //ingredient.transform.localScale *= _ratioInitialize;
                ingredient.transform.position = m_basket._slots[i].position;
                ingredient.transform.localEulerAngles = m_basket._slots[i].localEulerAngles;

                LinkIngredient(m_ingredients[i]);
            }
        }
    }

    //set the ingredient in the basket depending on the recipe we're making
    /*public void FillBasket()
    {
        int count = references._recipeManager._currentRecipe._recipeSteps.Length;

        m_ingredients = new GameObject[count];
        m_linkedIngredients = new bool[count];

        //instantiate the prefab in the basket
        GameObject go;

        for (int i = 0; i < count; i++)
        {
            if (references._recipeManager._currentRecipe._recipeSteps[i]._action != SORecipe.Interaction.Bake && references._recipeManager._currentRecipe._recipeSteps[i]._action != SORecipe.Interaction.Toast)
            {
                go = Instantiate(references._recipeManager._currentRecipe._recipeSteps[i]._ingredient._prefab, m_baskets[0]._slots[i].position, Quaternion.identity) as GameObject;
                //Rigidbody[] rigidbodies = go.GetComponentsInChildren<Rigidbody>();
                Ingredient ingredient = go.GetComponentInChildren<Ingredient>();

                m_ingredients[i] = ingredient.gameObject;

                ingredient._indexInSteps = i;

                ingredient.transform.localScale *= _ratioInitialize;
                ingredient.transform.position = m_baskets[0]._slots[i].position;
                ingredient.transform.localEulerAngles = m_baskets[0]._slots[i].localEulerAngles;

                LinkIngredient(m_ingredients[i]);
            }
            else
            {
                break;
            }
        }
    }*/

    public void UnlinkIngredient(GameObject ingredient)
    {
        m_linkedIngredients[ingredient.GetComponent<Ingredient>()._indexInSteps] = false;
        MeshCollider col = ingredient.GetComponent<MeshCollider>();
        if (col != null)
            col.isTrigger = false;
        ingredient.rigidbody.isKinematic = false;
    }

    public void LinkIngredient(GameObject ingredient)
    {
        m_linkedIngredients[ingredient.GetComponent<Ingredient>()._indexInSteps] = true;
        MeshCollider col = ingredient.GetComponent<MeshCollider>();
        if(col != null)
            col.isTrigger = true;
        ingredient.rigidbody.isKinematic = true;
    }

    //make the baskets fall in the scene
    public void Activate (bool activate)
    {
        m_basket.Move(activate);
    }

    
}
