﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Highlighter : MonoBehaviour {

	#region Enum

        public enum HighlightAxis { X, Y, Z };

	#endregion

	#region Public

        public Color _color = Color.white;
        public float _ratio;
        public Material _material;

	#endregion

	#region Private

        private List<int> m_index;

	#endregion

	#region Unity

		void Start ()
		{
            m_index = new List<int>();

            MeshRenderer[] renderers = transform.GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                List<Material> materials = new List<Material>(renderers[i].materials);
                if (materials.Count == 0 || materials[renderers[i].materials.Length - 1].name.Replace(" (Instance)", "") != _material.name)
                {
                    m_index.Add(materials.Count);
                    materials.Add(_material);
                    renderers[i].materials = materials.ToArray();

                    renderers[i].materials[m_index[i]].SetColor("_OutlineColor", _color);
                    renderers[i].materials[m_index[i]].SetFloat("_OutlineSize", 0);
                }
            }
		}

        void Update()
        {
            //renderer.materials[m_index].SetFloat("_Progress", _progress);
        }

        public void Highlight()
        {
            MeshRenderer[] renderers = transform.GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                if (m_index.Count > i && m_index[i] < renderers[i].materials.Length)
                {
                    renderers[i].materials[m_index[i]].SetColor("_OutlineColor", _color);
                    renderers[i].materials[m_index[i]].SetFloat("_OutlineSize", _ratio);
                }
            }
        }

        public void UnHighlight()
        {
            MeshRenderer[] renderers = transform.GetComponentsInChildren<MeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                if (m_index.Count > i && m_index[i] < renderers[i].materials.Length)
                {
                    renderers[i].materials[m_index[i]].SetFloat("_OutlineSize", 0);
                }
            }
        }

	#endregion

	#region Custom

	#endregion
	
}
