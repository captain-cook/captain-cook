﻿using UnityEngine;
using System.Collections;

public class RecipeCard : ReferencedMonoBehaviour {

    public SORecipe _soRecipe;
    public Highlighter _highlighter;

    //animation variables
    public Animation _animation;
    private int m_animStep = 0;
    private bool m_isPlayingForward = true;

    //reference to the item of the card
    public MeshRenderer _recipeImage;
    public TextMesh _title;
    public MeshRenderer _ingredientImage1;
    public MeshRenderer _ingredientImage2;
    public MeshRenderer _ingredientImage3;
    public MeshRenderer _ingredientImageSpecial;
    public MeshRenderer _ingredientInteration1;
    public MeshRenderer _ingredientInteration2;
    public MeshRenderer _ingredientInteration3;
    public MeshRenderer _ingredientInterationSpecial;
    public TextMesh _ingredientName1;
    public TextMesh _ingredientName2;
    public TextMesh _ingredientName3;
    public TextMesh _ingredientNameSpecial;

    //material creation
    public Shader _shaderMaterial;
    public Texture _cutIcon;
    public Texture _peelIcon;
    public Texture _smashIcon;
    public Texture _spreadIcon;
    public Texture _CheckIcon;

    private Timer m_spawnCompletePlateTimer;
    private Timer m_shipInSight;

    void Start()
    {
        base.LoadReferences();
    }

    void Update()
    {
        //if (Input.GetKeyUp(KeyCode.Alpha1))
        //{
        //    //CheckRkecipeStep(1);
        //    PlayAnimation(true);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha2))
        //{
        //    PlayAnimation(false);
        //    //CheckRecipeStep(2);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha3))
        //{
        //    CheckRecipeStep(3);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha4))
        //{
        //    CheckRecipeStep(4);
        //}

        //Set the complete plate and spawn it
        /*if(m_spawnCompletePlateTimer != null && m_spawnCompletePlateTimer.IsElapsedOnce)
        {
            m_spawnCompletePlateTimer = null;
            //References.PlateSpawner._tarte = _recipe.xx;
            references._plateSpawner.Spawn();
        }*/

        if(m_shipInSight != null && m_shipInSight.IsElapsedOnce)
        {
            m_shipInSight = null;
            SdAudioManager.Trigger("Pirate_Seeker_EnemySpotted", gameObject);
        }
    }
	
    //load every recipe field depending on the SORecipe 
    public void LoadRecipe(SORecipe recipe = null)
    {
        if (recipe != null)
            _soRecipe = recipe;

        if ( _soRecipe != null)
        {
            //set the recipe information
            _title.text = _soRecipe._name;
           // _recipeImage.material = CreateMaterial(_soRecipe._image);

            //set the ingredient icon
            /*if (_soRecipe._ingredient1 != null)
            {
                _ingredientImage1.material = CreateMaterial(_soRecipe._ingredient1._image);
                _ingredientInteration1.material = CreateMaterial(GetInteractionIcon(_soRecipe._interaction1));
                _ingredientName1.text = _soRecipe._ingredient1._name;
            }
            else
            {
                _ingredientImage1.material = CreateMaterial(_CheckIcon);
                _ingredientInteration1.material = CreateMaterial(_CheckIcon);
                _ingredientName1.text = "NO INGREDIENT";
            }

            if (_soRecipe._ingredient2 != null)
            {
                _ingredientImage2.material = CreateMaterial(_soRecipe._ingredient2._image);
                _ingredientInteration2.material = CreateMaterial(GetInteractionIcon(_soRecipe._interaction2));
                _ingredientName2.text = _soRecipe._ingredient2._name;
            }
            else
            {
                _ingredientImage2.material = CreateMaterial(_CheckIcon);
                _ingredientInteration2.material = CreateMaterial(_CheckIcon);
                _ingredientName2.text = "NO INGREDIENT";
            }

            if (_soRecipe._ingredient3 != null)
            {
                _ingredientImage3.material = CreateMaterial(_soRecipe._ingredient3._image);
                _ingredientInteration3.material = CreateMaterial(GetInteractionIcon(_soRecipe._interaction3));
                _ingredientName3.text = _soRecipe._ingredient3._name;
            }
            else
            {
                _ingredientImage3.material = CreateMaterial(_CheckIcon);
                _ingredientInteration3.material = CreateMaterial(_CheckIcon);
                _ingredientName3.text = "NO INGREDIENT";
            }

            //set the special ingredient if there is one
            if (_soRecipe._ingredientSpecial != null)
            {
                //set the icon & text
                _ingredientImageSpecial.material = CreateMaterial(_soRecipe._ingredientSpecial._image);
                _ingredientInterationSpecial.material = CreateMaterial(GetInteractionIcon(_soRecipe._interactionSpecial));
                _ingredientNameSpecial.text = _soRecipe._ingredientSpecial._name;

                //display the icon & text
                //_ingredientNameSpecial.GetComponent<MeshRenderer>().enabled = true;
                //_ingredientImageSpecial.enabled = true;
                //_ingredientInterationSpecial.enabled = true;
            }
            else
            {
                //disable the icon & text
                //_ingredientNameSpecial.GetComponent<MeshRenderer>().enabled = false;
                //_ingredientImageSpecial.enabled = false;
                //_ingredientInterationSpecial.enabled = false;
            }*/
        }
    }

    //create a new material with the parameter texture
    public Material CreateMaterial(Texture texture)
    {
        Material material = new Material(_shaderMaterial);
        material.color = Color.white;
        material.mainTexture = texture;

        return material;
    }

    //return the interaction icon depending on the type of interraction needed
    Texture GetInteractionIcon(SORecipe.Interaction interaction)
    {
        switch (interaction)
        {
            case SORecipe.Interaction.Cut :
                return _cutIcon;
                break;

            case SORecipe.Interaction.Peel:
                return _peelIcon;
                break;

            case SORecipe.Interaction.Smash:
                return _smashIcon;
                break;

            case SORecipe.Interaction.Spread:
                return _spreadIcon;
                break;

        }

        return _cutIcon;
    }

    //play the recipe card animation forward or backward
    public void PlayAnimation(bool forward)
    {        
        if (forward)
        {
            m_isPlayingForward = true;
            _animation["RecipeCard"].speed = 1;
            _animation["RecipeCard"].time = 0;
            SdAudioManager.Trigger("RecipeBoard_Open", gameObject);
        }
        else
        {
            m_isPlayingForward = false;
            _animation["RecipeCard"].speed = -1f;
            _animation["RecipeCard"].time = 0.5f;
            SdAudioManager.Trigger("RecipeBoard_Close", gameObject);
        }

        _animation.Play("RecipeCard");
    }

    public void DisplayCardStepByStep(int numStep)
    {
        switch ( numStep )
        {
            case 0:
                _recipeImage.enabled = m_isPlayingForward;
                break;
            case 1 :
                _title.GetComponent<MeshRenderer>().enabled = m_isPlayingForward;
                break;
            case 2:
                _ingredientImage1.enabled = m_isPlayingForward;
                _ingredientInteration1.enabled = m_isPlayingForward;
                _ingredientName1.GetComponent<MeshRenderer>().enabled = m_isPlayingForward;
                break;
            case 3:
                _ingredientImage2.enabled = m_isPlayingForward;
                _ingredientInteration2.enabled = m_isPlayingForward;
                _ingredientName2.GetComponent<MeshRenderer>().enabled = m_isPlayingForward;
                break;
            case 4:
                _ingredientImage3.enabled = m_isPlayingForward;
                _ingredientInteration3.enabled = m_isPlayingForward;
                _ingredientName3.GetComponent<MeshRenderer>().enabled = m_isPlayingForward;
                break;
            /*case 5:
                if(_soRecipe._ingredientSpecial != null)
                {
                    _ingredientImageSpecial.enabled = m_isPlayingForward;
                    _ingredientInterationSpecial.enabled = m_isPlayingForward;
                    _ingredientNameSpecial.GetComponent<MeshRenderer>().enabled = m_isPlayingForward;
                }
                break;*/
        }
    }

    //switch the icon of the interaction when the step is complited
    public void CheckRecipeStep(int numStep)
    {
        MeshRenderer step;
        switch (numStep)
        {
            case 1 :
                _ingredientInteration1.material = CreateMaterial(_CheckIcon);
                break;
            case 2:
                _ingredientInteration2.material = CreateMaterial(_CheckIcon);
                break;
            case 3:
                _ingredientInteration3.material = CreateMaterial(_CheckIcon);
                break;
            case 4:
                _ingredientInterationSpecial.material = CreateMaterial(_CheckIcon);
                break;
        }

        CheckRecipeComplete();
    }

    //check if the recipe is complete
    void CheckRecipeComplete()
    {
        //the whole recipe is complete 
        /*if (_ingredientInteration1.material.mainTexture == _CheckIcon
            && _ingredientInteration2.material.mainTexture == _CheckIcon
            && _ingredientInteration3.material.mainTexture == _CheckIcon
            && ((_soRecipe._ingredientSpecial != null  && _ingredientInterationSpecial.material.mainTexture == _CheckIcon )
                || _soRecipe._ingredientSpecial == null))
        {
            references._preparationManager.RecipeFinished();
            SpawnCompletePlate();
            m_shipInSight = new Timer(7f);
        }
        //recipe not complete yet
        else
        {
            //SpawnCompletePlate();
            references._preparationManager.SelectNextIngredient();
        }*/
            
    }

    void SpawnCompletePlate()
    {
        m_spawnCompletePlateTimer = new Timer(1f);
        //Roll back the recipe
        PlayAnimation(false);
    }

    //detection to return on phase 1 
    void OnTriggerStay(Collider other)
    {
        if (other.collider.tag == "Hand" && other.collider.GetComponent<HandController>().GetHandState() == KinectBody.HandState.Closed)
        {
            //references._phase1Manager.ExitFromCooking();
            GetComponent<Collider>().enabled = false;
        }
    }

    //visual feedback hand hover map
    void OnTriggerEnter(Collider other)
    {
        _highlighter.Highlight();
        Debug.Log("ENTER");
    }

    void OnTriggerExit(Collider other)
    {
        _highlighter.UnHighlight();
        Debug.Log("EXIT");
    }
}
