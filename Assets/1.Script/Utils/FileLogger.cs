﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class FileLogger {

    static string m_defaultFileName = "default.log";

    public static void Log(string log)
    {
        Log(m_defaultFileName, log);
    }

    public static void Log(string file, string log)
    {
        StreamWriter sr = File.AppendText(file);
        sr.WriteLine(log);
        sr.Close();
    }

    public static void Clear()
    {
        Clear(m_defaultFileName);
    }

    public static void Clear(string file)
    {
        File.Delete(file);
    }

}
