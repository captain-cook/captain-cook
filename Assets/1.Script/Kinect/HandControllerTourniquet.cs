﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class HandControllerTourniquet : CustomFixedHandController
{
    public Vector2 _screenPosition;
    

    public HandController _handController;
    public Tourniquet _tourniquet;
    [HideInInspector]
    private float m_rotateSpeed = 500f;

    private Camera m_handCamera;
    private bool m_isDragging = false;
    private Vector2 m_startPosition;
    private Vector2 m_lastPosition;
    private float m_startRotation;
    private float m_drag = 1;
    private float m_yOffset ;

    private TScroll _tscrollHold;
    private Highlighter _tscrollHighlighted;

    void Awake()
    {
        base.LoadReferences();
        _handController = GetComponent<HandController>();
        m_handCamera = references._handCamera;
        _tourniquet = references._tourniquet;
        m_startPosition = Vector2.zero;   
     
        m_yOffset = Screen.height * 0.25f;

       
    }

    public override void Update()
    {
         // get the coordinate of the hand on the screen
        _screenPosition = m_handCamera.WorldToScreenPoint(transform.position);

        Update1();

        base.Update();
    }

   
    void Update1()
    {

        // HIGHLIGHT
        Ray ray = Camera.main.ScreenPointToRay(_screenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1) && hit.collider.tag == "TScroll")
        {
            //Highlight the new one
            Highlighter highLightTemp = hit.collider.GetComponent<Highlighter>();
            //avoid to recolor at every frame
            if (highLightTemp != _tscrollHighlighted && _tscrollHold == null)
            {
                //unHighlight the old tscroll
                if (_tscrollHighlighted != null)
                {
                    _tscrollHighlighted.UnHighlight();
                    _tourniquet.ResetPreselectedRecipe();
                }
                
                _tscrollHighlighted = highLightTemp;
                _tscrollHighlighted._color = Color.yellow;
                _tscrollHighlighted.Highlight();
                _tourniquet.SetPreselectedRecipe(highLightTemp.GetComponent<TScroll>()._recipeData);
            }                           
        }
        //no tscroll preselcted so unhighlight the last tscroll
        else if (_tscrollHighlighted != null && _tscrollHold == null)
        {
            _tscrollHighlighted.UnHighlight();
            _tscrollHighlighted = null;
            _tourniquet.ResetPreselectedRecipe();

            //look if the other hand had a selection
            Highlighter otherTscroll = _handController._otherHand.GetComponent<HandControllerTourniquet>()._tscrollHighlighted;
            if (otherTscroll != null)
            {
                _tourniquet.SetPreselectedRecipe(otherTscroll.GetComponent<TScroll>()._recipeData);
            }
        }

        // MOVE
        if (_handController.IsClosingHand() && !m_isDragging && IsVisible())
        {
            m_startPosition = _screenPosition;

            //GRAB A SCROLL
            if (_tscrollHighlighted != null && !_tourniquet._isGrab)
            {
                SdAudioManager.Trigger("RecipeBoard_RecipePick", gameObject);
                _tscrollHighlighted.UnHighlight();
                _tscrollHighlighted._color = Color.green;
                _tscrollHighlighted.Highlight();
                _tscrollHold = _tscrollHighlighted.GetComponent<TScroll>();
                _tourniquet.SetPreselectedRecipe(_tscrollHold._recipeData);
                _tourniquet._isGrab = true;
            }           
            // TURN WHEEL
            else if(_tscrollHighlighted == null)
            {
                m_isDragging = true;
                
                m_startRotation = _tourniquet.transform.eulerAngles.y;
                _tourniquet.rigidbody.angularVelocity = Vector3.zero;
                SdAudioManager.Trigger("RecipeWheel_Wheel", _tourniquet.gameObject);
            }
        }

        // RELEASE 
        else if (_handController.IsOpeningHand() )
        {
            //release the rotation
            if (m_isDragging)
            {
                m_isDragging = false;
            }
            //release the tscroll drag
            else if (_tscrollHold != null)
            {
                _tourniquet._isGrab = false;
                _tscrollHold.Reset();
                _tscrollHighlighted.UnHighlight();
                _tscrollHold = null;
                _tscrollHighlighted = null;
            }
                   
        }

        //make the tourniquet rotate
        if (m_isDragging)
        {
            //_tourniquet.rigidbody.angularVelocity = Vector3.zero;
            //_tourniquet.rigidbody.AddTorque(-_tourniquet.rigidbody.angularVelocity);
            float force = (_screenPosition.x - m_lastPosition.x) / (float)Camera.main.pixelWidth;
            Vector3 torque = new Vector3(0, force * m_rotateSpeed, 0);
            _tourniquet.rigidbody.AddTorque(-torque, ForceMode.Acceleration);            
            //SdAudioManager.SetParameter("RecipeWheel_RecipeWheelSpeed", Mathf.Abs(torque.y) / 180 , gameObject);
            //SdAudioManager.SetParameter("RecipeWheel_RecipeWheelSpeed", Mathf.Abs(_tourniquet.rigidbody.angularVelocity.y) / 2, gameObject);
           

            //drop the tourniquet if the hand get out of the scene
            if (!IsVisible())
            {
                m_isDragging = false;
                //SdAudioManager.SetParameter("RecipeWheel_RecipeWheelSpeed", Mathf.Abs(_tourniquet.rigidbody.angularVelocity.y) / 2, gameObject);
            }
        }
        //moving the tscroll hold
        else if (_tscrollHold != null)
        {
            float newHeight = ( _screenPosition.y - m_startPosition.y ) / (Screen.height * 0.20f);
            
            if (newHeight >= 1)
            {
                references._tourniquet.EvaluateRecipeChoice();
                references._recipeManager.SetRecipe(_tourniquet._preselectedRecipe);                
                references._phaseManager.SwitchPhase("PhaseBasket");
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_RecipeSelected", references._binomialManager._binomialToDisable.gameObject);
                _tscrollHold = null;
                _tscrollHighlighted = null;
                this.enabled = false;
            }
            else if ( newHeight > 0 )
            {
                _tscrollHold.FollowHand(newHeight);
            }
            

        }
       
        
        m_lastPosition = _screenPosition;
    }

    /* Obsolete */
    void Update2()
    {
        //detect that the player wants to move the tourniquet
        if (_handController.IsClosingHand())
        {
            m_isDragging = true;
            m_startPosition = _screenPosition;
        }

        //release the tourniquet
        else if (_handController.IsOpeningHand())
        {
            m_isDragging = false;
        }

        //make the tourniquet rotate
        if (m_isDragging)
        {
            float ratio = _screenPosition.x - m_startPosition.x;
            float rotate = (ratio / 100) * m_rotateSpeed;
            _tourniquet.transform.Rotate(new Vector3(0, -rotate, 0));
        }
    }

    bool IsVisible()
    {
        return _screenPosition.x > 0 && _screenPosition.x < Screen.width
                && _screenPosition.y > 0 && _screenPosition.y < Screen.height;
    }

    
}