﻿using UnityEngine;
using System.Collections;

public class SdCookBoard : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        switch (action)
        {
            case "WokFallStart":
                SdAudioManager.SetSwitch("WokIngredientType", "Hard", source);
                SdAudioManager.SetSwitch("WokAction", "Start", source);
                SdAudioManager.Play("WokFallStart", source);
                break;
            case "WokFallEnd":
                SdAudioManager.SetSwitch("WokIngredientType", "Hard", source);
                SdAudioManager.SetSwitch("WokAction", "End", source);
                SdAudioManager.Play("WokFallEnd", source);
                break;
            case "WokFallSoftStart":
                SdAudioManager.SetSwitch("WokIngredientType", "Soft", source);
                SdAudioManager.SetSwitch("WokAction", "Start", source);
                SdAudioManager.Play("WokFallStart", source);
                break;
            case "WokFallSoftEnd":
                SdAudioManager.SetSwitch("WokIngredientType", "Soft", source);
                SdAudioManager.SetSwitch("WokAction", "End", source);
                SdAudioManager.Play("WokFallEnd", source);
                break;
            case "WokFallEgg":
                SdAudioManager.SetSwitch("WokIngredientType", "Egg", source);
                SdAudioManager.Play("WokFallEnd", source);
                break;
            case "CountToolOpen":
                SdAudioManager.SetSwitch("CountTool", "Open", source);
                SdAudioManager.Play("CountTool", source);
                break;
            case "CountToolClose":
                SdAudioManager.SetSwitch("CountTool", "Close", source);
                SdAudioManager.Play("CountTool", source);
                break;
            case "WickerBasketOpen":
                SdAudioManager.SetSwitch("WickerBasket", "Down", source);
                SdAudioManager.Play("WickerBasket", source);
                break;
            case "WickerBasketClose":
                SdAudioManager.SetSwitch("WickerBasket", "Up", source);
                SdAudioManager.Play("WickerBasket", source);
                break;
            case "ButtonOn":
                SdAudioManager.Play("Button", source);
                break;
            case "PlanchaOpen":
                SdAudioManager.SetSwitch("Plancha", "Open", source);
                SdAudioManager.Play("Plancha", source);
                break;
            case "PlanchaClose":
                SdAudioManager.SetSwitch("Plancha", "Close", source);
                SdAudioManager.Play("Plancha", source);
                break;
            case "PlanchaBump":
                SdAudioManager.SetSwitch("Plancha", "Bump", source);
                SdAudioManager.Play("Plancha", source);
                break;
            case "PlanchaGrillStart":
                SdAudioManager.SetSwitch("Plancha", "GrillLoop", source);
                SdAudioManager.Play("Plancha", source);
                break;
            case "PlanchaWhistleStart":
                AkSoundEngine.SetRTPCValue("GrillStatus", 0, source);
                SdAudioManager.Play("PlanchaWistle", source);
                break;
            case "PlanchaWhistleStop":
                SdAudioManager.Stop("PlanchaWistle",0, source);
                break;
            case "PlanchaGrillStop":
                SdAudioManager.Stop("Plancha", 300, source);
                SdAudioManager.Stop("PlanchaWistle", 300, source);
                break;
            case "OvenOpen":
                SdAudioManager.SetSwitch("Oven", "Open", source);
                SdAudioManager.Play("Oven", source);
                break;
            case "OvenClose":
                SdAudioManager.SetSwitch("Oven", "Close", source);
                SdAudioManager.Play("Oven", source);
                break;
            case "OvenTurnOn":
                SdAudioManager.SetSwitch("Oven", "TurnOn", source);
                SdAudioManager.Play("Oven", source);
                break;
            case "OvenIdle":
                SdAudioManager.Play("OvenIdle", source);
                break;
            case "OvenTurnOff":
                SdAudioManager.Stop("Oven", 0, source);
                SdAudioManager.SetSwitch("Oven", "TurnOff", source);
                SdAudioManager.Play("Oven", source);
                SdAudioManager.Stop("OvenIdle", 400, source);
                break;
            case "OvenTickTack":
                SdAudioManager.SetSwitch("Oven", "TickTack", source);
                SdAudioManager.Play("Oven", source);
                break;
            case "OvenTickEnd":
                SdAudioManager.Stop("Oven", 0, source);
                SdAudioManager.SetSwitch("Oven", "TickEnd", source);
                SdAudioManager.Play("Oven", source);
                break;
            case "OvenOverload":
                SdAudioManager.SetSwitch("Oven", "Overload", source);
                SdAudioManager.Play("Oven", source);
                break;
            case "PlateArrival":
                SdAudioManager.SetSwitch("Plate", "Arrival", source);
                SdAudioManager.Play("Plate", source);
                break;
            case "PlateEndRecipeFall":
                SdAudioManager.SetSwitch("Plate", "EndRecipeFall", source);
                SdAudioManager.Play("Plate", source);
                break;
            default:
                break;
        }    
    }

    public override void SetParameter(string parameter, float value, GameObject source = null)
    {
        switch (parameter)
        {
            case "GrillStatus":
            case "OvenTimer":
            case "OvenOverload":
                AkSoundEngine.SetRTPCValue(parameter, value, source);
                break;
            default:
                break;
        }
    }
}


