﻿using UnityEngine;
using System.Collections;

public class LinearFunction{

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

        private float m_a;
        private float m_b;

	#endregion

	#region Custom

        public LinearFunction(Vector2 pointA, Vector2 pointB)
        {
            SetPoints(pointA, pointB);
        }

        public LinearFunction(float a, float b)
        {
            m_a = a;
            m_b = b;
        }

        public void SetPoints(Vector2 pointA, Vector2 pointB)
        {
            float dx = pointB.x - pointA.x;
            float dy = pointB.y - pointA.y;

            m_a = (dy / dx);
            m_b = pointA.y - (m_a * pointA.x);
        }

        public void SetParameters(float a, float b)
        {
            m_a = a;
            m_b = b;
        }

        public float this[float x]
        {
            get
            {
                return (m_a * x) + m_b;
            }
        }

	#endregion
	
}
