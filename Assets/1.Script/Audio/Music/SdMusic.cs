﻿using UnityEngine;
using System.Collections;

public class SdMusic : SdItems
{

    private bool m_isPlaying = false;
    private int currentBattleLevel = 0;
    private int prevBattleLevel = 0;


    public override void Execute(string action, GameObject source = null)
    {
        string[] objects = action.Split('_');
        switch (objects[0])
        {
            case "StartPhase1":
                SdAudioManager.Play("Cooking", SdAudioManager.Audioinstance.gameObject);
                break;
            case "EndRecipe":
                SdAudioManager.SetSwitch("Jingles", "EndRecipe", SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.Play("Jingles",SdAudioManager.Audioinstance.gameObject);
                break;
            case "JuryVerdictStart":
                SdAudioManager.SetSwitch("JurySuspens", "Start", SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.Play("JurySuspens", SdAudioManager.Audioinstance.gameObject);
                break;
            case "JuryVerdictStop":
                SdAudioManager.SetSwitch("JurySuspens", "Stop", SdAudioManager.Audioinstance.gameObject);
                //SdAudioManager.Stop("JurySuspens",300, SdAudioManager.Audioinstance.gameObject);
                break;
            case "IngredientReward":
                SdAudioManager.Play("IngredientReward", SdAudioManager.Audioinstance.gameObject);
                break;
            case "ResultsVerdict":
                SdAudioManager.SetSwitch("ResultsStep1", objects[1], SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.SetSwitch("ResultsStep2", objects[2], SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.SetSwitch("ResultsStep3", objects[3], SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.Play("Results", SdAudioManager.Audioinstance.gameObject);
                break;
            case "WaitingForRound":
                if (objects[1] == "Start")
                {
                    SdAudioManager.Play("WaitingForRound", SdAudioManager.Audioinstance.gameObject);
                }
                else if (objects[1] == "Pause")
                {
                    SdAudioManager.Stop("WaitingForRound", 1000, SdAudioManager.Audioinstance.gameObject);
                }
                else if (objects[1] == "Resume")
                {
                    SdAudioManager.Stop("WaitingForRound", 500, SdAudioManager.Audioinstance.gameObject);
                }
                else
                {
                    SdAudioManager.Stop("WaitingForRound", 1000, SdAudioManager.Audioinstance.gameObject);
                }
                
                break;
            case "BossComing":
                SdAudioManager.SetSwitch("Jingles", "BossComing", SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.Play("Jingles", SdAudioManager.Audioinstance.gameObject);
                break;
            case "Abort":
                if(objects[1] == "PiratesArrival")
                {
                    SdAudioManager.SetSwitch("Jingles", "PiratesArrival", SdAudioManager.Audioinstance.gameObject);
                }
                else
                {
                    SdAudioManager.SetSwitch("Jingles", "Versus", SdAudioManager.Audioinstance.gameObject);
                }
                SdAudioManager.Play("Jingles", SdAudioManager.Audioinstance.gameObject);
                break;
            case "Battle":
                string battleMusicState = objects[1];
                int battleMusicLevel = int.Parse(objects[2]);

                if (battleMusicState == "Stop")
                {
                    SdAudioManager.Stop("BattleMusic", 2000, SdAudioManager.Audioinstance.gameObject);
                    m_isPlaying = false;
                    break;
                }

                SdAudioManager.SetSwitch("CombatStatus", battleMusicState, SdAudioManager.Audioinstance.gameObject);
                SdAudioManager.SetSwitch("CombatMusicLevel", "Level"+battleMusicLevel, SdAudioManager.Audioinstance.gameObject);
                if (battleMusicLevel < currentBattleLevel)
                {
                    string instrumentFail = "";
                    if (battleMusicLevel == 3)
                        instrumentFail = "FluteFail";
                    else if (battleMusicLevel == 1)
                        instrumentFail = "GuitarFail";
                    SdAudioManager.PlayTrigger(instrumentFail, SdAudioManager.Audioinstance.gameObject);
                }

                currentBattleLevel = battleMusicLevel;
                if (!m_isPlaying)
                {
                    SdAudioManager.Play("BattleMusic", SdAudioManager.Audioinstance.gameObject);
                    m_isPlaying = true;
                }
                break;
            case "EndRound":

                if(objects[1] == "Stop")
                {
                    SdAudioManager.Stop("Jingles", 1000, m_parentManager);
                    break;
                }
                SdAudioManager.SetSwitch("Jingles", "EndRound", m_parentManager);
                SdAudioManager.SetSwitch("EndRound", objects[1], m_parentManager);
                SdAudioManager.Stop("Jingles",300, m_parentManager);
                SdAudioManager.Play("Jingles", m_parentManager);
                if (objects[1] == "Victory")
                {
                    SdAudioManager.SetSwitch("VFXUI", "Results", m_parentManager);
                    SdAudioManager.SetSwitch("RecipeResultLevel", "Level2", m_parentManager);
                    SdAudioManager.Play("VFX_UI", m_parentManager);
                }
                else if (objects[1] == "Defeat")
                {
                    SdAudioManager.SetSwitch("VFXUI", "Results", m_parentManager);
                    SdAudioManager.SetSwitch("RecipeResultLevel", "Level0", m_parentManager);
                    SdAudioManager.Play("VFX_UI", m_parentManager);
                }
                break;
            default:
                break;
        }
    }
    public override void SetParameter(string parameter, float value, GameObject source = null)
    {
        switch (parameter)
        {
            case "ProgressBar":
                AkSoundEngine.SetRTPCValue("ProgressBar", value, m_parentManager);
                break;
            default:
                break;
        }
    }
}


