﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RecipeManager : ReferencedMonoBehaviour {

	#region Enum

        public enum RecipeStepProgress { WorkInProgress, Baking, Completed, ToTheWok }

	#endregion

	#region Public

        public SORecipe _currentRecipe;
        public SORecipe.RecipeStep _currentStep;

        public float _foodToWokDuration = 1f;
        public Timer _foodToWokTimer;

        public float _recipeStart = -1;

        public int _nbStepValidate = 0;

        public Transform _slotBakable;

        public float _recipeTimerDuration= 90f;

	#endregion

	#region Private

        private bool[] m_validatedSteps;
        private int m_indexCurrentStep;
        private RecipeStepProgress m_stepProgress;
        private bool m_rewardFxPlayed;
        private Ingredient.SliceType m_soundType;

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();

            

            //DEBUG
            if (references._phaseManager._phaseStart == PhaseManager.PhaseName.PhaseBasket || references._phaseManager._phaseStart == PhaseManager.PhaseName.PhaseBaking)
            {
                SetRecipe(_currentRecipe);

                if(references._phaseManager._phaseStart == PhaseManager.PhaseName.PhaseBaking)
                {
                    for (int i = 0; i < _currentRecipe._recipeSteps.Length; i++)
                    {
                        if (_currentRecipe._recipeSteps[i]._action != SORecipe.Interaction.Bake && _currentRecipe._recipeSteps[i]._action != SORecipe.Interaction.Toast)
                        {
                            m_validatedSteps[i] = true;
                        }
                    }

                    int index = this.FindFirstUncompleteStep();
                    SetStep(index);
                }
            }

		}

		void Update ()
		{
            switch(m_stepProgress)
            {
                //Validate the current step
                case RecipeStepProgress.WorkInProgress:
                    if(_currentStep._action == SORecipe.Interaction.Cut)
                    {
                        if (m_rewardFxPlayed == false && references._knife._sliceCount >= _currentStep._count)
                        {
                            SdAudioManager.Trigger("Music_IngredientReward");
                            references._recipeTimer.EndAnim();
                            m_rewardFxPlayed = true;
                        }
                        if (references._knife._sliceFinished == true && references._knife.GetComponent<Tool>()._inSlot == true && references._knife._ingredientBlown == true)
                        {
                            m_stepProgress = RecipeStepProgress.Completed;
                            ValidateCurrentStep();
                            if (++_nbStepValidate < 3)
                            {
                                SdAudioManager.Trigger("Music_Battle_Idle_" + (2 + _nbStepValidate));
                            }
                            else
                            {
                                SdAudioManager.Trigger("Music_Battle_Speed_4");
                            }

                            //Close the others toolboxes
                            Toolbox[] toolboxes = references._toolboxes;
                            for (int i = 0; i < toolboxes.Length; i++)
                            {
                                toolboxes[i].SwitchOff();
                            }

                            references._knife._ingredientBlown = false;
                        }
                    }
                    else if(_currentStep._action == SORecipe.Interaction.Spread)
                    {
                        if (references._roller._target != null && references._roller._target._squeezeRate >= _currentStep._count && references._roller.GetComponent<Tool>()._inSlot == true)
                        {
                            m_stepProgress = RecipeStepProgress.Completed;
                            ValidateCurrentStep();
                            if (++_nbStepValidate < 3)
                            {
                                SdAudioManager.Trigger("Music_Battle_Idle_" + (2 + _nbStepValidate));
                            }
                            else
                            {
                                SdAudioManager.Trigger("Music_Battle_Speed_4");
                            }
                            SdAudioManager.Trigger("Music_IngredientReward");
                            references._recipeTimer.EndAnim();

                            //Close the others toolboxes
                            Toolbox[] toolboxes = references._toolboxes;
                            for (int i = 0; i < toolboxes.Length; i++)
                            {
                                toolboxes[i].SwitchOff();
                            }
                        }
                    }
                    else if(_currentStep._action == SORecipe.Interaction.Break)
                    {
                        GameObject breakable = GameObject.FindGameObjectWithTag("Broken");
                        if (breakable != null && breakable.GetComponent<BreakableObject>().IsBreak() == true)
                        {
                            m_stepProgress = RecipeStepProgress.Completed;
                            ValidateCurrentStep();
                            if (++_nbStepValidate < 3)
                            {
                                SdAudioManager.Trigger("Music_Battle_Idle_" + (2 + _nbStepValidate));
                            }
                            else
                            {
                                SdAudioManager.Trigger("Music_Battle_Speed_4");
                            }
                            SdAudioManager.Trigger("Music_IngredientReward");
                            references._recipeTimer.EndAnim();

                        }
                    }
                    break; 

                case RecipeStepProgress.Baking:
                    //VERIFIER L'ETAT DE LA CUISSON
                    break;

                //Launch the food to the wok
                case RecipeStepProgress.Completed:
                    if(references._toWokCollider.bounds.Contains(references._phaseManager._actualPhase._customHandControllerLeft.transform.position) == true &&
                       references._toWokCollider.bounds.Contains(references._phaseManager._actualPhase._customHandControllerRight.transform.position) == true)
                    {
                        FoodToWok();
                        m_stepProgress = RecipeStepProgress.ToTheWok;

                        //RECORD SCORES
                        //Spreading
                         GameObject spreadable = GameObject.FindGameObjectWithTag("Spreadable");
                         if (spreadable != null )
                         {
                             references._verdictManager.AddScore(spreadable.GetComponent<SpreadableMeshController>()._squeezeRate);
                         }
                         else if (_currentStep._action == SORecipe.Interaction.Cut)
                         {
                             if (references._knife._lastSliceCount <= _currentStep._count)
                             {
                                 references._verdictManager.AddScore(references._knife._lastSliceCount / _currentStep._count);
                             }
                             else
                             {
                                 references._verdictManager.AddScore(_currentStep._count / references._knife._lastSliceCount);

                             }                             
                         } 
                    }
                    break;

                //Go To the next step
                case RecipeStepProgress.ToTheWok:
                    if(_foodToWokTimer.IsTimeElapsedOnce(0.4f) == true)
                    {
                        if (m_soundType == Ingredient.SliceType.Soft)
                        {
                            SdAudioManager.Trigger("CookBoard_WokFallSoftStart", references._wok.gameObject);
                        }
                        else if (m_soundType == Ingredient.SliceType.Hard)
                        {
                            SdAudioManager.Trigger("CookBoard_WokFallStart", references._wok.gameObject);
                        }
                        else if (m_soundType == Ingredient.SliceType.Egg)
                        {
                            SdAudioManager.Trigger("CookBoard_WokFallEgg", references._wok.gameObject);
                        }
                        else if (m_soundType == Ingredient.SliceType.Coco)
                        {
                            SdAudioManager.Trigger("CookBoard_WokFallEnd", references._wok.gameObject);
                        }
                    }

                    if(_foodToWokTimer.IsElapsedOnce == true)
                    {
                        if (m_soundType == Ingredient.SliceType.Soft)
                        {
                            SdAudioManager.Trigger("CookBoard_WokFallSoftEnd", references._wok.gameObject);
                        }
                        else if (m_soundType == Ingredient.SliceType.Hard)
                        {
                            SdAudioManager.Trigger("CookBoard_WokFallEnd", references._wok.gameObject);
                        }



                        if (IsPreparationComplete() == false)
                        {
                            references._phaseManager.SwitchPhase("PhaseBasket");
                        }
                        else
                        {
                            int index = FindFirstUncompleteStep();
                            if (HasBakingPhase() == false || index == -1)
                            {
                                //references._recipeTimer.transform.parent.gameObject.SetActive(false);
                                
                                // set the timing score
                                float time = GetTimerProgress() / ( _recipeTimerDuration - 30);                                
                                references._verdictManager.SetTimingScore(time * 100);

                                references._recipeTimer.FadeOut();

                                references._phaseManager.SwitchPhase("PhaseVerdict");
                                _recipeStart = -1;
                            }
                            else
                            {
                                SetStep(index);
                                references._phaseManager.SwitchPhase("PhaseBaking");
                            }
                        }
                    }
                    break;
            }
		}

	#endregion

	#region Custom

        public void FoodToWok()
        {
            List<GameObject> ingredients = new List<GameObject>();
            GameObject[] ing = GameObject.FindGameObjectsWithTag("Ingredient");
            if(ing != null)
            {
                ingredients.AddRange(ing);
            }

            GameObject[] broken = GameObject.FindGameObjectsWithTag("Broken");
            if (broken != null)
            {
                ingredients.AddRange(broken);
            }
            /*ingredients.AddRange(GameObject.FindGameObjectsWithTag("Ingredient"));
            ingredients.AddRange(GameObject.FindGameObjectsWithTag("Spreadable"));
            ingredients.AddRange(GameObject.FindGameObjectsWithTag("Broken"));*/

            //SdAudioManager.Trigger("CookBoard_WokFallStart", gameObject);


            for (int i = 0; i < ingredients.Count; i++)
            {
                //Debug.Log(ingredients[i].gameObject.tag);
                if (ingredients[i].GetComponent<Ingredient>() != null && ingredients[i].GetComponent<Ingredient>()._inBasket == false)
                {
                    m_soundType = ingredients[i].GetComponent<Ingredient>()._sliceType;
                    ingredients[i].GetComponent<Ingredient>().ToWok(references._wok.position, _foodToWokDuration, Random.Range(0.3f, 0.5f));
                }
            }

            _foodToWokTimer = new Timer(_foodToWokDuration);
        }

        public void ForceStepValidation()
        {
            ValidateCurrentStep();
            m_stepProgress = RecipeStepProgress.ToTheWok;
            _foodToWokTimer = new Timer(0f);
        }

        public void StartTimer()
        {
            _recipeTimerDuration = _currentRecipe._recipeDuration;
            _recipeStart = Time.time + _recipeTimerDuration;
        }

        public float GetTimerProgress()
        {
            if (_recipeStart - Time.time >= 0 && _recipeStart >= 0)
            {
                return _recipeStart - Time.time;
            }
            return 0;
        }

        public RecipeStepProgress StepProgress
        {
            get { return m_stepProgress; }
        }

        public void SetRecipe(SORecipe recipe)
        {
            _currentRecipe = recipe;
            m_validatedSteps = new bool[_currentRecipe._recipeSteps.Length];
            //references._basketManager.FillBasket();
            references._basketManager.SpawnBasket(_currentRecipe._basket);
        }

        public void ValidateCurrentStep()
        {
            references._recipeSumUp.ValidateStep(m_indexCurrentStep);
            m_validatedSteps[m_indexCurrentStep] = true;
        }

        public void SetStep(Ingredient ingredient)
        {
            m_indexCurrentStep = ingredient._indexInSteps;
            _currentStep = _currentRecipe._recipeSteps[m_indexCurrentStep];

            references._knife.SetObjectToSlice(ingredient.gameObject);
            if(ingredient.GetComponent<SpreadableMeshController>() != null)
            {
                ingredient.GetComponent<SpreadableMeshController>()._roller = references._roller;
                references._roller._target = ingredient.GetComponent<SpreadableMeshController>();
            }

            m_stepProgress = RecipeStepProgress.WorkInProgress;

            m_rewardFxPlayed = false;
        }

        //Bakable
        public void SetStep(int index)
        {
            m_indexCurrentStep = index;
            _currentStep = _currentRecipe._recipeSteps[m_indexCurrentStep];

            //references._knife.SetObjectToSlice(ingredient.gameObject);

            StartCoroutine("SpawnBakable");

            m_stepProgress = RecipeStepProgress.Baking;

            m_rewardFxPlayed = false;
        }

        public IEnumerator SpawnBakable()
        {
            yield return new WaitForSeconds(2f);
            GameObject go = GameObject.Instantiate(_currentRecipe._recipeSteps[m_indexCurrentStep]._ingredient._prefab) as GameObject;
            go.transform.position = _slotBakable.position;
            go.GetComponent<Ingredient>()._inBasket = false;
        }

        public int FindFirstUncompleteStep()
        {
            for (int i = 0; i < m_validatedSteps.Length; i++)
            {
                if (m_validatedSteps[i] == false)
                    return i;
            }
            return -1;
        }

        //check if all non toasting n baking pahse are complete
        public bool IsPreparationComplete()
        {
            bool complete = true;
            for (int i = 0; i < m_validatedSteps.Length; i++)
            {
                if (_currentRecipe._recipeSteps[i]._action != SORecipe.Interaction.Toast &&
                    _currentRecipe._recipeSteps[i]._action != SORecipe.Interaction.Bake)
                {
                    if (m_validatedSteps[i] == false)
                    {
                        complete = false;
                        break;
                    }
                }
            }
            return complete;
        }

        public bool HasBakingPhase()
        {
            int n = _currentRecipe._recipeSteps.Length - 1;
            return _currentRecipe._recipeSteps[n]._action == SORecipe.Interaction.Bake ||
                   _currentRecipe._recipeSteps[n]._action == SORecipe.Interaction.Toast;
        }

	#endregion
	
}
