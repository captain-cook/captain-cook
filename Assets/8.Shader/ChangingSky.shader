// Shader created with Shader Forge Beta 0.31 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.31;sub:START;pass:START;ps:flbk:Unlit/Texture,lico:0,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32589,y:32665|diff-10-OUT,emission-10-OUT,alpha-31-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33258,y:32393,ptlb:Clear Sky,ptin:_ClearSky,tex:7c8c481e0f250974cb333a86fc3c3430,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3,x:33362,y:32530,ptlb:Foggy Sky,ptin:_FoggySky,tex:a50bd7b0185e1744fae1ac6cd509f85a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4,x:33465,y:32666,ptlb:Stormy Sky,ptin:_StormySky,tex:ebd2c64b6fc1d424abc36aa70af0454a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:7,x:33104,y:32533|A-2-RGB,B-3-RGB,T-11-OUT;n:type:ShaderForge.SFN_Lerp,id:10,x:32928,y:32633|A-7-OUT,B-4-RGB,T-12-OUT;n:type:ShaderForge.SFN_Slider,id:11,x:33089,y:32810,ptlb:Fog %,ptin:_Fog,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:12,x:33091,y:32930,ptlb:Storm %,ptin:_Storm,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:31,x:33091,y:33036,ptlb:Alpha,ptin:_Alpha,min:0,cur:0,max:1;proporder:2-11-3-12-4-31;pass:END;sub:END;*/

Shader "Custom/ChangingSky" {
    Properties {
        _ClearSky ("Clear Sky", 2D) = "white" {}
        _Fog ("Fog %", Range(0, 1)) = 0
        _FoggySky ("Foggy Sky", 2D) = "white" {}
        _Storm ("Storm %", Range(0, 1)) = 0
        _StormySky ("Stormy Sky", 2D) = "white" {}
        _Alpha ("Alpha", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _ClearSky; uniform float4 _ClearSky_ST;
            uniform sampler2D _FoggySky; uniform float4 _FoggySky_ST;
            uniform sampler2D _StormySky; uniform float4 _StormySky_ST;
            uniform float _Fog;
            uniform float _Storm;
            uniform float _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_39 = i.uv0;
                float4 node_2 = tex2D(_ClearSky,TRANSFORM_TEX(node_39.rg, _ClearSky));
                float4 node_3 = tex2D(_FoggySky,TRANSFORM_TEX(node_39.rg, _FoggySky));
                float4 node_4 = tex2D(_StormySky,TRANSFORM_TEX(node_39.rg, _StormySky));
                float3 node_10 = lerp(lerp(node_2.rgb,node_3.rgb,_Fog),node_4.rgb,_Storm);
                float3 emissive = node_10;
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,_Alpha);
            }
            ENDCG
        }
    }
    FallBack "Unlit/Texture"
    CustomEditor "ShaderForgeMaterialInspector"
}
