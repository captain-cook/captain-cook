﻿using UnityEngine;
using System.Collections;

public class CustomFixedHandController : CustomHandController {

    public float _distanceFromCamera;

    public virtual void Update()
    {
        Vector3 lp = transform.localPosition;
        lp.z = _distanceFromCamera;
        transform.localPosition = lp;
    }

}
