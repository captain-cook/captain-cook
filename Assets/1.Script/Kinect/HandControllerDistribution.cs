﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HandControllerDistribution : CustomHandController
{

    #region Enum

        public enum DistributionMode { OneHand, TwoHand }
        public enum DistributionRole { Aim, Throw }

    #endregion

    #region Public

        public float _threshold = 0.5f;

        public float _minThrowDuration = 0.2f;
        public float _maxThrowDuration = 1.5f;

        public Transform _targetA;
        public float _weightA = 1;
        public Transform _targetB;
        public float _weightB = 1;
        public Transform _targetC;
        public float _weightC = 1;

        private List<Transform> m_historyTarget;
        private int m_historyTargetCount = 10;

        public GameObject _itemToThrow;

    #endregion

    #region Private

        private HandControllerDistribution m_otherHandControllerDistribution;

        private DistributionMode m_mode;
        private DistributionRole m_role;

        private HandController m_handController;
        private Transform m_shoulder;
        private Transform m_otherShoulder;

        private Vector3 m_throwDirection;
        private Transform m_targetSelected;

        private Vector3 m_directionA;
        private Vector3 m_directionB;
        private Vector3 m_directionC;


    #endregion

    #region Unity

        void Awake()
        {
            base.LoadReferences();
            m_handController = GetComponent<HandController>();

            m_historyTarget = new List<Transform>();

            //m_mode = references._playerData._distributionMode;

            if(m_handController._side == HandController.HandSide.Left)
            {
                m_shoulder = m_handController._body.transform.SearchChild("ShoulderLeft");
                m_otherShoulder = m_handController._body.transform.SearchChild("ShoulderRight");
                m_otherHandControllerDistribution = transform.parent.SearchChild("HandRight").GetComponent<HandControllerDistribution>();

                //if (references._playerData._masterHand == PlayerData.MasterHand.Left)
                //{
                //    m_role = DistributionRole.Throw;
                //}
                //else
                //{
                //    m_role = DistributionRole.Aim;
                //}
            }
            else
            {
                m_shoulder = m_handController._body.transform.SearchChild("ShoulderRight");
                m_otherShoulder = m_handController._body.transform.SearchChild("ShoulderLeft");
                m_otherHandControllerDistribution = transform.parent.SearchChild("HandLeft").GetComponent<HandControllerDistribution>();
                //if (references._playerData._masterHand == PlayerData.MasterHand.Right)
                //{
                //    m_role = DistributionRole.Throw;
                //}
                //else
                //{
                //    m_role = DistributionRole.Aim;
                //}
            }


        }

        void Update()
        {
            if(_itemToThrow != null)
            {
                _itemToThrow.transform.position = transform.position;
            }

            //Compute all target direction
            m_directionA = (_targetA.position - m_otherShoulder.position);
            m_directionA.y = 0;
            m_directionA.Normalize();

            m_directionB = (_targetB.position - m_otherShoulder.position);
            m_directionB.y = 0;
            m_directionB.Normalize();

            m_directionC = (_targetC.position - m_otherShoulder.position);
            m_directionC.y = 0;
            m_directionC.Normalize();

            //Get the position history and assosiated times
            List<float> times;
            List<Vector3> positions = m_handController.PositionHistoryWithTime(out times);

            if (m_mode == DistributionMode.OneHand)
            {
                if (DetectThrowWithDirection(positions, times) == true)
                {
                    if (m_targetSelected != null)
                    {
                        ThrowItemToTarget(m_targetSelected);
                    }
                }
            }
            else if (m_mode == DistributionMode.TwoHand)
            {
                //Select a pirate to feed
                if (m_role == DistributionRole.Aim)
                {
                    _targetA.renderer.material.color = Color.white;
                    _targetB.renderer.material.color = Color.white;
                    _targetC.renderer.material.color = Color.white;

                    m_targetSelected = SelectTarget();
                    m_otherHandControllerDistribution.m_targetSelected = m_targetSelected;

                    m_targetSelected.renderer.material.color = Color.red;
                }

                //Detect the throw movement
                if (m_role == DistributionRole.Throw)
                {
                    if (DetectThrow(positions, times) == true)
                    {
                        if (m_targetSelected != null)
                        {
                            ThrowItemToTarget(m_targetSelected);
                        }
                    }
                }
            }
        }

    #endregion

    #region Custom

        Transform SelectTarget()
        {
            Vector3 direction = transform.position - m_shoulder.position;
            direction.y = 0;

            

            float distanceA = Vector3.Distance(direction, m_directionA) / _weightA;
            float distanceB = Vector3.Distance(direction, m_directionB) / _weightB;
            float distanceC = Vector3.Distance(direction, m_directionC) / _weightC;

            if (distanceA <= distanceB && distanceA <= distanceC) { return _targetA; }
            else if (distanceB <= distanceA && distanceB <= distanceC) { return _targetB; }
            else if (distanceC <= distanceA && distanceC <= distanceB) { return _targetC; }

            return null;
        }

        //Detect if the player is throwing the food        
        bool DetectThrowWithDirection(List<Vector3> positions, List<float> times)
        {
            float timeBegin = -1;
            float timeEnd = -1;
            float coefLerp = 0.5f;
            Vector3 direction = Vector3.zero;

            for (int i = positions.Count - 2; i > 0; i--)
            {
                float deltaTime = times[i] - times[i - 1];
                Vector3 velocity = (positions[i] - positions[i - 1]) / deltaTime;

                if (timeEnd == -1)
                {
                    if (velocity.z > _threshold)
                    {
                        timeEnd = times[i + 1];
                        timeBegin = -1;
                        direction = velocity;
                    }
                }
                else if (timeBegin == -1)
                {
                    direction = Vector3.Lerp(direction, velocity, coefLerp);
                    coefLerp *= 0.5f;

                    if(velocity.z < _threshold)
                    {
                        timeBegin = times[i];
                        float duration = timeEnd - timeBegin;

                        if(MathPlus.IsInRange(duration, _minThrowDuration, _maxThrowDuration) == true)
                        {
                            direction.y = 0;
                            direction.Normalize();

                            m_throwDirection = direction;

                            float distanceA = Vector3.Distance(direction, m_directionA) / _weightA;
                            float distanceB = Vector3.Distance(direction, m_directionB) / _weightB;
                            float distanceC = Vector3.Distance(direction, m_directionC) / _weightC;

                            if (distanceA <= distanceB && distanceA <= distanceC) { m_targetSelected = _targetA; }
                            else if (distanceB <= distanceA && distanceB <= distanceC) { m_targetSelected = _targetB; }
                            else if (distanceC <= distanceA && distanceC <= distanceB) { m_targetSelected = _targetC; }

                            m_handController.ClearPositionHistory();

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        //Detect if the player is throwing the food
        bool DetectThrow(List<Vector3> positions, List<float> times)
        {
            float timeBegin = -1;
            float timeEnd = -1;

            for (int i = positions.Count - 2; i > 0; i--)
            {
                float deltaTime = times[i] - times[i - 1];
                Vector3 velocity = (positions[i] - positions[i - 1]) / deltaTime;

                if (timeEnd == -1)
                {
                    if (velocity.z > _threshold)
                    {
                        timeEnd = times[i + 1];
                        timeBegin = -1;
                    }
                }
                else if (timeBegin == -1)
                {
                    if (velocity.z < _threshold)
                    {
                        timeBegin = times[i];
                        float duration = timeEnd - timeBegin;

                        if (MathPlus.IsInRange(duration, _minThrowDuration, _maxThrowDuration) == true)
                        {
                            m_handController.ClearPositionHistory();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        void ThrowItemToTarget(Transform target)
        {
            if(_itemToThrow != null)
            {
                MoveOnBezier mob = _itemToThrow.GetComponent<MoveOnBezier>();
                mob._bezier = new Bezier(mob.transform.position, mob.transform.position + (Vector3.up), target.position + (Vector3.up), target.position);
                mob.StartMoving(true);
                mob._hand = this;
                _itemToThrow = null;
            }
        }

        void OnDrawGizmos()
        {
            //Debug.Log(m_throwDirection);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(Vector3.zero, m_directionA);
            Gizmos.DrawLine(Vector3.zero, m_directionB);
            Gizmos.DrawLine(Vector3.zero, m_directionC);

            Gizmos.color = Color.green;
            Gizmos.DrawLine(Vector3.zero, m_throwDirection);
        }

    #endregion

}
