﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnIngredients : MonoBehaviour {

    public List<Transform[]> _listPieces;

    public float _bigRadius;
    public float _littleRadius;

    // DEV VARIABLES 
    //public Transform[] _pieces1;
    //public Transform[] _pieces2;
    //public Transform[] _pieces3;

	// Use this for initialization
	void Start () {

        ResetSpawner();

        //AddPieces(_pieces1);
        //AddPieces(_pieces2);
        //AddPieces(_pieces3);

        //Spawn();

    }
	
	// Update is called once per frame
	void Update () {
	
        if (Input.GetKey(KeyCode.R))
        {
            Spawn();
        }
	}

    //add a tab of pieces corresponding to a ingredient
    public void AddPieces(Transform[] pieces)
    {
        _listPieces.Add(pieces);
    }

    //reset the list of pieces stored for the last recipe
    public void ResetSpawner()
    {
        _listPieces = new List<Transform[]>();
    }

    //place the ingredients pieces and drop it
    public void Spawn()
    {
        //get the total number of pieces
        int nbPieces = 0;
        foreach (Transform[] pieces in _listPieces)
        {
            nbPieces += pieces.Length;
        }

        //spill pieces
        int bigCircle = nbPieces - (nbPieces / 3);
        int littleCircle = nbPieces - bigCircle;

        //degree step between pieces
        int bigStep = 360 / bigCircle;
        int littleStep = 360 / littleCircle;

        //counter
        int cptPiece = 0;
        int cptStepLittleCircle = 0;
        int cptStepBigCircle = 0;
        float cptHeight = 0;

        //set new position
        Vector3 newPosition;
        foreach(Transform[] pieces in _listPieces)
        {
            foreach (Transform piece in pieces)
            {
                newPosition = transform.position;

                piece.localScale *= 0.8f;

                piece.rotation = Quaternion.identity;

                piece.rigidbody.angularVelocity = Vector3.zero;
                piece.rigidbody.velocity = Vector3.zero;
                piece.rigidbody.mass = 100;
                piece.rigidbody.useGravity = true;

                Destroy(piece.collider);
                piece.gameObject.AddComponent<BoxCollider>();
                piece.collider.material.bounciness = 0;

                piece.gameObject.layer = LayerMask.NameToLayer("IngredientsNoCollide");
                piece.gameObject.SetActive(true);


                //Big Circle
                if (cptPiece < bigCircle)
                {
                    newPosition.x += _bigRadius * Mathf.Cos(Mathf.Deg2Rad * cptStepBigCircle);
                    newPosition.z += _bigRadius * Mathf.Sin(Mathf.Deg2Rad * cptStepBigCircle);
                    newPosition.y += cptHeight;
                    cptStepBigCircle += bigStep;
                }
                //Little Circle
                else 
                {
                    newPosition.x += _littleRadius * Mathf.Cos(Mathf.Deg2Rad * cptStepLittleCircle);
                    newPosition.z += _littleRadius * Mathf.Sin(Mathf.Deg2Rad * cptStepLittleCircle);
                    newPosition.y += cptHeight;
                    cptStepLittleCircle += littleStep;
                }

                //cptHeight += piece.localScale.y;
                cptHeight += (piece.localScale.y * 4);
                piece.position = newPosition;
                cptPiece++;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _littleRadius);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, _bigRadius);
    }
}
