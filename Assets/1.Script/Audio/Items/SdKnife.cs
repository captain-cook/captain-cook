﻿using UnityEngine;
using System.Collections;

public class SdKnife : SdItems
{
    /*
    private static SdKnife m_instance;
    [Range(0,1)]
    */
    public float _cutSpeed;

    private bool m_isCutting = false;
    /*
    void Update()
    {
        if (m_instance.m_isCutting)
            SdAudioManager.SetParameter("Knife_CutSpeed", m_instance._cutSpeed, gameObject);
    }
    */
    public override void Execute(string action, GameObject source = null) 
    {
        switch (action)
        {
            case "Pickup":
            case "Drop":
            case "EndCut":
            case "WoodHit":
            case "Whoosh":
                SdAudioManager.SetSwitch("Knife_State", action, source);
                if (action != "Whoosh")
                {
                    this.m_isCutting = false;
                }
                break;
            case "CutHard":
                this.m_isCutting = true;
                SdAudioManager.SetSwitch("Knife_State", "Cut", source);
                SdAudioManager.SetSwitch("Knife_IngredientCut", "Hard", source);
                
                break;
            case "CutSoft":
                this.m_isCutting = true;
                SdAudioManager.SetSwitch("Knife_State", "Cut", source);
                SdAudioManager.SetSwitch("Knife_IngredientCut", "Soft", source);
                break;
            default:
                break;
        }
        SdAudioManager.Play("Knife",source);
    }
    public override void SetParameter(string parameter, float value, GameObject source = null) 
    {
        switch(parameter){
            case "CutSpeed":
                AkSoundEngine.SetRTPCValue("CutSpeed",value,source);
                break;
            default:
                break;
        }
    }
}


