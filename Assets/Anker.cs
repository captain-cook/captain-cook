﻿using UnityEngine;
using System.Collections;

public class Anker : ReferencedMonoBehaviour {

    public bool _isLocked = false;
    public MeshRenderer _posterEnemy;
    private EnemyBoat m_selectedBoat;
    private TweenPosition m_tPosition;
    private MeshRenderer m_mesh;

	// Use this for initialization
	void Start () {

        base.LoadReferences();
        m_tPosition = GetComponent<TweenPosition>();
        m_mesh = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Update the selected enemy boat
    public void UpdateEnemyBoat(EnemyBoat boat)
    {
        //Debug.Log("HEre = " + boat.gameObject.name);
        if (!_isLocked)
        {
            //Old Boat -> New Boat
            if (boat != null && m_selectedBoat != null && m_selectedBoat != boat)
            {
                m_selectedBoat.OnHover(false);
                m_selectedBoat = boat;
                m_selectedBoat.OnHover(true);
                SetAnkerPosition(boat.transform.localPosition);
                _posterEnemy.material = boat._posterMaterial;
            }
            //null -> New Boat
            else if (boat != null && m_selectedBoat == null)
            {
                m_selectedBoat = boat;
                m_selectedBoat.OnHover(true);
                SetAnkerPosition(boat.transform.localPosition);
                _posterEnemy.material = boat._posterMaterial;
            }
            //Old Boat -> null
            else if (boat == null && m_selectedBoat != boat)
            {
                m_selectedBoat.OnHover(false);
                m_selectedBoat = null;
                m_mesh.enabled = false;
            }
        }       
    }

    void SetAnkerPosition(Vector3 position)
    {
        m_mesh.enabled = true;

        Vector3 newPosition = position;
        newPosition.z = transform.localPosition.z;        

        Vector3 from = newPosition;
        from.z = m_tPosition.from.z;
        m_tPosition.from = from;

        Vector3 to = newPosition;
        to.z = m_tPosition.to.z;
        m_tPosition.to = to;

        transform.localPosition = newPosition;

        
    }
}
