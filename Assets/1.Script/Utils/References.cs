﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class References : MonoBehaviour {

    //Managers
    public RecipeManager _recipeManager;
    public PhaseManager _phaseManager;
    public BinomialManager _binomialManager;
    public BasketManager _basketManager;
    public TasteManager _tasteManager;
    public FinishPlatManager _finishPlatManager;
    public VerdictManager _verdictManager;
    public LabelManager _labelManager;
    public LootManager _lootManager;

    //cameras
    public Camera _handCamera;

    //UI
    public FadinPanel _fadinPanel;
    public RecipePanel _recipePanel;
    public RecipeTimer _recipeTimer;
    public CountDownCooking _countDownCooking;
    public VerdictPanel _verdictPanel;
    public RoundPanel _roundPanel;
    public RecipeSumUp _recipeSumUp;
    public VSPanel _vsPanel;
    public StarterRound _starterRound;
    public ShopPanel _shopPanel;

    //Scene objects
    public Chest _chest;
    public Tourniquet _tourniquet;
    public GameObject _binomialSelectionButtons;
    public FightingTable _fightingTable;
    public Plancha _plancha;
    public PlanchaButton _planchaButton;
    public Transform _oven;
    public Anker _anker;
    public Boat _boat;
    public BoatMama _boatMama;
    public GameObject _mapWheelDeMerde;
    public Shop _shop;
    public SkyChangingBehavior _sky;
    

    //Tools
    public Slicer _knife;
    public SpreaderController _roller;
    public Toolbox[] _toolboxes;

    //Hands
    public Transform _hands;
    public HandController _handLeft;
    public HandController _handRight;

    //Camera Position settings
    public Transform _frontCamPosition;
    public Transform _leftCamPosition;
    public Transform _rightCamPosition;
    public Transform _backCamPosition;

    //Other
    public PlayerData _playerData;
    public Collider _toWokCollider;
    public Workspace _workspace;

    public Vector3 _cuttingBoard;

    public Animator _mama;

    public Transform _wok;
    public Transform _table;
    public Transform _four;
    public TimerStartRecipe _timerStartRecipe;
    public CapsuleCollider _basketTrigger;

}
