﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class CreateSObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Create new recipe
    [MenuItem("Window/Create Recipe")]
    static void CreateRecipe() 
    {
        Debug.Log("New recipe created");        
        
        SORecipe asset = ScriptableObject.CreateInstance<SORecipe>();

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/5.ScriptableObject/Recipes/New " + typeof(SORecipe).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        //AssetDatabase.Refresh();
        //EditorUtility.FocusProjectWindow();
        //Selection.activeObject = asset;
    }

    //Create new ingredient
    [MenuItem("Window/Create ingredient")]
    static void CreateIngredient()
    {
        Debug.Log("New ingredient created");

        SOIngredient asset = ScriptableObject.CreateInstance<SOIngredient>();

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/5.ScriptableObject/Ingredients/New " + typeof(SOIngredient).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        //AssetDatabase.Refresh();
        //EditorUtility.FocusProjectWindow();
        //Selection.activeObject = asset;
    }
}
