﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SdPirates : iSd
{
    private List<string> m_actionList;
    private List<string> m_crewFeedbackList;
    private List<string> m_sbiresFeedbackList;

    public SdPirates()
    {

        m_sbiresFeedbackList = new List<string>();
        m_sbiresFeedbackList.Add("AbortAnnounce");
        m_sbiresFeedbackList.Add("FeedbackPositive");
        m_sbiresFeedbackList.Add("FeedBackNegative");

        m_crewFeedbackList = new List<string>();
        m_crewFeedbackList.Add("FinalDish");
        m_crewFeedbackList.Add("FeedbackPositive");
        m_crewFeedbackList.Add("FeedBackNegative");
        m_crewFeedbackList.Add("FinishedCooking");
        m_crewFeedbackList.Add("IngredientBreak");
        m_crewFeedbackList.Add("IngredientCut");
        m_crewFeedbackList.Add("IngredientRoll");
        m_crewFeedbackList.Add("PlanchaButton");
        m_crewFeedbackList.Add("PlayerPointing");
        m_crewFeedbackList.Add("RecipeSelected");
        m_crewFeedbackList.Add("SelectDishOven");
        m_crewFeedbackList.Add("SelectDishPlancha");
        m_crewFeedbackList.Add("Selected");
        m_crewFeedbackList.Add("VoteNegative");
        m_crewFeedbackList.Add("VotePositive");
        m_crewFeedbackList.Add("WaitForRecipe");

        m_actionList = new List<string>();
        m_actionList.Add("BoatStart");
        m_actionList.Add("BoatEnd");
        m_actionList.Add("EnemySpotted");
        m_actionList.Add("PlayerEndRecipe");
        m_actionList.Add("PlayerStartRecipe");
        m_actionList.Add("PlayerSelectKnife");
        m_actionList.Add("PlayerUsingKnife");
        m_actionList.Add("PlayerSelectRoller");
        m_actionList.Add("PlayerUsingRoller");
        m_actionList.Add("PlayerSelectPie");
        m_actionList.Add("PlayerUsingPie");
        m_actionList.Add("PlayerSelectTarget");
        m_actionList.Add("PlayerSelectPirate");
        m_actionList.Add("PlayerSendToWok");
        m_actionList.Add("ReadyToSend");
        m_actionList.Add("SendIngredient");
        m_actionList.Add("Idle01");
        m_actionList.Add("Idle02");
        m_actionList.Add("Idle03");
        m_actionList.Add("EatStart");
        m_actionList.Add("EatEnd");
    }

    public virtual void SetParameter(string parameter, float value, GameObject source = null) { }
    public virtual void Execute(string pirateaction, GameObject source = null)
    {
        string[] objects = pirateaction.Split('_');
        switch (objects[0])
        {
            case "Rodny":
            case "MingYao":
            case "Arnaud":
			case "Seeker":
                CrewSounds(objects[1], objects[0], source);
                break;
            case "Sbire1":
            case "Sbire2":
            case "Sbire3":
                SbireSounds(objects[1], objects[0], source);
                break;
            case "Common":
				CommonSounds(objects[1],objects[0], source);
				break;
            default:
                break;
        }

    }

    void CrewSounds(string action, string piratename, GameObject source)
    {
        if (m_crewFeedbackList.Contains(action))
        {
            SdAudioManager.SetSwitch("CrewName", piratename, source);
            SdAudioManager.SetSwitch("CrewActions", action, source);
            SdAudioManager.Play("Crew", source);
        }
    }
    void SbireSounds(string action, string piratename, GameObject source)
    {
        if (m_sbiresFeedbackList.Contains(action))
        {
            SdAudioManager.SetSwitch("SbireTypes", piratename, source);
            SdAudioManager.SetSwitch("SbireActions", action, source);
            SdAudioManager.Play("Sbires", source);
        }
    }
    void CommonSounds(string action, string piratename, GameObject source)
    {
        if (m_actionList.Contains(action))
        {
            SdAudioManager.SetSwitch("Pirate_Select", piratename, source);
            if (action == "EatStart")
            {
                SdAudioManager.SetSwitch("Pirate_Feedbacks", action, source);
                SdAudioManager.Play("PirateFeedbacks", source);
                SdAudioManager.SetSwitch("Pirate_Feedbacks", "EatLoop", source);
                SdAudioManager.Play("PirateFeedbacks", source);
            }
            else if (action == "EatEnd")
            {
                SdAudioManager.SetSwitch("Pirate_Feedbacks", action, source);
                SdAudioManager.Stop("PirateFeedbacks", 300, source);
            }
            else
            {
                SdAudioManager.SetSwitch("Pirate_Feedbacks", action, source);
                SdAudioManager.Play("PirateFeedbacks", source);
            }
        }
    }
}