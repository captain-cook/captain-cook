﻿using UnityEngine;
using System.Collections;

public class Shop : ReferencedMonoBehaviour {

    public Transform _slotAfterBuy;

    private Animator m_animator;
    private Animation m_animationKnife;
    private TweenPosition m_tPositionKnife;
    private Light m_lightChest;
    private bool m_destroyKnife = false;
	// Use this for initialization
	void Start () {

        base.LoadReferences();
        m_animator = GetComponent<Animator>();
        m_animationKnife = GetComponentInChildren<Animation>();
        m_tPositionKnife = m_animationKnife.transform.parent.GetComponent<TweenPosition>();
        m_tPositionKnife.gameObject.SetActive(false);
        m_lightChest = GetComponentInChildren<Light>();
        m_lightChest.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    //open the shop and display the UI
    public void OpenShop()
    {
        m_animator.SetTrigger("Open");
        SdAudioManager.Trigger("UI_Shop_ChestOpen", gameObject);
    }

    public void CloseShop()
    {
        m_animator.SetTrigger("Close");
        SdAudioManager.Trigger("UI_Shop_ChestClose", gameObject);
    }

    public void DisplayUI()
    {
        references._shopPanel.DisplayShopPanel();
    }

    public void PlayKnifeAnimation()
    {
        m_tPositionKnife.gameObject.SetActive(true);
        m_tPositionKnife.transform.localPosition = m_tPositionKnife.from;
        m_tPositionKnife.PlayForward();
        m_lightChest.intensity = 1;

        SdAudioManager.Trigger("UI_Shop_ItemAppearing", m_tPositionKnife.gameObject);
    }

    public void EndMoveKinfe()
    {
        if (m_destroyKnife)
        {
            Destroy(m_tPositionKnife.gameObject);
        }
    }

    public void LightShopChest()
    {
        m_lightChest.enabled = true;
    }

    //make the knife run into the camera
    public void MoveKnife()
    {
        m_tPositionKnife.ResetToBeginning();
        m_tPositionKnife.from = m_tPositionKnife.to;
        m_tPositionKnife.to = _slotAfterBuy.localPosition;
        
        m_tPositionKnife.PlayForward();
        //m_tPositionKnife.duration = 1.5f;  
        m_destroyKnife = true;
    }
}
