using UnityEngine;
using System;
using System.Collections.Generic;

public class Timer
{

    bool m_once = true;
    float m_duration = 1.0f;
    float m_startTime = Time.timeSinceLevelLoad;
    List<float> m_times;

    public Timer(float duration = 1.0f, float forceStartTime = -1f)
    {
        m_duration = duration;
        if (forceStartTime >= 0)
            m_startTime = Time.timeSinceLevelLoad;
    }

    /*
     * Returns current time left
     * Duration -> 0
     */
    public float CurrentInverse
    {
        get { return m_duration - (Time.timeSinceLevelLoad - m_startTime); }
    }

    /*
     * Returns inverted current time left
     * 0 -> Duration
     */
    public float Current
    {
        get { return (Time.timeSinceLevelLoad - m_startTime); }
    }

    /*
     * Returns current time left normalized
     * It's a more convenient way to send value to shaders
     * for example
     * 1 -> 0
     */
    public float CurrentInverseNormalized
    {
        get { return Mathf.Max(Mathf.Min(CurrentInverse / m_duration, 1.0f), 0.0f); }
    }

    // 0 -> 1
    public float CurrentNormalized
    {
        get { return 1 - Mathf.Max(Mathf.Min(CurrentInverse / m_duration, 1.0f), 0.0f); }
    }

    /*
     * Return true each frame the timer is done
     * (current value < 0)
     */
    public bool IsElapsedLoop
    {
        get
        {
            return (Time.timeSinceLevelLoad - m_startTime > m_duration);
        }
    }

    /*
     * Return true only once, after the timer is done
     * (current value < 0)
     */
    public bool IsElapsedOnce
    {
        get
        {
            if (IsElapsedLoop)
            {
                if (m_once)
                {
                    m_once = false;
                    return true;
                }
                else return false;
            }
            return false;
        }
    }

    /*
    * Return true only once, if the timer is after TIME
    */
    public bool IsTimeElapsedOnce(float time)
    {
        if (m_times == null) m_times = new List<float>();
        
        if(Current >= time)
        {
            if(m_times.Contains(time) == false)
            {
                m_times.Add(time);
                return true;
            }
        }

        return false;
    }

    /*
     * Make the Timer restart
     * Giving a new duration > 0 will make this
     * duration the new one
     */
    public void Reset(float newDuration = -1.0f)
    {
        if (newDuration > 0.0f)
            m_duration = newDuration;
        m_startTime = Time.timeSinceLevelLoad;
        m_once = true;
    }

    public void ForceToElapse()
    {
        m_startTime = Time.timeSinceLevelLoad - m_duration - 1;
    }
}
