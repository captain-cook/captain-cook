﻿using UnityEngine;
using System.Collections;

public class ChestCounter : ReferencedMonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public TextMesh A;
        public TextMesh B;

        public Color _counterColorLess;
        public Color _counterColorGreatherEqual;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();
		}

		void Update ()
		{
            int current = references._knife._sliceCount;
            int aim = (int)references._recipeManager._currentStep._count;

            if (current < aim)
            {
                A.renderer.material.color = _counterColorLess;
            }
            else
            {
                A.renderer.material.color = _counterColorGreatherEqual;
            }

            A.text = current.ToString("00");
            B.text = aim.ToString("00");
		}

	#endregion

	#region Custom

	#endregion
	
}
