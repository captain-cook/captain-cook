﻿using UnityEngine;
using System.Collections;

public class RandomForce : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{
            rigidbody.velocity = new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));
		}

	#endregion

	#region Custom

	#endregion
	
}
