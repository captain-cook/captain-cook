﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MeshData
{
    public enum ColliderType { Box, Sphere, Mesh }
    public Mesh _mesh;
    public Material[] _materials;
    public ColliderType _colliderType;
}

public class BreakableObject : ReferencedMonoBehaviour {

    public GameObject _prefabParticle;
    public GameObject _fxShock;

    public MeshData _meshDataLiquid;
    public MeshData _meshDataOnTable;

    public MeshData _meshDataBrokenA;
    public MeshData _meshDataBrokenB;

    public bool _inTwoPart;
    private bool m_isBroken;
    private bool m_onTable;

    private MeshFilter m_meshFilter;
    private MeshRenderer m_meshRenderer;

    private BoxCollider m_boxCollider;
    private SphereCollider m_sphereCollider;
    private MeshCollider m_meshCollider;

    public int _countBreakCurrent;
    public int _countBreakAim;

	// Use this for initialization
	void Start () {
        base.LoadReferences();
        LoadComponents();
	}

    void LoadComponents()
    {
        m_meshFilter = GetComponent<MeshFilter>();
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_boxCollider = GetComponent<BoxCollider>();
        m_sphereCollider = GetComponent<SphereCollider>();
        m_meshCollider = GetComponent<MeshCollider>();
    }

    public GameObject Break()
    {
        if (m_isBroken == false)
        {
            if(_countBreakCurrent < _countBreakAim - 1)
            {
                _countBreakCurrent++;
                if(_fxShock)
                {
                    GameObject fx = Instantiate(_fxShock) as GameObject;
                    fx.transform.position = transform.position;
                }
            }
            else
            {
                SdAudioManager.Trigger("Ingredient_EggBreak", gameObject);
                if (_prefabParticle != null)
                {
                    Instantiate(_prefabParticle);
                }
                gameObject.tag = "Broken";

                m_isBroken = true;


                if (_inTwoPart == true)
                {
                    LoadMesh(_meshDataBrokenA);

                    GameObject B = GameObject.Instantiate(gameObject, transform.position, transform.rotation) as GameObject;
                    BreakableObject bo = B.GetComponent<BreakableObject>();
                    bo.LoadComponents();
                    bo.LoadMesh(_meshDataBrokenB);
                    bo.m_isBroken = true;
                    B.tag = "Broken";

                    return B;
                }
                else
                {
                    LoadMesh(_meshDataLiquid);
                }
            }
        }

        return null;
    }

    public void TableContact()
    {
        LoadMesh(_meshDataOnTable);
        transform.eulerAngles = Vector3.zero;
        SdAudioManager.Trigger("Ingredient_EggFall", gameObject);
        rigidbody.freezeRotation = true;
        m_onTable = true;
    }


    //Load the proper mesh with its materials
    void LoadMesh(MeshData meshData)
    {
        m_meshFilter.mesh = meshData._mesh;
        m_meshRenderer.materials = meshData._materials;

        if (meshData._colliderType == MeshData.ColliderType.Box)
        {
            m_boxCollider.enabled = true;
            m_sphereCollider.enabled = false;
            m_meshCollider.enabled = false;
        }
        else if (meshData._colliderType == MeshData.ColliderType.Mesh)
        {
            m_sphereCollider.enabled = false;
            m_boxCollider.enabled = false;
            m_meshCollider.enabled = true;
            m_meshCollider.sharedMesh = meshData._mesh;
        }
        else
        {
            m_sphereCollider.enabled = true;
            m_boxCollider.enabled = false;
            m_meshCollider.enabled = false;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (_inTwoPart == false && m_isBroken == true && m_onTable == false)
        {
            TableContact();
        }
    }

    void OnCollisionStay(Collision other)
    {
        if (_inTwoPart == false && m_isBroken == true && m_onTable == false)
        {
            TableContact();
        }
    }

    public bool IsBreak()
    {
        return m_isBroken;
    }
}
