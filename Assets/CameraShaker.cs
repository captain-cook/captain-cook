﻿using UnityEngine;
using System.Collections;

public class CameraShaker : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public AnimationCurve _curveTranslationX;
        public AnimationCurve _curveTranslationY;
        public AnimationCurve _curveRotation;

	#endregion

	#region Private
        
        private Timer m_shakeTimer;

        private Vector3 _startPosition;

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{
            if(m_shakeTimer != null)
            {
                Vector3 position = _startPosition;
                position += (transform.right * _curveTranslationX.Evaluate(m_shakeTimer.Current));
                position += (transform.up * _curveTranslationY.Evaluate(m_shakeTimer.Current));
                transform.position = position;

                if(m_shakeTimer.IsElapsedOnce == true)
                {
                    m_shakeTimer = null;
                }
            }
		}

	#endregion

	#region Custom

        public void Shake()
        {
            m_shakeTimer = new Timer(5f);

            _startPosition = transform.position;
        }

	#endregion
	
}
