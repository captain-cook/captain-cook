﻿using UnityEngine;
using System.Collections;

public class Boat : ReferencedMonoBehaviour {

    [HideInInspector]
    public bool _isTargetSelected = false;
    [HideInInspector]
    public bool _isBoatTravelling = false;
    public float _moveSpeed = 1;

    private Vector3 m_moveTo;
    private Vector3 m_moveVector;
    

	// Use this for initialization
	void Start () {

        base.LoadReferences();
	}
	
	// Update is called once per frame
	void Update () {
	
        if (_isBoatTravelling)
        {
            transform.localPosition += m_moveVector * Time.deltaTime * _moveSpeed;
            //move finish
            if ((transform.localPosition - m_moveTo).magnitude < m_moveVector.magnitude * Time.deltaTime)
            {
                _isBoatTravelling = false;
                references._phaseManager.SwitchPhase("PhaseArrival");
            }
        }

	}

    //move near to the enemy boat selected
    public void Move(EnemyBoat enemy)
    {
        if (!enemy._isDefeated)
        {
            _isBoatTravelling = true;
            _isTargetSelected = true;

            references._anker._isLocked = true;

            Vector3 vectorMove = enemy.transform.localPosition - transform.localPosition;
            
            m_moveTo = transform.localPosition + vectorMove * 0.55f;

            m_moveVector = vectorMove.normalized;

            references._sky.skyIsFog = true;
            SdAudioManager.Trigger("Ambiances_General_Calm_Windy");
        }        
    }
}
