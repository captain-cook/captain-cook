﻿using UnityEngine;
using System.Collections;

public class Workspace : MonoBehaviour {

    public Bounds _bounds;
	
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + _bounds.center, _bounds.size);
    }

    public Vector3 Center
    {
        get { return transform.position + _bounds.center; }
    }

    public float MinX
    {
        get { return transform.position.x + _bounds.min.x; }
    }
    public float MaxX
    {
        get { return transform.position.x + _bounds.max.x; }
    }
    public float MinZ
    {
        get { return transform.position.z + _bounds.min.z; }
    }
    public float MaxZ
    {
        get { return transform.position.z + _bounds.max.z; }
    }

    public bool InWorkspace(Vector3 position)
    {
        return MathPlus.IsInRange(position.x, MinX, MaxX) && MathPlus.IsInRange(position.z, MinZ, MaxZ);
    }

}
