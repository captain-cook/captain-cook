﻿using UnityEngine;
using System.Collections;

public class CutableMeshController : ReferencedMonoBehaviour {

	#region Enum

        

	#endregion

	#region Public

        private string _nameBase;
        //Is the base model or not
        public bool _isBaseModel = true;

        public GameObject _fxDrops;
        //public SliceType _sliceType;

	#endregion

	#region Private

        private GameObject m_blade;
        private TextureData m_textureData;

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();

            m_textureData = GetComponent<TextureData>();

            Bounds meshBounds = GetComponent<MeshFilter>().mesh.bounds;

            if(_isBaseModel == true)
            {
                _nameBase = gameObject.name;
            }
		}

		/*void Update ()
		{
            if(m_blade != null)
            {
                SdAudioManager.SetParameter("Knife_CutSpeed", m_blade.GetComponent<BladeSpeedController>().Velocity / 5f, m_blade.gameObject);
            }
		}*/


        /*void OnTriggerEnter(Collider collider)
        {
            if (m_inSlice == false && collider.gameObject.layer == LayerMask.NameToLayer("Blades"))
            {
                m_inSlice = true;
                m_blade = collider.gameObject;
                Physics.IgnoreCollision(m_blade.transform.parent.collider, this.collider);
                Physics.IgnoreCollision(m_blade.collider, this.collider);
                    
                SdAudioManager.Trigger("Knife_Cut" + _sliceType.ToString(), m_blade.gameObject);
            }
        }*/

        /*void OnTriggerExit(Collider collider)
        {
            if (collider.gameObject == m_blade)
            {
                SliceDirect();
            }
        }*/

	#endregion

	#region Custom

        public void Slice()
        {
            Vector3 normal = m_blade.transform.parent.TransformPoint(Vector3.left) - m_blade.transform.parent.position;
            Plane slicePlane = new Plane(normal.normalized, m_blade.transform.parent.position);

            SdAudioManager.Trigger("Knife_EndCut", m_blade.gameObject);

            Physics.IgnoreCollision(m_blade.transform.parent.collider, this.collider, false);
            Physics.IgnoreCollision(m_blade.collider, this.collider, false);

            Vector3 sliceCenter = Vector3.zero;

            GameObject[] newParts = m_blade.transform.parent.GetComponent<Slicer>().Slice(gameObject, slicePlane, out sliceCenter);

            if (newParts != null)
            {
                InitializePart(newParts[0]);
                InitializePart(newParts[1]);
                m_blade.GetComponentInParent<Slicer>()._sliceCount++;

                //FX drop
                Vector3 fx_position = sliceCenter;
                GameObject.Instantiate(_fxDrops, fx_position, Quaternion.Euler(-90, 0, 0));
            }
        }

        void InitializePart(GameObject part)
        {
            CutableMeshController cm = part.GetComponent<CutableMeshController>();
            cm._isBaseModel = false;
            cm._nameBase = _nameBase;
            cm.tag = tag;
            cm._fxDrops = _fxDrops;
            //cm._sliceType = _sliceType;
            cm.GetComponent<Highlighter>().UnHighlight();
            cm.GetComponent<Ingredient>()._inBasket = false;
            part.name = _nameBase + "_sliced";

            TextureData td = part.GetComponent<TextureData>();
            td._topLeft = m_textureData._topLeft;
            td._bottomRight = m_textureData._bottomRight;
        }

	#endregion

}
