﻿using UnityEngine;
using System.Collections;

public class GizmoWallRenderer : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{

		}

        void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        }

	#endregion

	#region Custom

	#endregion
	
}
