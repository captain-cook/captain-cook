﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

    public float _duration;

	// Use this for initialization
	void Start () {
        GameObject.Destroy(gameObject, _duration);
	}
}
