﻿using UnityEngine;
using System.Collections;

public class FadinPanel : MonoBehaviour {

    public UISprite _sprite;
    public UILabel _text;

    private TweenAlpha m_tAlpha;
    private TweenAlpha m_tAlphaLogo;
    private TweenAlpha m_tAlphaText;


	// Use this for initialization
	void Start () {

        m_tAlpha = GetComponent<TweenAlpha>();
        m_tAlphaLogo = _sprite.GetComponent<TweenAlpha>();
        m_tAlphaText = _text.GetComponent<TweenAlpha>();

	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            Fade(true , 0 , 0 , 1 , 1);
        }
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    Fade(false , 7 , 10 , 3 , 2);
        //    SdAudioManager.Play("DemoEnd", SdAudioManager.Audioinstance.gameObject);
        //    //UpdatePanel("Sbouya !");
        //}
	}

    //launch the fading
    public void Fade(bool fadeOut , float delayFade = 0 ,  float delayLogo = 0 , float durationFade = 1 , float durationLogo = 1)
    {
        //initialisation fade panel
        m_tAlpha.delay = delayFade;
        m_tAlpha.duration = durationFade;

        //initialisation logo + text
        m_tAlphaLogo.delay = delayLogo;
        m_tAlphaText.delay = delayLogo;
        m_tAlphaLogo.duration = durationLogo;
        m_tAlphaText.duration = durationLogo;

        if (fadeOut)
        {            
            m_tAlphaLogo.PlayForward();
            m_tAlphaText.PlayForward();
            m_tAlpha.PlayForward();
        }
        else
        {
            
            m_tAlphaLogo.PlayReverse();
            m_tAlphaText.PlayReverse();
            m_tAlpha.PlayReverse();
        }
    }

    //update the logo and the text
    public void UpdatePanel(string text , string spriteName = "logo02")
    {
        _text.text = text;
        _sprite.spriteName = spriteName;
    }
}
