﻿using UnityEngine;
using System.Collections;

public class Loot : MonoBehaviour
{    
    public Transform[] _bezierPoints;
    public TweenScale _tScaleUI;

    private float m_moveSpeed = 7f;
    private bool m_isThrowing = false;
    private float m_moveValue = 0;
    private Bezier m_bezier;
    private TweenPosition m_tPosition;
    private TweenRotation m_tRotation;
    
    //return to wok
    private Bezier m_toWok;
    private Timer m_timerToWok;

	// Use this for initialization
	void Start () {

        m_tPosition = GetComponent<TweenPosition>();
        m_tRotation = GetComponent<TweenRotation>();
        _tScaleUI.transform.localScale = Vector3.zero;

        if (_bezierPoints.Length >= 3)
            m_bezier = new Bezier(_bezierPoints[0].position, _bezierPoints[1].position, _bezierPoints[2].position, _bezierPoints[3].position);

	}
	
	// Update is called once per frame
	void Update () {
	
        if (m_isThrowing)
        {
            transform.position = m_bezier.GetPosition(ref m_moveValue, Time.deltaTime, m_moveSpeed);

            if (m_moveValue >= 1 )
            {
                m_tPosition.PlayForward();
                m_tRotation.PlayForward();
                _tScaleUI.PlayForward();
                m_isThrowing = false;
            }
        }

        else if (m_timerToWok != null)
        {
            transform.position = m_toWok.GetPosition(m_timerToWok.CurrentNormalized);
            if (m_timerToWok.IsElapsedLoop == true)
            {
                m_timerToWok = null;
                //TEMPORAIRE
                Destroy(gameObject);
                //gameObject.SetActive(false);
            }
        }
	}

    public void ThrowLoot()
    {
        m_isThrowing = true;
    }

    public void Reset()
    {
        transform.position = _bezierPoints[0].position;
    }

    public void ToWok(Vector3 wokPosition, float time, float upMultiplier)
    {
        _tScaleUI.PlayReverse();
        m_toWok = new Bezier(transform.position, transform.position + (Vector3.up * upMultiplier), wokPosition + (Vector3.up * upMultiplier), wokPosition);
        m_timerToWok = new Timer(time);
    }
}
