﻿using UnityEngine;
using System.Collections;

public class ScalerController : ReferencedMonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public KinectSensor _kinectSensor;

	#endregion

	#region Private

        private float m_oldHeight;
        private float m_armLength;

        private LinearFunction m_lfHeightArmLength;

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();
            m_oldHeight = references._playerData._heigth;
            m_lfHeightArmLength = new LinearFunction(references._playerData._minHeightArmLength, references._playerData._maxHeightArmLength);
            Scale(m_oldHeight);
        }

		void Update ()
		{
            if(m_oldHeight != references._playerData._heigth)
            {
                Scale(references._playerData._heigth);
                m_oldHeight = references._playerData._heigth;
            }
		}

	#endregion

	#region Custom

        private void Scale(float newHeight)
        {
            m_armLength = m_lfHeightArmLength[newHeight];

            _kinectSensor._ratio = 1.2f / (newHeight / 183f);
        }

	#endregion
	
}
