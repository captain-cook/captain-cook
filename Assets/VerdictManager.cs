﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VerdictManager : ReferencedMonoBehaviour {

    //store the % of succes on the different step of a recipe (0 -> 1)
    private List<float> m_skillScore1;
    private List<float> m_skillScore2;
    private List<float> m_skillScore3;

    //store the entire time the player take to cook (0 -> 100)
    private float m_timingScore1 = 50;
    private float m_timingScore2 = 50;
    private float m_timingScore3 = 50;

    //store the taste score depending to the selected recipe (0 -> 100)
    private float m_choiceScore1 = 50;
    private float m_choiceScore2 = 50;
    private float m_choiceScore3 = 50;

    //store the all round score (0 -> 100)
    private float m_roundScore1 = 10;
    private float m_roundScore2 = 10;
    private float m_roundScore3 = 10;

    //store the all round score (0 -> 100)
    private float m_mamaScore1 = 54;
    private float m_mamaScore2 = 98;
    private float m_mamaScore3 = 48;   

	// Use this for initialization
	void Start () {

        base.LoadReferences();

        m_skillScore1 = new List<float>();
        m_skillScore2 = new List<float>();
        m_skillScore3 = new List<float>();
	}

    void Update ()
    {
        /*if (Input.GetKeyUp(KeyCode.Space))
        {
            //references._verdictPanel.gameObject.SetActive(true);
            //references._verdictPanel.DisplayPanel();
            references._roundPanel.DisplayPanel();
        }*/
    }
	
    // add score in the proper score list
    public void AddScore(float score)
    {
        switch ( references._binomialManager._numPhase)
        {
            case 1 :
                m_skillScore1.Add(score);
                break;
            case 2:
                m_skillScore2.Add(score);
                break;
            case 3:
                m_skillScore3.Add(score);
                break;
        }
    }

    //set the timing score 
    public void SetTimingScore(float timeScore)
    {
        float time = timeScore;
        if (timeScore < 0)
            time = 0;

        switch (references._binomialManager._numPhase)
        {
            case 1:
                m_timingScore1 = time;
                break;
            case 2:
                m_timingScore2 = time;
                break;
            case 3:
                m_timingScore3 = time;
                break;
        }
    }

    //set the taste score 
    public void SetChoiceScore(float taste)
    {
        switch (references._binomialManager._numPhase)
        {
            case 1:
                m_choiceScore1 = taste;
                break;
            case 2:
                m_choiceScore2 = taste;
                break;
            case 3:
                m_choiceScore3 = taste;
                break;
        }
    }

    public void SetRoundScore()
    {
        float recipeScore = 0;
        float roundScore = 0;

        switch (references._binomialManager._numPhase)
        {
            case 1:
                // recipe Score average
                foreach (float score in m_skillScore1)
                {
                    recipeScore += score;
                }
                roundScore += recipeScore * 100 / m_skillScore1.Count;
                roundScore += m_choiceScore1;
                roundScore += m_timingScore1;
                m_roundScore1 = roundScore / 3;
                
                break;
            case 2:
                // recipe Score average
                foreach (float score in m_skillScore2)
                {
                    recipeScore += score;
                }
                roundScore += recipeScore * 100 / m_skillScore2.Count;
                roundScore += m_choiceScore2;
                roundScore += m_timingScore2;
                m_roundScore2 = roundScore / 3;
                
                break;
            case 3:
                // recipe Score average
                foreach (float score in m_skillScore3)
                {
                    recipeScore += score;
                }
                roundScore += recipeScore * 100 / m_skillScore3.Count;
                roundScore += m_choiceScore3;
                roundScore += m_timingScore3;
                m_roundScore3 = roundScore / 3;
                
                
                break;
        }
    }
	
    //return true if the player win the round 
    public bool IsPlayerWonTheRound(int numRound = 0)
    {
        int round;
        if (numRound != 0)
            round = numRound;
        else
            round = references._binomialManager._numPhase;

        switch (round)
        {
            case 1:
                return m_roundScore1 > m_mamaScore1;
            case 2:
                return m_roundScore2 > m_mamaScore2;
            case 3:
                return m_roundScore3 > m_mamaScore3;
        }

        return false;
    }

    //return the number of round won by the player
    public int GetNbRoundsWin()
    {
        int result = 0;

        if (m_roundScore1 > m_mamaScore1)
            result++;
        if (m_roundScore2 > m_mamaScore2)
            result++;
        if (m_roundScore3 > m_mamaScore3)
            result++;

        return result;
    }

    //return the player round score depending on the round he wants or of the round he's on
    public float GetRoundScore(int numRound = 0)
    {
        int round;
        if (numRound != 0)
            round = numRound;
        else
            round = references._binomialManager._numPhase;

        switch (round)
        {
            case 1:
                return m_roundScore1;
            case 2:
                return m_roundScore2;
            case 3:
                return m_roundScore3;
        }

        return 0;
    }

    //return the mama round score depending on the round he wants or of the round he's on
    public float GetMamaScore(int numRound = 0)
    {
        int round;
        if (numRound != 0)
            round = numRound;
        else
            round = references._binomialManager._numPhase;

        switch (round)
        {
            case 1:
                return m_mamaScore1;
            case 2:
                return m_mamaScore2;
            case 3:
                return m_mamaScore3;
        }

        return 0;
    }

    //return the timing score depending on the numround
    public float GetTimingScore(int numRound)
    {
        switch (numRound)
        {
            case 1:
                return m_timingScore1;
            case 2:
                return m_timingScore2;
            case 3:
                return m_timingScore3;
        }

        return -1f;
    }

    //return the choices score depending on the numround
    public float GetChoicesScore(int numRound)
    {
        switch (numRound)
        {
            case 1:
                return m_choiceScore1;
            case 2:
                return m_choiceScore2;
            case 3:
                return m_choiceScore3;
        }

        return -1f;
    }

    //return the skill score depending on the numround
    public float GetSkillScore(int numRound)
    {
        float result = 0;
        switch (numRound)
        {
            case 1:
                foreach (float score in m_skillScore1)
                {
                    result += score;
                }
                return (result * 100 / m_skillScore1.Count);
            case 2:
                foreach (float score in m_skillScore2)
                {
                    result += score;
                }
                return (result * 100 / m_skillScore2.Count);
            case 3:
                foreach (float score in m_skillScore3)
                {
                    result += score;
                }
                return (result * 100 / m_skillScore3.Count);
        }

        return -1f;
    }
    
    public string GetScoreToString()
    {
        string result = "";
        for (int i = 1 ; i <= 3 ; i++)
        {
            if (IsPlayerWonTheRound(i))
            {
                result += "_Success";
            }
            else
            {
                result += "_Fail";
            }
        }

        return result;
    }
}
