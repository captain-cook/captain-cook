﻿using UnityEngine;
using System.Collections;

public class LootManager : ReferencedMonoBehaviour {

    public Loot[] _loots;
    [HideInInspector]
    public bool _isReadyToPick = false;
    private bool m_hasBeenPicked = false;
	// Use this for initialization
	void Start () {

        base.LoadReferences();
        gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {
	
        if (_isReadyToPick && !m_hasBeenPicked &&
         references._toWokCollider.bounds.Contains(references._phaseManager._actualPhase._customHandControllerLeft.transform.position) == true &&
         references._toWokCollider.bounds.Contains(references._phaseManager._actualPhase._customHandControllerRight.transform.position) == true 
           )
        {
            m_hasBeenPicked = true;
            StartCoroutine("RecupLoot");
        }
	}

    IEnumerator RecupLoot()
    {
        _loots[0].ToWok(references._wok.position, 1, Random.Range(0.3f, 0.5f));
        yield return new WaitForSeconds(0.5f);
        SdAudioManager.Trigger("UI_Loot_Gold");
        _loots[1].ToWok(references._wok.position, 1, Random.Range(0.3f, 0.5f));
        yield return new WaitForSeconds(0.5f);
        SdAudioManager.Trigger("UI_Loot_Knife");
        _loots[2].ToWok(references._wok.position, 1, Random.Range(0.3f, 0.5f));
        yield return new WaitForSeconds(0.5f);
        SdAudioManager.Trigger("UI_Loot_Recipe");
    }

    public void ThrowLoots()
    {
        
        //initialize positions
        foreach (Loot loot in _loots)
        {
            loot.Reset();
        }
        gameObject.SetActive(true);
        Debug.Log("TrhowLoot");
        StartCoroutine("LauncherLoot");
        
    }

    IEnumerator LauncherLoot()
    {
        _loots[0].ThrowLoot();
        yield return new WaitForSeconds(0.5f);
        StartCoroutine("PlayLootAppearSound");
        _loots[1].ThrowLoot();
        yield return new WaitForSeconds(0.5f);
        StartCoroutine("PlayLootAppearSound");
        _loots[2].ThrowLoot();
        yield return new WaitForSeconds(0.5f);
        StartCoroutine("PlayLootAppearSound");
    }

    public void LootIsReady()
    {
        Debug.Log("ready");
        _isReadyToPick = true;
    }
    IEnumerator PlayLootAppearSound()
    {
        yield return new WaitForSeconds(0.8f);
        SdAudioManager.Trigger("UI_Loot_Appearing");
    }
}
