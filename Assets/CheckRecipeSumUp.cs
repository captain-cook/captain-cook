﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckRecipeSumUp : MonoBehaviour {

    private float m_duration = 1;
    private bool m_isPlaying = false;
    private float m_alphaLevel = 0;
    private float m_startScale = 4;

    private TweenScale m_tScale;
    private Image m_image;



	// Use this for initialization
	void Awake ()
    {
        m_image = GetComponent<Image>();
        m_tScale = GetComponent<TweenScale>();

        m_tScale.duration = m_duration;
        m_tScale.from = new Vector3(m_startScale, m_startScale, m_startScale);
	}
	
	// Update is called once per frame
	void Update () {
	
        //fade the check
        if (m_isPlaying)
        {
            m_alphaLevel += Time.deltaTime * m_duration;
            m_image.color = new Color(1, 1, 1, m_alphaLevel);

            //transition is over
            if (m_alphaLevel >= m_duration)
            {
                m_isPlaying = false;
            }
        }

	}

    public void Reset()
    {
        m_isPlaying = false;
        m_tScale.ResetToBeginning();
        m_image.color = new Color(1, 1, 1, 0);
        m_alphaLevel = 0;
    }

    public void Play()
    {
        m_image.enabled = true;
        m_isPlaying = true;
        m_tScale.PlayForward();
    }
}
