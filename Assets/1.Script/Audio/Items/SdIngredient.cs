﻿using UnityEngine;
using System.Collections;

public class SdIngredient : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        switch (action)
        {
            case "Fall":
                SdAudioManager.Play("IngredientFall", source);
                break;
            case "PieFall":
                SdAudioManager.Play("PieFall", source);
                break;
            case "MoreyFall":
                SdAudioManager.Play("MoreyFall", source);
                break;
            case"EggBreak":
                SdAudioManager.SetSwitch("Egg", "Break", source);
                SdAudioManager.Play("Egg", source);
                break;
            case "EggFall":
                SdAudioManager.SetSwitch("Egg", "Fall", source);
                SdAudioManager.Play("Egg", source);
                break;
            case "Grill":
                SdAudioManager.SetSwitch("Plancha", "GrillPut", source);
                SdAudioManager.Play("Plancha", source);
                break;
            case "Explosion":
                SdAudioManager.Play("IngrExplosion", source);
                break;
            default:
                break;
        }    
    }
}


