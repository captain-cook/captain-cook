﻿using UnityEngine;
using System.Collections;

public class SdBoat : iSd
{
    public virtual void Execute(string action, GameObject source = null) 
    {
        switch (action)
        {
            case "Sail":
                SdAudioManager.Play("Boat_Sail", source);
                break;
            case "Anchor":
                SdAudioManager.Play("Boat_Anchor", source);
                break;
            case "BalistaOn":
                SdAudioManager.Play("Balista", source);
                break;
            case "DoorOpen":
            case "CannonOpen":
            case "Rattle":
            case "Deploy":
                SdAudioManager.SetSwitch("SFXBoat", action, source);
                SdAudioManager.Play("SFXBoat", source);
                break;
            default:
                break;
        }
    }
    public virtual void SetParameter(string parameter, float value, GameObject source = null) { }

    public static void Sail()
    {
        Debug.Log("pickup");
    }
    public static void Moor()
    {
        Debug.Log("drop");
    }
}


