﻿using UnityEngine;
using System.Collections;

public class SOTaste : ScriptableObject {

    public TasteType _tasteType;
    public enum TasteType
    {
        None,
        Seasoning,
        Spice,
        Temperature,
        Acidity,
        Nature,
        Fatness,
        Smell,
        Texture,
        MealType,
        Ingredient
    }


    public string _value;
    public Sprite _image;

    //return -12.5 if dislike
    //return 0 if neutral
    //return 12.5 if like
    public float Compare(SOTaste taste)
    {
        if (taste._tasteType == _tasteType)
        {
            if (taste._value == _value)
            {
                return 12.5f;
            }
            else 
            {
                return -12.5f;
            }
        }

        return 0;
    }
}
