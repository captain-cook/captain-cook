﻿using UnityEngine;
using System.Collections;

public class MoveOnBezier : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Bezier _bezier;
        public float _speed = 1;

        public HandControllerDistribution _hand;

	#endregion

	#region Private

        private bool m_started = false;
        private float m_level = 0;

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{
            if(m_started == true)
            {
                transform.position = _bezier.GetPosition(ref m_level, Time.deltaTime, _speed);
                if(m_level >= 1)
                {
                    m_started = false;
                    _hand._itemToThrow = gameObject;
                }
            }
		}

	#endregion

	#region Custom

        public void StartMoving(bool forceResetPosition = false)
        {
            if(forceResetPosition == true)
            {
                m_level = 0;
            }
            m_started = true;
        }

        public void StopMoving()
        {
            m_started = false;
        }

	#endregion
	
}
