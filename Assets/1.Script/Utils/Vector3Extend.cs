﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector3Extend
{
    public static Vector3 Divide(this Vector3 source, Vector3 div)
    {
        Vector3 res = source;
        res.x /= div.x;
        res.y /= div.y;
        res.z /= div.z;
        return res;
    }

    public static float Volume(this Vector3 source)
    {
        return source.x * source.y * source.y;
    }

    public static float Average(this Vector3 source)
    {
        return (source.x + source.y + source.z) / 3f;
    }
}
