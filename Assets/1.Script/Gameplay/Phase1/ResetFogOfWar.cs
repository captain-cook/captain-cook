﻿using UnityEngine;
using System.Collections;

public class ResetFogOfWar : MonoBehaviour {

    public GameObject _resetFog;
    int _nbFrame = 0;

	
	
	// Update is called once per frame
	void Update () {

        
        if (_nbFrame < 5)
        {
            _nbFrame++;               
        }
        else
        {
            _resetFog.SetActive(false);
            Destroy(this);
        }

	}
}
