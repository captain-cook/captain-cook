﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class HandControllerShop: CustomFixedHandController
{
    [HideInInspector]
    public Vector2 _screenPosition;
    [HideInInspector]
    public HandController _handController;

    private Camera m_handCamera;

    void Awake()
    {
        base.LoadReferences();
        _handController = GetComponent<HandController>();
        m_handCamera = references._handCamera;
    }

    public override void Update()
    {
         // get the coordinate of the hand on the screen
        _screenPosition = m_handCamera.WorldToScreenPoint(transform.position);

        //check if the hand is over the knife
        Ray ray = Camera.main.ScreenPointToRay(_screenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100) && hit.collider.tag == "ShopItem")
        {
            references._shopPanel.HandOnHover();
            //select the item
            if (_handController.IsClosingHand())
            {
                references._shopPanel.BuyItem();
            }
        }
    }
    
}