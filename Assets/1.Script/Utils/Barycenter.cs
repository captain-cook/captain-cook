﻿using UnityEngine;
using System.Collections;

public class Barycenter : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Vector3 _barycenter;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
            Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
            _barycenter = Vector3.zero;
            for (int i = 0; i < mesh.vertices.Length; i++)
            {
                _barycenter += mesh.vertices[i];
            }
            _barycenter /= mesh.vertices.Length;
            Vector4 v = _barycenter;
            v.w = 1;
            renderer.materials[renderer.materials.Length - 1].SetVector("_Barycenter", v);
		}

		void Update ()
		{

		}

        void OnDrawGizmos()
        {
            /*Gizmos.color = Color.red;
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawSphere(_barycenter, 0.1f);*/
        }

	#endregion

	#region Custom

	#endregion
	
}
