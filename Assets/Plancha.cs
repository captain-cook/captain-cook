﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plancha : ReferencedMonoBehaviour {

    private List<BakableIngredient> m_ingredientOnThePlancha;

    public float _foodToWokDuration = 1f;
    public Timer _foodToWokTimer;

    private bool m_isSoundRunning = false;

	// Use this for initialization
	void Start () {

        base.LoadReferences();
        m_ingredientOnThePlancha = new List<BakableIngredient>();

	}

    void Update()
    {
        
        if (references._toWokCollider.bounds.Contains(references._phaseManager._actualPhase._customHandControllerLeft.transform.position) == true &&
          references._toWokCollider.bounds.Contains(references._phaseManager._actualPhase._customHandControllerRight.transform.position) == true 
            && m_ingredientOnThePlancha.Count > 0)
        {
            ResetPlancha();
            //SdAudioManager.Trigger("CookBoard_WokFallStart", gameObject);
            foreach (BakableIngredient ing in m_ingredientOnThePlancha)
            {
                if (!ing._isFinish)
                {
                    ing.GetComponent<Ingredient>().ToWok(references._wok.position, _foodToWokDuration, Random.Range(0.3f, 0.5f));
                    ing._isFinish = true;
                    StartCoroutine(PlaySoundPlanchaWok());
                    //ResetSound();
                    ing.SetBakingScore();
                    references._recipeManager.ForceStepValidation();

                    Destroy(ing._currentBubblesFx);

                    Vibrator[] vibrators = ing.transform.GetComponentsInChildren<Vibrator>();
                    for (int i = 0; i < vibrators.Length; i++)
                    {
                        vibrators[i].enabled = false;
                    }

                    references._planchaButton.animation.Stop();
                    references._planchaButton.animation["ButtonPlancha"].time = 0;
                }  
            }
            m_ingredientOnThePlancha.Clear();
        }
    }

    IEnumerator PlaySoundPlanchaWok()
    {
        yield return new WaitForSeconds(0.4f);
        SdAudioManager.Trigger("CookBoard_WokFallEgg", references._wok.gameObject);
    }
	
	public void AddIngredientOnPlancha(BakableIngredient ingredient)
    {
        if (!m_ingredientOnThePlancha.Contains(ingredient))
            m_ingredientOnThePlancha.Add(ingredient);
    }

    public void RemoveIngredientOnPlancha(BakableIngredient ingredient)
    {
        m_ingredientOnThePlancha.Remove(ingredient);
    }

    public void Bump()
    {
        foreach (BakableIngredient  ing in m_ingredientOnThePlancha)
        {
            ing.Bump();

            SdAudioManager.Trigger("CookBoard_PlanchaBump", gameObject);
        }
    }

    public void ActivateSound()
    {
        if (m_ingredientOnThePlancha.Count > 0 && !m_isSoundRunning)
        {
            m_isSoundRunning = true;
            SdAudioManager.Trigger("CookBoard_PlanchaGrillStart", gameObject);
            //SdAudioManager.Trigger("CookBoard_PlanchaWhistleStart", gameObject);
            
        }        
    }

    public void ResetPlancha()
    {
        
        SdAudioManager.Trigger("CookBoard_PlanchaGrillStop", gameObject);
        references._planchaButton.animation.Stop();
        references._planchaButton.animation["ButtonPlancha"].time = 0;
    }
    
    public void UpdateWhistle(float level)
    {
       
        //SdAudioManager.SetParameter("CookBoard_GrillStatus",level, gameObject);
    }
   
}
