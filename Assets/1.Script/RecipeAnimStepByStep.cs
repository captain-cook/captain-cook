﻿using UnityEngine;
using System.Collections;

public class RecipeAnimStepByStep : ReferencedMonoBehaviour {

    void Start()
    {
        base.LoadReferences();
    }

    //function called in the animtion event system
	public void IncStep(int numStep)
    {
        references._recipeSumUp.DisplayCardStepByStep(numStep);
    }

}
