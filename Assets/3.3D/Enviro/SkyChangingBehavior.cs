﻿using UnityEngine;
using System.Collections;

public class SkyChangingBehavior : MonoBehaviour
{
		public Material OceanMatPicker;
		public GameObject StormParticles;
		public Color fogColor = Color.white;
		public float fogEndDistance = 300f;
		public Color ambientColor = Color.white;
		private Color fogColor_Clear = Color.white;
		public Color OceanColor_Clear = Color.white;
		private float fogEndDistance_Clear = 300f;
		private Color ambientColor_Clear = Color.white;
		public Color fogColor_Foggy = Color.white;
		public Color OceanColor_Foggy = Color.white;
		public float fogEndDistance_Foggy = 150f;
		public Color ambientColor_Foggy = Color.white;
		public Color fogColor_Stormy = Color.white;
		public Color OceanColor_Stormy = Color.white;
		public float fogEndDistance_Stormy = 250f;
		public Color ambientColor_Stormy = Color.white;
		private float speed = 1f;
		private float fogPercent = 0f;
		private float stormPercent = 0f;
		public bool skyIsFog = false;
		public bool skyIsStorm = false;

		// Use this for initialization
		void Start ()
		{
				fogColor = RenderSettings.fogColor;
				ambientColor = RenderSettings.ambientLight;
				fogEndDistance = RenderSettings.fogEndDistance;

				fogColor_Clear = RenderSettings.fogColor;
				OceanMatPicker.color = OceanColor_Clear;
				ambientColor_Clear = RenderSettings.ambientLight;
				fogEndDistance_Clear = RenderSettings.fogEndDistance;


		}
	
		// Update is called once per frame
		void Update ()
		{

				if (skyIsFog) {
					
						fogColor = Color.Lerp (fogColor, fogColor_Foggy, speed * Time.deltaTime);
						ambientColor = Color.Lerp (ambientColor, ambientColor_Foggy, speed * Time.deltaTime);
						fogEndDistance = Mathf.Lerp (fogEndDistance, fogEndDistance_Foggy, speed * Time.deltaTime);
						fogPercent = Mathf.Lerp (fogPercent, 1f, speed * Time.deltaTime);
						stormPercent = Mathf.Lerp (stormPercent, 0f, speed * Time.deltaTime);

						Transform[] allChildren = this.GetComponentsInChildren<Transform> ();
						foreach (Transform child in gameObject.transform) {
								if (child.renderer != null) {
										child.renderer.material.SetFloat ("_Fog", fogPercent);
								}
						}

						OceanMatPicker.color = Color.Lerp (OceanMatPicker.color, OceanColor_Foggy, speed * Time.deltaTime);

				} else if (skyIsStorm) {
						fogColor = Color.Lerp (fogColor, fogColor_Stormy, speed * Time.deltaTime);
						ambientColor = Color.Lerp (ambientColor, ambientColor_Stormy, speed * Time.deltaTime);
						fogEndDistance = Mathf.Lerp (fogEndDistance, fogEndDistance_Stormy, speed * Time.deltaTime);
						stormPercent = Mathf.Lerp (stormPercent, 1f, speed * Time.deltaTime);fogPercent = Mathf.Lerp (fogPercent, 0f, speed * Time.deltaTime);
						fogPercent = Mathf.Lerp (fogPercent, 0f, speed * Time.deltaTime);

						Transform[] allChildren = this.GetComponentsInChildren<Transform> ();
						foreach (Transform child in gameObject.transform) {
								if (child.renderer != null) {
										child.renderer.material.SetFloat ("_Storm", stormPercent);
								}
						}
						OceanMatPicker.color = Color.Lerp (OceanMatPicker.color, OceanColor_Stormy, speed * Time.deltaTime);
						


				} else {
						
						fogColor = Color.Lerp (fogColor, fogColor_Clear, speed * Time.deltaTime);
						ambientColor = Color.Lerp (ambientColor, ambientColor_Clear, speed * Time.deltaTime);
						fogEndDistance = Mathf.Lerp (fogEndDistance, fogEndDistance_Clear, speed * Time.deltaTime);
						fogPercent = Mathf.Lerp (fogPercent, 0f, speed * Time.deltaTime);
						stormPercent = Mathf.Lerp (stormPercent, 0f, speed * Time.deltaTime);

						Transform[] allChildren = this.GetComponentsInChildren<Transform> ();
						foreach (Transform child in gameObject.transform) {
								if (child.renderer != null) {
										child.renderer.material.SetFloat ("_Fog", fogPercent);
										child.renderer.material.SetFloat ("_Storm", stormPercent);
								}
						}
					
						OceanMatPicker.color = Color.Lerp (OceanMatPicker.color, OceanColor_Clear, speed * Time.deltaTime);
				}

				RenderSettings.fogColor = fogColor;
				RenderSettings.fogEndDistance = fogEndDistance;
				RenderSettings.ambientLight = ambientColor;

				OceanMatPicker.SetFloat("_WaterTexLerpLevel", stormPercent);
				OceanMatPicker.SetFloat("_HeightMapLerpLevel", stormPercent);
				OceanMatPicker.SetFloat("_HeightFactor", (1.5f+(stormPercent*6f)));


				if (stormPercent > 0.8f) {
						StormParticles.active = true;
				} else {
						StormParticles.active = false;
				}



		}
}
