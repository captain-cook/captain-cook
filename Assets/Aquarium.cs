﻿using UnityEngine;
using System.Collections;

public class Aquarium : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //hand is near to a fruit 
    void OnTriggerEnter(Collider other)
    {

        if (other.collider.tag == "Cutable" || other.collider.tag == "Ingredient")
        {
            Debug.Log("Enter in the aquarium");
        }
    }

    //hand is far from a fruit 
    void OnTriggerExit(Collider other)
    {
        if (other.collider.tag == "Cutable" || other.collider.tag == "Ingredient")
        {
            Debug.Log("Exit BOcal");
        }
    }
}
