﻿using UnityEngine;
using System.Collections;

public class SdFootsteps : iSd
{
    public virtual void Execute(string action, GameObject source = null) 
    {
        switch (action)
        {
            case "Start":
                
            case "Foot":
            case "End":
            case "Fall":
                SdAudioManager.SetSwitch("Footsteps", action, source);
                SdAudioManager.Play("Footsteps", source);
                break;
            default:
                break;
        }
    }
    public virtual void SetParameter(string parameter, float value, GameObject source = null) { }

    public static void Walk(string action, GameObject source = null)
    {
        SdAudioManager.SetSwitch("Footsteps", action, source);
        if (source.tag == "Player")
        {
            SdAudioManager.Play("FootstepsPlayer", source);
        }
        else 
        {
            SdAudioManager.Play("Footsteps", source);
        }
    }
}


