﻿using UnityEngine;
using System.Collections;

public class Tool : ReferencedMonoBehaviour {

    public bool _couldBeReleased = true;
    public bool _couldBeGrabbed = true;

    public bool _inSlot = true;

    public GameObject _slot;
    public GameObject _slotPreposition;
    public Vector3 _orientationInSlot;
    public float _timeToPreposition = 1f;
    public float _timeToFinal = 0.3f;

    private Vector3 m_positionRelease;
    private Vector3 m_orientationRelease;

    private Vector3 m_rotationDestination;

    private Timer m_timerStepPreposition;
    private Timer m_timerStepFinal;

    public AnimationCurve _curvePreposition;
    public AnimationCurve _curveBack;

	void Start () {
        m_timerStepPreposition = null;
        base.LoadReferences();
	}
	
	void Update () {
        //Preposition of the tool
        if (m_timerStepPreposition != null)
        {
            transform.position = Vector3.Lerp(m_positionRelease, _slotPreposition.transform.position, _curvePreposition.Evaluate(m_timerStepPreposition.CurrentNormalized));
            transform.localRotation = Quaternion.Euler(Vector3.Lerp(m_orientationRelease, m_rotationDestination, _curvePreposition.Evaluate(m_timerStepPreposition.CurrentNormalized)));
            if(m_timerStepPreposition.IsElapsedOnce == true)
            {
                m_timerStepPreposition = null;
                m_positionRelease = transform.position;

                //If it's a knife
                if (GetComponentInChildren<BladeSpeedController>() != null)
                {
                    SdAudioManager.Trigger("Knife_Drop", gameObject);
                }
                if (gameObject.name == "rouleau_patisserie")
                {
                    SdAudioManager.SetParameter("Roller_RollSpeed", 0f, gameObject);
                    SdAudioManager.Trigger("Roller_Drop", gameObject);
                }
                m_timerStepFinal = new Timer(_timeToFinal);
            }
        }

        //Final back of the tool (super anglais mon petit pote)
        if(m_timerStepFinal != null)
        {
            transform.position = Vector3.Lerp(m_positionRelease, _slot.transform.position, _curveBack.Evaluate(m_timerStepFinal.CurrentNormalized));
            if (m_timerStepFinal.IsElapsedOnce == true)
            {
                m_timerStepFinal = null;
                _inSlot = true;
            }
        }
	}

    public void ReturnToSlot()
    {
        m_timerStepPreposition = new Timer(_timeToPreposition);
        m_positionRelease = transform.position;
        m_orientationRelease = transform.localRotation.eulerAngles;

        m_rotationDestination = _orientationInSlot;
        if (m_rotationDestination.x - m_orientationRelease.x > 180)
        {
            m_rotationDestination.x -= 360;
        }
        else if (m_rotationDestination.x - m_orientationRelease.x < -180)
        {
            m_rotationDestination.x += 360;
        }

        if (m_rotationDestination.y - m_orientationRelease.y > 180)
        {
            m_rotationDestination.y -= 360;
        }
        else if (m_rotationDestination.y - m_orientationRelease.y < -180)
        {
            m_rotationDestination.y += 360;
        }

        if (m_rotationDestination.z - m_orientationRelease.z > 180)
        {
            m_rotationDestination.z -= 360;
        }
        else if (m_rotationDestination.z - m_orientationRelease.z < -180)
        {
            m_rotationDestination.z += 360;
        }
    }

}
