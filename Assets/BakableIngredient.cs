﻿using UnityEngine;
using System.Collections;

public class BakableIngredient : ReferencedMonoBehaviour {


    private HandControllerBaking m_hand;
    private ParticleSystem m_smoke;
    [HideInInspector]
    public bool _isGrabbed = false;
    private Highlighter m_highlighter;

    public bool _isToastable;
    public bool _isBakable;

    //plancha variables    
    public float _bumpingOffset = 0.1f;
    [HideInInspector]
    public bool _isToasting = false;
    [HideInInspector]
    public bool _isFinish = false;
    public GameObject _fxBubblesPlancha;
    private bool m_isToasted1 = false;
    private bool m_isToasted2 = false;
    private bool m_isFirstSide = true;
    private bool m_isInTheAir = false;
    private float m_toastLevel1 = 0;
    private float m_toastLevel2 = 0;
    private float m_toastTimer = 6;
    private TweenPosition m_tPosition;
    private TweenRotation m_tRotation;

    public GameObject _currentBubblesFx;


    //Oven variables
    [HideInInspector]
    public bool _isBaking = false;
    [HideInInspector]
    public bool _isOnTheTrail = false;
    [HideInInspector]
    public bool _isInTheOven = false;
    private bool m_isBaked = false;
    private bool m_isBurnt = false;
    private float m_bakingLevel = 0;
    private float m_bakeTimer = 6;
    [HideInInspector]
    public Vector3 _handPositionStart;
    [HideInInspector]
    public Vector3 _trailOven;
    [HideInInspector]
    public Vector3 _startTrail;

    //public Color color;

    private bool m_soundOvenBurn;

	// Use this for initialization
	void Start () {
        base.LoadReferences();
        m_tPosition = GetComponent<TweenPosition>();
        m_tRotation = GetComponent<TweenRotation>();
        m_tRotation.duration = m_tPosition.duration * 2;

        m_smoke = GetComponentInChildren<ParticleSystem>();
        m_smoke.Stop();

        m_highlighter = GetComponent<Highlighter>();
	}
		
    void Update()
    {
        /*if (Input.GetKeyUp(KeyCode.Space))
        {
            if(_isToasting == true)
            {
                Bump();
            }
        }*/

        if((_isToastable == true && _isGrabbed == false && _isToasting == false) || (_isBakable == true && (_isOnTheTrail == false || _isBaking == false)))
        {
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        }
    }

    //make the ingredient bumping on the plancha
    void StartBump()
    {
        if (_isToasting)
        {
            m_tPosition.from = transform.position + new Vector3(0, 0.005f, 0);
            m_tPosition.to = m_tPosition.from + new Vector3(0, _bumpingOffset, 0);
            m_tPosition.PlayForward();
        }
    }

    public void Bump()
    {
        if (!m_isInTheAir )
        {
            m_isFirstSide = !m_isFirstSide;
            m_smoke.Stop();

            m_isInTheAir = true;
            m_tPosition.from = transform.position + new Vector3(0, 0.0f, 0);
            m_tPosition.to = m_tPosition.from + new Vector3(0, 0.3f, 0);
            m_tPosition.PlayForward();

                   
            m_tRotation.from = transform.eulerAngles;            
            m_tRotation.to = m_tRotation.from + new Vector3(0, 0, 180);
            m_tRotation.ResetToBeginning();
            m_tRotation.PlayForward();

            _currentBubblesFx.gameObject.SetActive(false);

            Vibrator[] vibrators = transform.GetComponentsInChildren<Vibrator>();
            for (int i = 0; i < vibrators.Length; i++)
            {
                vibrators[i].enabled = false;
            }

            references._planchaButton.animation.Stop();
            references._planchaButton.animation["ButtonPlancha"].time = 0;
        }
    }

    public void ReturnOfBump()
    {
        //Moving
        if (m_isInTheAir)
        {
            m_isInTheAir = false;
            m_tPosition.PlayReverse();            
        }       
        //Movement is finish
        else
        {
            if(_currentBubblesFx != null)
            {
                _currentBubblesFx.gameObject.SetActive(true);
            }
            Vibrator[] vibrators = transform.GetComponentsInChildren<Vibrator>();
            for (int i = 0; i < vibrators.Length; i++)
            {
                vibrators[i].enabled = true;
            }

            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;

            if ((m_isFirstSide && m_toastLevel1 >= m_toastTimer) 
                || (!m_isFirstSide && m_toastLevel2 >= m_toastTimer))
            {
                SetSmokeColor();
                 m_smoke.Play();
            }
        }
    }

    public void SetHand(HandControllerBaking hand)
    {
        m_hand = hand;
        _isBaking = false;
        if(hand != null)
        {
            if (_isToastable == true)
            {
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_SelectDishPlancha", references._fightingTable._pirateLeft.gameObject);
            }
            else if (_isBakable == true)
            {
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_SelectDishOven", references._fightingTable._pirateLeft.gameObject);
            }
        }
    }

    //enter in the oven area
    void EnterInTheOven()
    {
        
        _isOnTheTrail = true;
        _handPositionStart = m_hand.transform.position;

        //rotate the ingredient to face the oven
        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;

        Vector3 rotation = new Vector3(0, references._oven.eulerAngles.y - 90, 0);
        transform.eulerAngles = rotation;

        _startTrail = references._oven.transform.position + references._oven.right
            * references._oven.GetComponent<BoxCollider>().size.x * 0.5f
            * references._oven.localScale.x;

        transform.position = _startTrail;

    }

    public void UpdatePositionInOven(Vector3 handPosition)
    {
        //set the position 
        Vector3 handVector = handPosition - _startTrail;
        float distanceFromStart = Vector3.Dot(- references._oven.transform.right , handVector);               

        //is out of the trail
        if (distanceFromStart < -0.05f && m_bakingLevel <= 0)
        {
            _isOnTheTrail = false;
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
            m_highlighter.UnHighlight();
        }
        //baking is finish
        else if (distanceFromStart < -0.05f &&  m_bakingLevel >= 0 && !_isFinish)
        {
            _isFinish = true;
            _isOnTheTrail = false;
            m_hand.ReleaseGrabbedObject();
            GetComponent<Ingredient>().ToWok(references._wok.position, 1, Random.Range(0.3f, 0.5f));

            Vibrator[] vibrators = transform.GetComponentsInChildren<Vibrator>();
            for (int i = 0; i < vibrators.Length; i++)
            {
                vibrators[i].enabled = false;
            }

            StartCoroutine(PlaySoundOvenWok());
            SetBakingScore();
            references._recipeManager.ForceStepValidation();
            ComputeOvenSelfIllumin(0);
            if(m_isBurnt == false)
            {
                SdAudioManager.Trigger("CookBoard_OvenTurnOff", references._oven.gameObject);
            }
            SdAudioManager.Trigger("CookBoard_PlanchaWhistleStop", references._oven.gameObject);
        }
        //is on the oven
        else if (distanceFromStart >= (_startTrail - references._oven.transform.position).magnitude && !_isBaking)
        {
            _isInTheOven = true;
        }
        //move on the trail
        else
        {
            _isInTheOven = false;
            float ratio = distanceFromStart / (_startTrail - references._oven.transform.position).magnitude;
            transform.position = _startTrail - references._oven.transform.right * distanceFromStart;

            if (!_isInTheOven)
                m_highlighter._color = Color.Lerp(Color.white, Color.green, ratio);
            else
                m_highlighter._color = Color.Lerp(Color.green , Color.white, ratio);

            m_highlighter.Highlight();

            if (m_smoke.isPlaying)
                m_smoke.Stop();
        }
    }

    IEnumerator PlaySoundOvenWok()
    {
        yield return new WaitForSeconds(0.4f);
        SdAudioManager.Trigger("CookBoard_WokFallEgg", references._wok.gameObject);
    }

    public void StartBaking()
    {        
        _isBaking = true;
        m_highlighter.UnHighlight();
        SdAudioManager.Trigger("CookBoard_OvenTurnOn", references._oven.gameObject);
        SdAudioManager.Trigger("CookBoard_OvenTickTack", references._oven.gameObject);
        SdAudioManager.Trigger("CookBoard_OvenIdle", references._oven.gameObject);

        ParticleSystem[] part = references._oven.Find("FX_OvenTurnOn").GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < part.Length; i++)
		{
            part[i].Play();
		}
    }

    //the player was to lame... 
    void FailedLooser()
    {

    }

    //set the proper color depending on the side
    void SetSmokeColor()
    {
        float color = 0;
        if (_isToasting)
        {
            if (m_isFirstSide)
            {
                color = 1 - (m_toastLevel1 - m_toastTimer) / (2 * m_toastTimer);
            }
            else
            {
                color = 1 - (m_toastLevel2 - m_toastTimer) / (2 * m_toastTimer);
                //renderer.materials[1].SetFloat("_Ratio", Mathf.Clamp(m_toastLevel2 / m_toastTimer, 0f, 2f));
            }

            references._plancha.UpdateWhistle( 1 - color);
        }
        else if (_isBaking)
        {
            color = 1 - (m_bakingLevel - m_bakeTimer) / (2 * m_bakeTimer);   
        }

        m_smoke.startColor = new Color(color, color, color, 0.7f);
        
    }

    //set some parameter to difference toasting smoke and baking smoke
    void SetSmoke()
    {
        if (_isToasting)
        {
            m_smoke.startSpeed = 0;
        }
        else if (_isBaking)
        {
            //m_smoke.startSpeed = 1f;
        }
    }

    // check the baking level to set the score
    public void SetBakingScore()
    {
        float score = 0;

        //BAKING
        if (references._recipeManager._currentStep._action == SORecipe.Interaction.Bake)
        {
            // not fully baked
            if (m_bakingLevel < m_bakeTimer)
            {
                references._verdictManager.AddScore(m_bakingLevel / m_bakeTimer);
            }
            // 100%
            else if (m_bakingLevel  <= (m_bakeTimer + m_bakeTimer * 0.2f))
            {
                references._verdictManager.AddScore(1);
            }
            // burned
            else if (m_bakingLevel >= 3*m_bakeTimer)
            {
                references._verdictManager.AddScore(0);
            }
            //Too backed
            else
            {
                score = (3 * m_bakeTimer - m_bakingLevel) / (2 * m_bakeTimer);
                references._verdictManager.AddScore(score);
            }
        }
        // TOASTING
        else if (references._recipeManager._currentStep._action == SORecipe.Interaction.Toast)
        {
           
            //FIRST SIDE
            // not fully toast
            if (m_toastLevel1 < m_toastTimer)
            {
                score = m_toastLevel1 / m_toastTimer;
            }
            // 100%
            else if (m_toastLevel1 <= (m_toastTimer + m_toastTimer * 0.2f))
            {
                score = 1;
            }
            // burned
            else if (m_toastLevel1 >= 3 * m_toastTimer)
            {
                score = 0;
            }
            //Too toasted
            else
            {
                score = (3 * m_toastTimer - m_toastLevel1 ) / (2 * m_toastTimer);
            }
            

            //SECOND SIDE
            // not fully toast
            if (m_toastLevel2 < m_toastTimer)
            {
                score += m_toastLevel2 / m_toastTimer;
            }
            // 100%
            else if (m_toastLevel2 <= (m_toastTimer + m_toastTimer * 0.2f))
            {
                score += 1;
            }
            // burned
            else if (m_toastLevel2 >= 3 * m_toastTimer)
            {
                score += 0;
            }
            //Too toasted
            else
            {
                score += (3 * m_toastTimer - m_toastLevel2) / (2 * m_toastTimer);
            }         
            
            references._verdictManager.AddScore(score * 0.5f);
        }
            
       
    }

    void SetHeat()
    {
        references._oven.Find("HeatPlane").renderer.material.SetFloat("transparency", Mathf.Clamp(2f - (m_bakingLevel / m_bakeTimer), 0.1f, 1f));
    }

    void ComputeTextureLerp(float ratio, int face = 0)
    {
        MeshRenderer[] renderers = transform.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].materials[face].SetFloat("_Ratio", ratio);
        }
    }

    void ComputeVibratorsValue(float ratio)
    {
        float value = Mathf.Lerp(0f, 2f, ratio);
        Vibrator[] vibrators = transform.GetComponentsInChildren<Vibrator>();
        for (int i = 0; i < vibrators.Length; i++)
        {
            vibrators[i]._factor = value;
        }
    }

    void ComputeOvenSelfIllumin(float ratio)
    {
        Color a = Color.black;
        Color b = new Color(1f, 0.854f, 0.854f, 1f);
        references._four.renderer.material.SetColor("_SelfIllu_Tint", Color.Lerp(a, b, ratio));
        /*MeshRenderer[] renderers = transform.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < renderers.Length; i++)
        {
            //renderers[i].materials[face].SetFloat("_Ratio", ratio);
        }*/
        //color = Color.Lerp(a, b, ratio);
    }

    void OnTriggerEnter(Collider other)
    {
        //Start toasting
        if (other.collider.tag == "Plancha"  && !_isGrabbed && _isToastable == true)
        {            
            _isToasting = true;
            references._plancha.AddIngredientOnPlancha(this);
            SdAudioManager.Trigger("Ingredient_Grill", gameObject);
            references._plancha.ActivateSound();

            if(_currentBubblesFx == null)
            {
                _currentBubblesFx = GameObject.Instantiate(_fxBubblesPlancha) as GameObject;
                _currentBubblesFx.transform.position = new Vector3(transform.position.x, 4.8356f, transform.position.z);
            }

            Vibrator[] vibrators = transform.GetComponentsInChildren<Vibrator>();
            for (int i = 0; i < vibrators.Length; i++)
            {
                vibrators[i].enabled = true;
            }
        }

        //Start baking 
        if (other.collider.tag == "Oven" && !_isBaking && m_hand != null && !_isOnTheTrail && _isBakable == true)
        {
            EnterInTheOven();

            Vibrator[] vibrators = transform.GetComponentsInChildren<Vibrator>();
            for (int i = 0; i < vibrators.Length; i++)
            {
                vibrators[i].enabled = true;
            }
        }
    }    

    void OnTriggerStay(Collider other)
    {
        //Toasting
        if (other.collider.tag == "Plancha" && _isToastable == true)
        {
            references._plancha.AddIngredientOnPlancha(this);

            //look on which side the ingredient is
            // FIRST SIDE
            if (m_isFirstSide)
            {
                m_toastLevel1 += Time.deltaTime;

                ComputeTextureLerp(Mathf.Clamp(m_toastLevel1 / m_toastTimer, 0f, 2f), 1);
                ComputeVibratorsValue(Mathf.Clamp(m_toastLevel1 / m_toastTimer, 0f, 1f));

                if (m_toastLevel1 / m_toastTimer >= 1f && references._planchaButton.animation.isPlaying == false)
                {
                    references._planchaButton.animation.Play();
                    SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_PlanchaButton", references._fightingTable._pirateLeft.gameObject);
                }

                //ingredient is perfect for 100% to 110%
                if (m_toastLevel1 >= m_toastTimer && !m_isToasted1)
                {
                    _isToasting = true;
                    m_isToasted1 = true;
                    m_smoke.startColor = new Color(1, 1, 1, 0.7f);
                    SetSmoke();
                    m_smoke.Play();
                }
                //ingredient is burned
                else if (m_toastLevel1 >= 3 * m_toastTimer)
                {
                    FailedLooser();
                }
                // ingredient if after 110% toasting
                else if (m_toastLevel1 > (m_toastTimer + m_toastTimer * 0.2f))
                {
                    SetSmokeColor();
                }
            }
            //SECOND SIDE
            else
            {
                m_toastLevel2 += Time.deltaTime;

                ComputeTextureLerp(Mathf.Clamp(m_toastLevel2 / m_toastTimer, 0f, 2f), 0);
                ComputeVibratorsValue(Mathf.Clamp(m_toastLevel2 / m_toastTimer, 0f, 1f));

                if (m_toastLevel2 / m_toastTimer >= 1f && references._planchaButton.animation.isPlaying == false)
                {
                    references._planchaButton.animation.Play();
                }

                //ingredient is perfect for 100% to 110%
                if (m_toastLevel2 >= m_toastTimer && !m_isToasted2)
                {
                    _isToasting = true;
                    m_isToasted2 = true;
                    m_smoke.startColor = new Color(1, 1, 1, 0.7f);
                    m_smoke.Play();
                }
                //ingredient is burned
                else if (m_toastLevel2 >= 3 * m_toastTimer)
                {
                    FailedLooser();
                }
                // ingredient if after 110% toasting
                else if (m_toastLevel2 > (m_toastTimer + m_toastTimer * 0.2f))
                {
                    SetSmokeColor();
                }
            }
        }

        //Baking
        if (other.collider.tag == "Oven" && _isBaking && _isBakable == true)
        {
            
            m_bakingLevel += Time.deltaTime;

            ComputeTextureLerp(Mathf.Clamp(m_bakingLevel / m_bakeTimer, 0f, 2f), 0);
            ComputeOvenSelfIllumin(m_bakingLevel / m_bakeTimer);
            ComputeVibratorsValue(Mathf.Clamp(m_bakingLevel / m_bakeTimer, 0f, 1f));

            SdAudioManager.SetParameter("CookBoard_GrillStatus", Mathf.Clamp(Mathf.InverseLerp(m_bakeTimer, m_bakeTimer * 3f, m_bakingLevel), 0f, 1f), references._oven.gameObject);

            SdAudioManager.SetParameter("CookBoard_OvenTimer", m_bakingLevel * (3f / m_bakeTimer));
            //ingredient is perfect -> 100% to 110%
            if (m_bakingLevel >= m_bakeTimer && !m_isBaked)
            {
                m_isBaked = true;
                m_smoke.startColor = new Color(1f, 1f, 1f, 0.7f);
                SetSmoke();
                m_smoke.Play();
                SdAudioManager.Trigger("CookBoard_OvenTickEnd", references._oven.gameObject);
                SdAudioManager.Trigger("CookBoard_PlanchaWhistleStart", references._oven.gameObject);
            }
            //ingredient is burned
            else if (m_bakingLevel >= 3 * m_bakeTimer)
            {
                if (m_isBurnt == false)
                {
                    FailedLooser();
                    references._oven.Find("Four_FX").gameObject.SetActive(true);
                    m_isBurnt = true;
                    SdAudioManager.Trigger("CookBoard_OvenOverload", references._oven.gameObject);
                    SdAudioManager.Trigger("CookBoard_PlanchaWhistleStop", references._oven.gameObject);
                    StartCoroutine("SoundTurnOff");
                }
            }
            // ingredient if after 110% baking
            else if (m_bakingLevel > (m_bakeTimer + m_bakeTimer * 0.2f))
            {
                if (m_soundOvenBurn == false && m_bakingLevel > (m_bakeTimer + m_bakeTimer * 0.5f))
                {
                    SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_FinishedCooking", references._fightingTable._pirateLeft.gameObject);
                    m_soundOvenBurn = true;
                }
                SetSmokeColor();
                SetHeat();
            }
        }
    }

    IEnumerator SoundTurnOff()
    {
        yield return new WaitForSeconds(1f);
        //SdAudioManager.Trigger("CookBoard_OvenTurnOff", references._oven.gameObject);
        references._oven.Find("Four_FX").gameObject.SetActive(false);
    }

    void OnTriggerExit(Collider other)
    {
        //Out of the oven 
        if (other.collider.tag == "Oven" && _isBakable == true)
        {
            //_isInTheOven = false;
            //_isBaking = false;  
            references._oven.Find("HeatPlane").renderer.material.SetFloat("transparency", 1f);            
            m_highlighter.UnHighlight();
        }
    }
}
