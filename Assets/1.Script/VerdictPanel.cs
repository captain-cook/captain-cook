﻿using UnityEngine;
using System.Collections;

public class VerdictPanel : ReferencedMonoBehaviour {

    public float _offset = 0.167f;
    public float _switchOffset = 0.115f;
    public float _jaugeDuration = 0.347f;

    private bool m_isPanelDisplayed = false;

    private TweenScale m_tScale;
    private UISprite m_jauge;
    private Color m_colorJauge;
    private float m_scoreRound = 0;
    private float m_jaugeLevel = 0;
    private int m_line = 1;
    private bool m_isJaugesLoading = false;    

    public UISprite _jauge1;
    public TweenScale _tScaleTime1;
    public TweenScale _tScaleChoice1;
    public TweenScale _tScaleSkill1;

    public UISprite _jauge2;
    public TweenScale _tScaleTime2;
    public TweenScale _tScaleChoice2;
    public TweenScale _tScaleSkill2;

    public UISprite _jauge3;
    public TweenScale _tScaleTime3;
    public TweenScale _tScaleChoice3;
    public TweenScale _tScaleSkill3;

    public TweenAlpha _tAlphaStamp;
    public TweenScale _tScaleStamp;
    public UISpriteAnimation _stampAnimation;
    
	// Use this for initialization
	void Awake () {

        base.LoadReferences();        
        m_tScale = GetComponent<TweenScale>();
	}
	
    void Start()
    {
        gameObject.SetActive(false);
    }
    	
    void Update()
    {
        if (m_isJaugesLoading)
        {
            m_jaugeLevel += Time.deltaTime ;

            // Filling
            if (m_jaugeLevel < _jaugeDuration)
            {
                m_jauge.fillAmount = m_jaugeLevel / _jaugeDuration * m_scoreRound;
                m_jauge.color = Color.Lerp(Color.white, m_colorJauge, m_jaugeLevel / _jaugeDuration);
            }
            // Full
            else 
            {
                m_isJaugesLoading = false;                
            }
        }
    }
    
    public void DisplayPanel()
    {
        gameObject.SetActive(true);
        Init();
        m_tScale.transform.localScale = Vector3.zero;
        m_tScale.PlayForward();
        m_isPanelDisplayed = true;
    }

    public void DisablePanel()
    {
        m_tScale.PlayReverse();
        m_isPanelDisplayed = false;
    }

    //set all the parameters
    public void Init()
    {
        
        //reset jauges
        m_isJaugesLoading = false;
        m_line = 1;

        //set jauges
        _jauge1.color = Color.white;
        _jauge2.color = Color.white;
        _jauge3.color = Color.white;
        _jauge1.fillAmount = 0;
        _jauge2.fillAmount = 0;
        _jauge3.fillAmount = 0;

        //reset tween to begining
        _tScaleTime1.ResetToBeginning();
        _tScaleTime2.ResetToBeginning();
        _tScaleTime3.ResetToBeginning();
        _tScaleChoice1.ResetToBeginning();
        _tScaleChoice2.ResetToBeginning();
        _tScaleChoice3.ResetToBeginning();
        _tScaleSkill1.ResetToBeginning();
        _tScaleSkill2.ResetToBeginning();
        _tScaleSkill3.ResetToBeginning();

        //set duration
        _tScaleTime1.duration = _offset;
        _tScaleTime2.duration = _tScaleTime1.duration;
        _tScaleTime3.duration = _tScaleTime1.duration;
        _tScaleChoice1.duration = _tScaleTime1.duration;
        _tScaleChoice2.duration = _tScaleTime1.duration;
        _tScaleChoice3.duration = _tScaleTime1.duration;
        _tScaleSkill1.duration = _tScaleTime1.duration;
        _tScaleSkill2.duration = _tScaleTime1.duration;
        _tScaleSkill3.duration = _tScaleTime1.duration;

        //set delay
        _tScaleTime1.delay = _jaugeDuration + _offset;
        _tScaleChoice1.delay = _jaugeDuration + _offset * 2;
        _tScaleSkill1.delay = _jaugeDuration + _offset * 3;
        _tScaleTime2.delay = _jaugeDuration + _offset;
        _tScaleChoice2.delay = _jaugeDuration + _offset * 2;
        _tScaleSkill2.delay = _jaugeDuration + _offset * 3;
        _tScaleTime3.delay = _jaugeDuration + _offset;
        _tScaleChoice3.delay = _jaugeDuration + _offset * 2;
        _tScaleSkill3.delay = _jaugeDuration + _offset * 3;     
   
        //set stamp
        _tAlphaStamp.ResetToBeginning();
        _tScaleStamp.ResetToBeginning();
        _tAlphaStamp.delay = _switchOffset;
        _tScaleStamp.delay = _tAlphaStamp.delay;
        _tAlphaStamp.duration = _jaugeDuration - 0.145f;
        _tScaleStamp.duration = _tAlphaStamp.duration;  
     
        //set variables
        _tScaleTime1.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetTimingScore(1)) + "%";
        _tScaleTime2.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetTimingScore(2)) + "%";
        _tScaleTime3.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetTimingScore(3)) + "%";

        _tScaleChoice1.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetChoicesScore(1)) + "%";
        _tScaleChoice2.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetChoicesScore(2)) + "%";
        _tScaleChoice3.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetChoicesScore(3)) + "%";

        _tScaleSkill1.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetSkillScore(1)) + "%";
        _tScaleSkill2.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetSkillScore(2)) + "%";
        _tScaleSkill3.GetComponent<UILabel>().text = Mathf.Round(references._verdictManager.GetSkillScore(3)) + "%";
        
    }
          
    public void DisplayLine()
    {

        m_jaugeLevel = 0;        
        SetJaugeColor(m_line);
        m_scoreRound = references._verdictManager.GetRoundScore(m_line) / 100;
        m_isJaugesLoading = true;

        switch (m_line)
        {
            case 1 :
                _tScaleTime1.PlayForward();
                _tScaleChoice1.PlayForward();
                _tScaleSkill1.PlayForward();
                m_jauge = _jauge1;
                break;

            case 2 :
                _tScaleTime2.PlayForward();
                _tScaleChoice2.PlayForward();
                _tScaleSkill2.PlayForward();
                m_jauge = _jauge2;
                break;

            case 3:
                _tScaleTime3.PlayForward();
                _tScaleChoice3.PlayForward();
                _tScaleSkill3.PlayForward();
                m_jauge = _jauge3;
                break;
        }       
    }

    /*
    public void DisplayLine()
    {        
        m_jaugeLevel = 0;
        m_isJaugeLoading = false;

        //set variables depending on the line
        TweenScale tTime = new TweenScale();
        TweenScale tChoice = new TweenScale();
        TweenScale tSkill = new TweenScale();
        
        switch (references._binomialManager._numPhase)
        {
            case 1 :
                numPhase = 1;
                tTime = _tScaleTime1;
                tChoice = _tScaleChoice1;
                tSkill = _tScaleSkill1;
                m_jauge = _jauge1;
                break;

            case 2 :
                numPhase = 2;
                tTime = _tScaleTime1;
                tChoice = _tScaleChoice1;
                tSkill = _tScaleSkill1;
                break;

            case 3:
                numPhase = 3;
                tTime = _tScaleTime1;
                tChoice = _tScaleChoice1;
                tSkill = _tScaleSkill1;
                break;
        }

        // launch the animation ,on the good line
        if (numPhase > 0)
        {
            //set parameter
            tChoice.duration = tTime.duration;
            tSkill.duration = tTime.duration;
            tChoice.delay = tTime.duration;
            tSkill.delay = tTime.duration * 2;

            //start animation
            tTime.PlayForward();
            tChoice.PlayForward();
            tSkill.PlayForward();
        }
    }
     * */

    //set green or red color to the current jauge depending if the player won the round
    void SetJaugeColor(int line)
    {
        if ( references._verdictManager.IsPlayerWonTheRound(line))
        {
            m_colorJauge = new Color(61/255f , 212/255f , 48/255f);
        }
        else
        {
            m_colorJauge = new Color(229 / 255f, 37 / 255f, 44 / 255f);
        }
    }

    public void StartNextLine()
    {
        if (m_line < 3)
        {
            StartCoroutine("WaitBeforeNextLine");
        }
        else
        {
            PlayStamp();
        }
    }

    // wiat a little before check if there is other lines to display
    IEnumerator WaitBeforeNextLine()
    {
        yield return new WaitForSeconds(_switchOffset);
        m_line++;
        DisplayLine();
       
    }

    public void StartStats()
    {
        if ( m_isPanelDisplayed )
            StartCoroutine("WaitForSound");
        else
        {
            references._mama.SetTrigger("Selection");
            if (references._verdictManager.GetNbRoundsWin() >= 2)
            {
                SdAudioManager.Trigger("Boss_Mama_Looser", references._mama.gameObject);
            }
            else
            {
                SdAudioManager.Trigger("Boss_Mama_Winner", references._mama.gameObject);
            }
        }
    }

    IEnumerator WaitForSound()
    {
        SdAudioManager.Trigger("Music_ResultsVerdict" + references._verdictManager.GetScoreToString(), SdAudioManager.Audioinstance.gameObject);
        yield return new WaitForSeconds(_offset);
        DisplayLine();
    }

    IEnumerator WaitForStamp()
    {
        yield return new WaitForSeconds(_switchOffset);
        SdAudioManager.Trigger("UI_Stamp");
    }

    IEnumerator WaitForSchim()
    {
        yield return new WaitForSeconds(0.42f);
        _stampAnimation.namePrefix = "coin_";

        //closing panel
        yield return new WaitForSeconds(3);
        DisablePanel();
    }

    //launch the stamp animation
    void PlayStamp()
    {
        StartCoroutine("WaitForStamp");
        _tAlphaStamp.PlayForward();
        _tScaleStamp.PlayForward();
    }

    //play the stamp schim animation and laucnh then close the window
    public void EndOfStamp()
    {
        StartCoroutine("WaitForSchim");        
    }
}
