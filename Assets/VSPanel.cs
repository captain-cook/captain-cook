﻿using UnityEngine;
using System.Collections;

public class VSPanel : ReferencedMonoBehaviour {

    public Animator _player;
    public Animator _mama;
    public Animator _vs;



	// Use this for initialization
	void Start () {

        base.LoadReferences();
        _player.enabled = false;
        _mama.enabled = false;
        _vs.enabled = false;

        transform.parent.gameObject.SetActive(false);
	}
	
	

    public void DisplayVSPanel()
    {
        transform.parent.gameObject.SetActive(true);

        _player.enabled = true;
        _mama.enabled = true;
        _vs.enabled = true;

        //_player.animation.Play();
        //_mama.animation.Play();
        //_vs.animation.Play();

        SdAudioManager.Trigger("Music_Abort_Versus");

    }

    public void EndOfVsPanel()
    {
        transform.parent.gameObject.SetActive(false);
        references._phaseManager.SwitchPhase("PhaseBinomial");
    }
}
