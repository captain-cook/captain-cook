﻿using UnityEngine;
using System.Collections;

public class Phase1Manager : MonoBehaviour {

    //all cameras of the phase 1 and 2     
    Camera m_mainCamera;
    Camera m_handCamera;
    public Camera _camMap;
    public Camera _camFog;
    public Camera _camBackground;
    public Camera _camMiniMap;

    //reference to the 2 hands pf the player
    public HandController _leftHand;
    public HandController _rightHand;

    [HideInInspector]
    public HandControllerUI _leftHandUI;
    [HideInInspector]
    public HandControllerUI _rightHandUI;

    //different position of the camera depending on the phase
    public Transform _positionMapCam;
    public Transform _positionCrewCam;
    public Transform _positionRecipeCam;
    public bool _debugDisplay;
    private Bezier m_bezierMove;
    private Phase1State m_targetPhase;
    private Phase1State m_oldPhase;
    private Transform m_targetPosition;
    private Transform m_oldPosition;
    private float m_stepMove = 0;

    //Ui phase icon
    public UISprite _leftUIIcon;
    public UISprite _rightUIIcon;
    private TweenAlpha m_tAlphaLeft;
    private TweenAlpha m_tAlphaRight;
    
    //curve for the speed transition of camera position between phase    
    public AnimationCurve _speedCurve;
    private float m_switchingSpeed;
    public float _speedCrewToMap = 1f;
    public float _speedMapToRecipe = 2f;

    //Bezier Point
    public Transform _bpMovCrewToMap1;
    public Transform _bpMovCrewToMap2;
    public Transform _bpMovCrewToRecipe1;
    public Transform _bpMovCrewToRecipe2;
    public Transform _bpMovMapToRecipe1;
    public Transform _bpMovMapToRecipe2;

    //general left map
    public Transform _sceneMap;
    private Vector3 m_mapLeftTop;
    private Vector3 m_mapRightBottom;

    //general zoomed map
    public Transform _sceneZoomedMap;
    private Vector2 m_centerZoomedMap;
    private float m_radius;

    public BoxCollider _TriggerLeft;
    public BoxCollider _TriggerRight;

    // in which phase the player is currently on
    public Phase1State _phaseState;
    public enum Phase1State
    {
        Map,
        Crew,
        Recipes,
        Switching,
        Cooking
    }

    // Manager of the different phases
    public OldMap _map;
    public CrewManager _crew;
    public MiniRecipeManager _miniRecipeManager;

    private bool m_alreadyLaunch = false;

	// Use this for initialization
	void Start ()
    {
        
        m_mainCamera = Camera.main;
        m_handCamera = _leftHand.transform.parent.GetComponentInChildren<Camera>();
        _leftHandUI = _leftHand.GetComponent<HandControllerUI>();
        _rightHandUI = _rightHand.GetComponent<HandControllerUI>();

        m_tAlphaLeft = _leftUIIcon.GetComponent<TweenAlpha>();
        m_tAlphaRight = _rightUIIcon.GetComponent<TweenAlpha>();

        // INITIALISATION FOR THE MAP
        m_mainCamera.transform.position = _positionMapCam.position;
        m_mainCamera.transform.eulerAngles = _positionMapCam.eulerAngles;

        //get the left top corner screen coordinate of the map
        Vector3 corner = Vector3.zero;        
        //0.5 * 10 cause the basic unity plane size is 10
        corner.x += 5f;
        corner.z -= 5f;
        corner = _sceneMap.transform.TransformPoint(corner);  
        m_mapLeftTop = m_mainCamera.WorldToScreenPoint(corner);


        //get the right bottom corner screen coordinate of the map
        corner = Vector3.zero;
        //0.5 * 10 cause the basic unity plane size is 10
        corner.x -= 5f;
        corner.z += 5f;
        corner = _sceneMap.transform.TransformPoint(corner);
        m_mapRightBottom = m_mainCamera.WorldToScreenPoint(corner);

        //get the radius of the zoomed map    
        m_centerZoomedMap = m_mainCamera.WorldToScreenPoint(_sceneZoomedMap.position);
        //recup of the border of the map depending on the rotation
        corner = new Vector3(5,0,0);
        corner = _sceneZoomedMap.transform.TransformPoint(corner);
        Vector2 border = m_mainCamera.WorldToScreenPoint(corner);
        m_radius = (border - m_centerZoomedMap).magnitude;

        // INITIALISATION FOR THE CREW

        // INITIALISATION FOR THE RECIPES

        //initialise the scene with the crew phase
        m_mainCamera.transform.position = _positionCrewCam.position;
        m_mainCamera.transform.eulerAngles = _positionCrewCam.eulerAngles;

        _phaseState = Phase1State.Cooking;
        InitCookingPhase();
        m_targetPhase = _phaseState;
        //SetHandController();
        SetCamera(m_targetPhase);
        UpdateUIIcon();
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch ( _phaseState )
        {
            case Phase1State.Map :
                UpdateMap();
                break;

            case Phase1State.Crew :
                UpdateCrew();
                break;

            case Phase1State.Recipes :
                UpdateRecipe();
                break;

            case Phase1State.Switching :
                UpdateSwitching();
                break;

            case Phase1State.Cooking:
                UpdateCooking();
                break;

            default :
                break;
        }
	} 

    //update apply when the pahse 1 is in map
    void UpdateMap()
    {        
        // BIG MAP INPUT (Left hand)
        Vector2 leftHandPosition =  GetHandPositionRelativeToMap(_leftHandUI);
        
        if (leftHandPosition.x >= 0 && leftHandPosition.x <= 1 && leftHandPosition.y >= 0 && leftHandPosition.y <= 1)
        {
            _map.InputMap(_leftHandUI, leftHandPosition);            
        }

        //ZOOMED MAP INPUT (Right hand)
        Vector2 handFromCenter = _rightHandUI._screenPosition - m_centerZoomedMap;
        if (handFromCenter.magnitude < m_radius)
        {
            
            float ratio = handFromCenter.magnitude / m_radius;
            _map.InputZoomMap(_rightHandUI, handFromCenter.normalized, ratio);
        }
    }

    //update apply when the pahse 1 is in crew
    void UpdateCrew()
    {

    }

    //update apply when the pahse 1 is in recipe
    void UpdateRecipe()
    {

    }

    //updat eapply when the player is on transition between two phase
    void UpdateSwitching()
    {
        //swtching
        if (m_stepMove < 1)
        {
            //fix the angle issue when the rotation is to long
            Vector3 targetRotation = m_targetPosition.transform.eulerAngles;
            if (m_targetPosition.transform.eulerAngles.y - m_oldPosition.transform.eulerAngles.y > 180)
            {
                targetRotation.y -= 360;
            }
            else if (m_targetPosition.transform.eulerAngles.y - m_oldPosition.transform.eulerAngles.y < -180)
            {
                targetRotation.y += 360;
            }

            //change camera's transform   
            m_mainCamera.transform.position = m_bezierMove.GetPosition(ref m_stepMove, Time.deltaTime, _speedCurve.Evaluate(m_stepMove) * m_switchingSpeed);
            Vector3 directRotation = Vector3.Lerp(m_oldPosition.transform.eulerAngles, targetRotation, m_stepMove);
            m_mainCamera.transform.eulerAngles = directRotation;           
            
        }
        //switch is finish
        else
        {
            _phaseState = m_targetPhase;
            SetHandController();
            InitCookingPhase();
        }
    }

    void UpdateCooking()
    {

    }

    //return coordinate of the hand on the map on ratio format (ex : left bottom corner is 0,0)
    Vector2 GetHandPositionRelativeToMap(HandControllerUI hand)
    {
        Vector2 handCoor;
        handCoor.x = (hand._screenPosition.x - m_mapLeftTop.x) / (m_mapRightBottom.x - m_mapLeftTop.x);
        handCoor.y = (hand._screenPosition.y - m_mapRightBottom.y) / (m_mapLeftTop.y - m_mapRightBottom.y);
        return handCoor;
    }

    //switch the phase betwwen crew, map and recipe
    /* obsolete : old keyboard switching phase
     * void SwitchPhase (Phase1State left , Phase1State right)
    {
        bool keyDetected = false;

        //get if player want to move to the right or left phase
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            keyDetected = true;
            m_targetPosition = GetPositionByPhase(left);
            m_targetPhase = left;
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            keyDetected = true;
            m_targetPosition = GetPositionByPhase(right);
            m_targetPhase = right;
        }

        //if key detected we launch the processus of switching
        if ( keyDetected)
        {
            //store the actual phase
            m_oldPhase = m_phaseState;
            m_oldPosition = GetPositionByPhase(m_phaseState);

            //init the transition
            m_phaseState = Phase1State.Switching;
            m_stepMove = 0;
            SetBezierPoints();
        }       
    }*/

    //prepare the scene to go on cooking mode
    public void SwitchToCooking()
    {
        m_oldPhase = _phaseState;
        m_oldPosition = GetPositionByPhase(_phaseState);

        m_targetPhase = Phase1State.Cooking;
        m_targetPosition = _positionCrewCam;

        //init the transition
        _phaseState = Phase1State.Switching;
        m_stepMove = 0;
        SetBezierPoints();

        //A CHANGER
        //_leftUIIcon.transform.parent.gameObject.SetActive(false);
        //_rightUIIcon.transform.parent.gameObject.SetActive(false);
        _leftUIIcon.GetComponent<TweenAlpha>().PlayForward();
        _rightUIIcon.GetComponent<TweenAlpha>().PlayForward();
        _leftUIIcon.transform.parent.GetComponent<TweenAlpha>().PlayForward();
        _rightUIIcon.transform.parent.GetComponent<TweenAlpha>().PlayForward();

        //_TriggerLeft.enabled = false;
        //_TriggerRight.enabled = false;
        _leftUIIcon.transform.parent.GetComponent<LabelUIPhase1>().enabled = false;
        _rightUIIcon.transform.parent.GetComponent<LabelUIPhase1>().enabled = false;

        SdAudioManager.Trigger("Music_StartPhase1", SdAudioManager.Audioinstance.gameObject);

        //References.RecipeCard.GetComponent<Collider>().enabled = true ;
    }

    public IEnumerator WaitBeforeSwitchingToCooking(float duration)
    {
        if (!m_alreadyLaunch)
        {
            m_alreadyLaunch = true;
            yield return new WaitForSeconds(duration);
            SwitchToCooking();
            m_alreadyLaunch = false;
        }
    }

    //return to the phase1 mode
    public void ExitFromCooking()
    {

        m_oldPhase = Phase1State.Crew;
        m_oldPosition = _positionCrewCam;

        m_targetPhase = Phase1State.Map;
        m_targetPosition = _positionMapCam;

        //init the transition
        _phaseState = Phase1State.Switching;
        m_stepMove = 0;
        SetBezierPoints();

        //A CHANGER
        //_leftUIIcon.transform.parent.gameObject.SetActive(true);
        //_rightUIIcon.transform.parent.gameObject.SetActive(true);

        _leftUIIcon.GetComponent<TweenAlpha>().PlayReverse();
        _rightUIIcon.GetComponent<TweenAlpha>().PlayReverse();
        _leftUIIcon.transform.parent.GetComponent<TweenAlpha>().PlayReverse();
        _rightUIIcon.transform.parent.GetComponent<TweenAlpha>().PlayReverse();

        //_TriggerLeft.enabled = true;
        //_TriggerRight.enabled = true;
        _leftUIIcon.transform.parent.GetComponent<LabelUIPhase1>().enabled = true;
        _rightUIIcon.transform.parent.GetComponent<LabelUIPhase1>().enabled = true;

        //References.RecipeCard.GetComponent<Collider>().enabled = true ;
    }

    //switch the phase betwwen crew, map and recipe
    public void SwitchPhase(bool totheLeft)
    {
        //store the actual phase
        m_oldPhase = _phaseState;
        m_oldPosition = GetPositionByPhase(_phaseState);

        m_targetPhase = GetNextPhase(totheLeft);
        m_targetPosition = GetPositionByPhase(m_targetPhase);

        //init the transition
        _phaseState = Phase1State.Switching;
        m_stepMove = 0;
        SetBezierPoints();
        SetCamera(m_targetPhase);

        //fade the UIicon
        m_tAlphaLeft.PlayForward();
        m_tAlphaRight.PlayForward();
    }

    //return the target phase depending on the actual one and the switch we want (left or right)
    Phase1State GetNextPhase(bool toTheLeft)
    {
        if (_phaseState == Phase1State.Crew)
        {
            if (toTheLeft)
                return Phase1State.Map;
            else
                return Phase1State.Recipes;
        }
        else if (_phaseState == Phase1State.Map)
        {
            if (toTheLeft)
                return Phase1State.Recipes;
            else
                return Phase1State.Crew;
        }
        else if (_phaseState == Phase1State.Recipes)
        {
            if (toTheLeft)
                return Phase1State.Crew;
            else
                return Phase1State.Map;
        }

        return Phase1State.Switching;
    }

    //return the transform of the position coirresponding to the phase in parameter
    Transform GetPositionByPhase(Phase1State phase)
    {
        if (phase == Phase1State.Crew)
        {
            return _positionCrewCam;
        }
        else if (phase == Phase1State.Map)
        {
            return _positionMapCam;
        }
        else if (phase == Phase1State.Recipes)
        {
            return _positionRecipeCam;
        }
        else
        {
            return _positionCrewCam;
        }
    }
        
    //set the curve dependnig on the old phase and the new one
    void SetBezierPoints()
    {
        if (m_targetPhase == Phase1State.Cooking)
        {
            m_switchingSpeed = _speedCrewToMap;
            m_bezierMove = new Bezier(_positionMapCam.position, _bpMovCrewToMap2.position,
                    _bpMovCrewToMap1.position, _positionCrewCam.position);
        }
        //FROM CREW
        else if (m_oldPhase == Phase1State.Crew)
        {
            m_switchingSpeed = _speedCrewToMap;
            //CREW -> MAP
            if (m_targetPhase == Phase1State.Map)
            {
                m_bezierMove = new Bezier(_positionCrewCam.position , _bpMovCrewToMap1.position ,
                    _bpMovCrewToMap2.position , _positionMapCam.position);
            }
            //CREW -> RECIPE
            else if (m_targetPhase == Phase1State.Recipes)
            {
                m_bezierMove = new Bezier(_positionCrewCam.position, _bpMovCrewToRecipe1.position,
                    _bpMovCrewToRecipe2.position, _positionRecipeCam.position);
            }
        }
        //FROM MAP
        else if (m_oldPhase == Phase1State.Map)
        {
            //MAP -> CREW
            if (m_targetPhase == Phase1State.Crew)
            {
                m_switchingSpeed = _speedCrewToMap;
                m_bezierMove = new Bezier(_positionMapCam.position,_bpMovCrewToMap2.position, 
                    _bpMovCrewToMap1.position, _positionCrewCam.position);
            }
            //MAP -> RECIPE
            else if (m_targetPhase == Phase1State.Recipes)
            {
                m_switchingSpeed = _speedMapToRecipe;
                m_bezierMove = new Bezier(_positionMapCam.position, _bpMovMapToRecipe1.position,
                    _bpMovMapToRecipe2.position, _positionRecipeCam.position);                
            }
        }
        //FROM RECIPE
        else if (m_oldPhase == Phase1State.Recipes)
        {
            //RECIPE -> CREW
            if (m_targetPhase == Phase1State.Crew)
            {
                m_switchingSpeed = _speedCrewToMap;
                m_bezierMove = new Bezier(_positionRecipeCam.position, _bpMovCrewToRecipe2.position,
                    _bpMovCrewToRecipe1.position, _positionCrewCam.position);
            }
            //RECIPE -> MAP
            else if (m_targetPhase == Phase1State.Map)
            {
                m_switchingSpeed = _speedMapToRecipe;
                m_bezierMove = new Bezier(_positionRecipeCam.position, _bpMovCrewToMap2.position,
                   _bpMovCrewToMap1.position, _positionMapCam.position);
            }
        }
    }
   
    //hand the proper hand controller depending on the phase state
    void SetHandController()
    {
        if(_phaseState == Phase1State.Cooking)
        {
            _leftHandUI.GetComponent<HandControllerCooking>().enabled = true;
            _leftHandUI.enabled = false;

            _rightHandUI.GetComponent<HandControllerCooking>().enabled = true;
            _rightHandUI.enabled = false;

            m_mainCamera.cullingMask |= 4096;
            m_handCamera.enabled = false;
        }
        else
        {
            _leftHandUI.GetComponent<HandControllerCooking>().enabled = false;
            _leftHandUI.enabled = true;

            _rightHandUI.GetComponent<HandControllerCooking>().enabled = false;
            _rightHandUI.enabled = true;

            m_handCamera.enabled = true;
            m_mainCamera.cullingMask ^= 4096;
        }
       
    }

    //initialize the cooking phase
    void InitCookingPhase()
    {
        if (_phaseState == Phase1State.Cooking)
        {
            _miniRecipeManager.Activate(true);
        }
    }

    //change the icon on the UI label to fit the futur phase
    public void UpdateUIIcon()
    {
        switch (m_targetPhase)
        {
            case Phase1State.Crew :
                _leftUIIcon.spriteName = "IconMap";
                _rightUIIcon.spriteName = "IconRecipe";
                break;

            case Phase1State.Map :
                _leftUIIcon.spriteName = "IconRecipe";
                _rightUIIcon.spriteName = "IconCrew";
                break;

            case Phase1State.Recipes :
                _leftUIIcon.spriteName = "IconCrew";
                _rightUIIcon.spriteName = "IconMap";
                break;
        }

        if (_leftUIIcon.alpha <= 0)
        {
            m_tAlphaLeft.PlayReverse();
            m_tAlphaRight.PlayReverse();
        }
    }

    //enable and disable cameras depending on the phase
    void SetCamera(Phase1State targetPhase)
    {
        switch (targetPhase)
        {
            case Phase1State.Map:
                
                _camFog.enabled = true;
                _camMap.enabled = true;
                _camMiniMap.enabled = true;
                break;

            case Phase1State.Crew:
                _camFog.enabled = true;
                _camMap.enabled = false;
                _camMiniMap.enabled = true;
                break;

            case Phase1State.Recipes:
                _camFog.enabled = true;
                _camMap.enabled = false;
                _camMiniMap.enabled = false;
                break;

            case Phase1State.Cooking:
                
                _camFog.enabled = true;
                _camMap.enabled = false;
                _camMiniMap.enabled = true;
                break;
        }
    }

    void OnDrawGizmos()
    {
        if (_debugDisplay )
        {
            //Phase position
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(_positionCrewCam.position, 0.05f);
            Gizmos.DrawSphere(_positionMapCam.position, 0.05f);
            Gizmos.DrawSphere(_positionRecipeCam.position, 0.05f);

            //bezier midle position
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(_bpMovCrewToMap1.position, 0.025f);
            Gizmos.DrawSphere(_bpMovCrewToMap2.position, 0.025f);
            Gizmos.DrawSphere(_bpMovCrewToRecipe1.position, 0.025f);
            Gizmos.DrawSphere(_bpMovCrewToRecipe2.position, 0.025f);
            Gizmos.DrawSphere(_bpMovMapToRecipe1.position, 0.025f);
            Gizmos.DrawSphere(_bpMovMapToRecipe2.position, 0.025f);

            //bezier midle rotation
            Gizmos.color = Color.blue;
            //Gizmos.DrawSphere(cornerLeft, 0.025f);
        }
    }
}
