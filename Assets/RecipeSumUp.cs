﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RecipeSumUp : ReferencedMonoBehaviour {

    public SORecipe _soRecipe;

    //animation variables
    public Animation _animation;
    private int m_animStep = 0;
    private bool m_isPlayingForward = true;

    //UI elements
    public Text _title;
    public Image _split;
    public Image[] _ingredients;
    public Image[] _actions;
    public CheckRecipeSumUp[] _checks;

    public SpriteRenderer _imageRecipe;
    private TweenScale m_tScaleImageRecipe;

    
	// Use this for initialization
	void Start () {

        base.LoadReferences();
        gameObject.SetActive(false);
        _animation["RecipeCard"].speed = 0;
        m_tScaleImageRecipe = _imageRecipe.GetComponent<TweenScale>();
	}    

    // load the all UI panel depending with the SOrecipe data
    public void LoadRecipeSumUp()
    {
        gameObject.SetActive(true);

        //get the actual recipe
        if (references._recipeManager._currentRecipe != null)
           _soRecipe = references._recipeManager._currentRecipe;

        // set the recipe image in the globe
        _imageRecipe.sprite = _soRecipe._image;
        _imageRecipe.transform.localScale = Vector3.zero;

        //load ingredient non bakable
        for ( int i = 0 ; i < 4 ; i++)
        {
            //display icon
            if (i < _soRecipe._recipeSteps.Length  
                &&_soRecipe._recipeSteps[i]._action != SORecipe.Interaction.Bake
                && _soRecipe._recipeSteps[i]._action != SORecipe.Interaction.Toast)
            {
                _ingredients[i].sprite = _soRecipe._recipeSteps[i]._ingredient._image;
                _actions[i].sprite = _soRecipe._recipeSteps[i]._imageInterraction;
                _ingredients[i].color = new Color(1, 1, 1, 1);
                _actions[i].color = new Color(1, 1, 1, 1);
            }
            //disable icon
            else
            {
                _ingredients[i].color = new Color(1, 1, 1, 0);
                _actions[i].color = new Color(1, 1, 1, 0);
            }
        }

        //load bakable ingredient
        int lenght = _soRecipe._recipeSteps.Length ;
        for (int i = 2; i >= 1; i--)
        {
            //display icon
            if (i <= _soRecipe._recipeSteps.Length
                && ( _soRecipe._recipeSteps[lenght - i]._action == SORecipe.Interaction.Bake
                || _soRecipe._recipeSteps[lenght - i]._action == SORecipe.Interaction.Toast))
            {
                _ingredients[_ingredients.Length - i].sprite = _soRecipe._recipeSteps[lenght - i]._ingredient._image;
                _actions[_ingredients.Length - i].sprite = _soRecipe._recipeSteps[lenght - i]._imageInterraction;
                _ingredients[_ingredients.Length - i].color = new Color(1, 1, 1, 1);
                _actions[_ingredients.Length - i].color = new Color(1, 1, 1, 1);
            }
            //disable icon
            else
            {
                _ingredients[_ingredients.Length - i].color = new Color(1, 1, 1, 0);
                _actions[_ingredients.Length - i].color = new Color(1, 1, 1, 0);
            }
        }

        //reset checks
        foreach (CheckRecipeSumUp check in _checks)
        {
            check.Reset();
        }

        // unroll the recipe sum up scroll
        PlaySumUpAnimation(true);
    }

    //play the recipe card animation forward or backward
    public void PlaySumUpAnimation(bool forward)
    {        
        if (forward)
        {            
            m_isPlayingForward = true;
            _animation["RecipeCard"].speed = 1;
            _animation["RecipeCard"].time = 0;
            SdAudioManager.Trigger("RecipeBoard_Open", gameObject);
        }
        else 
        {            
            m_isPlayingForward = false;
            _animation["RecipeCard"].speed = -1f;
            _animation["RecipeCard"].time = 20 / 30f;
            SdAudioManager.Trigger("RecipeBoard_Close", gameObject);
        }

        _animation.Play("RecipeCard");
    }

    public void DisplayCardStepByStep(int numStep)
    {
        switch (numStep)
        {
            case 0:
                _title.enabled = m_isPlayingForward;
                break;
            case 1:
                _ingredients[0].enabled = m_isPlayingForward;
                _actions[0].enabled = m_isPlayingForward;
                _checks[0].enabled = m_isPlayingForward;
                
                _ingredients[1].enabled = m_isPlayingForward;
                _actions[1].enabled = m_isPlayingForward;
                _checks[1].enabled = m_isPlayingForward;

                _checks[0].Reset();
                _checks[1].Reset();
                
                break;

            case 2:
                _ingredients[2].enabled = m_isPlayingForward;
                _actions[2].enabled = m_isPlayingForward;
                _checks[2].enabled = m_isPlayingForward;
                
                _ingredients[3].enabled = m_isPlayingForward;
                _actions[3].enabled = m_isPlayingForward;
                _checks[3].enabled = m_isPlayingForward;

                _checks[2].Reset();
                _checks[3].Reset();

                break;

            case 3:
                _split.enabled = m_isPlayingForward;

                break;

            case 4:
                
                _ingredients[4].enabled = m_isPlayingForward;
                _actions[4].enabled = m_isPlayingForward;
                _checks[4].enabled = m_isPlayingForward;
                
                _ingredients[5].enabled = m_isPlayingForward;
                _actions[5].enabled = m_isPlayingForward;
                _checks[5].enabled = m_isPlayingForward;

                _checks[4].Reset();
                _checks[5].Reset();

                break;

            case 5 :
                
                if (m_isPlayingForward)
                {
                    m_tScaleImageRecipe.PlayForward();
                    _imageRecipe.enabled = m_isPlayingForward;
                }                    
                else
                    m_tScaleImageRecipe.PlayReverse();
                break;
        }
    }

    //check a step icon of the card
    public void ValidateStep(int indexStep)
    {
        //normal step
        if (_soRecipe._recipeSteps[indexStep]._action != SORecipe.Interaction.Bake
            && _soRecipe._recipeSteps[indexStep]._action != SORecipe.Interaction.Toast)
        {
            _checks[indexStep].Play();
        }
        //baking step
        else
        {
            int ind = _soRecipe._recipeSteps.Length - indexStep;
            _checks[6 - ind].Play();
        }
    }
}
