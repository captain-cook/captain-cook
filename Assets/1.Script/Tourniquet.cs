﻿using UnityEngine;
using System.Collections;

public class Tourniquet : ReferencedMonoBehaviour {

    [HideInInspector]
    public SORecipe _preselectedRecipe;
    [HideInInspector]
    public bool _isGrab = false;
    private bool m_checkTime = false;
    private float m_timeToHurry = 0;
    private bool m_isTimeToHurry = false;

    //store the pirate taste
    [HideInInspector]
    public SOPirate _tastePirate1;
    [HideInInspector]
    public SOPirate _tastePirate2;

    private bool m_isPrecipePreselected = false;
    

	// Use this for initialization
	void Start () {

        base.LoadReferences();
	}

    void Update()
    {
        if (m_isTimeToHurry)
        {
            m_timeToHurry += Time.deltaTime;
            if (m_timeToHurry >= 10)
            {
                m_isTimeToHurry = false;
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_WaitForRecipe", references._binomialManager._binomialToDisable.gameObject);
            }
        }

        //Debug.Log("velocity = " + Mathf.Abs(rigidbody.angularVelocity.y));
        if (Mathf.Abs(rigidbody.angularVelocity.y) > 0.048f)
        {
            //SdAudioManager.Trigger("RecipeWheel_Wheel", _tourniquet.gameObject);
            SdAudioManager.SetParameter("RecipeWheel_RecipeWheelSpeed", Mathf.Abs(rigidbody.angularVelocity.y) /3 , gameObject);
        }
        else
        {
            SdAudioManager.SetParameter("RecipeWheel_RecipeWheelSpeed", 0, gameObject);
        }
    }
	
    //store the recipe actually overed
    public bool SetPreselectedRecipe(SORecipe recipe)
    {
        if (!_isGrab)
        {
            references._recipePanel.gameObject.SetActive(true);
            m_isPrecipePreselected = true;
            _preselectedRecipe = recipe;
            references._recipePanel.UpdateRecipePanel(recipe , _tastePirate1, _tastePirate2);
            return true;
        }        

        return false;
    }


    public SORecipe GetPreselectedRecipe()
    {
        return _preselectedRecipe;
    }

    public void ResetPreselectedRecipe()
    {
        if (!_isGrab)
        {
            _preselectedRecipe = null;
            m_isPrecipePreselected = false;

            references._recipePanel.gameObject.SetActive(false);
        }     
    }

    //enable.disable the tscroll mathcing with the actual pahse of the fight
    public void InitTscroll()
    {
        m_timeToHurry = 0;
        m_isTimeToHurry = true;

        TScroll[] tscrolls = GetComponentsInChildren<TScroll>();
        SORecipe.Type actualPhase = references._binomialManager.GetActualPhase();
        foreach (TScroll ts in tscrolls)
        {
            if (ts._recipeData._recipeType == actualPhase)
            {
                ts.Enable();
            }
            else
            {
                ts.Disable();
            }
        }
    }
    
    public void EvaluateRecipeChoice()
    {
        float score = 50;
        foreach (SOTaste recipeTaste in _preselectedRecipe._tastes)
        {
            score += recipeTaste.Compare(_tastePirate1._tastePermanent);
            score += recipeTaste.Compare(_tastePirate2._tastePermanent);
            score += recipeTaste.Compare(_tastePirate1._tasteTemp);
            score += recipeTaste.Compare(_tastePirate2._tasteTemp);
        }

        references._verdictManager.SetChoiceScore(score);
    }
   

}
