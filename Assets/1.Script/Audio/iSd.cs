﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface iSd
{
    void Execute(string action,GameObject source = null);
    void SetParameter(string parameter, float value, GameObject source = null);
}
