﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MagnitudeHelper : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Transform _a;
        public Transform _b;
        public float _distance;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{
            if(_a != null && _b != null)
            {
                _distance = Vector3.Distance(_a.position, _b.position);
            }
		}

	#endregion

	#region Custom

	#endregion
	
}
