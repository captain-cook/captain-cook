﻿using UnityEngine;
using System.Collections;

public class CountDownCooking : MonoBehaviour {

    private int _count = 3;
    private UILabel m_label;
    private TweenScale m_tScale;

	// Use this for initialization
	void Start () {

        m_label = GetComponentInChildren<UILabel>();
        m_tScale = m_label.GetComponent<TweenScale>();
	}
	
	

    public void Play(string text)
    {
        m_tScale.ResetToBeginning();
        m_label.text = text;
        m_tScale.PlayForward();        
    }

    //public void IncCountDown(string text)
    //{
    //    _count -- ;
    //    //countdown's over
    //    if (_count == 0)
    //    {
           
    //    }
    //    //countdown not over yet
    //    else if (_count > 0)
    //    {
    //        m_label.text = "" + _count;
    //        m_tScale.ResetToBeginning();
    //        m_tScale.PlayForward();
    //    }
    
}
