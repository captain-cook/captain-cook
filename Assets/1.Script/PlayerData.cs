﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour {

	#region Enum

    public enum MasterHand { Left, Right }

	#endregion

	#region Public

        public MasterHand _masterHand;
        public HandControllerDistribution.DistributionMode _distributionMode;
        public int _heigth;

        public Vector2 _minHeightArmLength;
        public Vector2 _maxHeightArmLength;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{

		}

	#endregion

	#region Custom

	#endregion
	
}
