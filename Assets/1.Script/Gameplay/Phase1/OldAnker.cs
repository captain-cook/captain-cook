﻿using UnityEngine;
using System.Collections;

public class OldAnker : MonoBehaviour
{

    public OldBoat _boat;
    public Camera _mapCamera;
    public Highlighter _highlighter;

    //state of the target cross
    [HideInInspector]
    public bool _isDraged = false;

   
    //hand dragging the anker
    private HandControllerUI m_hand;

    // Use this for initialization
    void Start()
    {

        //init te cross on the boat
        Vector3 replace = _boat.transform.position;
        replace.y = transform.position.y;
        transform.position = replace;

        _highlighter = GetComponent<Highlighter>();
    }


    public void StartDrag(HandControllerUI hand)
    {
        _isDraged = true;
        m_hand = hand;
        SdAudioManager.Trigger("UIAnchor_Pickup");
    }

    //drag the targetCross under the mouse
    public void Drag(Vector3 handPosition)
    {
        //follow the mouse
        Vector3 moveTo = handPosition;
        moveTo.y = transform.localPosition.y;
        transform.localPosition = handPosition;
    }

    public void MoveTo(Vector2 handRatio , Vector3 handPosition)
    {
        //we are on the map so we can set the travel
        if (handRatio.x <= 1 && handRatio.x >= 0 && handRatio.y <= 1 && handRatio.y >=0 )
        {                   
            _boat.MoveTo(transform.position);
            _isDraged = false;
            SdAudioManager.Trigger("UIAnchor_Put");
        }
        else
        {
            _isDraged = false;
            Vector3 replace = _boat.transform.position;
            replace.y = transform.position.y;
            transform.position = replace;
        }
    }
    
    
}
