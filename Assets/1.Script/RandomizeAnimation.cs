﻿using UnityEngine;
using System.Collections;

public class RandomizeAnimation : MonoBehaviour {

    public int _index;

    //make the pirate animation a little random
    public void RandomizePirateAnimation()
    {
        int rand = Random.Range(1, 4);
        GetComponent<Animator>().SetInteger("RandAnim", rand);
        
    }

    //void OnCollisionEnter(Collision other)
    //{
    //    if (other.collider.tag == "Respawn")
    //    {
    //        GetComponent<Animator>().SetTrigger("Landing");
    //    }
    //}
    void OnTriggerEnter(Collider other)
    {
        if (other.collider.tag == "Respawn")
        {
            GetComponent<Animator>().SetTrigger("Landing");
            SdAudioManager.Trigger("Footsteps_Fall",gameObject);
            SdAudioManager.Trigger("Music_Abort_PiratesArrival");
            SdAudioManager.Trigger("Pirate_Sbire" + _index + "_AbortAnnounce", gameObject);
        }

        
    }


}
