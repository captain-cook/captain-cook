Shader "Captain-Cook/Outline-Regular-Progressive" {
		Properties
		{
			_OutlineColor("Outline Color", Color) = (0.5, 0.5, 0.5, 1)
			_OutlineSize("Outline Size", Float) = 0.1
			_Barycenter("Barycenter", Vector) = (0, 0, 0, 1)
			_Min("Min", Float) = -1
			_Max("Max", Float) = 1
			_Progress("Progress", Float) = 0.5
			_Axis("Axis", Int) = 0
		}

		SubShader
		{
				Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Geometry"}
				Blend SrcAlpha OneMinusSrcAlpha

				CGINCLUDE
					fixed4 _OutlineColor;
					float _OutlineSize;
					float4 _Barycenter;
					float _Min;
					float _Max;
					float _Axis;
					float _Progress;

					#include "UnityCG.cginc"

					struct v2f
					{
						float4 pos : SV_POSITION;
						float4 color : COLOR;
					};

					v2f vert(appdata_base v)
					{
						v2f o;

						float value = v.vertex.x;
						if(_Axis == 1) { value = v.vertex.y; }
						else { value = v.vertex.z; }
						value -= _Min;
						float length = _Max - _Min;
						float percent = value / length;

						if(1 - percent > _Progress)
						{
							o.color = float4(0,0,0,0);
						}
						else
						{
							o.color = _OutlineColor;
						}

						float4 relative = float4(
							v.vertex.x - _Barycenter.x,
							v.vertex.y - _Barycenter.y,
							v.vertex.z - _Barycenter.z,
							0);

						relative *= _OutlineSize;

						o.pos = mul(UNITY_MATRIX_MVP, _Barycenter + relative);
						return o;
					}

					half4 frag(v2f i) : COLOR
					{
						return i.color;
					}
				ENDCG

				Pass
				{
					Name "REGULAR"
					Cull Front
					ZWrite On
					LOD 200

					CGPROGRAM
						#pragma vertex vert
						#pragma fragment frag
					ENDCG
					//SetTexture [_MainTex] { combine primary }
				}
		}
		//FallBack "Diffuse"
}
