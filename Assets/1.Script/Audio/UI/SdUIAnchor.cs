﻿using UnityEngine;
using System.Collections;

public class SdUIAnchor : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        switch (action)
        {
            case "Pickup":
                SdAudioManager.SetSwitch("UIAnchor", action, source);
                SdAudioManager.Play("UIAnchor", source); 
                break;
            case "Put":
                SdAudioManager.SetSwitch("UIAnchor", action, source);
                SdAudioManager.Play("UIAnchor", source);
                break;
            default:
                break;
        }
    }
}


