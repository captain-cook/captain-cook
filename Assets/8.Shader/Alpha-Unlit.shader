Shader "Transparent/UnlitColor" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
	}

	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200

		Pass{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				float4 _Color;

				struct v2f {
					float4 pos : SV_POSITION;
					float4 color : COLOR;
				};

				v2f vert (appdata_base v)
				{
						v2f o;
						o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
						o.color = _Color;
						return o;
				}

				half4 frag (v2f i) : COLOR
				{
						return i.color;
				}
			ENDCG
		}

	}

	Fallback "Transparent/VertexLit"
}
