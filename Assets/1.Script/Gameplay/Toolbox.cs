﻿using UnityEngine;
using System.Collections;

public class Toolbox : ReferencedMonoBehaviour {

	#region Enum

        private enum AutoRotateState { Off, On }

	#endregion

	#region Public

        public Vector3 _angleOff;
        public Vector3 _angleOn;

        public AnimationCurve _off_on;
        public AnimationCurve _on_off;

	#endregion

	#region Private

        private AutoRotateState m_state;

        private Vector3 m_source;
        private Vector3 m_destination;

        private AnimationCurve m_curve;

        private Timer m_timer;

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();

            transform.localRotation = Quaternion.Euler(_angleOff);
            m_state = AutoRotateState.Off;
		}

		void Update ()
		{
            if (m_timer != null)
            {
                if (m_timer.IsElapsedLoop == false)
                {
                    transform.localRotation = Quaternion.Euler(Vector3.Lerp(m_source, m_destination, m_curve.Evaluate(m_timer.Current)));
                }
                else
                {
                    m_timer = null;
                    if (m_state == AutoRotateState.Off)
                    {
                        m_state = AutoRotateState.On;
                        GetComponentInChildren<Tool>()._couldBeGrabbed = true;

                        if(gameObject.name == "bras_couteau")
                        {
                            int rand = Random.Range(0, 100);
                            if(rand < 50)
                            {
                                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_IngredientCut", references._fightingTable._pirateLeft.gameObject);
                            }
                            else
                            {
                                SdAudioManager.Trigger("Boss_Mama_NewIngredient", references._mama.gameObject);
                            }
                        }
                        if (gameObject.name == "bras_rouleau")
                        {
                            int rand = Random.Range(0, 100);
                            if (rand < 50)
                            {
                                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_IngredientRoll", references._fightingTable._pirateLeft.gameObject);
                            }
                            else
                            {
                                SdAudioManager.Trigger("Boss_Mama_NewIngredient", references._mama.gameObject);
                            }
                        }
                    }
                    else if (m_state == AutoRotateState.On) m_state = AutoRotateState.Off;
                }
            }

            if(Input.GetKeyDown(KeyCode.T) == true)
            {
                SwitchOn();
            }
		}

	#endregion

	#region Custom

        public void SwitchOn()
        {
            if (m_state == AutoRotateState.Off)
            {
                m_source = _angleOff;
                m_destination = _angleOn;
                m_curve = _off_on;
                SdAudioManager.Trigger("ToolBox_Open", gameObject);
                Switch();
            }
        }

        public void SwitchOff()
        {
            if (m_state == AutoRotateState.On)
            {
                m_source = _angleOn;
                m_destination = _angleOff;
                m_curve = _on_off;
                SdAudioManager.Trigger("ToolBox_Close", gameObject);
                Switch();
            }
        }

        private void Switch()
        {
            m_timer = new Timer(m_curve.keys[m_curve.length - 1].time);

            if (m_destination.x - m_source.x > 180)
            {
                m_destination.x -= 360;
            }
            else if (m_destination.x - m_source.x < -180)
            {
                m_destination.x += 360;
            }

            if (m_destination.y - m_source.y > 180)
            {
                m_destination.y -= 360;
            }
            else if (m_destination.y - m_source.y < -180)
            {
                m_destination.y += 360;
            }

            if (m_destination.z - m_source.z > 180)
            {
                m_destination.z -= 360;
            }
            else if (m_destination.z - m_source.z < -180)
            {
                m_destination.z += 360;
            }
        }

	#endregion
	
}
