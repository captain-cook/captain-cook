﻿using UnityEngine;
using System.Collections;

public class TimerStartRecipe : ReferencedMonoBehaviour {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();
		}

		void Update ()
		{

		}

	#endregion

	#region Custom

        public IEnumerator Timer()
        {
            yield return new WaitForSeconds(1f);
            SdAudioManager.Trigger("UI_RecipeCountdown");
            references._recipeTimer.enabled = true;
            references._countDownCooking.Play("3");
            yield return new WaitForSeconds(1f);
            references._countDownCooking.Play("2");
            yield return new WaitForSeconds(1f);
            references._countDownCooking.Play("1");
            yield return new WaitForSeconds(1f);
            references._countDownCooking.Play("Cook!");
            references._phaseManager._actualPhase._customHandControllerLeft.enabled = true;
            references._phaseManager._actualPhase._customHandControllerRight.enabled = true;
            references._recipeManager.StartTimer();
            references._basketManager.Activate(true);
            references._mama.SetBool("Cook", true);

            yield return new WaitForSeconds(0.5f);
            SdAudioManager.Trigger("Boss_Mama_BeginCook", references._mama.gameObject);

            yield return new WaitForSeconds(0.5f);
            references._countDownCooking.Play("");
            SdAudioManager.Trigger("Music_Battle_Idle_2");
        }

	#endregion
	
}
