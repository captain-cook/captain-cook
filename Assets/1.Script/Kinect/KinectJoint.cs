﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KinectJoint : MonoBehaviour {

    public Quaternion Orientation;

    private List<Vector3> HistoryRawPosition;
    private List<Quaternion> HistoryRawRotation;

    public int HistoryLength;

    void Start()
    {
        HistoryRawPosition = new List<Vector3>();
        HistoryRawRotation = new List<Quaternion>();
    }

    public Vector3 AverageHistoryPosition
    {
        get
        {
            Vector3 position = Vector3.zero;
            for (int i = 0; i < HistoryRawPosition.Count; i++)
            {
                position += HistoryRawPosition[i];
            }
            position /= HistoryRawPosition.Count;
            return position;
        }
    }

    public Quaternion AverageHistoryRotation
    {
        get
        {
            Quaternion rotation = HistoryRawRotation[0];
            float coef = 0.5f;
            for (int i = 1; i < HistoryRawRotation.Count; i++)
            {
                rotation = Quaternion.Slerp(rotation, HistoryRawRotation[i], coef);
                coef *= 0.5f;
            }
            return rotation;
        }
    }

    public void AddHistoryRawPosition(Vector3 position)
    {
        HistoryRawPosition.Add(position);
        if (HistoryRawPosition.Count > HistoryLength)
        {
            HistoryRawPosition.RemoveAt(0);
        }
    }
    
    public void AddHistoryRawRotation(Quaternion rotation)
    {
        HistoryRawRotation.Add(rotation);
        if (HistoryRawRotation.Count > HistoryLength)
        {
            HistoryRawRotation.RemoveAt(0);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        for (int i = 0; i < transform.childCount; i++)
        {
            Gizmos.DrawLine(transform.position, transform.GetChild(i).position);
        }
    }

}
