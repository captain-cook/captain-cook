﻿using UnityEngine;
using System.Collections;
using System;

public class Phase  {

    public PhaseManager.CamPosition _camPosition; 
    public CustomHandController _customHandControllerLeft;
    public CustomHandController _customHandControllerRight;
    public References _references;

    public Phase ()
    {
        _references = GameObject.Find("References").GetComponent<References>();
    }

    //launch when entering in the phase before the switchCamera
    public virtual void EarlyEnter()
    {

    }

    //launch when entering in the phase after the swhitcamera
    public virtual void Enter()
    {

    }

    //launch when exiting in the phase before the switchCamera
    public virtual void EarlyExit()
    {

    }

    //launch when exiting in the phase after the swhitcamera
    public virtual void Exit()
    {

    }

    
}
