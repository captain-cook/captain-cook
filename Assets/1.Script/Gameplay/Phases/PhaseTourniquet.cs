﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseTourniquet : Phase  {

   public PhaseTourniquet()
    {
        _camPosition = PhaseManager.CamPosition.Right;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerTourniquet>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerTourniquet>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
        _references._handCamera.enabled = true;
        Camera.main.cullingMask ^= 4096;

        SdAudioManager.Trigger("RecipeWheel_Wheel",_references._tourniquet.gameObject);

        _references._tourniquet._isGrab = false;
        
    }

    public override void Exit()
    {
        _references._handCamera.enabled = false;
        Camera.main.cullingMask |= 4096;

        SdAudioManager.Trigger("RecipeWheel_Stop", _references._tourniquet.gameObject);

        //update and unroll the recipeSumUp
        _references._recipeSumUp.LoadRecipeSumUp();

    }

    public override void EarlyEnter()
    {
        _references._tourniquet.InitTscroll();
        _references._recipePanel.gameObject.SetActive(false); 
    }

    public override void EarlyExit()
    {
        _references._fightingTable.SetPirateFromNextIndex();
        _references._fightingTable.gameObject.SetActive(true);
        _references._binomialManager.ActivateWaitingBinomial();
    }
}
