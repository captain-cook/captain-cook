﻿using UnityEngine;
using System.Collections;

public class ReferencedMonoBehaviour : MonoBehaviour {

	#region Private

        protected References references;

	#endregion

	#region Unity

		public void LoadReferences()
		{
            references = GameObject.Find("References").GetComponent<References>();
		}

	#endregion

	#region Custom

	#endregion

}
