﻿using UnityEngine;
using System.Collections;

public class LabelUIPhase1 : ReferencedMonoBehaviour {

    //phasemanager to switch phase
    private LabelManager m_labelManager;
    private HandController m_hand;

    //draging variables
    public bool _isLeftLabel;
    public float _dragLimit = 0.35f;
    private Vector2 m_startPosition;
    private float m_handStartPositon;

    //activate variables
    private TweenPosition m_tPosition;
    private string m_nextIcon;
    public TweenAlpha _tAlphaIcon;
    

    //state value 
    private bool m_isHover;
    private bool m_isDraged;
    private bool m_isFading = false;

	// Use this for initialization
	void Start () {

        base.LoadReferences();

        m_labelManager = references._labelManager;
      
        m_startPosition = transform.position;
        m_tPosition = GetComponent<TweenPosition>();

        _tAlphaIcon.duration = references._phaseManager._moveSpeed * 0.5f;

        m_tPosition.PlayReverse();
	}
	
	// Update is called once per frame
	void Update () {

        //drop the label
        if (m_isDraged && m_hand.GetHandState() == KinectBody.HandState.Open)
        {
            m_isDraged = false;
            m_tPosition.PlayReverse();
        }
        //drag the label
        if (m_isDraged)
        {
            Drag();
        }        
        else if (m_isHover  && m_hand.GetHandState() == KinectBody.HandState.Closed)
        {
            StartDrag(true);
        }
	}

    //follow the hand on X axis
    void Drag()
    {
        float handMove = 0;
        Vector2 newPosition = m_startPosition;

        //set the stepValue to the left 
        if (_isLeftLabel)
        {
            handMove = m_hand.transform.position.x - m_handStartPositon;
            newPosition.x += handMove;
        }
        //set the stepValue to the right
        else
        {
            handMove = m_handStartPositon - m_hand.transform.position.x;
            newPosition.x -= handMove;
        }

        //draging
        if (handMove > 0)
        {
            transform.position = newPosition;

            //move is ok, switch phase
            if (handMove > _dragLimit )
            {
                m_labelManager.Switch(_isLeftLabel);
                m_isDraged = false;
                m_tPosition.PlayReverse();
            }
        }       
    }

    //activate the  dragging
    public void StartDrag(bool start)
    {
        m_isDraged = start;
        if (start)
        {
            m_handStartPositon = m_hand.transform.position.x;
        }
    }

    public void Fade(string nextIconName)
    {
        if (!m_isFading)
        {
            m_isFading = true;
            m_nextIcon = nextIconName;
            _tAlphaIcon.PlayForward();
        }        
    }

    public void EndOfFade()
    {
        if (m_isFading)
        {
            m_isFading = false;
            _tAlphaIcon.GetComponent<UISprite>().spriteName = m_nextIcon;
            _tAlphaIcon.PlayReverse();
        }        
    }

    //launch the tween
    public void LaunchTween(bool playForward)
    {
        m_isHover = playForward;

        if (!m_isDraged)
        {
            if (playForward)
            {
                m_tPosition.PlayForward();   
            }
            else
            {
                m_tPosition.PlayReverse();
            }
        }
    }

    public void SetHandActive(HandController hand)
    {
        m_hand = hand;
    }
}
