﻿using UnityEngine;
using System.Collections;

public class SdToolbox : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        switch (action)
        {
            case "Open":
            case "Close":
                OpenClose(action, source);
                break;
            default:
                break;
        }
    }

    void OpenClose(string action,GameObject source = null)
    {
        string goTag = source.tag;
        SdAudioManager.SetSwitch("Toolbox_State", action, source);
        if (goTag == "ToolboxKnife")
        {
            SdAudioManager.SetSwitch("Toolbox", "Knife",source);
        }
        else
        {
            SdAudioManager.SetSwitch("Toolbox", "Roller", source);
        }
        SdAudioManager.Play("Toolbox", source);
    }
}


