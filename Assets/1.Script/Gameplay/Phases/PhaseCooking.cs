﻿using UnityEngine;
using System.Collections;

public class PhaseCooking : Phase {

    public PhaseCooking()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerCooking>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerCooking>();
    }
	
    public override void Enter()
    {
        base.Enter();

        Toolbox[] tb = _references._toolboxes;
        for (int i = 0; i < tb.Length; i++)
		{
            if(_references._recipeManager._currentStep._action == SORecipe.Interaction.Cut)
            {
                if (tb[i].tag == "ToolboxKnife") tb[i].SwitchOn();
            }
            if (_references._recipeManager._currentStep._action == SORecipe.Interaction.Spread)
            {
                if (tb[i].tag == "ToolboxRoller") tb[i].SwitchOn();
            }
		}
        
    }

    public override void Exit()
    {

    }

    public override void EarlyEnter()
    {
        
    }

    public override void EarlyExit()
    {

    }
}
