﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Collections.Generic;

public class KinectSensor : MonoBehaviour
{

    public int _portReceive = 5555;
    public UdpClient _client;

    public KinectBody _body;

    public bool _smooth = true;
    public int _smoothLevel = 10;
    private int m_oldSmoothLevel;

    public enum KinectSkeletonJointType { SpineBase, SpineMid, Neck, Head, ShoulderLeft, ElbowLeft, WristLeft, HandLeft, ShoulderRight, ElbowRight, WristRight, HandRight, HipLeft, KneeLeft, AnkleLeft, FootLeft, HipRight, KneeRight, AnkleRight, FootRight, SpineShoulder, HandTipLeft, ThumbLeft, HandTipRight, ThumbRight }

    public string _separatorValues = ";";
    public string _separatorBodies = "!";

    public float _ratio = 2f;

    enum Value { Type, X, Y, Z }

    void Start()
    {
        _client = new UdpClient(_portReceive);
        _client.Client.ReceiveBufferSize = 4096;
        _client.Client.ReceiveTimeout = 1;

        RefreshSmoothLevel();
    }

    void Update()
    {
        //Update Smooth Level
        if (_smoothLevel != m_oldSmoothLevel)
        {
            RefreshSmoothLevel();
        }

        try
        {
            IPEndPoint source = new IPEndPoint(IPAddress.Any, _portReceive);
            string message = toString(_client.Receive(ref source));
            //Sample
            //message = "72057594037929714;1;0;0;-0.7109362;-0.4246193;1.275553;0.03314777;0.9807159;-0.1924531;0.00770424;1;-0.6895678;-0.1342719;1.157062;0.03621731;0.9807138;-0.1918991;-0.007960299;2;-0.6578785;0.1512809;1.022338;0.06746522;0.9700705;-0.2323534;-0.02057843;3;-0.7014171;0.1901137;0.9518948;0;0;0;0;4;-0.7713344;0.03017659;1.032254;0.824707;-0.5335289;0.1872359;-0.01216496;5;-0.8375326;-0.2205915;1.116112;0.9305157;-0.1668628;-0.3068109;0.1102922;6;-0.7596112;-0.4240108;1.08254;0.9483296;0.1546076;0.2492761;-0.1209499;7;-0.7810757;-0.4670865;1.055527;0.9265358;-0.2513911;0.1782662;-0.2157667;8;-0.5670603;0.07530075;0.9960097;0.6328968;0.6884388;-0.3527809;-0.03223693;9;-0.4381361;-0.08020049;0.8677207;0.7996895;0.1212231;0.4306014;-0.4004798;10;-0.3824086;-0.2249016;0.625105;0.2886741;-0.4211329;0.8172769;-0.267157;11;-0.3582485;-0.248346;0.5789996;0.2311647;-0.3928925;0.8074019;-0.3745672;12;-0.7852154;-0.4238708;1.247397;-0.6549041;0.7103742;-0.2576625;-0.008887561;13;-0.7691894;-0.5710192;0.8260899;0.6027932;0.4058573;-0.549;-0.4129398;14;-0.7646078;-0.7172008;0.4411163;0.6036037;0.3910292;-0.559594;-0.4118414;15;-0.7508929;-0.7341325;0.3203275;0;0;0;0;16;-0.6019178;-0.4054261;1.240356;0.6094067;0.7630334;-0.2144894;0.0199449;17;-0.5039116;-0.8058645;1.228857;0.8358487;0.1083548;-0.5355685;0.05274956;18;-0.4495885;-0.4987257;0.9656565;0.2946512;0.659159;-0.192076;-0.664678;19;-0.4420482;-0.4890529;0.8220331;0;0;0;0;20;-0.6675059;0.08122155;1.058329;0.05245968;0.9757443;-0.2112912;-0.02295573;21;-0.777712;-0.4674841;1.053207;0;0;0;0;22;-0.7603523;-0.4463688;1.030344;0;0;0;0;23;-0.311799;-0.2716664;0.521116;0;0;0;0;24;-0.3332576;-0.2550739;0.5874286;0;0;0;0;!72057594037929792;0;0;0;0.07602344;-0.4382632;1.766882;-0.01116056;0.9247133;0.05391278;-0.376662;1;0.08130401;-0.1754825;1.795466;-0.011993;0.9187775;0.05373373;-0.3909177;2;0.08586751;0.08020151;1.811345;0.001495471;0.9136597;0.0166435;-0.4061365;3;0.06218505;0.2015297;1.81861;0;0;0;0;4;-0.05742371;-0.04123375;1.921103;0.7512872;-0.5644132;-0.298929;0.166273;5;-0.110877;-0.3141238;1.918152;0.709318;-0.06507438;-0.6982232;-0.07153704;6;-0.1497578;-0.5206771;1.810792;0.6808647;-0.2268323;0.6884853;-0.1046821;7;-0.1574799;-0.565519;1.772974;0.6513808;-0.2935327;0.6742804;-0.1867818;8;0.1629416;-0.01770003;1.673205;0.7000819;0.5186393;-0.34405;-0.3500402;9;0.1721858;-0.2111762;1.582089;0.9733586;0.007083091;0.06707185;-0.219144;10;0.05319842;-0.3945006;1.477559;0.8138643;-0.341687;0.4647368;-0.06996204;11;-0.01330916;-0.4531265;1.43274;0.777905;-0.4511613;0.4358112;-0.03722712;12;0.02727153;-0.4406754;1.78187;0.7086378;-0.6871266;-0.1464672;0.06508996;13;-0.03830588;-0.7925562;1.764657;-0.3793263;0.01271901;0.9203702;0.09417268;14;-0.02330756;-1.093592;1.775123;-0.3903553;0.006264395;0.9201645;-0.02968023;15;-0.1051019;-1.131577;1.689917;0;0;0;0;16;0.1219613;-0.4205509;1.691133;0.5629518;0.6602235;-0.291646;-0.402657;17;0.1424922;-0.7064201;1.618027;0.9120103;-0.01672629;0.3892038;-0.1283664;18;0.145526;-1.081469;1.552336;0.9029587;-0.03295668;0.420895;-0.08016805;19;0.07106292;-1.100624;1.458735;0;0;0;0;20;0.0848433;0.01721531;1.809504;-0.006397927;0.9130687;0.03693723;-0.4060794;21;-0.155968;-0.6168296;1.739704;0;0;0;0;22;-0.1026129;-0.5509476;1.7714;0;0;0;0;23;-0.0542585;-0.5045913;1.401975;0;0;0;0;24;-0.009347457;-0.4828706;1.398373;0;0;0;0;!";
            toSkeleton(message);
        }
        catch (SocketException e)
        {
            if(e == null) { }
        }
    }

    string toString(byte[] message)
    {
        return Encoding.ASCII.GetString(message).Replace(',', '.');
    }

    void RefreshSmoothLevel()
    {
        KinectJoint[] joints = _body.GetComponentsInChildren<KinectJoint>();

        foreach (KinectJoint joint in joints)
        {
            joint.HistoryLength = _smoothLevel;
        }
        m_oldSmoothLevel = _smoothLevel;
    }

    public void toSkeleton(string message)
    {
        _body.transform.localScale = new Vector3(1, 1, 1);

        string[] bodies = message.Split(_separatorBodies.ToCharArray()[0]);

        for (int i = 0; i < bodies.Length; i++)
        {
            if (i > 0)
                break;
            if(bodies[i] != string.Empty)
            {
                string[] values = bodies[i].Split(_separatorValues.ToCharArray()[0]);
                List<string> valuesList = new List<string>(values);

                long trackingID = long.Parse(valuesList[0]);
                valuesList.RemoveAt(0);

                KinectBody.HandState leftState = (KinectBody.HandState)int.Parse(valuesList[0]);
                _body.LeftHandState = leftState;
                valuesList.RemoveAt(0);

                KinectBody.HandState rightState = (KinectBody.HandState)int.Parse(valuesList[0]);
                _body.RightHandState = rightState;
                valuesList.RemoveAt(0);

                int valueCount = 8;
                int jointCount = values.Length / valueCount;

                for (int j = 0; j < jointCount; j++)
                {
                    int index = j * valueCount;

                    KinectSkeletonJointType type = (KinectSkeletonJointType)(int.Parse(valuesList[index]));

                    Transform joint = _body.transform.SearchChild(type.ToString());
                    KinectJoint kinectJoint = joint.GetComponent<KinectJoint>();


                    float x = float.Parse(valuesList[index + 1]);
                    float y = float.Parse(valuesList[index + 2]);
                    float z = float.Parse(valuesList[index + 3]);

                    Vector3 position = new Vector3(x, y, z);
                    kinectJoint.AddHistoryRawPosition(position);


                    float a = float.Parse(valuesList[index + 4]);
                    float b = float.Parse(valuesList[index + 5]);
                    float c = float.Parse(valuesList[index + 6]);
                    float d = float.Parse(valuesList[index + 7]);

                    Quaternion rotation = new Quaternion(a, b, c, d);
                    kinectJoint.AddHistoryRawRotation(rotation);

                    if (_smooth == true)
                    {
                        joint.position = kinectJoint.AverageHistoryPosition * _ratio;
                        joint.rotation = kinectJoint.AverageHistoryRotation;
                    }
                    else
                    {
                        joint.position = (position * _ratio);
                        joint.rotation = rotation;
                    }
                }
            }
        }

        _body.transform.GetChild(0).position = Vector3.zero;
        _body.transform.localScale = new Vector3(1, 1, -1);

        _body.TriggerDirty();

    }

}
