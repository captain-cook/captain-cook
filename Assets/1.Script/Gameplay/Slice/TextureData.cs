﻿using UnityEngine;
using System.Collections;

public class TextureData : MonoBehaviour {

    public Vector2 _topLeft;
    public Vector2 _bottomRight;

    public Vector2 Center
    {
        get
        {
            return (_topLeft + _bottomRight) / 2f;
        }
    }

    public Vector2 UV(Vector2 normalized)
    {
        return new Vector2(
            _topLeft.x + (_bottomRight.x - _topLeft.x) * normalized.x,
            _topLeft.y + (_bottomRight.y - _topLeft.y) * normalized.y);
    }

}
