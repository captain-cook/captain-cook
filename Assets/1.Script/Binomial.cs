﻿using UnityEngine;
using System.Collections;

public class Binomial : ReferencedMonoBehaviour {

    public Transform _pirate1;
    public Transform _pirate2;

    public SOPirate _tastePirate1;
    public SOPirate _tastePirate2;

    public bool _ate = false;
    public TastePanel _tastePanel;


    void Awake()
    {
        _tastePanel = GetComponentInChildren<TastePanel>();
    }

	// Use this for initialization
	void Start () {
        
        base.LoadReferences();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //update the position and the content of the tasteUI
    public void SetTasteUI()
    {
        if ( ! _ate )
        {
            _tastePanel.gameObject.SetActive(true);
            _tastePanel._tasteIconLeft.sprite = _tastePirate1._tasteTemp._image;
            _tastePanel._tasteIconRight.sprite = _tastePirate2._tasteTemp._image;
        }        
    }

    //function called after punching the linked button and switch to tourniquet mode
    public void BinomialSelected()
    {
        _ate = true;
        references._tourniquet._tastePirate1 = _tastePirate1;
        references._tourniquet._tastePirate2 = _tastePirate2;

        SetAnimation(2);
        StartCoroutine("WaitForSwitchPhase");
        
    }

    IEnumerator WaitForSwitchPhase()
    {
        yield return new WaitForSeconds(2);
        references._phaseManager.SwitchPhase("PhaseTourniquet");
    }

    public void Disable()
    {
        if (_ate )
        {
            _pirate1.gameObject.SetActive(false);
            _pirate2.gameObject.SetActive(false);
        }        
    }

    /*change the pirates animation to feedback if they're selected
     * 0 = idle
     * 1 = onhover
     * 2 = validate
     */
    public void SetAnimation(int numAnim)
    {
        switch (numAnim)
        {
            case 0 :
                _pirate1.GetComponent<Animator>().SetTrigger("Reset");
                _pirate2.GetComponent<Animator>().SetTrigger("Reset");
                break;

            case 1 :
                _pirate1.GetComponent<Animator>().SetTrigger("Selection");
                _pirate2.GetComponent<Animator>().SetTrigger("Selection");
                break;

            case 2 :
                _pirate1.GetComponent<Animator>().SetTrigger("Validate");
                _pirate2.GetComponent<Animator>().SetTrigger("Validate");
                break;
        }
    }
}
