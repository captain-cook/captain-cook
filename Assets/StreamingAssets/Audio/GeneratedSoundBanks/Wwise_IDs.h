/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID _3DTEST = 588255985U;
        static const AkUniqueID AMB_BOAT = 1588297844U;
        static const AkUniqueID AMB_BOAT_FADEOUT = 2286143883U;
        static const AkUniqueID AMB_SHOP = 197734838U;
        static const AkUniqueID AMB_WEATHER_CALM_DW_RAINY = 2002559430U;
        static const AkUniqueID AMB_WEATHER_CALM_DW_SUNNY = 162290800U;
        static const AkUniqueID AMB_WEATHER_CALM_DW_WINDY = 3143944362U;
        static const AkUniqueID AMB_WEATHER_CALM_SB_RAINY = 1470567138U;
        static const AkUniqueID AMB_WEATHER_CALM_SB_SUNNY = 1979025724U;
        static const AkUniqueID AMB_WEATHER_CALM_SB_WINDY = 3798977302U;
        static const AkUniqueID AMB_WEATHER_STORM = 1177288372U;
        static const AkUniqueID BATTLEMUSIC = 762511844U;
        static const AkUniqueID BOAT_SAIL = 759347957U;
        static const AkUniqueID BUTTON = 977454165U;
        static const AkUniqueID CANNONSHOOTDIST = 513450039U;
        static const AkUniqueID CAROUSSEL = 4010898548U;
        static const AkUniqueID COOKING = 2449017287U;
        static const AkUniqueID COUNTTOOL = 797919056U;
        static const AkUniqueID CREW = 4136758402U;
        static const AkUniqueID DEMOEND = 1195087571U;
        static const AkUniqueID EGG = 680725036U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID FOOTSTEPSPLAYER = 3865216745U;
        static const AkUniqueID INGREDIENTFALL = 3965951971U;
        static const AkUniqueID INGREDIENTREWARD = 1659031005U;
        static const AkUniqueID INGREXPLOSION = 1375952820U;
        static const AkUniqueID JINGLES = 843017309U;
        static const AkUniqueID JURYSUSPENS = 1495483576U;
        static const AkUniqueID KNIFE = 3312069844U;
        static const AkUniqueID MAP = 1048449605U;
        static const AkUniqueID MOREYFALL = 22889124U;
        static const AkUniqueID OVEN = 3677122683U;
        static const AkUniqueID OVENIDLE = 1403328813U;
        static const AkUniqueID PIEFALL = 2836483446U;
        static const AkUniqueID PIRATEFEEDBACKS = 920727014U;
        static const AkUniqueID PLANCHA = 2694194474U;
        static const AkUniqueID PLANCHAWISTLE = 1429749810U;
        static const AkUniqueID PLATE = 282449107U;
        static const AkUniqueID RECIPE = 3952605219U;
        static const AkUniqueID RECIPEBOARD = 1117366685U;
        static const AkUniqueID RESULTS = 3780578133U;
        static const AkUniqueID REWARDS = 3894118027U;
        static const AkUniqueID ROLLER = 4054296493U;
        static const AkUniqueID SBIRES = 3436440407U;
        static const AkUniqueID SEEKER = 1703490104U;
        static const AkUniqueID SFXBOAT = 2899737624U;
        static const AkUniqueID SHOP = 251412225U;
        static const AkUniqueID TOOLBOX = 2792017586U;
        static const AkUniqueID UI = 1551306167U;
        static const AkUniqueID UIANCHOR = 120330124U;
        static const AkUniqueID VFX_UI = 2629175008U;
        static const AkUniqueID VFXBOSS = 1549807708U;
        static const AkUniqueID WAITINGFORROUND = 2906693421U;
        static const AkUniqueID WICKERBASKET = 1340235568U;
        static const AkUniqueID WOKFALLEND = 456238260U;
        static const AkUniqueID WOKFALLSTART = 1797649447U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace PIRATE_BEHAVIOUR_COMBAT
        {
            static const AkUniqueID GROUP = 1729018809U;

            namespace STATE
            {
                static const AkUniqueID ATTACK = 180661997U;
                static const AkUniqueID HELP = 3632828356U;
                static const AkUniqueID HURT = 3193947170U;
                static const AkUniqueID KO = 1719082483U;
                static const AkUniqueID PROVOKE = 691317703U;
                static const AkUniqueID WIN = 979765101U;
            } // namespace STATE
        } // namespace PIRATE_BEHAVIOUR_COMBAT

        namespace PIRATE_BEHAVIOUR_NORMAL
        {
            static const AkUniqueID GROUP = 3489612188U;

            namespace STATE
            {
                static const AkUniqueID FIGHT = 514064485U;
                static const AkUniqueID IDLE = 1874288895U;
                static const AkUniqueID JOKE = 3728748628U;
                static const AkUniqueID SING = 805220764U;
            } // namespace STATE
        } // namespace PIRATE_BEHAVIOUR_NORMAL

        namespace PIRATE_BEHAVIOUR_WORK
        {
            static const AkUniqueID GROUP = 1453254850U;

            namespace STATE
            {
                static const AkUniqueID GOOD_MOOD = 2556020212U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID SERIOUS = 4181648385U;
            } // namespace STATE
        } // namespace PIRATE_BEHAVIOUR_WORK

    } // namespace STATES

    namespace SWITCHES
    {
        namespace BOAT_BEHAVIOUR_SWITCH
        {
            static const AkUniqueID GROUP = 3135643266U;

            namespace SWITCH
            {
                static const AkUniqueID BOAT_BEHAVIOUR_CALM = 2229279479U;
                static const AkUniqueID BOAT_BEHAVIOUR_MODERATE = 4171322941U;
                static const AkUniqueID BOAT_BEHAVIOUR_STRONG = 4214184219U;
            } // namespace SWITCH
        } // namespace BOAT_BEHAVIOUR_SWITCH

        namespace BOAT_POSITION
        {
            static const AkUniqueID GROUP = 463490187U;

            namespace SWITCH
            {
                static const AkUniqueID DEEP_WATERS = 266954758U;
                static const AkUniqueID SHALLOW_BAYS = 1462134913U;
            } // namespace SWITCH
        } // namespace BOAT_POSITION

        namespace BOAT_STATE
        {
            static const AkUniqueID GROUP = 2731622425U;

            namespace SWITCH
            {
                static const AkUniqueID MOOR = 3128647854U;
                static const AkUniqueID SAIL = 887725902U;
            } // namespace SWITCH
        } // namespace BOAT_STATE

        namespace BOSSACTION
        {
            static const AkUniqueID GROUP = 2526587780U;

            namespace SWITCH
            {
                static const AkUniqueID ABORTTAUNT = 3872423899U;
                static const AkUniqueID BEGINCOOK = 893094588U;
                static const AkUniqueID FINISHED = 3683254051U;
                static const AkUniqueID LOOSER = 598450249U;
                static const AkUniqueID MAPTAUNT = 3255394967U;
                static const AkUniqueID NEWINGREDIENT = 2937918864U;
                static const AkUniqueID ROUNDLOSE = 568636112U;
                static const AkUniqueID ROUNDWIN = 3391830779U;
                static const AkUniqueID SERVING = 4294629123U;
                static const AkUniqueID STARTRECIPE = 4167601349U;
                static const AkUniqueID WINNER = 3035999688U;
            } // namespace SWITCH
        } // namespace BOSSACTION

        namespace BOSSARRIVAL
        {
            static const AkUniqueID GROUP = 4212712537U;

            namespace SWITCH
            {
                static const AkUniqueID PIRATESARRIVAL = 3037956620U;
                static const AkUniqueID VERSUS = 1976107535U;
            } // namespace SWITCH
        } // namespace BOSSARRIVAL

        namespace BOSSNAME
        {
            static const AkUniqueID GROUP = 3441059239U;

            namespace SWITCH
            {
                static const AkUniqueID LEEROY = 2255038093U;
                static const AkUniqueID MAMA = 3094798569U;
            } // namespace SWITCH
        } // namespace BOSSNAME

        namespace COMBATMUSICLEVEL
        {
            static const AkUniqueID GROUP = 972171208U;

            namespace SWITCH
            {
                static const AkUniqueID LEVEL0 = 2678230383U;
                static const AkUniqueID LEVEL1 = 2678230382U;
                static const AkUniqueID LEVEL2 = 2678230381U;
                static const AkUniqueID LEVEL3 = 2678230380U;
                static const AkUniqueID LEVEL4 = 2678230379U;
            } // namespace SWITCH
        } // namespace COMBATMUSICLEVEL

        namespace COMBATSTATUS
        {
            static const AkUniqueID GROUP = 3629971499U;

            namespace SWITCH
            {
                static const AkUniqueID IDLE = 1874288895U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID SPEED = 640949982U;
            } // namespace SWITCH
        } // namespace COMBATSTATUS

        namespace COUNTTOOL
        {
            static const AkUniqueID GROUP = 797919056U;

            namespace SWITCH
            {
                static const AkUniqueID CLOSE = 1451272583U;
                static const AkUniqueID OPEN = 3072142513U;
            } // namespace SWITCH
        } // namespace COUNTTOOL

        namespace CREW_BEHAVIOUR_SWITCH
        {
            static const AkUniqueID GROUP = 1872503841U;

            namespace SWITCH
            {
                static const AkUniqueID CREW_BEHAVIOUR_CALM = 3910871840U;
                static const AkUniqueID CREW_BEHAVIOUR_FIGHTING = 3481913543U;
                static const AkUniqueID CREW_BEHAVIOUR_SLEEPING = 3561482414U;
                static const AkUniqueID CREW_BEHAVIOUR_WORKING_HARD = 4223967622U;
                static const AkUniqueID CREW_BEHAVIOUR_WORKING_SOFT = 2639989049U;
            } // namespace SWITCH
        } // namespace CREW_BEHAVIOUR_SWITCH

        namespace CREWACTIONS
        {
            static const AkUniqueID GROUP = 3001863327U;

            namespace SWITCH
            {
                static const AkUniqueID FEEDBACKNEGATIVE = 2271052049U;
                static const AkUniqueID FEEDBACKPOSITIVE = 2508732829U;
                static const AkUniqueID FINALDISH = 4070292879U;
                static const AkUniqueID FINISHEDCOOKING = 2647580705U;
                static const AkUniqueID INGREDIENTBREAK = 3499501269U;
                static const AkUniqueID INGREDIENTCUT = 2362407904U;
                static const AkUniqueID INGREDIENTROLL = 1315082985U;
                static const AkUniqueID PLANCHABUTTON = 1661737386U;
                static const AkUniqueID PLAYERPOINTING = 998971368U;
                static const AkUniqueID RECIPESELECTED = 2370528392U;
                static const AkUniqueID SELECTDISHOVEN = 3008712639U;
                static const AkUniqueID SELECTDISHPLANCHA = 615781878U;
                static const AkUniqueID SELECTED = 363882074U;
                static const AkUniqueID VOTENEGATIVE = 1275253296U;
                static const AkUniqueID VOTEPOSITIVE = 3727103960U;
                static const AkUniqueID WAITFORRECIPE = 2734103501U;
            } // namespace SWITCH
        } // namespace CREWACTIONS

        namespace CREWNAME
        {
            static const AkUniqueID GROUP = 1788074503U;

            namespace SWITCH
            {
                static const AkUniqueID ARNAUD = 1944539264U;
                static const AkUniqueID MINGYAO = 4050332423U;
                static const AkUniqueID RODNY = 1376873287U;
            } // namespace SWITCH
        } // namespace CREWNAME

        namespace DYNAMIC_DIALOG
        {
            static const AkUniqueID GROUP = 232541975U;

            namespace SWITCH
            {
            } // namespace SWITCH
        } // namespace DYNAMIC_DIALOG

        namespace EGG
        {
            static const AkUniqueID GROUP = 680725036U;

            namespace SWITCH
            {
                static const AkUniqueID BREAK = 941442534U;
                static const AkUniqueID FALL = 2512384458U;
            } // namespace SWITCH
        } // namespace EGG

        namespace ENDROUND
        {
            static const AkUniqueID GROUP = 2295455204U;

            namespace SWITCH
            {
                static const AkUniqueID DEFEAT = 1593864692U;
                static const AkUniqueID LOOP = 691006007U;
                static const AkUniqueID VICTORY = 2716678721U;
            } // namespace SWITCH
        } // namespace ENDROUND

        namespace FOOTSTEPS
        {
            static const AkUniqueID GROUP = 2385628198U;

            namespace SWITCH
            {
                static const AkUniqueID END = 529726532U;
                static const AkUniqueID FALL = 2512384458U;
                static const AkUniqueID FOOT = 3031504797U;
                static const AkUniqueID START = 1281810935U;
            } // namespace SWITCH
        } // namespace FOOTSTEPS

        namespace JINGLES
        {
            static const AkUniqueID GROUP = 843017309U;

            namespace SWITCH
            {
                static const AkUniqueID BOSSCOMING = 883730187U;
                static const AkUniqueID ENDRECIPE = 3709942958U;
                static const AkUniqueID ENDROUND = 2295455204U;
                static const AkUniqueID PIRATESARRIVAL = 3037956620U;
                static const AkUniqueID VERSUS = 1976107535U;
            } // namespace SWITCH
        } // namespace JINGLES

        namespace JURYSUSPENS
        {
            static const AkUniqueID GROUP = 1495483576U;

            namespace SWITCH
            {
                static const AkUniqueID START = 1281810935U;
                static const AkUniqueID STOP = 788884573U;
            } // namespace SWITCH
        } // namespace JURYSUSPENS

        namespace KNIFE_INGREDIENTCUT
        {
            static const AkUniqueID GROUP = 1366982270U;

            namespace SWITCH
            {
                static const AkUniqueID HARD = 3599861390U;
                static const AkUniqueID SOFT = 670602561U;
            } // namespace SWITCH
        } // namespace KNIFE_INGREDIENTCUT

        namespace KNIFE_STATE
        {
            static const AkUniqueID GROUP = 1366765054U;

            namespace SWITCH
            {
                static const AkUniqueID CUT = 647463975U;
                static const AkUniqueID DROP = 1878686274U;
                static const AkUniqueID ENDCUT = 3783318444U;
                static const AkUniqueID PICKUP = 3978245845U;
                static const AkUniqueID WHOOSH = 1323846727U;
                static const AkUniqueID WOODHIT = 4031930149U;
            } // namespace SWITCH
        } // namespace KNIFE_STATE

        namespace MAP
        {
            static const AkUniqueID GROUP = 1048449605U;

            namespace SWITCH
            {
                static const AkUniqueID SELECTBOAT = 2035808255U;
                static const AkUniqueID WHEEL = 2892932786U;
            } // namespace SWITCH
        } // namespace MAP

        namespace OVEN
        {
            static const AkUniqueID GROUP = 3677122683U;

            namespace SWITCH
            {
                static const AkUniqueID CLOSE = 1451272583U;
                static const AkUniqueID OPEN = 3072142513U;
                static const AkUniqueID OVERLOAD = 1841468723U;
                static const AkUniqueID PRESSURE = 1205348660U;
                static const AkUniqueID TICKEND = 1505772553U;
                static const AkUniqueID TICKTACK = 3631298289U;
                static const AkUniqueID TURNOFF = 637426651U;
                static const AkUniqueID TURNON = 2576072551U;
            } // namespace SWITCH
        } // namespace OVEN

        namespace PATTERNS
        {
            static const AkUniqueID GROUP = 4163340556U;

            namespace SWITCH
            {
                static const AkUniqueID PATTERN000 = 2084901031U;
                static const AkUniqueID PATTERN001 = 2084901030U;
                static const AkUniqueID PATTERN010 = 2068123444U;
                static const AkUniqueID PATTERN011 = 2068123445U;
                static const AkUniqueID PATTERN100 = 1984088254U;
                static const AkUniqueID PATTERN101 = 1984088255U;
                static const AkUniqueID PATTERN110 = 2000865809U;
                static const AkUniqueID PATTERN111 = 2000865808U;
            } // namespace SWITCH
        } // namespace PATTERNS

        namespace PIRATE_BEHAVIOUR
        {
            static const AkUniqueID GROUP = 2662812498U;

            namespace SWITCH
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID WORK = 1739275118U;
            } // namespace SWITCH
        } // namespace PIRATE_BEHAVIOUR

        namespace PIRATE_FEEDBACKS
        {
            static const AkUniqueID GROUP = 628170075U;

            namespace SWITCH
            {
                static const AkUniqueID BOATEND = 2926387126U;
                static const AkUniqueID BOATSTART = 3367332509U;
                static const AkUniqueID EATEND = 2073848840U;
                static const AkUniqueID EATLOOP = 3221930707U;
                static const AkUniqueID EATSTART = 2224043827U;
                static const AkUniqueID ENEMYSPOTTED = 3938259894U;
                static const AkUniqueID IDLE01 = 3262002518U;
                static const AkUniqueID IDLE02 = 3262002517U;
                static const AkUniqueID IDLE03 = 3262002516U;
                static const AkUniqueID PLAYERENDRECIPE = 3396117159U;
                static const AkUniqueID PLAYERSELECTKNIFE = 3947239701U;
                static const AkUniqueID PLAYERSELECTPIE = 1946151668U;
                static const AkUniqueID PLAYERSELECTPIRATE = 1020728241U;
                static const AkUniqueID PLAYERSELECTROLLER = 1261261622U;
                static const AkUniqueID PLAYERSELECTTARGET = 861990861U;
                static const AkUniqueID PLAYERSENDTOWOK = 3503198840U;
                static const AkUniqueID PLAYERSTARTRECIPE = 1726257476U;
                static const AkUniqueID PLAYERUSINGKNIFE = 2589172353U;
                static const AkUniqueID PLAYERUSINGROLLER = 678940690U;
                static const AkUniqueID READYTOSEND = 3738543591U;
                static const AkUniqueID SENDINGREDIENT = 4262188894U;
            } // namespace SWITCH
        } // namespace PIRATE_FEEDBACKS

        namespace PIRATE_SELECT
        {
            static const AkUniqueID GROUP = 4271465825U;

            namespace SWITCH
            {
                static const AkUniqueID COMMON = 2395677314U;
                static const AkUniqueID MINGYAO = 4050332423U;
                static const AkUniqueID RODNY = 1376873287U;
                static const AkUniqueID SEEKER = 1703490104U;
            } // namespace SWITCH
        } // namespace PIRATE_SELECT

        namespace PLANCHA
        {
            static const AkUniqueID GROUP = 2694194474U;

            namespace SWITCH
            {
                static const AkUniqueID BUMP = 1796733489U;
                static const AkUniqueID CLOSE = 1451272583U;
                static const AkUniqueID GRILLLOOP = 2359014057U;
                static const AkUniqueID GRILLPUT = 2166434682U;
                static const AkUniqueID OPEN = 3072142513U;
            } // namespace SWITCH
        } // namespace PLANCHA

        namespace PLATE
        {
            static const AkUniqueID GROUP = 282449107U;

            namespace SWITCH
            {
                static const AkUniqueID ARRIVAL = 13604068U;
                static const AkUniqueID ENDRECIPEFALL = 1265258917U;
                static const AkUniqueID SEND = 803249283U;
                static const AkUniqueID TABLEFALL = 2071809692U;
            } // namespace SWITCH
        } // namespace PLATE

        namespace RECIPE
        {
            static const AkUniqueID GROUP = 3952605219U;

            namespace SWITCH
            {
                static const AkUniqueID CHANGE = 522632697U;
                static const AkUniqueID DROP = 1878686274U;
                static const AkUniqueID PICKUP = 3978245845U;
            } // namespace SWITCH
        } // namespace RECIPE

        namespace RECIPEBOARD_STATE
        {
            static const AkUniqueID GROUP = 867315583U;

            namespace SWITCH
            {
                static const AkUniqueID CLOSE = 1451272583U;
                static const AkUniqueID OPEN = 3072142513U;
            } // namespace SWITCH
        } // namespace RECIPEBOARD_STATE

        namespace RECIPERESULTLEVEL
        {
            static const AkUniqueID GROUP = 544030586U;

            namespace SWITCH
            {
                static const AkUniqueID LEVEL0 = 2678230383U;
                static const AkUniqueID LEVEL1 = 2678230382U;
                static const AkUniqueID LEVEL2 = 2678230381U;
            } // namespace SWITCH
        } // namespace RECIPERESULTLEVEL

        namespace RESULTSSTEP1
        {
            static const AkUniqueID GROUP = 2265768876U;

            namespace SWITCH
            {
                static const AkUniqueID FAIL = 2596272617U;
                static const AkUniqueID SUCCESS = 3625060726U;
            } // namespace SWITCH
        } // namespace RESULTSSTEP1

        namespace RESULTSSTEP2
        {
            static const AkUniqueID GROUP = 2265768879U;

            namespace SWITCH
            {
                static const AkUniqueID FAIL = 2596272617U;
                static const AkUniqueID SUCCESS = 3625060726U;
            } // namespace SWITCH
        } // namespace RESULTSSTEP2

        namespace RESULTSSTEP3
        {
            static const AkUniqueID GROUP = 2265768878U;

            namespace SWITCH
            {
                static const AkUniqueID FAIL = 2596272617U;
                static const AkUniqueID SUCCESS = 3625060726U;
            } // namespace SWITCH
        } // namespace RESULTSSTEP3

        namespace REWARDS
        {
            static const AkUniqueID GROUP = 3894118027U;

            namespace SWITCH
            {
                static const AkUniqueID APPEARING = 3613730138U;
                static const AkUniqueID GOLD = 651855279U;
                static const AkUniqueID KNIFE = 3312069844U;
                static const AkUniqueID RECIPE = 3952605219U;
            } // namespace SWITCH
        } // namespace REWARDS

        namespace ROLLER_STATE
        {
            static const AkUniqueID GROUP = 3697240271U;

            namespace SWITCH
            {
                static const AkUniqueID DROP = 1878686274U;
                static const AkUniqueID PICKUP = 3978245845U;
                static const AkUniqueID ROLL = 2026920480U;
                static const AkUniqueID WOKFALL = 3728525141U;
            } // namespace SWITCH
        } // namespace ROLLER_STATE

        namespace SBIREACTIONS
        {
            static const AkUniqueID GROUP = 1325546453U;

            namespace SWITCH
            {
                static const AkUniqueID ABORTANNOUNCE = 1547820340U;
                static const AkUniqueID FEEDBACKNEGATIVE = 2271052049U;
                static const AkUniqueID FEEDBACKPOSITIVE = 2508732829U;
            } // namespace SWITCH
        } // namespace SBIREACTIONS

        namespace SBIRETYPES
        {
            static const AkUniqueID GROUP = 3881825119U;

            namespace SWITCH
            {
                static const AkUniqueID SBIRE1 = 3436440341U;
                static const AkUniqueID SBIRE2 = 3436440342U;
                static const AkUniqueID SBIRE3 = 3436440343U;
            } // namespace SWITCH
        } // namespace SBIRETYPES

        namespace SEA_BEHAVIOUR_SWITCH
        {
            static const AkUniqueID GROUP = 2301553809U;

            namespace SWITCH
            {
                static const AkUniqueID SEA_BEHAVIOUR_CALM = 1166438704U;
                static const AkUniqueID SEA_BEHAVIOUR_MODERATE = 3405464538U;
                static const AkUniqueID SEA_BEHAVIOUR_STRONG = 184483172U;
            } // namespace SWITCH
        } // namespace SEA_BEHAVIOUR_SWITCH

        namespace SFXBOAT
        {
            static const AkUniqueID GROUP = 2899737624U;

            namespace SWITCH
            {
                static const AkUniqueID CANNONOPEN = 4042406062U;
                static const AkUniqueID DEPLOY = 367668828U;
                static const AkUniqueID DOOROPEN = 1404805401U;
                static const AkUniqueID RATTLE = 4180262031U;
            } // namespace SWITCH
        } // namespace SFXBOAT

        namespace SHOP
        {
            static const AkUniqueID GROUP = 251412225U;

            namespace SWITCH
            {
                static const AkUniqueID BUY = 714721615U;
                static const AkUniqueID CHESTCLOSE = 1044338672U;
                static const AkUniqueID CHESTOPEN = 2892697348U;
                static const AkUniqueID ITEMAPPEARING = 2014457493U;
            } // namespace SWITCH
        } // namespace SHOP

        namespace THUNDER
        {
            static const AkUniqueID GROUP = 186852181U;

            namespace SWITCH
            {
                static const AkUniqueID CLOSE = 1451272583U;
                static const AkUniqueID FAR = 1183803292U;
                static const AkUniqueID MIDCLOSE = 2136542155U;
            } // namespace SWITCH
        } // namespace THUNDER

        namespace TOOLBOX
        {
            static const AkUniqueID GROUP = 2792017586U;

            namespace SWITCH
            {
                static const AkUniqueID KNIFE = 3312069844U;
                static const AkUniqueID ROLLER = 4054296493U;
            } // namespace SWITCH
        } // namespace TOOLBOX

        namespace TOOLBOX_STATE
        {
            static const AkUniqueID GROUP = 3171187556U;

            namespace SWITCH
            {
                static const AkUniqueID CLOSE = 1451272583U;
                static const AkUniqueID OPEN = 3072142513U;
            } // namespace SWITCH
        } // namespace TOOLBOX_STATE

        namespace UI
        {
            static const AkUniqueID GROUP = 1551306167U;

            namespace SWITCH
            {
                static const AkUniqueID JAUGE = 432185499U;
                static const AkUniqueID RECIPECOUNTDOWN = 3998078020U;
                static const AkUniqueID RECIPERESULTFEEDBACK = 1668772549U;
                static const AkUniqueID ROUND = 1413238543U;
                static const AkUniqueID STAMP = 1231477946U;
            } // namespace SWITCH
        } // namespace UI

        namespace UIANCHOR
        {
            static const AkUniqueID GROUP = 120330124U;

            namespace SWITCH
            {
                static const AkUniqueID PICKUP = 3978245845U;
                static const AkUniqueID PUT = 913787308U;
            } // namespace SWITCH
        } // namespace UIANCHOR

        namespace UIRECIPEANNOUNCE
        {
            static const AkUniqueID GROUP = 199950488U;

            namespace SWITCH
            {
                static const AkUniqueID DESSERT = 687459865U;
                static const AkUniqueID MAINCOURSE = 823300689U;
                static const AkUniqueID STARTER = 2798660194U;
            } // namespace SWITCH
        } // namespace UIRECIPEANNOUNCE

        namespace VFXUI
        {
            static const AkUniqueID GROUP = 2054041585U;

            namespace SWITCH
            {
                static const AkUniqueID ANNOUNCE = 2507417816U;
                static const AkUniqueID COUNTDOWN = 1505888634U;
                static const AkUniqueID RESULTS = 3780578133U;
            } // namespace SWITCH
        } // namespace VFXUI

        namespace WEATHER
        {
            static const AkUniqueID GROUP = 317282339U;

            namespace SWITCH
            {
                static const AkUniqueID RAINY = 2599410036U;
                static const AkUniqueID SUNNY = 3569642402U;
                static const AkUniqueID WINDY = 742231792U;
            } // namespace SWITCH
        } // namespace WEATHER

        namespace WEATHERBEHAVIOUR
        {
            static const AkUniqueID GROUP = 1863812544U;

            namespace SWITCH
            {
                static const AkUniqueID CALM = 3753286132U;
                static const AkUniqueID MODERATE = 1747842238U;
                static const AkUniqueID STRONG = 3522403288U;
            } // namespace SWITCH
        } // namespace WEATHERBEHAVIOUR

        namespace WICKERBASKET
        {
            static const AkUniqueID GROUP = 1340235568U;

            namespace SWITCH
            {
                static const AkUniqueID DOWN = 2280510569U;
                static const AkUniqueID UP = 1551306158U;
            } // namespace SWITCH
        } // namespace WICKERBASKET

        namespace WOKACTION
        {
            static const AkUniqueID GROUP = 3631809256U;

            namespace SWITCH
            {
                static const AkUniqueID END = 529726532U;
                static const AkUniqueID START = 1281810935U;
            } // namespace SWITCH
        } // namespace WOKACTION

        namespace WOKINGREDIENTTYPE
        {
            static const AkUniqueID GROUP = 244060323U;

            namespace SWITCH
            {
                static const AkUniqueID EGG = 680725036U;
                static const AkUniqueID HARD = 3599861390U;
                static const AkUniqueID PLATE = 282449107U;
                static const AkUniqueID SOFT = 670602561U;
            } // namespace SWITCH
        } // namespace WOKINGREDIENTTYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AMB_VOLUME = 509499584U;
        static const AkUniqueID CUTSPEED = 2315433696U;
        static const AkUniqueID GRILLSTATUS = 3508615829U;
        static const AkUniqueID INSTRUMENTVOLUME = 1027964044U;
        static const AkUniqueID MASTER_VOLUME = 4179668880U;
        static const AkUniqueID MUS_VOLUME = 889708287U;
        static const AkUniqueID OVENOVERLOAD = 181416593U;
        static const AkUniqueID OVENTIMER = 3341023146U;
        static const AkUniqueID PROGRESSBAR = 1922958967U;
        static const AkUniqueID RECIPEWHEELSPEED = 2037000977U;
        static const AkUniqueID ROLLSPEED = 2883929993U;
        static const AkUniqueID SFX_VOLUME = 1564184899U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID STORMDISTANCE = 4088770425U;
        static const AkUniqueID VFX_VOLUME = 619609672U;
        static const AkUniqueID WEATHER_PROXIMITY = 564339653U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID CYMBALS = 1730380282U;
        static const AkUniqueID FLUTEFAIL = 1514746477U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID GUITARFAIL = 586953675U;
        static const AkUniqueID TEST = 3157003241U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID CAPTAINCOOK_SOUNDBANK = 2067021429U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB_AUDIO_BUS = 2770274119U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID MUS_AUDIO_BUS = 3376092206U;
        static const AkUniqueID SFX_AUDIO_BUS = 4084199202U;
        static const AkUniqueID VFX_AUDIO_BUS = 444401471U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID BOATREVERB = 1162477199U;
    } // namespace AUX_BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
