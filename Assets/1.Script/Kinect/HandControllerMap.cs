﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class HandControllerMap : CustomFixedHandController
{
    [HideInInspector]
    public Vector2 _screenPosition;
    [HideInInspector]
    public HandController _handController;

    private Camera m_handCamera;

    private EnemyBoat m_selectedBoat;

    void Awake()
    {
        base.LoadReferences();
        _handController = GetComponent<HandController>();
        m_handCamera = references._handCamera;
        m_selectedBoat = null;
    }

    public override void Update()
    {
         // get the coordinate of the hand on the screen
        _screenPosition = m_handCamera.WorldToScreenPoint(transform.position);

        // look up for an ennemyboat
        Ray ray = Camera.main.ScreenPointToRay(_screenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100) && hit.collider.tag == "EnemyBoat")
        {
            EnemyBoat boatTemp = hit.collider.GetComponent<EnemyBoat>();
            if (boatTemp != m_selectedBoat)
            {
                m_selectedBoat = boatTemp;
                references._anker.UpdateEnemyBoat(m_selectedBoat);
            }            
        }
        // there is no more enemyboat under the hand 
        else if (m_selectedBoat != null)
        {
            m_selectedBoat = null;
            references._anker.UpdateEnemyBoat(null);

        }

        // check if the player want to abord
        if (m_selectedBoat != null && _handController.HandState == KinectBody.HandState.Closed && !references._boat._isTargetSelected)
        {
            references._boat.Move(m_selectedBoat);
            SdAudioManager.Trigger("UI_Map_SelectBoat", SdAudioManager.Audioinstance.gameObject); //references._boat.gameObject);
            SdAudioManager.Trigger("Boat_Sail", SdAudioManager.Audioinstance.gameObject); //references._boat.gameObject);
            //VALIDATION GRAB BATEAU
        }
    }
    
}