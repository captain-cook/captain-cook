﻿using UnityEngine;
using System.Collections;

public class Bezier{

	#region Enum

	#endregion

	#region Public

        public Vector3 _pointA;
        public Vector3 _pointB;

        public Vector3 _modifierA;
        public Vector3 _modifierB;

	#endregion

	#region Private

	#endregion

	#region Methods

        public Bezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            _pointA = p0;
            _modifierA = p1;
            _modifierB = p2;
            _pointB = p3;
        }

        public Vector3 GetPosition(ref float t, float deltaTime, float velocity)
        {
            if (MathPlus.IsInRange(t, 0, 1) == true)
            {
                float tt = t * t;
                float ttt = tt * t;

                Vector3 a = -1 * _pointA + 3 * _modifierA - 3 * _modifierB + _pointB;
                Vector3 b = 3 * _pointA - 6 * _modifierA + 3 * _modifierB;
                Vector3 c = -3 * _pointA + 3 * _modifierA;
                Vector3 d = 1 * _pointA;

                Vector3 instantSpeed = 3 * a * tt + 2 * b * t + c;

                t += (velocity * deltaTime) / instantSpeed.magnitude;
                t = Mathf.Clamp(t, 0, 1);
                tt = t * t;
                ttt = tt * t;

                return a * ttt + b * tt + c * t + d;
            }
            else
            {
                return _pointA;
            }
        }

        public Vector3 GetPosition(float t)
        {
            if (MathPlus.IsInRange(t, 0, 1) == true)
            {
                float tt = t * t;
                float ttt = tt * t;

                Vector3 a = -1 * _pointA + 3 * _modifierA - 3 * _modifierB + _pointB;
                Vector3 b = 3 * _pointA - 6 * _modifierA + 3 * _modifierB;
                Vector3 c = -3 * _pointA + 3 * _modifierA;
                Vector3 d = 1 * _pointA;

                return a * ttt + b * tt + c * t + d;
            }
            else
            {
                return _pointA;
            }
        }

        public Vector3[] GetCurve(int subdivision)
        {
            Vector3[] curve = new Vector3[subdivision];

            for (int i = 0; i < subdivision; i++)
            {
                float position = (i * 1f) / (subdivision * 1f);
                curve[i] = GetPosition(position);
            }
            return curve;
        }

	#endregion
	
}
