﻿﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class HandControllerCooking : CustomHandController
{

	#region Enum

	#endregion

	#region Public

        [HideInInspector]
        public GameObject _objectGrabbed;

        public float _throwForce;
        public float _breakRadius = 0.1f;


	#endregion

	#region Private

        private HandController m_handController;
        private HandControllerCooking m_otherHand;
        private float m_reloadBreakRadius = 0.5f;
        private bool m_recentlySnap = false;

        private GameObject m_preselectedObject;
        private GameObject m_preselectedGrabSocket;

        private GameObject m_socketGrabbed;

	#endregion

	#region Unity

        void Awake()
        {
            base.LoadReferences();
            m_handController = GetComponent<HandController>();

            HandControllerCooking[] hands = transform.parent.GetComponentsInChildren<HandControllerCooking>();
            if ( hands[0] == this)
            {
                m_otherHand = hands[1];
            }
            else
            {
                m_otherHand = hands[0];
            }
		}

		void Update () {
            //Preselect
            if(_objectGrabbed == null && m_handController.HandState != KinectBody.HandState.Closed)
            {
                PreselectInteractiveObject();
            }
            
            //Grab
            else if (_objectGrabbed == null && m_handController.HandState == KinectBody.HandState.Closed && m_preselectedObject != null)
            {
                GrabPreselectedObject();
            }

            //Update object
            if (_objectGrabbed != null)
            {
                MoveGrabbedObject();
            }

            //Release
            if (_objectGrabbed != null && m_handController.HandState == KinectBody.HandState.Open)
            {
                ReleaseGrabbedObject();
            }

		}

        void OnDisable()
        {
            //Release
            if (_objectGrabbed != null)
            {
                ReleaseGrabbedObject();
            }
        }

        void FixedUpdate()
        {
            ////Update object
            //if (_objectGrabbed != null)
            //{
            //    MoveGrabbedObject();
            //}
        }

	#endregion

	#region Custom

        bool IsGrabbable(GameObject go)
        {
            return go.tag == "GrabSocket" || go.tag == "Ingredient" || go.tag == "Murene";
        }

        void PreselectInteractiveObject()
        {
            //Unhighlight previous
            if (m_preselectedObject != null)
            {
                if (m_preselectedObject.GetComponent<Highlighter>() != null)
                {
                    m_preselectedObject.GetComponent<Highlighter>().UnHighlight();
                }
                m_preselectedObject = null;
            }

            Collider[] colliders = Physics.OverlapSphere(transform.position, m_handController._radius);
            List<GameObject> gameObjects = new List<GameObject>();

            float distance = float.MaxValue;

            //Recuperer l'objet le plus proche
            for (int i = 0; i < colliders.Length; i++)
            {
                if(IsGrabbable(colliders[i].gameObject) == true)
                {
                    float d = Vector3.Distance(transform.position, colliders[i].transform.position);
                    if(d < distance)
                    {
                        distance = d;
                        if(colliders[i].tag == "GrabSocket")
                        {
                            m_preselectedObject = colliders[i].transform.parent.gameObject;
                            m_preselectedGrabSocket = colliders[i].gameObject;
                        }
                        else
                        {
                            m_preselectedGrabSocket = null;
                            m_preselectedObject = colliders[i].gameObject;
                        }
                    }
                }
            }
            
            //Highlight un objet si il est dans le range
            if(m_preselectedObject != null)
            {
                if (m_handController._otherHand.GetComponent<HandControllerCooking>()._objectGrabbed != m_preselectedObject && m_preselectedObject.GetComponent<Highlighter>() != null)
                {
                    m_preselectedObject.GetComponent<Highlighter>().Highlight();
                }
            }
        }

        void GrabPreselectedObject()
        {
            if (m_preselectedObject.tag == "Tool" && m_preselectedObject.GetComponent<Tool>()._couldBeGrabbed == false) return;
            if (m_preselectedObject.tag == "Ingredient" && m_preselectedObject.GetComponent<Ingredient>()._couldBeGrabbed == false) return;

            _objectGrabbed = m_preselectedObject;
            m_socketGrabbed = m_preselectedGrabSocket;

            m_preselectedGrabSocket = null;
            m_preselectedObject = null;

            if (_objectGrabbed.GetComponent<Highlighter>() != null)
            {
                _objectGrabbed.GetComponent<Highlighter>().UnHighlight();
            }

            //Show ghost hand on tool
            if (_objectGrabbed.tag == "Tool")
            {
                m_socketGrabbed.transform.Find("HandGhost").renderer.enabled = true;
                _objectGrabbed.rigidbody.isKinematic = false;
                _objectGrabbed.rigidbody.interpolation = RigidbodyInterpolation.Interpolate;

                _objectGrabbed.GetComponent<Tool>()._inSlot = false;

                //If it is a knife
                if(_objectGrabbed.GetComponentInChildren<BladeSpeedController>() != null)
                {
                    SdAudioManager.Trigger("Knife_Pickup", _objectGrabbed);

                    references._chest.PlayChest(true);

                    GameObject murene = GameObject.FindGameObjectWithTag("Murene");
                    Ingredient ing = null;
                    if(murene != null)
                    {
                        ing = murene.GetComponent<Ingredient>();
                    }
                    if (ing != null && ing._inBasket == false)
                    {
                        murene.GetComponent<SwitchToRigidMurene>().Bake();
                    }

                    _objectGrabbed.GetComponent<Slicer>()._objectToSlice.GetComponent<Ingredient>().Bump();
                }

                //If it is a roller
                if (_objectGrabbed.name == "rouleau_patisserie")
                {
                    if (references._roller._target != null && references._roller._target._roller != null)
                    {
                        references._roller._target._spreadable = true;
                        references._roller._target.LockOnTable();
                    }
                    //references._recipeManager._
                    if(m_handController._otherHand.GetComponent<HandControllerCooking>()._objectGrabbed != _objectGrabbed)
                    {
                        SdAudioManager.Trigger("Roller_Pickup", _objectGrabbed);
                        SdAudioManager.SetParameter("Roller_RollSpeed", 0f, _objectGrabbed);
                        SdAudioManager.Trigger("Roller_Roll", _objectGrabbed);
                    }
                }
            }

            //Hide idle hand cursor
            m_handController.Hide();
        }

        void MoveGrabbedObject()
        {
            if (_objectGrabbed.rigidbody != null)
            {
                Vector3 position = transform.position;
                Quaternion rotation = m_handController._rotation;

                //If it is the roller, we compute the position between the two hands
                if (_objectGrabbed.name == "rouleau_patisserie")
                {
                    position = (transform.position + m_handController._otherHand.transform.position) / 2f;
                }

                
                if(_objectGrabbed.rigidbody != null && _objectGrabbed.rigidbody.isKinematic == false)
                {
                    _objectGrabbed.rigidbody.velocity = Vector3.zero;
                    _objectGrabbed.rigidbody.angularVelocity = Vector3.zero;
                }

                _objectGrabbed.transform.rotation = rotation;
                _objectGrabbed.transform.position = position;

                if (_objectGrabbed.name == "rouleau_patisserie")
                {
                    Vector3 direction = Vector3.zero;

                    //Toujours une direction droite -> gauche
                    if (m_handController._side == HandController.HandSide.Left)
                    {
                        direction = transform.position - m_handController._otherHand.transform.position;
                    }
                    else
                    {
                        direction = m_handController._otherHand.transform.position - transform.position;
                    }
                    direction.y = 0;

                    Vector3 forward = Vector3.Cross(direction, Vector3.up);
                    _objectGrabbed.transform.LookAt(_objectGrabbed.transform.position - forward, direction);
                }

                //Block the tool to the table
                if (_objectGrabbed.collider.bounds.min.y < references._table.collider.bounds.max.y)
                {
                    float y = _objectGrabbed.transform.position.y + (references._table.collider.bounds.max.y - _objectGrabbed.collider.bounds.min.y) - 0.01f;
                    _objectGrabbed.transform.position = new Vector3(_objectGrabbed.transform.position.x, y, _objectGrabbed.transform.position.z);
                }

                //break a breakable ingredient
                if (_objectGrabbed.GetComponent<BreakableObject>() != null)
                {
                    //two hands in the same area
                    if (m_recentlySnap == false && (transform.position - m_otherHand.transform.position).magnitude < _breakRadius)
                    {
                        m_recentlySnap = true;
                        if (_objectGrabbed.GetComponent<BreakableObject>()._inTwoPart == false)
                        {
                            _objectGrabbed.GetComponent<BreakableObject>().Break();
                            ReleaseGrabbedObject();
                        }
                        else
                        {
                            GameObject B = _objectGrabbed.GetComponent<BreakableObject>().Break();
                            HandControllerCooking otherHandCooking = m_otherHand.GetComponent<HandControllerCooking>();

                            _objectGrabbed.rigidbody.velocity = Vector3.zero;
                            _objectGrabbed.rigidbody.angularVelocity = Vector3.zero;
                            B.rigidbody.velocity = Vector3.zero;
                            B.rigidbody.angularVelocity = Vector3.zero;

                            ReleaseGrabbedObject();
                        }
                        
                    }
                    //we check is the player separe his hands before resnaping then ingredient
                    else if (m_recentlySnap && (transform.position - m_otherHand.transform.position).magnitude > m_reloadBreakRadius)
                    {
                        m_recentlySnap = false;
                    }                    
                }

                
            }

            if (m_handController._body.IsDirty == true && _objectGrabbed != null)
            {
                BladeSpeedController bsc = _objectGrabbed.transform.GetComponentInChildren<BladeSpeedController>();
                if (bsc != null)
                {
                    bsc.HoldPosition();
                }
            }
        }

        public void ReleaseGrabbedObject()
        {
            //Hide ghost hand on tool
            if (_objectGrabbed.tag == "Tool")
            {
                if (_objectGrabbed.GetComponent<Tool>()._couldBeReleased == false) return;

                //If it is a knife
                if (_objectGrabbed.GetComponentInChildren<BladeSpeedController>() != null)
                {
                    references._chest.PlayChest(false);
                    _objectGrabbed.GetComponent<Tool>().ReturnToSlot();
                    _objectGrabbed.GetComponent<Slicer>()._enabled = false;
                }

                //If it is a roller
                if (_objectGrabbed.name == "rouleau_patisserie")
                {
                    if (m_handController._otherHand.GetComponent<HandControllerCooking>()._objectGrabbed != _objectGrabbed)
                    {
                        _objectGrabbed.GetComponent<Tool>().ReturnToSlot();

                        if (references._roller._target != null && references._roller._target._roller != null)
                        {
                            references._roller._target._spreadable = false;
                        }
                    }
                }

                _objectGrabbed.rigidbody.isKinematic = true;
                _objectGrabbed.rigidbody.interpolation = RigidbodyInterpolation.Extrapolate;
                m_socketGrabbed.transform.Find("HandGhost").renderer.enabled = false;
            }

            //Show idle hand cursor
            m_handController.Show();

            _objectGrabbed = null;
            m_socketGrabbed = null;
        }

	#endregion

}