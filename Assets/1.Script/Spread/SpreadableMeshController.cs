﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpreadableMeshController : ReferencedMonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public SpreaderController _roller;
        public AnimationCurve _curveScaleX;
        public AnimationCurve _curveScaleZ;
        public float _squeezeRate;
        public bool _spreadable = false;
        public float _yMaxFlat = 0.01f;
        public GameObject _fxStep;

        public float m_minBarrycenterY;
        public float m_maxBarrycenterY;

	#endregion

	#region Private

        private Mesh m_mesh;
        private MeshFilter m_meshFilter;
        private List<Vector3> m_vertices;
        private float m_minY;
        private float m_barycenterBaseY;

        private float m_lastFrames;


	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();

            m_meshFilter = GetComponent<MeshFilter>();
            Mesh meshBase = m_meshFilter.sharedMesh;

            m_mesh = new Mesh();
            m_mesh.vertices = meshBase.vertices;
            m_mesh.triangles = meshBase.triangles;
            m_mesh.uv = meshBase.uv;
            m_mesh.normals = meshBase.normals;
            m_mesh.name = "COPY";
            m_vertices = new List<Vector3>(m_mesh.vertices);

            m_mesh.RecalculateBounds();
            m_minY = m_mesh.bounds.min.y;

            //Barycenter computing for the X/Z Scale
            Vector3 barycenter = Vector3.zero;
            for (int i = 0; i < m_vertices.Count; i++)
            {
                barycenter += m_vertices[i];
            }
            m_barycenterBaseY = barycenter.y / m_vertices.Count;

            m_meshFilter.sharedMesh = m_mesh;
		}

        void Update()
		{
            if(GetComponent<Ingredient>()._inBasket == false)
            {
                transform.rotation = Quaternion.identity;
            }

            if (_roller != null && _spreadable == true)
            {
                if(_fxStep != null)
                {
                    _fxStep.renderer.enabled = true;
                    _fxStep.transform.parent = null;
                }

                //Roller direction
                Vector3 RP_forward = _roller.transform.up;
                //Position of the roller relative to the batter pivot
                Vector3 RP_position_relative = transform.InverseTransformPoint(_roller.transform.position);
                //Radius of the Roller
                float RP_radius = 0.025f;

                //Barycenter computing for the X/Z Scale
                Vector3 barycenter = Vector3.zero;

                bool vertex_squeezed = false;

                for (int i = 0; i < m_vertices.Count; i++)
                {
                    //For all the vertex greather than the minY
                    if (m_vertices[i].y > m_minY + _yMaxFlat)
                    {
                        //Direction between the selected vertex and the roller pivot
                        Vector3 direction_vertex_RP = m_vertices[i] - RP_position_relative;
                        //Projection of the vertex on the roller axis
                        Vector3 vertex_projected_RP_pivot = Vector3.Project(direction_vertex_RP, RP_forward) + RP_position_relative;
                        //Direction between the projected vertex and the original vertex
                        Vector3 direction_vertex_RP_projection = m_vertices[i] - vertex_projected_RP_pivot;

                        //Position of the selected vertex relative to the roller pivot
                        Vector3 vertex_position_relative = _roller.transform.InverseTransformPoint(transform.TransformPoint(m_vertices[i]));

                        //If vertex is on the radius of the roller or over
                        if (
                            (direction_vertex_RP_projection.magnitude < RP_radius &&
                            MathPlus.IsInRange(vertex_position_relative.y, -_roller.transform.localScale.y, _roller.transform.localScale.y))
                            ||
                            (vertex_position_relative.x > 0 &&
                            MathPlus.IsInRange(vertex_position_relative.z, -RP_radius, RP_radius) &&
                            MathPlus.IsInRange(vertex_position_relative.y, -_roller.transform.localScale.y, _roller.transform.localScale.y))
                            )
                        {
                            m_lastFrames = Time.time;
                            vertex_squeezed = true;

                            Vector3 projected = Vector3.Project(direction_vertex_RP_projection, _roller.transform.forward);
                            float distanceProjection = projected.magnitude;

                            float distanceY = Mathf.Sqrt(Mathf.Pow(RP_radius, 2) - Mathf.Pow(distanceProjection, 2));
                            Vector3 finalPosition = vertex_projected_RP_pivot + projected + (Vector3.down * distanceY);
                            if (finalPosition.y < m_minY + +_yMaxFlat) { finalPosition.y = m_minY + _yMaxFlat; }
                            m_vertices[i] = finalPosition;
                        }

                        barycenter += m_vertices[i];
                    }
                }
                m_mesh.vertices = m_vertices.ToArray();
                m_mesh.RecalculateNormals();

                //Scale X/Z
                float barycenterY = (barycenter.y / m_vertices.Count);

                _squeezeRate = Mathf.InverseLerp(m_minBarrycenterY, m_maxBarrycenterY, barycenterY);

                transform.localScale = new Vector3(_curveScaleX.Evaluate(_squeezeRate), transform.localScale.y, _curveScaleZ.Evaluate(_squeezeRate));

                if (vertex_squeezed == false && (Time.time - m_lastFrames) >= 0.2f )
                {
                    SdAudioManager.SetParameter("Roller_RollSpeed", 0f, _roller.gameObject);
                }
                else if( vertex_squeezed == true)
                {
                    SdAudioManager.SetParameter("Roller_RollSpeed", 0.5f, _roller.gameObject);
                }

            }
            else
            {
                if(_fxStep != null)
                {
                    _fxStep.renderer.enabled = false;
                    _fxStep.transform.parent = transform;
                }
            }
            
        }

        public void LockOnTable()
        {
            GetComponent<MeshCollider>().enabled = false;
            //rigidbody.velocity = Vector3.zero;
            //rigidbody.angularVelocity = Vector3.zero;
            rigidbody.useGravity = false;
            rigidbody.isKinematic = true;
        }

	#endregion

	#region Custom

	#endregion
	
}
