﻿using UnityEngine;
using System.Collections;

public class SOPirate : ScriptableObject {

    public string _name;
    public SOTaste _tasteTemp;
    public SOTaste _tastePermanent;
        
    public Sprite _sealOfApprouval;
    public Sprite _sealOfDisagree;
}
