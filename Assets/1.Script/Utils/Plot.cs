﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Plot {

	#region Enum

	#endregion

	#region Public

        public float _historyDuration;
        public AnimationCurve _plot;

	#endregion

	#region Private

        private float m_lastUpdate;
        private bool m_overflow = false;

        private Keyframe[] m_keyframes;
        private int m_nbKeys;

	#endregion

	#region Unity

		public void Update (float deltaTime)
		{
            if(m_keyframes == null || m_keyframes.Length < m_nbKeys)
            {
                m_keyframes = _plot.keys;
                GC.Collect();
            }

            for (int i = 0; i < m_keyframes.Length; i++)
            {
                if (m_overflow == false && m_keyframes[i].time < 0)
                {
                    m_overflow = true;
                }

                m_keyframes[i].time -= deltaTime;
            }

            _plot.keys = m_keyframes;
		}

	#endregion

	#region Custom

        private void ShiftArray()
        {
            for (int i = 1; i < m_keyframes.Length; i++)
            {
                m_keyframes[i - 1] = m_keyframes[i];
            }
        }

        public void AddPoint(float value)
        {
            if(m_overflow == true)
            {
                ShiftArray();
                m_keyframes[m_keyframes.Length - 1].time = _historyDuration;
                m_keyframes[m_keyframes.Length - 1].value = value;
                m_overflow = false;
            }
            else
            {
                _plot.AddKey(_historyDuration, value);
                m_nbKeys++;
            }
        }

	#endregion

}
