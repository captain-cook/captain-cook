﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Slicer : ReferencedMonoBehaviour {

    public bool _enabled;

    public Transform _slicePart;

    public GameObject _objectToSlice;
    public GameObject _newPiecePrefab;
    public GameObject _sliceFxPrefab;
    public GameObject _ingredientExplosionFxPrefab;
    public GameObject _ingredientLoadExplosionFxPrefab;
    public CameraShaker _shaker;

    private List<GameObject> m_parts;

    //private Vector3 m_centerObjectToSlice;

    private GameObject m_fxLoadExplosion;

    public int _meshCount;

    private Plane m_plane;

    private int m_triangleLength = 3;
    private int m_triangleCount;

    private GameObject m_objectA;
    private GameObject m_objectB;

    private Mesh m_meshA;
    private Mesh m_meshB;
    private Mesh m_meshToSlice;

    private List<Vector3> m_verticiesA;
    private List<Vector3> m_verticiesB;
    private List<Vector3> m_verticiesMeshToSlice;

    public Dictionary<int, int> _indiciesMeshA;
    public Dictionary<int, int> _indiciesMeshB;

    private List<Vector3> m_intersectionVerticies;

    private List<int> m_trianglesA;
    private List<int> m_trianglesB;
    private List<int> m_trianglesMeshToSlice;

    private List<Vector2> m_uvA;
    private List<Vector2> m_uvB;

    private List<Vector2> m_uvMeshToSlice;

    //List<List<Vector3>> m_ccwIntersections;

    private Bounds m_boundsIntersection;
    private List<Vector3> m_projectedIntersectionVerticies;

    public int _sliceCount;
    public int _lastSliceCount;

    public int _maxSliceByFrame = 3;
    private int m_sliceAtThisFrame = 0;

    public Color _startIngredientColor;
    public Color _stopIngredientColor;

    private BladeSpeedController m_blade;

    //public Material _materialLines;
    //public Color _linesColor;

    //Slashes
    public struct Direction
    {
        public float time;
        public Vector3 forward;
        public Vector3 normal;
    }
    private List<Direction> m_directions;

    private float m_lastSpeed;
    private float m_lastSlashRelativeTime;

    public Plot m_plotRaw;
    public AnimationCurve m_plotSmooth;
    public AnimationCurve m_plotDx;
    public int m_halfSizeKernel = 15;

    private Ingredient.SliceType m_sliceType;

    public bool _sliceFinished = false;

    private Timer m_timerLoadExplosion;

    private Vector3 m_startScale;
    public AnimationCurve m_scaleCurve;

    public bool _ingredientBlown  = false;

    public class Triangle
    {
        public int A;
        public int B;
        public int C;

        public bool SideA;
        public bool SideB;
        public bool SideC;

        public bool Cut = false;

        public int CutAB = -1;
        public int CutBC = -1;
        public int CutAC = -1;
    }

	void Start () {
        _meshCount = 1;

        m_blade = GetComponentInChildren<BladeSpeedController>();

        base.LoadReferences();

        m_directions = new List<Direction>();

        m_parts = new List<GameObject>();
        //SdAudioManager.Trigger("Ingredient_Explosion", Camera.main.gameObject);

        //m_ccwIntersections = new List<List<Vector3>>();
    }
	
    void Update()
    {
        //Fill de raw plot
        m_plotRaw.Update(Time.deltaTime);

        float speed = m_blade.VelocityXY.magnitude;

        m_plotRaw.AddPoint(speed);

        //Hold direction
        Direction dir = new Direction();
        dir.time = Time.time;
        dir.normal = transform.right;
        dir.forward = transform.forward;
        m_directions.Add(dir);

        for (int i = 0; i < m_directions.Count; i++)
        {
            if (Time.time - m_directions[i].time > m_plotRaw._historyDuration)
            {
                m_directions.RemoveAt(i);
                i--;
            }
            else
            {
                break;
            }
        }

        /*if(Time.time - m_lastSlashTime > 1.2f)
        {
            ResetSlicer();
        }*/

        if(_enabled == true)
        {
            //Compute the plot to find slashes
            ComputeSlashes();
        }

        GetComponentInChildren<TrailRenderer>().enabled = _enabled;

        if(m_timerLoadExplosion != null)
        {
            for (int j = 0; j < m_parts.Count; j++)
            {
                m_parts[j].renderer.material.color = Color.Lerp(_startIngredientColor, _stopIngredientColor, m_timerLoadExplosion.CurrentNormalized);
                m_parts[j].transform.localScale = m_startScale * m_scaleCurve.Evaluate(m_timerLoadExplosion.CurrentNormalized);
            }

            if(m_timerLoadExplosion.IsElapsedOnce == true)
            {
                for (int i = 0; i < m_parts.Count; i++)
                {
                    m_parts[i].transform.localScale = m_startScale;
                    m_parts[i].renderer.material.color = _startIngredientColor;
                    m_parts[i].collider.enabled = true;
                    m_parts[i].rigidbody.isKinematic = false;
                    m_parts[i].rigidbody.useGravity = true;
                    float x = Random.Range(-5f, 5f);
                    float y = Random.Range(-5f, 5f);
                    float z = Random.Range(-5f, 5f);
                    m_parts[i].rigidbody.velocity = new Vector3(x, y, z).normalized * 2f;
                    m_parts[i].GetComponent<Ingredient>()._soundOnCollision = true;
                    m_parts[i].rigidbody.freezeRotation = false;

                }

                SdAudioManager.Trigger("Ingredient_Explosion", Camera.main.gameObject);

                Destroy(m_fxLoadExplosion);

                GameObject fx = GameObject.Instantiate(_ingredientExplosionFxPrefab) as GameObject;
                fx.transform.position = m_parts[0].transform.position;

                m_timerLoadExplosion = null;

                _shaker.Shake();

                _ingredientBlown = true;

            }
        }
    }

    private bool m_woosh;

    void ComputeSlashes()
    {
        int n = m_plotRaw._plot.length;
        int halfSize = 5;

        if(n > halfSize * 2)
        {
            //Smooth
            Keyframe[] keyframesSmooth = new Keyframe[n - (halfSize * 2)];

            for (int i = halfSize; i < n - halfSize; i++)
            {
                float value = 0;
                for (int j = -halfSize; j <= halfSize; j++)
                {
                    value += m_plotRaw._plot[i + j].value;
                }
                value /= ((halfSize * 2) + 1);
                keyframesSmooth[i - halfSize] = new Keyframe(m_plotRaw._plot[i].time, value);
            }

            m_plotSmooth.keys = keyframesSmooth;

            //Dx
            Keyframe[] keyframesDx = new Keyframe[n - (halfSize * 2) - 1];

            for (int i = 1; i <= keyframesDx.Length; i++)
            {
                keyframesDx[i - 1] = new Keyframe(keyframesSmooth[i].time, keyframesSmooth[i].value - keyframesSmooth[i - 1].value);
            }

            //Hold positive
            for (int i = 0; i < keyframesDx.Length; i++)
            {
                if (keyframesDx[i].value < 0) keyframesDx[i].value = 0;
            }

            //Detect slashes
            float threshold = 0.10f;
            int indexFromLast = 0;
            bool overThreshold = false;

            m_lastSlashRelativeTime -= Time.deltaTime;

            for (int i = 0; i < keyframesDx.Length; i++)
            {
                if (keyframesDx[i].time > m_lastSlashRelativeTime)
                {
                    if (overThreshold == false)
                    {
                        if (keyframesDx[i].value > threshold)
                        {
                            indexFromLast = keyframesDx.Length - i - 1;
                            overThreshold = true;
                            if (m_woosh == false)
                            {
                                //SdAudioManager.Trigger("Knife_WoodHit", m_blade.gameObject);
                                SdAudioManager.Trigger("Knife_Whoosh", m_blade.gameObject);
                                m_woosh = true;
                                SdAudioManager.Trigger("Knife_Cut" + IngredientType().ToString(), m_blade.gameObject);
                                SdAudioManager.SetParameter("Knife_CutSpeed", 1f, m_blade.gameObject);
                            }
                            
                        }
                    }
                    else
                    {
                        if (keyframesDx[i].value == 0)
                        {
                            SdAudioManager.Trigger("Knife_EndCut", m_blade.gameObject);


                            m_lastSlashRelativeTime = keyframesDx[i].time;

                            Plane plane = new Plane(m_directions.FromLast(indexFromLast).normal, CenterObjectToSlice());
                            Vector3 b;

                            float angle = Mathf.Atan(m_directions.FromLast(indexFromLast).forward.y / m_directions.FromLast(indexFromLast).forward.x) * Mathf.Rad2Deg;

                            RefreshPartsList();

                            if (_objectToSlice != null)
                            {
                                GameObject[] parts = Slice(_objectToSlice, plane, out b);
                                if (parts != null)
                                {
                                    m_parts.AddRange(parts);

                                    float size = 0.005f;

                                    parts[0].transform.position += (plane.normal.normalized * size);
                                    parts[0].GetComponent<Ingredient>()._bumpStartPosition += (plane.normal.normalized * size);
                                    parts[1].transform.position -= (plane.normal.normalized * size);
                                    parts[1].GetComponent<Ingredient>()._bumpStartPosition -= (plane.normal.normalized * size);

                                    //FX slice
                                    GameObject fx = GameObject.Instantiate(_sliceFxPrefab, CenterObjectToSlice(), Quaternion.identity) as GameObject;
                                    fx.transform.right = m_directions.FromLast(indexFromLast).normal;

                                    //fx.transform.Find("Slash").position = fx.transform.position + (Vector3.back * 0.25f);
                                    fx.transform.Find("Slash").eulerAngles = new Vector3(0, 0, angle);

                                    //Fx drops
                                    if (_objectToSlice.GetComponent<Ingredient>()._fxDropsPrefab != null)
                                        fx = GameObject.Instantiate(_objectToSlice.GetComponent<Ingredient>()._fxDropsPrefab, CenterObjectToSlice(), Quaternion.identity) as GameObject;

                                    RotateAllTheParts(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));

                                    _objectToSlice = null;

                                    m_woosh = false;

                                    _sliceCount++;
                                    references._knife.GetComponent<Tool>()._couldBeReleased = true;
                                }
                            }
                            else
                            {
                                List<GameObject> newParts = new List<GameObject>();
                                for (int j = 0; j < m_parts.Count; j++)
                                {
                                    GameObject[] parts = Slice(m_parts[j], plane, out b);
                                    if (parts != null)
                                    {
                                        newParts.AddRange(parts);
                                        
                                        float size = 0.005f;

                                        parts[0].transform.position += (plane.normal.normalized * size);
                                        parts[0].GetComponent<Ingredient>()._bumpStartPosition += (plane.normal.normalized * size);
                                        parts[1].transform.position -= (plane.normal.normalized * size);
                                        parts[1].GetComponent<Ingredient>()._bumpStartPosition -= (plane.normal.normalized * size);

                                        //FX slice
                                        GameObject fx = GameObject.Instantiate(_sliceFxPrefab, CenterObjectToSlice(), Quaternion.identity) as GameObject;
                                        fx.transform.right = m_directions.FromLast(indexFromLast).normal;

                                        //fx.transform.Find("Slash").position = fx.transform.position + (Vector3.back * 0.25f);
                                        fx.transform.Find("Slash").eulerAngles = new Vector3(0, 0, angle);

                                        //Fx drops
                                        if (newParts[0].GetComponent<Ingredient>()._fxDropsPrefab != null)
                                            fx = GameObject.Instantiate(newParts[0].GetComponent<Ingredient>()._fxDropsPrefab, CenterObjectToSlice(), Quaternion.identity) as GameObject;

                                        RotateAllTheParts(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));

                                        m_woosh = false;

                                        _sliceCount++;
                                        m_parts.RemoveAt(j);
                                        j--;
                                    }
                                }
                                m_parts.AddRange(newParts);
                            }

                            Debug.Log(m_parts.Count);
                        }
                    }
                }
            }

            m_plotDx.keys = keyframesDx;
        }

    }

    void RefreshPartsList()
    {
        m_parts.Clear();
        for (int k = 0; k < _slicePart.childCount; k++)
        {
            m_parts.Add(_slicePart.GetChild(k).gameObject);
        }
    }

    void RotateAllTheParts(Vector3 euler)
    {
        /*for (int i = 0; i < m_parts.Count; i++)
        {
            m_parts[i].transform.eulerAngles = euler;
        }*/
    }

    Vector3 CenterObjectToSlice()
    {
        if (_objectToSlice != null) return _objectToSlice.transform.position;
        else return m_parts[0].transform.position;
    }

    Ingredient.SliceType IngredientType()
    {
        if (_objectToSlice != null) return _objectToSlice.GetComponent<Ingredient>()._sliceType;
        else if (m_parts.Count > 0 && m_parts[0] != null) return m_parts[0].GetComponent<Ingredient>()._sliceType;
        else return Ingredient.SliceType.Soft;
    }

    public void SetObjectToSlice(GameObject objectToSlice)
    {
        _objectToSlice = objectToSlice;
        m_sliceType = _objectToSlice.GetComponent<Ingredient>()._sliceType;
        _sliceFinished = false;
    }

	void LateUpdate ()
    {
        m_sliceAtThisFrame = 0;
	}

    public void ResetSlicer()
    {
        SdAudioManager.Stop("Knife_Cut", 0, m_blade.gameObject);

        RefreshPartsList();

        GameObject fx = GameObject.Instantiate(_ingredientLoadExplosionFxPrefab) as GameObject;
        fx.transform.position = m_parts[0].transform.position;

        m_fxLoadExplosion = fx.gameObject;

        m_timerLoadExplosion = new Timer(1f);
        m_startScale = m_parts[0].transform.localScale;
        _startIngredientColor = m_parts[0].renderer.material.color;

        SdAudioManager.Trigger("Ingredient_Explosion", Camera.main.gameObject);

        _lastSliceCount = _sliceCount;
        _sliceCount = 0;


        _sliceFinished = true;
    }

    public GameObject[] Slice(GameObject objectToSlice, Plane slicePlane, out Vector3 barycenter)
    {
        barycenter = Vector3.zero;
        if (_enabled == true && m_sliceAtThisFrame < _maxSliceByFrame)
        {
            _objectToSlice = objectToSlice;

            m_intersectionVerticies = new List<Vector3>();

            _indiciesMeshA = new Dictionary<int, int>();
            _indiciesMeshB = new Dictionary<int, int>();

            m_verticiesA = new List<Vector3>();
            m_verticiesB = new List<Vector3>();

            m_trianglesA = new List<int>();
            m_trianglesB = new List<int>();

            m_uvA = new List<Vector2>();
            m_uvB = new List<Vector2>();

            m_uvMeshToSlice = new List<Vector2>();

            //Vector3 normal = transform.TransformPoint(Vector3.left) - transform.position;
            //m_plane.SetNormalAndPosition(normal.normalized, transform.position);

            m_plane = slicePlane;

            m_intersectionVerticies.Clear();

            m_meshToSlice = _objectToSlice.GetComponent<MeshFilter>().sharedMesh;
            m_triangleCount = m_meshToSlice.triangles.Length / m_triangleLength;

            m_meshA = new Mesh();
            m_meshA.name = "PieceMesh";
            m_meshB = new Mesh();
            m_meshB.name = "PieceMesh";


            m_verticiesA.Clear();
            m_verticiesB.Clear();

            m_verticiesMeshToSlice = new List<Vector3>(m_meshToSlice.vertices);
            m_trianglesMeshToSlice = new List<int>(m_meshToSlice.triangles);
            m_uvMeshToSlice = new List<Vector2>(m_meshToSlice.uv);

            m_trianglesA.Clear();
            m_trianglesB.Clear();

            _indiciesMeshA.Clear();
            _indiciesMeshB.Clear();

            m_uvA.Clear();
            m_uvB.Clear();

            m_meshA.triangles = null;
            m_meshB.triangles = null;

            int indexA;
            int indexB;
            int indexC;

            for (int i = 0; i < m_triangleCount; i++)
            {
                indexA = m_trianglesMeshToSlice[(i * m_triangleLength) + 0];
                indexB = m_trianglesMeshToSlice[(i * m_triangleLength) + 1];
                indexC = m_trianglesMeshToSlice[(i * m_triangleLength) + 2];

                Triangle triangle = new Triangle();
                triangle.A = indexA;
                triangle.B = indexB;
                triangle.C = indexC;

                Vector3 worldA = _objectToSlice.transform.TransformPoint(m_verticiesMeshToSlice[triangle.A]);
                Vector3 worldB = _objectToSlice.transform.TransformPoint(m_verticiesMeshToSlice[triangle.B]);
                Vector3 worldC = _objectToSlice.transform.TransformPoint(m_verticiesMeshToSlice[triangle.C]);

                Vector3 intersection;
                bool sideA, sideB;
                float distanceFromPlan;

                if (MathPlus.IsLinePlaneIntersection(worldA, worldB, m_plane, out intersection, out sideA, out sideB, out distanceFromPlan) == true)
                {
                    m_intersectionVerticies.Add(intersection);
                    m_verticiesMeshToSlice.Add(_objectToSlice.transform.InverseTransformPoint(intersection));
                    m_uvMeshToSlice.Add(Vector2.Lerp(m_meshToSlice.uv[triangle.A], m_meshToSlice.uv[triangle.B], distanceFromPlan / Vector3.Distance(worldA, worldB)));
                    triangle.Cut = true;
                    triangle.CutAB = m_verticiesMeshToSlice.Count - 1;
                }
                triangle.SideA = sideA;
                triangle.SideB = sideB;
                if (MathPlus.IsLinePlaneIntersection(worldB, worldC, m_plane, out intersection, out sideA, out sideB, out distanceFromPlan) == true)
                {
                    m_intersectionVerticies.Add(intersection);
                    m_verticiesMeshToSlice.Add(_objectToSlice.transform.InverseTransformPoint(intersection));
                    m_uvMeshToSlice.Add(Vector2.Lerp(m_meshToSlice.uv[triangle.B], m_meshToSlice.uv[triangle.C], distanceFromPlan / Vector3.Distance(worldB, worldC)));
                    triangle.Cut = true;
                    triangle.CutBC = m_verticiesMeshToSlice.Count - 1;
                }
                triangle.SideB = sideA;
                triangle.SideC = sideB;
                if (MathPlus.IsLinePlaneIntersection(worldA, worldC, m_plane, out intersection, out sideA, out sideB, out distanceFromPlan) == true)
                {
                    m_intersectionVerticies.Add(intersection);
                    m_verticiesMeshToSlice.Add(_objectToSlice.transform.InverseTransformPoint(intersection));
                    m_uvMeshToSlice.Add(Vector2.Lerp(m_meshToSlice.uv[triangle.A], m_meshToSlice.uv[triangle.C], distanceFromPlan / Vector3.Distance(worldA, worldC)));
                    triangle.Cut = true;
                    triangle.CutAC = m_verticiesMeshToSlice.Count - 1;
                }
                triangle.SideA = sideA;
                triangle.SideC = sideB;

                try
                {
                    TriangleToMesh(triangle);
                }
                catch(System.Exception ex)
                {
                    return null;
                }
            }

            if (m_intersectionVerticies.Count > 0)
            {
                m_sliceAtThisFrame++;

                barycenter = CreateFaceFromIntersections();

                //RecalculatePivot(m_verticiesA);
                //RecalculatePivot(m_verticiesB);

                m_objectA = GameObject.Instantiate(_newPiecePrefab, _objectToSlice.transform.position, _objectToSlice.transform.rotation) as GameObject;
                m_objectA.transform.localScale = _objectToSlice.transform.localScale;

                m_objectB = GameObject.Instantiate(_newPiecePrefab, _objectToSlice.transform.position, _objectToSlice.transform.rotation) as GameObject;
                m_objectB.transform.localScale = _objectToSlice.transform.localScale;

                /*m_objectA.transform.position += RecalculatePivot(m_verticiesA);
                m_objectB.transform.position += RecalculatePivot(m_verticiesB);*/

                m_objectA.GetComponent<MeshFilter>().sharedMesh = m_meshA;
                m_objectB.GetComponent<MeshFilter>().sharedMesh = m_meshB;

                m_objectA.renderer.materials = _objectToSlice.renderer.materials;
                m_objectB.renderer.materials = _objectToSlice.renderer.materials;

                m_meshA.vertices = m_verticiesA.ToArray();
                m_meshB.vertices = m_verticiesB.ToArray();

                m_meshA.triangles = m_trianglesA.ToArray();
                m_meshB.triangles = m_trianglesB.ToArray();

                m_meshA.uv = m_uvA.ToArray();
                m_meshB.uv = m_uvB.ToArray();

                //m_meshA.tangents = m_meshToSlice.tangents;
                //m_meshB.tangents = m_meshToSlice.tangents;

                m_meshA.RecalculateNormals();
                m_meshB.RecalculateNormals();

                m_meshA.UploadMeshData(false);
                m_meshB.UploadMeshData(false);

                m_objectA.GetComponent<MeshCollider>().sharedMesh = m_meshA;
                m_objectB.GetComponent<MeshCollider>().sharedMesh = m_meshB;

                /*m_objectA.rigidbody.AddForce(m_plane.normal, ForceMode.Impulse);
                m_objectB.rigidbody.AddForce(-m_plane.normal, ForceMode.Impulse);*/

                GameObject[] newParts = new GameObject[2];
                newParts[0] = m_objectA;
                newParts[1] = m_objectB;

                InitializePart(newParts[0]);
                InitializePart(newParts[1]);

                Destroy(_objectToSlice);

                _meshCount += 2;

                return newParts;
            }
        }
        return null;
    }

    void TriangleToMesh(Triangle triangle)
    {
        if (triangle.Cut == false)
        {
            if (triangle.SideA == true)
            {
                AddVertexToMesh(triangle.A, _indiciesMeshA, m_verticiesA, m_uvA);
                AddVertexToMesh(triangle.B, _indiciesMeshA, m_verticiesA, m_uvA);
                AddVertexToMesh(triangle.C, _indiciesMeshA, m_verticiesA, m_uvA);
                m_trianglesA.Add(_indiciesMeshA[triangle.A]);
                m_trianglesA.Add(_indiciesMeshA[triangle.B]);
                m_trianglesA.Add(_indiciesMeshA[triangle.C]);
            }
            else
            {
                AddVertexToMesh(triangle.A, _indiciesMeshB, m_verticiesB, m_uvB);
                AddVertexToMesh(triangle.B, _indiciesMeshB, m_verticiesB, m_uvB);
                AddVertexToMesh(triangle.C, _indiciesMeshB, m_verticiesB, m_uvB);
                m_trianglesB.Add(_indiciesMeshB[triangle.A]);
                m_trianglesB.Add(_indiciesMeshB[triangle.B]);
                m_trianglesB.Add(_indiciesMeshB[triangle.C]);
            }
        }
        else
        {
            AddVertexToMesh(triangle.A, triangle.SideA);
            AddVertexToMesh(triangle.B, triangle.SideB);
            AddVertexToMesh(triangle.C, triangle.SideC);

            AddNewVerticiesToAllMesh(triangle);

            RecreateFaceFromTriangle(triangle, true, m_trianglesA, _indiciesMeshA);
            RecreateFaceFromTriangle(triangle, false, m_trianglesB, _indiciesMeshB);
        }
    }

    void AddNewVerticiesToAllMesh(Triangle triangle)
    {
        if(triangle.CutAB > -1)
        {
            AddVertexToMesh(triangle.CutAB, _indiciesMeshA, m_verticiesA, m_uvA);
            AddVertexToMesh(triangle.CutAB, _indiciesMeshB, m_verticiesB, m_uvB);
        }
        if (triangle.CutBC > -1)
        {
            AddVertexToMesh(triangle.CutBC, _indiciesMeshA, m_verticiesA, m_uvA);
            AddVertexToMesh(triangle.CutBC, _indiciesMeshB, m_verticiesB, m_uvB);
        }
        if (triangle.CutAC > -1)
        {
            AddVertexToMesh(triangle.CutAC, _indiciesMeshA, m_verticiesA, m_uvA);
            AddVertexToMesh(triangle.CutAC, _indiciesMeshB, m_verticiesB, m_uvB);
        }
    }

    void AddVertexToMesh(int index, Dictionary<int, int> indiciesMesh, List<Vector3> verticies, List<Vector2> uv)
    {
        if(indiciesMesh.ContainsKey(index) == false)
        {
            verticies.Add(m_verticiesMeshToSlice[index]);
            indiciesMesh.Add(index, verticies.Count - 1);
            uv.Add(m_uvMeshToSlice[index]);
        }
    }

    void AddVertexToMesh(int index, bool side)
    {
        if(side)
        {
            AddVertexToMesh(index, _indiciesMeshA, m_verticiesA, m_uvA);
        }
        else
        {
            AddVertexToMesh(index, _indiciesMeshB, m_verticiesB, m_uvB);
        }
    }

    void RecreateFaceFromTriangle(Triangle triangle, bool side, List<int> trianglesMesh, Dictionary<int, int> indiciesMesh)
    {
        int vertexOnMySide = 0;
        if (triangle.SideA == side) { vertexOnMySide++; }
        if (triangle.SideB == side) { vertexOnMySide++; }
        if (triangle.SideC == side) { vertexOnMySide++; }

        if(vertexOnMySide == 2)
        {
            if(triangle.SideA == triangle.SideB)
            {
                trianglesMesh.Add(indiciesMesh[triangle.A]);
                trianglesMesh.Add(indiciesMesh[triangle.B]);
                trianglesMesh.Add(indiciesMesh[triangle.CutAC]);

                trianglesMesh.Add(indiciesMesh[triangle.B]);
                trianglesMesh.Add(indiciesMesh[triangle.CutBC]);
                trianglesMesh.Add(indiciesMesh[triangle.CutAC]);

            }
            else if(triangle.SideB == triangle.SideC)
            {
                trianglesMesh.Add(indiciesMesh[triangle.B]);
                trianglesMesh.Add(indiciesMesh[triangle.C]);
                trianglesMesh.Add(indiciesMesh[triangle.CutAB]);

                trianglesMesh.Add(indiciesMesh[triangle.C]);
                trianglesMesh.Add(indiciesMesh[triangle.CutAC]);
                trianglesMesh.Add(indiciesMesh[triangle.CutAB]);
            }
            else if (triangle.SideA == triangle.SideC)
            {
                trianglesMesh.Add(indiciesMesh[triangle.C]);
                trianglesMesh.Add(indiciesMesh[triangle.A]);
                trianglesMesh.Add(indiciesMesh[triangle.CutBC]);

                trianglesMesh.Add(indiciesMesh[triangle.A]);
                trianglesMesh.Add(indiciesMesh[triangle.CutAB]);
                trianglesMesh.Add(indiciesMesh[triangle.CutBC]);
            }
        }
        else if(vertexOnMySide == 1)
        {
            if (triangle.SideA == triangle.SideB) { trianglesMesh.Add(indiciesMesh[triangle.C]); trianglesMesh.Add(indiciesMesh[triangle.CutAC]); trianglesMesh.Add(indiciesMesh[triangle.CutBC]); }
            else if (triangle.SideB == triangle.SideC) { trianglesMesh.Add(indiciesMesh[triangle.A]); trianglesMesh.Add(indiciesMesh[triangle.CutAB]); trianglesMesh.Add(indiciesMesh[triangle.CutAC]); }
            else if (triangle.SideA == triangle.SideC) { trianglesMesh.Add(indiciesMesh[triangle.B]); trianglesMesh.Add(indiciesMesh[triangle.CutBC]); trianglesMesh.Add(indiciesMesh[triangle.CutAB]); }
        }
    }

    Vector3 RecalculatePivot(List<Vector3> vertices)
    {
        Vector3 barycenter = Vector3.zero;
        for (int i = 0; i < vertices.Count; i++)
        {
            barycenter += vertices[i];
        }
        barycenter /= vertices.Count;

        Vector3 direction = -barycenter;
        for (int i = 0; i < vertices.Count; i++)
        {
            vertices[i] += direction;
        }

        return barycenter;
        //mesh.UploadMeshData();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Table" && transform.GetComponentInChildren<BladeSpeedController>().VelocityY < -0.3f)
        {
            GameObject[] cutables = GameObject.FindGameObjectsWithTag("Cutable");
            for (int i = 0; i < cutables.Length; i++)
            {
                //cutables[i].GetComponent<CutableMeshController>().SliceDirect();
            }
            SdAudioManager.Trigger("Knife_WoodHit", gameObject);
        }
    }

    Vector3 CreateFaceFromIntersections()
    {
        //Face
        Vector3 barycentre = Vector3.zero;
        List<int> indiciesIntersection = new List<int>();

        //Uv
        m_boundsIntersection = new Bounds();

        //projectedIntersectionVerticies
        m_projectedIntersectionVerticies = new List<Vector3>();


        //Pour tout les points, on les duplique (pour les normales)
        //On calcul aussi le barycentre
        for (int i = m_meshToSlice.vertexCount; i < m_verticiesMeshToSlice.Count; i++)
        {
            bool alreadyInList = false;
            for (int j = 0; j < indiciesIntersection.Count; j++)
            {
                if (m_verticiesMeshToSlice[indiciesIntersection[j]] == m_verticiesMeshToSlice[i])
                {
                    alreadyInList = true;
                    break;
                }
            }

            //Si on l'a pas deja duplique
            if(alreadyInList == false)
            {
                barycentre += m_verticiesMeshToSlice[i];

                //Add point to meshToSlice
                m_verticiesMeshToSlice.Add(m_verticiesMeshToSlice[i]);

                m_projectedIntersectionVerticies.Add(transform.InverseTransformPoint(_objectToSlice.transform.TransformPoint(m_verticiesMeshToSlice[i])));
                m_boundsIntersection.Encapsulate(m_projectedIntersectionVerticies.Last());

                //Add point to meshA
                m_verticiesA.Add(m_verticiesMeshToSlice[i]);
                _indiciesMeshA.Add(m_verticiesMeshToSlice.Count - 1, m_verticiesA.Count - 1);

                //Add point to meshB
                m_verticiesB.Add(m_verticiesMeshToSlice[i]);
                _indiciesMeshB.Add(m_verticiesMeshToSlice.Count - 1, m_verticiesB.Count - 1);

                //Add point index to a list
                indiciesIntersection.Add(m_verticiesMeshToSlice.Count - 1);
            }

        }

        barycentre /= indiciesIntersection.Count;
        
        m_verticiesA.Add(barycentre);
        m_verticiesB.Add(barycentre);

        m_projectedIntersectionVerticies.Add(transform.InverseTransformPoint(_objectToSlice.transform.TransformPoint(barycentre)));
        m_boundsIntersection.Encapsulate(m_projectedIntersectionVerticies.Last());

        TextureData textureData = _objectToSlice.GetComponent<TextureData>();

        //UV Compute from data texture
        for (int i = 0; i < m_projectedIntersectionVerticies.Count; i++)
        {

            Vector2 normalizedVertex = new Vector2(
                (m_projectedIntersectionVerticies[i].z - m_boundsIntersection.min.z) / (m_boundsIntersection.max.z - m_boundsIntersection.min.z),
                (m_projectedIntersectionVerticies[i].y - m_boundsIntersection.min.y) / (m_boundsIntersection.max.y - m_boundsIntersection.min.y));

            Vector2 uv = textureData.UV(normalizedVertex);
            m_uvA.Add(uv);
            m_uvB.Add(uv);
        }

        Plane p = new Plane(barycentre, m_verticiesMeshToSlice[indiciesIntersection[0]], barycentre + _objectToSlice.transform.InverseTransformDirection(m_plane.normal.normalized));

        List<float> angles = new List<float>();
        angles.Add(0);

        List<int> counterclockwiseOrdered = new List<int>();
        counterclockwiseOrdered.Add(indiciesIntersection[0]);

        //List<Vector3> listIntersections = new List<Vector3>();

        for (int i = 1; i < indiciesIntersection.Count; i++)
        {
            float angle = Vector3.Angle(m_verticiesMeshToSlice[indiciesIntersection[0]] - barycentre, m_verticiesMeshToSlice[indiciesIntersection[i]] - barycentre);
            if (p.GetSide(m_verticiesMeshToSlice[indiciesIntersection[i]]) == false)
            {
                angle = 360 - angle;
            }
            int indexToInsert = 1;
            while(indexToInsert < angles.Count && angle > angles[indexToInsert])
            {
                indexToInsert++;
            }
            if(indexToInsert == angles.Count)
            {
                counterclockwiseOrdered.Add(indiciesIntersection[i]);
                angles.Add(angle);
            }
            else
            {
                counterclockwiseOrdered.Insert(indexToInsert, indiciesIntersection[i]);
                angles.Insert(indexToInsert, angle);
            }
        }


        //m_counterclockwiseOrderedIntersection = counterclockwiseOrdered;

        for (int i = 0; i < counterclockwiseOrdered.Count - 1; i++)
        {
            //listIntersections.Add(m_verticiesMeshToSlice[counterclockwiseOrdered[i]]);

            m_trianglesA.Add(m_verticiesA.Count - 1);
            m_trianglesA.Add(_indiciesMeshA[counterclockwiseOrdered[i + 0]]);
            m_trianglesA.Add(_indiciesMeshA[counterclockwiseOrdered[i + 1]]);

            m_trianglesB.Add(m_verticiesB.Count - 1);
            m_trianglesB.Add(_indiciesMeshB[counterclockwiseOrdered[i + 1]]);
            m_trianglesB.Add(_indiciesMeshB[counterclockwiseOrdered[i + 0]]);
        }

        //m_ccwIntersections.Add(listIntersections);

        m_trianglesA.Add(m_verticiesA.Count - 1);
        m_trianglesA.Add(_indiciesMeshA[counterclockwiseOrdered[counterclockwiseOrdered.Count - 1]]);
        m_trianglesA.Add(_indiciesMeshA[counterclockwiseOrdered[0]]);

        m_trianglesB.Add(m_verticiesB.Count - 1);
        m_trianglesB.Add(_indiciesMeshB[counterclockwiseOrdered[0]]);
        m_trianglesB.Add(_indiciesMeshB[counterclockwiseOrdered[counterclockwiseOrdered.Count - 1]]);

        return _objectToSlice.transform.TransformPoint(barycentre);
    }

    void InitializePart(GameObject part)
    {
        /*CutableMeshController cm = part.GetComponent<CutableMeshController>();
        cm._isBaseModel = false;
        cm._nameBase = _nameBase;
        cm.tag = tag;
        cm._fxDrops = _fxDrops;
        cm._sliceType = _sliceType;*/
        //part.GetComponent<Highlighter>().UnHighlight();
        Ingredient srcIng = _objectToSlice.GetComponent<Ingredient>();
        Ingredient ing = part.GetComponent<Ingredient>();
        ing._inBasket = false;
        ing._fxDropsPrefab = srcIng._fxDropsPrefab;
        ing._sliceType = srcIng._sliceType;
        ing._couldBeGrabbed = srcIng._couldBeGrabbed;

        //Bump information
        ing._bumpCurve = srcIng._bumpCurve;
        ing._startCuttingTime = srcIng._startCuttingTime;
        ing._bumpTimer = srcIng._bumpTimer;
        ing._bumpStartPosition = srcIng._bumpStartPosition;

        TextureData td = part.GetComponent<TextureData>();
        td._topLeft = _objectToSlice.GetComponent<TextureData>()._topLeft;
        td._bottomRight = _objectToSlice.GetComponent<TextureData>()._bottomRight;

        part.transform.parent = _slicePart;

        part.rigidbody.freezeRotation = true;
    }

    void OnDrawGizmos()
    {
        //if (m_ccwIntersection != null)
        //{
        //    for (int i = 0; i < m_ccwIntersection.Count; i++)
        //    {
        //        Gizmos.DrawSphere(m_ccwIntersection[i], 0.01f);
        //    }
        //}
        /*if(m_end != null)
        //{
            for (int i = 0; i < m_end.Count; i++)
            {
                Gizmos.DrawLine(_centerOfSlashes.position, _centerOfSlashes.position + m_end[i]);
            }
        }

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position - transform.up);*/
    }

}
