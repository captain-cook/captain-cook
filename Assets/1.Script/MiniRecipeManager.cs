﻿using UnityEngine;
using System.Collections;

public class MiniRecipeManager : MonoBehaviour {

    public RecipeCard _recipeCard;

    public HandController _leftHand;
    public HandController _rightHand;

    private Camera _mainCamera;

    private TweenPosition[] m_tPositions;
    public float _moveSpeed = 0.5f;

	// Use this for initialization
	void Start () {

        _mainCamera = Camera.main;

        m_tPositions = GetComponentsInChildren<TweenPosition>();
        foreach (TweenPosition tp in m_tPositions)
        {
            tp.duration = _moveSpeed;
        }

        Activate(false);
	}
	
	// Update is called once per frame
	void Update () {

        //if (Input.GetKeyUp(KeyCode.Alpha1))
        //{
        //    Activate(true);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha2))
        //{
        //    Activate(false);
        //}
	}    

    //activate the recipe selection
    public void Activate(bool activate)
    {
        
        foreach (TweenPosition tp in m_tPositions)
        {
            if (activate)
            {
                tp.PlayForward();
                tp.GetComponent<MiniRecipeCard>().enabled = true;
                tp.GetComponent<BoxCollider>().enabled = true;
            }
                
            else
            {
                tp.PlayReverse();
                tp.GetComponent<MiniRecipeCard>().enabled = false;
                tp.GetComponent<BoxCollider>().enabled = false;
            }
                
        }
    }


    
   
}
