﻿using UnityEngine;
using System.Collections;

public class OldTourniquet : MonoBehaviour {

    public int _nbItemMax = 30;
    public float _rotateSpeed = 1;
    public GameObject TScroll;
    public SORecipe[] m_recipes;

    //link to the Tscroll element in the scene
    private TScroll[] m_tscrolls;
    private Vector3[] m_spots;
    
    //rotation variables
    private float m_angle;
    private float m_radius;
    private int m_indTScroll = 0;
    private bool m_isRotating = false;
    private float m_targetRotation;
    private float m_step;
    private Transform m_firstSpot;

	// Use this for initialization
	void Start () {

        m_firstSpot = transform.FindChild("Spot1").transform;
        //get the spot of tscroll
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Spot");
        m_spots = new Vector3[gos.Length];
        for (int i = 0; i < m_spots.Length ; i++)
        {
            m_spots[i] = gos[gos.Length - 1 - i].transform.position;
            Debug.Log("name = " + gos[i].name);
        }
        
        m_angle = 360 / _nbItemMax;
        m_radius = (transform.position - m_firstSpot.position).magnitude;
        
        //create the tscrolls item
        m_tscrolls = new TScroll[m_recipes.Length];
        CreateTScrolls();

        //initialise the to the first tscroll
        transform.eulerAngles = new Vector3(0 , ScrollToAngle(m_tscrolls[0].transform) , 0);
	}
	
	// Update is called once per frame
	void Update () {

        int oldInd = m_indTScroll;
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            m_indTScroll++;
            m_step = _rotateSpeed * Time.deltaTime;
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (m_indTScroll <= 0)
                m_indTScroll = m_tscrolls.Length - 1;
            else
                m_indTScroll--;
            m_step = -_rotateSpeed * Time.deltaTime;
        }

        //launch the rotation
        if (oldInd != m_indTScroll && !m_isRotating)
        {
            RotateToScroll(m_tscrolls[m_indTScroll % m_tscrolls.Length]);
        }

        //rotate
        if (m_isRotating)
        {
            transform.eulerAngles += new Vector3(0, m_step, 0);

            //rotation is finished
            if (Mathf.Abs( transform.eulerAngles.y - m_targetRotation ) < m_step)
            {
                m_isRotating = false;
                transform.eulerAngles = new Vector3(0, m_targetRotation, 0);
            }
        }
        
	}

    //create on tscroll for every recipe known 
    void CreateTScrolls()
    {
        int indEntree = 0;
        int indMeal = 0;
        int indDesert = 0;

        for (int i = 0; i < m_recipes.Length; i++ )
        {
            //create the prefab
            GameObject ts = Instantiate(TScroll) as GameObject;
            ts.transform.parent = this.transform;

            //store the tscroll with every recipe data
            m_tscrolls[i] = ts.GetComponent<TScroll>();
            m_tscrolls[i].Init(m_recipes[i]);

            //set the higher position depending on the tourniquet position
            //ts.transform.localPosition = new Vector3(0f, transform.localScale.y * 0f, 0f);
            
            //set the position of the scroll around the tourniquet
            SetScrollPosition(ref indEntree, ref indMeal, ref indDesert , m_tscrolls[i]);
        }
    }

    //update the position of the scroll to dispatch them aroudn the tourniquet
    void SetScrollPosition ( ref int entree , ref int meal , ref int desert , TScroll tscroll)
    {
        float angle;
        
        switch (tscroll._recipeData._recipeType)
        {
            case SORecipe.Type.Entree:
                tscroll.transform.position = m_spots[entree];
                entree++;
                break;

            case SORecipe.Type.Meal:
                tscroll.transform.position = m_spots[meal+10];
                meal++;
                break;

            case SORecipe.Type.Desert:
                tscroll.transform.position = m_spots[desert+ 20];
                desert++;
                break;
        }

        //switch ( tscroll._recipeData._recipeType )
        //{                
        //    case SORecipe.Type.Entree :
        //        angle = Mathf.Deg2Rad * (entree * m_angle + offset);
        //        tscroll.transform.position += new Vector3(m_radius * Mathf.Cos(angle), 0, m_radius * Mathf.Sin(angle));
        //        entree++;
        //        break;

        //    case SORecipe.Type.Meal :
        //        angle = Mathf.Deg2Rad * (meal * m_angle + 120 + offset);
        //        tscroll.transform.position += new Vector3(m_radius * Mathf.Cos(angle), 0, m_radius * Mathf.Sin(angle));
        //        meal++;
        //        break;

        //    case SORecipe.Type.Desert :
        //        angle = Mathf.Deg2Rad * (desert * m_angle + 240 + offset);
        //        tscroll.transform.position += new Vector3(m_radius * Mathf.Cos(angle), 0, m_radius * Mathf.Sin(angle));
        //        desert++;
        //        break;
        //}
    }

    //return the rotation the tourniquet should be rotate whith to be in front of the tscroll
    float ScrollToAngle(Transform trans)
    {
        float angle = Mathf.Rad2Deg * Mathf.Atan(trans.localPosition.z / trans.localPosition.x);
        if (trans.localPosition.x < 0)  angle -= 90;
        else angle += 90 ;

        if (angle < 0) angle += 360;

        return angle;
    }

    //rotate the tourniquet to the spécifide tscroll
    void RotateToScroll(TScroll tscroll)
    {
        m_isRotating = true;
        m_targetRotation = ScrollToAngle(tscroll.transform);
        Debug.Log("target = " + m_targetRotation);
        //transform.eulerAngles = new Vector3(0, ScrollToAngle(tscroll), 0);
    }

    
}
