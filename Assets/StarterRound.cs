﻿using UnityEngine;
using System.Collections;

public class StarterRound : ReferencedMonoBehaviour {

    private UISprite m_sprite;
    private TweenScale m_tScale;
    private bool m_isDisplayed = false;

    public float _delay = 1;
    public float _duration = 0.3f;

    public TweenScale _tScaleTools;

    public TweenRotation _tRotationFork;
    public TweenRotation _tRotationKnife;

    public TweenPosition _tPositionFork;
    public TweenPosition _tPositionKinfe;

	// Use this for initialization
	void Start () {
        
        base.LoadReferences();
        m_sprite = GetComponent<UISprite>();
        m_tScale = GetComponent<TweenScale>();

        _tScaleTools.delay = m_tScale.duration * 0.5f;
        _tScaleTools.duration = m_tScale.duration * 0.5f;

        _tRotationFork.delay = _delay;
        _tPositionFork.delay = _delay;

        _tRotationKnife.delay = _delay;
        _tPositionKinfe.delay = _delay;

        _tRotationFork.duration = _duration;
        _tPositionFork.duration = _duration;

        _tRotationKnife.duration = _duration;
        _tPositionKinfe.duration = _duration;

        transform.parent.gameObject.SetActive(false);
	}
	
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    DisplayStarterRound();
        //}
    }

    IEnumerator PlaySounds()
    {
        yield return new WaitForSeconds(0.5f);
        SdAudioManager.Trigger("UI_Round_Round");
        yield return new WaitForSeconds(0.40f);
        SdAudioManager.Trigger("UI_RecipeAnnounce_" + GetPhaseName());
        yield return new WaitForSeconds(1.05f);
        SdAudioManager.Trigger("UI_Round_Round");
    }

    string GetPhaseName()
    {
        switch(references._binomialManager._numPhase)
        {
            case 0 :
                return "Starter";
            case 1 :
                return "MainCourse";
            case 2 :
                return "Dessert";
        }
        
        return "Starter";
    }

	public void DisplayStarterRound()
    {
        transform.parent.gameObject.SetActive(true);
        
        m_isDisplayed = true;

        StartCoroutine(PlaySounds());
        //SdAudioManager.Trigger("UI_Round_Round");

        //if (references == null)
        //    LoadReferences();

        //upgrate the scroll
        SetSprite();
        //m_tScale.ResetToBeginning();
        transform.localScale = Vector3.zero;        
        m_tScale.PlayForward();

        _tRotationFork.GetComponent<UISprite>().enabled = false;
        _tRotationKnife.GetComponent<UISprite>().enabled = false;

        //animate the fork
        _tRotationFork.ResetToBeginning();
        _tPositionFork.ResetToBeginning();

        //animate the knife
        _tRotationKnife.ResetToBeginning();
        _tPositionKinfe.ResetToBeginning();
    }

    
    public void EndOfScale()
    {
        //the panel is displayed
        if(m_isDisplayed)
        {
            m_isDisplayed = false;
            m_tScale.delay = 0f;
            _tRotationFork.GetComponent<UISprite>().enabled = true;
            _tRotationKnife.GetComponent<UISprite>().enabled = true;

            _tRotationFork.PlayForward();
            _tPositionFork.PlayForward();

            _tRotationKnife.PlayForward();
            _tPositionKinfe.PlayForward();

        }      
        else
        {
            m_tScale.delay = 0.5f;
        }
    }

    //launch closiong animation
    public void CloseStarterRound()
    {
        m_tScale.PlayReverse();

        _tScaleTools.ResetToBeginning();
        _tScaleTools.PlayForward();
    }

   
    //Update the sprite depending on the entree/Meal/desert
    void SetSprite()
    {

        switch (references._binomialManager._numPhase)
        {
            case 0 :
                m_sprite.spriteName = "rounds_bannerfork";
                break;

            case 1 :
                m_sprite.spriteName = "rounds_bannermaincourse";

                break;

            case 2 :
                m_sprite.spriteName = "rounds_bannerdessert";

                break;

            default :
                m_sprite.spriteName = "rounds_bannerfork";
                break;
        }
    }
}
