﻿using UnityEngine;
using System.Collections;

public class Map : ReferencedMonoBehaviour {

	// Use this for initialization
	void Start () {

        base.LoadReferences();
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //start
    public void SoundWheelMap()
    {
        
            SdAudioManager.Trigger("UI_Map_Wheel", references._mapWheelDeMerde);

    }
}
