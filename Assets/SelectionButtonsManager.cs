﻿using UnityEngine;
using System.Collections;

public class SelectionButtonsManager : MonoBehaviour {

    public void EnableAllButtons()
    {
        SelectionButton[] buttons = transform.GetComponentsInChildren<SelectionButton>();
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i]._enabled = true;
            buttons[i].rigidbody.constraints ^= RigidbodyConstraints.FreezePositionY;
        }
    }

    public void ResetButtonsPosition()
    {
        SelectionButton[] buttons = transform.GetComponentsInChildren<SelectionButton>();
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].transform.localPosition = buttons[i]._resetLocalPosition;
        }
    }
	
}
