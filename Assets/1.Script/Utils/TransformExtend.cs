﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public static class TransformExtend {

    public static Transform SearchChild(this Transform me, string name)
    {
        if (me.name == name) { return me; }
        Transform[] list = me.GetComponentsInChildren<Transform>();
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].name == name) { return list[i]; }
        }
        return null;
    }

}
