﻿using UnityEngine;
using System.Collections;

public class PlanchaButton : ReferencedMonoBehaviour {

	// Use this for initialization
	void Start () {

        base.LoadReferences();

        gameObject.SetActive(false);

	}
	
	void OnTriggerEnter(Collider other )
    {
        if (other.collider.tag == "Hand")
        {
            HandControllerBaking hand = other.GetComponent<HandControllerBaking>();
            /*if (hand.transform.position.y > 4.80f
                //&& hand._handController.GetHandState() == KinectBody.HandState.Closed
                && hand._handController.GetHandSpeed() > 0.008f
                )
            {
                references._plancha.Bump();
            }*/

            if(hand._handController.GetHandYSpeed() < -0.008f)
            {
                references._plancha.Bump();
            }
            
       }
    }
}
