Switch Group	ID	Name			Wwise Object Path	Notes
	186852181	Thunder			\Default Work Unit\AMB\Weather\Thunder	
	317282339	Weather			\Default Work Unit\AMB\Weather\Weather	
	1863812544	WeatherBehaviour			\Default Work Unit\AMB\Weather\WeatherBehaviour	

Switch	ID	Name	Switch Group			Notes
	1183803292	Far	Thunder			
	1451272583	Close	Thunder			
	2136542155	Midclose	Thunder			
	742231792	Windy	Weather			
	2599410036	Rainy	Weather			
	3569642402	Sunny	Weather			
	1747842238	Moderate	WeatherBehaviour			
	3522403288	Strong	WeatherBehaviour			
	3753286132	Calm	WeatherBehaviour			

State Group	ID	Name			Wwise Object Path	Notes
	1453254850	Pirate_Behaviour_Work			\Default Work Unit\Pirates\Pirate_Behaviour_Work	
	1729018809	Pirate_Behaviour_Combat			\Default Work Unit\Pirates\Pirate_Behaviour_Combat	
	3489612188	Pirate_Behaviour_Normal			\Default Work Unit\Pirates\Pirate_Behaviour_Normal	

State	ID	Name	State Group			Notes
	748895195	None	Pirate_Behaviour_Work			
	1160234136	Normal	Pirate_Behaviour_Work			
	2556020212	Good_Mood	Pirate_Behaviour_Work			
	4181648385	Serious	Pirate_Behaviour_Work			
	180661997	Attack	Pirate_Behaviour_Combat			
	691317703	Provoke	Pirate_Behaviour_Combat			
	748895195	None	Pirate_Behaviour_Combat			
	979765101	Win	Pirate_Behaviour_Combat			
	1719082483	KO	Pirate_Behaviour_Combat			
	3193947170	Hurt	Pirate_Behaviour_Combat			
	3632828356	Help	Pirate_Behaviour_Combat			
	514064485	Fight	Pirate_Behaviour_Normal			
	748895195	None	Pirate_Behaviour_Normal			
	805220764	Sing	Pirate_Behaviour_Normal			
	1874288895	Idle	Pirate_Behaviour_Normal			
	3728748628	Joke	Pirate_Behaviour_Normal			

Game Parameter	ID	Name			Wwise Object Path	Notes
	509499584	Amb_Volume			\Default Work Unit\Volume\Amb_Volume	
	619609672	Vfx_Volume			\Default Work Unit\Volume\Vfx_Volume	
	889708287	Mus_Volume			\Default Work Unit\Volume\Mus_Volume	
	1564184899	Sfx_Volume			\Default Work Unit\Volume\Sfx_Volume	
	4179668880	Master_Volume			\Default Work Unit\Volume\Master_Volume	

Audio Bus	ID	Name			Wwise Object Path	Notes
	444401471	VFX Audio Bus			\Default Work Unit\Master Audio Bus\VFX Audio Bus	
	805203703	Master Secondary Bus			\Default Work Unit\Master Secondary Bus	
	2770274119	AMB Audio Bus			\Default Work Unit\Master Audio Bus\AMB Audio Bus	
	3376092206	MUS Audio Bus			\Default Work Unit\Master Audio Bus\MUS Audio Bus	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	4084199202	SFX Audio Bus			\Default Work Unit\Master Audio Bus\SFX Audio Bus	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	1162477199	BoatReverb			\Default Work Unit\Master Audio Bus\VFX Audio Bus\BoatReverb	

Effect plug-ins	ID	Name	Type				Notes
	352718863	Wwise Meter (Custom)	Wwise Meter			
	540398251	CaptainCook_VFX_EQ	Wwise Parametric EQ			
	597707427	Wwise Peak Limiter (Custom)	Wwise Peak Limiter			
	1502954315	CaptainCook_AMB_EQ	Wwise Parametric EQ			
	2891462749	Room_Medium_High_Absorbtion	Wwise RoomVerb			

