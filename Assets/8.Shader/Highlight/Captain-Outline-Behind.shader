Shader "Captain-Cook/Outline-Behind" {
		Properties
		{
			_OutlineColor("Outline Color", Color) = (0.5, 0.5, 0.5, 1)
			_OutlineSize("Outline Size", Float) = 0.1
			_Barycenter("Barycenter", Vector) = (0, 0, 0, 1)
		}

		SubShader
		{
				CGINCLUDE
					fixed4 _OutlineColor;
					float _OutlineSize;
					float4 _Barycenter;

					#include "UnityCG.cginc"

					struct v2f
					{
						float4 pos : SV_POSITION;
						float4 color : COLOR;
					};

					v2f vert(appdata_base v)
					{
						v2f o;
						float4 relative = float4(
							v.vertex.x - _Barycenter.x,
							v.vertex.y - _Barycenter.y,
							v.vertex.z - _Barycenter.z,
							0);

						relative *= _OutlineSize;

						o.pos = mul(UNITY_MATRIX_MVP, _Barycenter + relative);
						o.color = _OutlineColor;
						return o;
					}

					half4 frag(v2f i) :COLOR
					{
						return i.color;
					}
				ENDCG

				Tags { "Queue" = "Overlay" }
				ZWrite On
				ZTest Greater
				Cull Back

				Pass
				{
					Name "BEHIND"
					Tags { "LightMode" = "Always" }
					/*Cull Front
					ZWrite On
					ZTest Greater
					LOD 200*/

					CGPROGRAM
						#pragma vertex vert
						#pragma fragment frag
					ENDCG
				}
		}
		//FallBack "Diffuse"
}
