﻿using UnityEngine;
using System.Collections;

public class MayaBoneScaleCompensator : MonoBehaviour {

    private Vector3 m_oldLocalScale;

    private bool m_compensate = false;
    private Vector3 m_compensateRatio;

    private Transform child;

	void Start () {
        m_oldLocalScale = transform.localScale;
        if(transform.childCount != 1)
        {
            this.enabled = false;
        }
        else
        {
            child = transform.GetChild(0);
        }
	}
	
	void Update () {
	    if(m_oldLocalScale != transform.localScale)
        {
            Vector3 ratio = Vector3.zero;
            ratio.x = (m_oldLocalScale.x / transform.localScale.x);
            ratio.y = (m_oldLocalScale.y / transform.localScale.y);
            ratio.z = (m_oldLocalScale.z / transform.localScale.z);
            child.GetComponent<MayaBoneScaleCompensator>().Compensate(ratio);

            m_oldLocalScale = transform.localScale;
        }

        if (m_compensate == true)
        {
            Debug.Log("Compensate");
            transform.localScale = Vector3.Scale(transform.localScale, m_compensateRatio);
            m_oldLocalScale = transform.localScale;
            m_compensate = false;
        }
	}

    //void LateUpdate()
    //{
    //    if(compensate == true)
    //    {
    //        transform.localScale = Vector3.Scale(transform.localScale, compensateRatio);
    //        oldLocalScale = transform.localScale;
    //        compensate = false;
    //    }
    //}

    public void Compensate(Vector3 ratio)
    {
        Debug.Log("Compensator");

        m_compensateRatio = ratio;
        m_compensate = true;
    }
}
