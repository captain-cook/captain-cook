﻿using UnityEngine;
using System.Collections;

public class BoatMama : ReferencedMonoBehaviour {

    private Animation m_animation;
    private TweenPosition m_tPosition;

	// Use this for initialization
	void Start () {

        

        base.LoadReferences();

        m_animation = GetComponent<Animation>();
        m_animation["BoatMama"].time = 0;
        m_animation["BoatMama"].speed = 0;
        


        m_tPosition = GetComponent<TweenPosition>();
        m_tPosition.ResetToBeginning();
        m_tPosition.enabled = false;

        gameObject.SetActive(false);
	}
	
	

    //launch the animation of the mama boat traveling toward us
    public void StartArrival()
    {
        gameObject.SetActive(true);
        SdAudioManager.Trigger("Music_BossComing");
        m_tPosition.PlayForward();
    }

    //launch the events after the boat is in place
    public void MamaArrived()
    {
        //SdAudioManager.Trigger("Boss_Mama_AbortTaunt", gameObject);

        references._binomialManager.StartCoroutine("JumpEnemies");

    }

    public IEnumerator WaitForMamaTable()
    {
        m_animation["BoatMama"].speed = 1;
        yield return new WaitForSeconds(3f);
        references._vsPanel.DisplayVSPanel();
    }
    
    public void PlaySound(string name)
    {
        SdAudioManager.Trigger("Boat_"+name , gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        SdAudioManager.Trigger("Boat_Rattle", gameObject);
    }
}
