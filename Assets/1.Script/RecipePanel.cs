﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RecipePanel : MonoBehaviour {

    public Text _title;
    public Image _image;
    public Image _ingredient1;
    public Image _ingredient2;
    public Image _ingredient3;
    public Image _ingredient4;
    public Image _interraction1;
    public Image _interraction2;
    public Image _interraction3;
    public Image _interraction4;
    public Image _stat1;
    public Image _stat2;
    public Image _stat3;
    public Image _stat4;
    public Image _sealOfApprouval1;
    public Image _sealOfApprouval2;
    public Image _plus;

    public Sprite _sealOfApprouvalNeutral;


    void Start()
    {
        gameObject.SetActive(false);
    }


	public void UpdateRecipePanel(SORecipe data, SOPirate pirate1 , SOPirate pirate2)
    {
        _title.text = data._name;
        _image.sprite = data._image;
        _ingredient1.sprite = data._recipeSteps[0]._ingredient._image;
        _ingredient2.sprite = data._recipeSteps[1]._ingredient._image;
        _ingredient3.sprite = data._recipeSteps[2]._ingredient._image;

        _interraction1.sprite = data._recipeSteps[0]._imageInterraction;
        _interraction2.sprite = data._recipeSteps[1]._imageInterraction;
        _interraction3.sprite = data._recipeSteps[2]._imageInterraction;

        //if there is a 4eme ingredient
        if (data._recipeSteps.Length > 3 
            && data._recipeSteps[3]._action != SORecipe.Interaction.Toast
            && data._recipeSteps[3]._action != SORecipe.Interaction.Bake
            )
        {
            _ingredient4.sprite = data._recipeSteps[3]._ingredient._image;
            _interraction4.sprite = data._recipeSteps[3]._imageInterraction;
            _plus.enabled = true;
            _ingredient4.enabled = true;
            _interraction4.enabled = true;
        }
        else
        {
            _plus.enabled = false;
            _ingredient4.enabled = false;
            _interraction4.enabled = false;
        }

        _stat1.sprite = data._tastes[1]._image;
        _stat2.sprite = data._tastes[2]._image;
        _stat3.sprite = data._tastes[3]._image;
        _stat4.sprite = data._tastes[4]._image;


        int notePirate1 = 0;
        int notePirate2 = 0;

        //seals of approuvals
        foreach ( SOTaste sot in data._tastes)
        {
            //look if the first pirate like or dislike the attribut
            if (sot._tasteType == pirate1._tasteTemp._tasteType)
            {
                if (sot._value == pirate1._tasteTemp._value)
                {
                    notePirate1++;
                }
                else
                {
                    notePirate1--;
                }
            }
            //look if the second pirate like or dislike the attribut
            if (sot._tasteType == pirate2._tasteTemp._tasteType)
            {
                if (sot._value == pirate2._tasteTemp._value)
                {
                    notePirate2++;
                }
                else
                {
                    notePirate2--;
                }
            }
        }

        //set the proper icon depending on the average note
        if (notePirate1 > 0)
            _sealOfApprouval1.sprite = pirate1._sealOfApprouval;
        else if (notePirate1 < 0)
            _sealOfApprouval1.sprite = pirate1._sealOfDisagree;
        else
            _sealOfApprouval1.sprite = _sealOfApprouvalNeutral;

        if (notePirate2 > 0)
            _sealOfApprouval2.sprite = pirate2._sealOfApprouval;
        else if (notePirate2 < 0)
            _sealOfApprouval2.sprite = pirate2._sealOfDisagree;
        else
            _sealOfApprouval2.sprite = _sealOfApprouvalNeutral;
    }
}
