﻿using UnityEngine;
using System.Collections;

public class CrewManager : MonoBehaviour {

    //switch camera position
	public Camera _camera;
	Vector3 _camInitPosition;
    Vector3 _camActualPosition;
    float _step;
    Vector3 _target;
    Vector3 _oldTarget;
    float _zoomTarget;
    float _oldZoomTarget;
    bool _isMoving = false;


	// Use this for initialization
	void Start () {
	
        
		_camInitPosition = _camera.ScreenToWorldPoint(new Vector3 ( 0.5f , 0.5f , 0));
        _zoomTarget = _camera.fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {

        
        if (_isMoving && _step < 1)
        {
            _step += Time.deltaTime;

            //move
            Vector3 actualPosition = Vector3.Lerp(_oldTarget, _target, _step);
            _camera.transform.LookAt(actualPosition);

            //zoom
            float actualZoom = _oldZoomTarget + (_zoomTarget - _oldZoomTarget) * _step;
            _camera.fieldOfView = actualZoom;
                        
            //we arrived
            if (_step >= 1 )
            {
                _isMoving = false;
            }
        }

        // detect if a pirate is selected
		if ( Input.GetMouseButtonDown(0))
		{
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100) && hit.collider.tag == "PirateMenu" )
            {
                _isMoving = true;
                _oldTarget = _target;
                _target = hit.collider.transform.position;
                _oldZoomTarget = _zoomTarget;
                _zoomTarget = 20;
                _step = 0;
            }
		}

        else if (Input.GetMouseButtonDown(1))
        {
            _isMoving = true;
            _oldTarget = _target;
            _target = _camInitPosition;
            _oldZoomTarget = _zoomTarget;
            _zoomTarget = 30;
            _step = 0;
        }
	}

    //move the camera to look at a pirate or return to it initiale state
    IEnumerator MoveCameraCrew(Vector3 target)
    {       
        float step= 0;

        Vector3 old = _camera.transform.eulerAngles;

        //go to initiale position 
        if (target == _camInitPosition)
        {
            while (step < 1)
            {
                Vector3 actualPosition = Vector3.Lerp(old, target, step);
                _camera.transform.LookAt(actualPosition);
                _camera.fieldOfView = 20 + 10 * step;
                step += Time.deltaTime;
            }
        }
        // look at a pirate
        else
        {
            while (step < 1)
            {
                Vector3 actualPosition = Vector3.Lerp(old, target, step);
                _camera.transform.LookAt(actualPosition);
                //_camera.fieldOfView = 30 - 10 * step;
                step += Time.deltaTime;
                Debug.Log("step n°"+step);
            }
        }

        _isMoving = false;
        yield return new WaitForSeconds(1);
    }

}
