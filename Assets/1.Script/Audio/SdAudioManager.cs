using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public class SdAudioManager : MonoBehaviour
{
	private static SdAudioManager m_audioinstance;

    protected float _oldMasterVolume;
    protected float _oldMusVolume;
    protected float _oldAmbVolume;
    protected float _oldSfxVolume;
    protected float _oldVfxVolume;

    protected bool m_started =false;

    public Dictionary<string, iSd> _managerDictionary;

    //Inspector

    [Range(-96, 0)]
    public float _masterVolume;
    [Range(-96, 0)]
    public float _musVolume;
    [Range(-96, 0)]
    public float _ambVolume;
    [Range(-96, 0)]
    public float _sfxVolume;
    [Range(-96, 0)]
    public float _vfxVolume;

    public bool _isbattleMusic = false;

    public bool IsBattleMusic
    {
        get
        {
            return _isbattleMusic;
        }
        set
        {
            _isbattleMusic = value;
        }
    }
	public enum Events
	{
		Play,
		SetSwitch,
		Stop
	};
    public enum BattleMusicState
    {
        Idle,
        Speed
    };
    public BattleMusicState _battleMusicState;
    
    [Range(0,4)]
    public int _battleMusicLevel;
    protected int _prevBattleMusicLevel;

	public static SdAudioManager Audioinstance
	{
		get
		{
			if(m_audioinstance == null)
			{
				m_audioinstance = GameObject.FindObjectOfType<SdAudioManager>();
				DontDestroyOnLoad(m_audioinstance.gameObject);
			}
			return m_audioinstance;
		}
	}

	void Awake(){
		if(m_audioinstance == null)
		{
			m_audioinstance = this;
            _managerDictionary = new Dictionary<string, iSd>();
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			if(this != m_audioinstance)
			{
				Destroy(this.gameObject);
			}
		}
	}
    
    void Start()
    {
        _oldMasterVolume = _masterVolume;
        _oldMusVolume = _musVolume;
        _oldAmbVolume = _ambVolume;
        _oldSfxVolume = _sfxVolume;
        _oldVfxVolume = _sfxVolume;

        SetBUSVolume("Master", _masterVolume);
        SetBUSVolume("Mus", _musVolume);
        SetBUSVolume("Amb", _ambVolume);
        SetBUSVolume("Sfx", _sfxVolume);
        SetBUSVolume("Vfx", _vfxVolume);

        m_audioinstance._managerDictionary.Add("Items", new SdItems());
        m_audioinstance._managerDictionary.Add("RecipeBoard", new SdRecipeBoard());
        m_audioinstance._managerDictionary.Add("RecipeWheel", new SdRecipeWheel());
        m_audioinstance._managerDictionary.Add("ToolBox", new SdToolbox());
        m_audioinstance._managerDictionary.Add("Roller", new SdRoller());
        m_audioinstance._managerDictionary.Add("Knife", new SdKnife());
        m_audioinstance._managerDictionary.Add("Ingredient", new SdIngredient());
        m_audioinstance._managerDictionary.Add("CookBoard", new SdCookBoard());
        m_audioinstance._managerDictionary.Add("Music", new SdMusic());
        m_audioinstance._managerDictionary.Add("UI", new SdUI());
        m_audioinstance._managerDictionary.Add("UIAnchor", new SdUIAnchor());
        m_audioinstance._managerDictionary.Add("Boat", new SdBoat());
        m_audioinstance._managerDictionary.Add("Footsteps", new SdFootsteps());
        m_audioinstance._managerDictionary.Add("Pirate", new SdPirates());
        m_audioinstance._managerDictionary.Add("Boss", new SdBoss());
        m_audioinstance._managerDictionary.Add("Ambiances", new SdAmbiances());

        
        if (_isbattleMusic)
        {
            //Trigger("Music_Battle_Idle_" + _battleMusicLevel);
            Trigger("Music_Battle_" + _battleMusicState.ToString() + "_" + _battleMusicLevel);
            _prevBattleMusicLevel = _battleMusicLevel;
        }
        m_started = true;
        Trigger("Ambiances_General_Calm_Sunny");
    }

    protected void OnValidate()
    {
        /*
        if (m_started)
        {
            if (_isbattleMusic == false)
                Trigger("Music_Battle_Stop_4");
                //Trigger("Music_Battle_Stop");
            Trigger("Music_Battle_" + _battleMusicState.ToString() + "_" + _battleMusicLevel);
        }
        */
    }
    void FixedUpdate()
    {
        if (_oldMasterVolume != _masterVolume)
        {
            SetBUSVolume("Master", _masterVolume);
            _oldMasterVolume = _masterVolume;
        }
        if (_oldMusVolume != _musVolume)
        {
            SetBUSVolume("Mus", _musVolume);
            _oldMusVolume = _musVolume;
        }
        if (_oldAmbVolume != _ambVolume)
        {
            SetBUSVolume("Amb", _ambVolume);
            _oldAmbVolume = _ambVolume;
        }
        if (_oldSfxVolume != _sfxVolume)
        {
            SetBUSVolume("Sfx", _sfxVolume);
            _oldSfxVolume = _sfxVolume;
        }
        if (_oldVfxVolume != _vfxVolume)
        {
            SetBUSVolume("Vfx", _vfxVolume);
            _oldVfxVolume = _vfxVolume;
        }
        /*if (_prevBattleMusicLevel != _battleMusicLevel && _isbattleMusic)
        {
            Trigger("Music_Battle_Idle_" + _battleMusicLevel);
            _prevBattleMusicLevel = _battleMusicLevel;
        }*/
    }
    public static void TriggerEvent(GameObject audiosource = null, params object[] args)
    {
        string arg1 = (string)args[0];
        if (arg1.Contains("_Stop"))
        {
            Stop((string)args[0],(int)args[1], audiosource);
        }
        else if (arg1.Contains("_Switch"))
        {
            SetSwitch((string)args[0], (string)args[1], audiosource);
        }
        else//if (arg1.Contains("_Play"))
        {
            Play((string)args[0], audiosource);
        }
        
    }
	public static void TriggerEvent(Events eventtype, GameObject audiosource = null, params object[] args)
	{
		switch(eventtype)
		{
			case Events.Play:
				Play((string)args[0], audiosource);
				break;
			case Events.SetSwitch:
				SetSwitch((string)args[0],(string)args[1], audiosource);
				break;
		}
	}
	public static void Play(string eventname, GameObject audiosource)
	{
		AkSoundEngine.PostEvent(eventname,audiosource);
    }
    public static void Pause(string eventname, int duration_ms = 0, GameObject audiosource = null)
    {
        AkSoundEngine.ExecuteActionOnEvent(eventname, AkActionOnEventType.AkActionOnEventType_Pause, audiosource, duration_ms, AkCurveInterpolation.AkCurveInterpolation_Log3);
    }
    public static void Resume(string eventname, int duration_ms = 0, GameObject audiosource = null)
    {
        AkSoundEngine.ExecuteActionOnEvent(eventname, AkActionOnEventType.AkActionOnEventType_Resume, audiosource, duration_ms, AkCurveInterpolation.AkCurveInterpolation_Log3);
    }
    public static void PlayTrigger(string trigger, GameObject audiosource)
    {
        AkSoundEngine.PostTrigger(trigger, audiosource);
    }
	public static void SetSwitch(string switchname, string statename, GameObject audiosource = null)
	{
		AkSoundEngine.SetSwitch(switchname,statename,audiosource);
	}
    public static void Stop(string eventname, int duration_ms = 0, GameObject audiosource = null)
    {
        AkSoundEngine.ExecuteActionOnEvent(eventname, AkActionOnEventType.AkActionOnEventType_Stop, audiosource,duration_ms,AkCurveInterpolation.AkCurveInterpolation_Log3);
    }

    public static void Trigger(string eventname,GameObject go = null)
    {
        string[] objects = eventname.Split('_');
        try
        {
            string action = String.Join("_", objects.ToList().GetRange(1, objects.Length - 1).ToArray());
            Audioinstance._managerDictionary[objects[0]].Execute(action, go);
            
        }
        catch (Exception e)
        {
            Debug.Log("Event '" + objects[0] + "' does not exist");
        }
    }
    public static void SetParameter(string objecttype, float value, GameObject go = null)
    {
        string[] objects = objecttype.Split('_');
        try
        {
            Audioinstance._managerDictionary[objects[0]].SetParameter(objects[1],value, go);
        }
        catch (Exception e)
        {
            Debug.Log("Event '" + objects[0] + "' does not exist");
            Debug.Log("Reason : " + e.Message);
        }
    }
    public static void SetBUSVolume(string busname, float volume)
    {
        if(busname == "Master" || busname == "Mus" || busname == "Amb"
            || busname == "Sfx" || busname == "Vfx")
            AkSoundEngine.SetRTPCValue(busname+"_Volume", volume);
    }
}

