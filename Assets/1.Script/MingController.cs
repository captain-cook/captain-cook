﻿using UnityEngine;
using System.Collections;

public class MingController : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

	#endregion

	#region Unity

	#endregion

	#region Custom

        public void AnimIdleVariantRandom()
        {
            int rand = Random.Range(0, 100);
            GetComponent<Animator>().SetInteger("IdleVariant", rand);
        }

        public void PlaySoundHachetAnimation()
        {
            SdAudioManager.Trigger("Pirate_MingYao_Idle01", gameObject);
        }

	#endregion
	
}
