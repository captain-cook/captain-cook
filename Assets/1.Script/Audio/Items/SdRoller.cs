using UnityEngine;
using System.Collections;

public class SdRoller : SdItems
{
    /*
	private static SdRoller m_instance;
	[Range(0,1)]
    */
	public float _cutSpeed;
	
	private bool m_isCutting = false;
	
	/*
    void Update()
	{
		if (m_instance.m_isCutting)
			SdAudioManager.SetParameter("Roller_RollSpeed", m_instance._cutSpeed, gameObject);
	}
    */
	
	public override void Execute(string action, GameObject source = null) 
	{
		switch (action)
		{
		case "Pickup":
		case "Drop":
		case "WokFall":
			SdAudioManager.SetSwitch("Roller_State", action, source);
			this.m_isCutting = false;
			break;
		case "Roll":
			this.m_isCutting = true;
			SdAudioManager.SetSwitch("Roller_State", action, source);
			break;
		default:
			break;
		}
		SdAudioManager.Play("Roller",source);
	}
	public override void SetParameter(string parameter, float value, GameObject source = null) 
	{
		switch(parameter){
		case "RollSpeed":
			AkSoundEngine.SetRTPCValue("RollSpeed",value,source);
			break;
		default:
			break;
		}
	}
}


