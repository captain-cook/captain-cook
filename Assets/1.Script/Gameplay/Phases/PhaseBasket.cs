﻿using UnityEngine;
using System.Collections;

public class PhaseBasket : Phase {

    public PhaseBasket()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerBasket>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerBasket>();
        
    }
	
    public override void Enter()
    {
        base.Enter();

        if(_references._recipeManager._recipeStart == -1)
        {
            _customHandControllerLeft.enabled = false;
            _customHandControllerRight.enabled = false;
            _references._timerStartRecipe.StartCoroutine("Timer");
            
        }
        else
        {
            _references._basketManager.Activate(true);
        }

        Debug.Log("Salut le petit pot'! <3");
    }

    public override void Exit()
    {
        _references._basketManager.Activate(false);
    }

    public override void EarlyEnter()
    {

    }

    public override void EarlyExit()
    {
        GameObject grabLeft =  _references._handLeft.GetComponent<HandControllerBasket>()._objectGrabbed;
        GameObject grabRight =  _references._handRight.GetComponent<HandControllerBasket>()._objectGrabbed;
        GameObject objectGrabbed = null;

        if (grabLeft != null)
        {
            _references._handLeft.GetComponent<HandControllerCooking>()._objectGrabbed = grabLeft;
            objectGrabbed = grabLeft;
        }
        
        if(grabRight != null)
        {
            _references._handRight.GetComponent<HandControllerCooking>()._objectGrabbed = grabRight;
            objectGrabbed = grabRight;
        }

        if (objectGrabbed != null)
        {
            objectGrabbed.GetComponent<Ingredient>()._forceInWorkspace = true;
            /*if(_references._workspace.InWorkspace(objectGrabbed.transform.position) == true)
            {
                
            }
            else
            {
                objectGrabbed.GetComponent<Ingredient>()._forceInWorkspace = false;
                objectGrabbed.GetComponent<Ingredient>().ToCuttingBoard();
            }*/
        }
        
    }
    
}
