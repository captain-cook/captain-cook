﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseMap : Phase  {

   public PhaseMap()
    {
        _camPosition = PhaseManager.CamPosition.Left;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerMap>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerMap>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
        _references._labelManager.ActivateLabelsUI(true);
        

    }

    public virtual void Exit()
    {
        _references._mapWheelDeMerde.transform.parent.gameObject.SetActive(false);
    }

    public override void EarlyEnter()
    {
        Camera.main.cullingMask ^= 4096;
        _references._handCamera.enabled = true;
        _references._mapWheelDeMerde.transform.parent.gameObject.SetActive(true);
    }

    public override void EarlyExit()
    {
        _references._handCamera.enabled = false;
        Camera.main.cullingMask |= 4096;

       
        SdAudioManager.Trigger("UI_Map_Stop", _references._mapWheelDeMerde);

    }
}
