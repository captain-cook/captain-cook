﻿using UnityEngine;
using System.Collections;

public class AutoScale : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public bool _loop;

        public AnimationCurve _curveX;
        public AnimationCurve _curveY;
        public AnimationCurve _curveZ;

        public float _ratio = 1;

	#endregion

	#region Private

        private float m_startTime;

	#endregion

	#region Unity

		void Start ()
		{
            m_startTime = Time.time;
		}

		void Update ()
		{
            if(_loop == true)
            {
                _curveX.postWrapMode = WrapMode.Loop;
                _curveY.postWrapMode = WrapMode.Loop;
                _curveZ.postWrapMode = WrapMode.Loop;
            }
            else
            {
                _curveX.preWrapMode = WrapMode.Once;
                _curveY.preWrapMode = WrapMode.Once;
                _curveZ.preWrapMode = WrapMode.Once;
            }

            transform.localScale = new Vector3(_curveX.Evaluate(Time.time - m_startTime) * _ratio, _curveY.Evaluate(Time.time - m_startTime) * _ratio, _curveZ.Evaluate(Time.time - m_startTime) * _ratio);

        }

	#endregion

	#region Custom

	#endregion
	
}
