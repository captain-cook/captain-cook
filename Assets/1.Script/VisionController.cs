﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VisionController : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Vector3 _planeIntersection;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{

		}

        void OnDrawGizmos()
        {
            Vector3[] points = CalculateBottomVisibleLine();

            Gizmos.color = Color.white;
            Gizmos.DrawSphere(points[0], 0.01f);
            Gizmos.DrawSphere(points[1], 0.01f);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(points[0], points[1]);

            float y = Mathf.Tan(Mathf.Deg2Rad * Camera.main.fieldOfView * 0.5f) * Camera.main.nearClipPlane;
            float x = (16f * y) / 9f;

            Vector3 bottomLeft = Camera.main.transform.TransformPoint(new Vector3(-x, -y, Camera.main.nearClipPlane) / transform.lossyScale.x);
            Vector3 bottomRight = Camera.main.transform.TransformPoint(new Vector3(x, -y, Camera.main.nearClipPlane) / transform.lossyScale.x);

            Gizmos.DrawSphere(bottomLeft, 0.01f);
            Gizmos.DrawSphere(bottomRight, 0.01f);
        }

	#endregion

	#region Custom

        public Vector3[] CalculateBottomVisibleLine()
        {
            Vector3[] pointsList = new Vector3[2];

            Plane groundPlane = new Plane(Vector3.up, _planeIntersection);
            float dist;

            float y = Mathf.Tan(Mathf.Deg2Rad * Camera.main.fieldOfView * 0.5f) * Camera.main.nearClipPlane;
            float x = (16f * y) / 9f;

            Vector3 bottomLeft = Camera.main.transform.TransformPoint(new Vector3(-x, -y, Camera.main.nearClipPlane) / transform.lossyScale.x);
            Vector3 bottomRight = Camera.main.transform.TransformPoint(new Vector3(x, -y, Camera.main.nearClipPlane) / transform.lossyScale.x);

            // Bottom Left
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(0, 0, 0));
            if (!groundPlane.Raycast(ray, out dist))
                return new Vector3[2] { Vector3.zero, Vector3.zero };
            pointsList[0] = bottomLeft + ray.direction * dist;

            // Bottom Right
            ray = Camera.main.ScreenPointToRay(new Vector3(Camera.main.pixelWidth, 0, 0));
            if (!groundPlane.Raycast(ray, out dist))
                return new Vector3[2] { Vector3.zero, Vector3.zero };
            pointsList[1] = bottomRight + ray.direction * dist;

            return pointsList;
        }


	#endregion
	
}
