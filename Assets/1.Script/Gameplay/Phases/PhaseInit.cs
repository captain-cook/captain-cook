﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseInit : Phase  {

    public PhaseInit()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerCrew>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerCrew>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {

    }

    public override void EarlyEnter()
    {
    }

    public override void EarlyExit()
    {
    }
}
