﻿using UnityEngine;
using System.Collections;

public class NormalDroite : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public float _radius;
        public Vector3 _A;
        public Vector3 _B;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
            float phi = Random.Range(0, 180 * Mathf.Deg2Rad);
            float theta = Random.Range(0, 360  * Mathf.Deg2Rad);
            _A = new Vector3(_radius * Mathf.Sin(phi) * Mathf.Cos(theta), _radius * Mathf.Sin(phi) * Mathf.Sin(theta), _radius * Mathf.Cos(phi));

            phi = Random.Range(0, 180 * Mathf.Deg2Rad);
            theta = Random.Range(0, 360 * Mathf.Deg2Rad);
            _B = new Vector3(_radius * Mathf.Sin(phi) * Mathf.Cos(theta), _radius * Mathf.Sin(phi) * Mathf.Sin(theta), _radius * Mathf.Cos(phi));
		}

		void Update ()
		{

		}

        void OnDrawGizmos()
        {
            Vector3 D = Vector3.Project(-_A, _B - _A);
            Gizmos.DrawSphere(transform.position + _A + D, 0.1f);

            Gizmos.DrawLine(transform.position, transform.position + _A + D);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position + _A, transform.position + _B);

            Gizmos.color = Color.white;
            Gizmos.DrawSphere(transform.position + _A, 0.1f);
            Gizmos.DrawSphere(transform.position + _B, 0.1f);

            Gizmos.DrawWireSphere(transform.position, _radius);
        }

	#endregion

	#region Custom

	#endregion
	
}
