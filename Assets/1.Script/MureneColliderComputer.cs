﻿using UnityEngine;
using System.Collections;

public class MureneColliderComputer : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public Vector3 _value;
        public Transform _next;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{
            GetComponent<BoxCollider>().center = transform.InverseTransformPoint((_next.position + transform.position) * 0.5f);
		}

		void Update ()
		{
            //rigidbody.inertiaTensor = _value;
		}

	#endregion

	#region Custom

	#endregion
	
}
