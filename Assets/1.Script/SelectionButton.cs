﻿using UnityEngine;
using System.Collections;

public class SelectionButton : ReferencedMonoBehaviour {

    public float _heightThreshold;

    private Vector3 m_distance;
    private bool m_handTrack;
    private Transform m_hand;

    private Vector3 m_startPosition;

    public Vector3 _resetLocalPosition;

    public int _index;

    public Binomial _binomialLink;

    public bool _enabled = true;

	// Use this for initialization
	void Start () {
        base.LoadReferences();
        m_startPosition = transform.position;
	}

    void OnEnable()
    {
        if (_binomialLink._ate)
        {
            gameObject.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (rigidbody != null)
        {
            Vector3 position = transform.position;

            if(m_handTrack == true)
            {
                position.y = m_hand.position.y + m_distance.y;
            }
            else
            {
                position.y += (1 * Time.deltaTime);
            }

            if (position.y > m_startPosition.y)
            {
                position.y = m_startPosition.y;
            }

            if (position.y < m_startPosition.y - _heightThreshold)
            {
                m_handTrack = false;
                position.y = m_startPosition.y - _heightThreshold;

                ExecuteAction();

                enabled = false;
            }

            rigidbody.position = position;
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Hand")
        {
            m_handTrack = true;
            m_hand = collider.transform;
            m_distance = transform.position - collider.transform.position;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Hand")
        {
            m_handTrack = false;
        }
    }

    void ExecuteAction()
    {
        if (!references._phaseManager._isSwitching && _enabled == true)
        {
            references._binomialManager._numBinomialActive = 0;
            references._binomialManager._numPhase++;
            references._binomialManager._binomialToDisable = _binomialLink;
            references._binomialManager._buttonToDisable = this;

            references._fightingTable._nextIndex = _index;
            SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName(_binomialLink) + "_Selected" , _binomialLink.gameObject);

            _binomialLink.BinomialSelected();
            SdAudioManager.Trigger("CookBoard_ButtonOn", gameObject);

            SelectionButton[] buttons = transform.parent.parent.GetComponentsInChildren<SelectionButton>();
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i]._enabled = false;
                rigidbody.constraints ^= RigidbodyConstraints.FreezePositionY;
            }
        }
    }

    
}
