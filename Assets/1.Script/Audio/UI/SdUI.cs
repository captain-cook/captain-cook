﻿using UnityEngine;
using System.Collections;

public class SdUI : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        
        string[] objects = action.Split('_');
        switch (objects[0])
        {
            case "RecipeCountdown":
                SdAudioManager.SetSwitch("UI", objects[0], m_parentManager);
                SdAudioManager.Play("UI", m_parentManager); 
                break;
            case "RecipeResultFeedback":
                SdAudioManager.SetSwitch("UI", objects[0], m_parentManager);
                SdAudioManager.SetSwitch("RecipeResultLevel", objects[1], m_parentManager);
                SdAudioManager.Play("UI", m_parentManager); 
                break;
            case "Stamp":
            case "Jauge":
                if (objects.Length > 1 && objects[1] == "Stop")
                {
                    SdAudioManager.Stop("UI", 0, m_parentManager);
                    break;
                }
                SdAudioManager.SetSwitch("UI", objects[0], m_parentManager);
                SdAudioManager.Play("UI", m_parentManager);
                break;
            case "Map":
                if (objects.Length < 2)
                    break;
                if (objects[1] != "Wheel")
                {
                    source = m_parentManager;
                }
                if (objects[1] == "Stop")
                {
                    SdAudioManager.Stop("Map", 150, source);
                }
                else
                {
                    SdAudioManager.SetSwitch("Map", objects[1], source);
                    SdAudioManager.Play("Map", source);
                }
                break;
            case "Shop":
                if (objects[1] == "ChestOpen")
                {
                    SdAudioManager.Play("AMB_Shop", m_parentManager);
                }
                else if (objects[1] == "ChestClose")
                {
                    SdAudioManager.Stop("AMB_Shop", 1000, m_parentManager);
                }
                SdAudioManager.SetSwitch("Shop", objects[1], source);
                SdAudioManager.Play("Shop", source);
                break;
            case "Loot":
                SdAudioManager.SetSwitch("Rewards", objects[1], m_parentManager);
                SdAudioManager.Play("Rewards", m_parentManager);
                break;
            case "Round":
                SdAudioManager.SetSwitch("UI", objects[1], m_parentManager);
                SdAudioManager.Play("UI", m_parentManager);
                break;
            case "RecipeAnnounce":
                SdAudioManager.SetSwitch("VFXUI", "Announce", m_parentManager);
                SdAudioManager.SetSwitch("UIRecipeAnnounce", objects[1], m_parentManager);
                SdAudioManager.Play("VFX_UI", m_parentManager);
                break;
            default:
                break;
        }
    }
    public override void SetParameter(string parameter, float value, GameObject source = null)
    {
        
    }
}


