﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ListExtend
{
    public static T Last<T>(this List<T> list)
    {
        return list[list.Count - 1];
    }

    public static T FromLast<T>(this List<T> list, int index)
    {
        return list[list.Count - 1 - index];
    }

    public static T First<T>(this List<T> list)
    {
        return list[0];
    }

    public static void RemoveFirst<T>(this List<T> list)
    {
        list.RemoveAt(0);
    }

    public static void RemoveLast<T>(this List<T> list)
    {
        list.RemoveAt(list.Count - 1);
    }
}
