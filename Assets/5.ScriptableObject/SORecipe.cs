﻿using UnityEngine;
using System.Collections;


public class SORecipe : ScriptableObject {

    // INFORMATION
    public string _name;
    public Sprite _image;
    public GameObject _prefab;
    public float _recipeDuration;

    public GameObject _basket;

    //INTERACTION TYPE
    public enum Interaction
    {
        Cut,
        Peel,
        Smash,
        Break,
        Spread,
        Bake,
        Toast
    }

    // RECIPE TYPE
    public Type _recipeType;
    public enum Type
    {
        Entree,
        Meal,
        Desert
    }

    // STEPS
    public RecipeStep[] _recipeSteps;
    [System.Serializable]
    public struct RecipeStep
    {
        public SOIngredient _ingredient;
        public Interaction _action;
        public Sprite _imageInterraction;
        public float _count;
    }    
    
    
    public RecipeStep GetStep(int index)
    {
        return _recipeSteps[index];
    }

    // TASTES
    public SOTaste[] _tastes ;
}
