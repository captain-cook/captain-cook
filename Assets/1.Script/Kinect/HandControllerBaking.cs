﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandControllerBaking : CustomHandController {

    [HideInInspector]
    public GameObject _objectGrabbed;

    [HideInInspector]
    public HandController _handController;

    private Highlighter m_hoverIngredient;

    private GameObject m_preselectedObject;
    private BakableIngredient m_BakableIngredientGrabbed;
    
   

	// Use this for initialization
    void Awake()
    {
        base.LoadReferences();
        _handController = GetComponent<HandController>();  

        
    }
       	
	// Update is called once per frame
	void Update () {

        //Preselect
        if (_objectGrabbed == null && _handController.HandState != KinectBody.HandState.Closed)
        {
            PreselectInteractiveObject();
        }

        //Grab
        else if (_objectGrabbed == null && _handController.HandState == KinectBody.HandState.Closed && m_preselectedObject != null)
        {
            GrabPreselectedObject();
        }

        //Update object
        if (_objectGrabbed != null)
        {
            MoveGrabbedObject();
        }

        //Release
        if (_objectGrabbed != null && _handController.HandState == KinectBody.HandState.Open)
        {
            ReleaseGrabbedObject();
        }

       
	}

    void OnDisable()
    {
        //Release
        if (_objectGrabbed != null)
        {
            ReleaseGrabbedObject();
        }
    }

    bool IsGrabbable(GameObject go)
    {
        return (go.GetComponent<BakableIngredient>() != null);
    }

    void PreselectInteractiveObject()
    {
        //Unhighlight previous
        if (m_preselectedObject != null)
        {
            if (m_preselectedObject.GetComponent<Highlighter>() != null)
            {
                m_preselectedObject.GetComponent<Highlighter>().UnHighlight();
            }
            m_preselectedObject = null;
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, _handController._radius);
        List<GameObject> gameObjects = new List<GameObject>();

        float distance = float.MaxValue;

        //Recuperer l'objet le plus proche
        for (int i = 0; i < colliders.Length; i++)
        {
            if (IsGrabbable(colliders[i].gameObject) == true)
            {
                float d = Vector3.Distance(transform.position, colliders[i].transform.position);
                if (d < distance)
                {
                    distance = d;
                    m_preselectedObject = colliders[i].gameObject;
                }
            }
        }

        //Highlight un objet si il est dans le range
        if (m_preselectedObject != null)
        {
            if (_handController._otherHand.GetComponent<HandControllerBaking>()._objectGrabbed != m_preselectedObject
                && m_preselectedObject.GetComponent<Highlighter>() != null
                && m_preselectedObject.GetComponent<BakableIngredient>() != null
                && !m_preselectedObject.GetComponent<BakableIngredient>()._isToasting)
            {
                
                Highlighter hl = m_preselectedObject.GetComponent<Highlighter>();               
                hl._color = Color.yellow;
                hl.Highlight();
            }
        }
    }

    void GrabPreselectedObject()
    {

        BakableIngredient bIng = m_preselectedObject.GetComponent<BakableIngredient>();
        if (bIng != null && !bIng._isToasting )
        {
            bIng.SetHand(this);
            bIng._isGrabbed = true;
            m_BakableIngredientGrabbed = bIng;

            _objectGrabbed = m_preselectedObject;
            m_preselectedObject = null;

            Highlighter hl = _objectGrabbed.GetComponent<Highlighter>();
            if (bIng != null && bIng._isOnTheTrail)
            {
                hl._color = Color.green;
                hl.Highlight();
            }
                
            else if (hl != null)
            {
                hl.UnHighlight();
            }

            //Hide idle hand cursor
            _handController.Hide();
        }

       
    }

    void MoveGrabbedObject()
    {
        if (_objectGrabbed.rigidbody != null)
        {
            Vector3 position = transform.position;

            //normal behaviour, the ingredient follow the hand
            if (!m_BakableIngredientGrabbed._isOnTheTrail )
            {
                
                Quaternion rotation = _handController._rotation;

                if (_objectGrabbed.rigidbody != null && _objectGrabbed.rigidbody.isKinematic == false)
                {
                    _objectGrabbed.rigidbody.velocity = Vector3.zero;
                    _objectGrabbed.rigidbody.angularVelocity = Vector3.zero;
                }

                _objectGrabbed.transform.rotation = rotation;
                _objectGrabbed.transform.position = position;

                //Block the tool to the table
                if (_objectGrabbed.collider.bounds.min.y < references._table.collider.bounds.max.y)
                {
                    float y = _objectGrabbed.transform.position.y + (references._table.collider.bounds.max.y - _objectGrabbed.collider.bounds.min.y) - 0.01f;
                    _objectGrabbed.transform.position = new Vector3(_objectGrabbed.transform.position.x, y, _objectGrabbed.transform.position.z);
                }
            }
            //special behaviour for the enter/exit of the oven
            else 
            {
                m_BakableIngredientGrabbed.UpdatePositionInOven(position);                
            }
        }
    }

    public void ReleaseGrabbedObject()
    {
        //Show idle hand cursor
        _handController.Show();

        
        if (m_BakableIngredientGrabbed != null)
        {
            m_BakableIngredientGrabbed._isGrabbed = false;
            m_BakableIngredientGrabbed.SetHand(null);
            //bIng.StartBaking();

            //release on the oven
            if (m_BakableIngredientGrabbed._isInTheOven)
            {
                m_BakableIngredientGrabbed.StartBaking();
            }
        }

        _objectGrabbed = null;
    }
  
    
    
}
