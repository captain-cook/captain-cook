﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SdBoss : iSd
{
    private List<string> m_actionList;
    public SdBoss()
    {
        m_actionList = new List<string>();
        m_actionList.Add("AbortTaunt");
        m_actionList.Add("BeginCook");
        m_actionList.Add("Finished");
        m_actionList.Add("Looser");
        m_actionList.Add("MapTaunt");
        m_actionList.Add("NewIngredient");
        m_actionList.Add("RoundLose");
        m_actionList.Add("RoundWin");
        m_actionList.Add("Serving");
        m_actionList.Add("StartRecipe");
        m_actionList.Add("Winner");
    }

    public virtual void SetParameter(string parameter, float value, GameObject source = null) { }
    public virtual void Execute(string pirateaction, GameObject source = null)
    {
        string[] objects = pirateaction.Split('_');
        switch (objects[0])
        {
            case "Mama":
            case "Leeroy":
                SdAudioManager.SetSwitch("BossName", objects[0], source);
                SdAudioManager.SetSwitch("BossAction", objects[1], source);
                SdAudioManager.Play("VFXBoss", source);
				break;
            default:
                break;
        }

    }
}