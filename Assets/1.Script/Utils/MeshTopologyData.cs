﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshTopologyData : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start () {

		}

		void Update () {

		}

	#endregion

	#region Custom
    
        public void CalculateTopology()
        {
            Mesh mesh = GetComponent<MeshFilter>().sharedMesh;

            for (int i = 0; i < mesh.vertices.Length; i++)
            {
                List<int> trianglesIndex = new List<int>();
                for (int j = 0; j < mesh.triangles.Length; j+=3)
                {
                    if(mesh.triangles[j] == i)
                    {
                        trianglesIndex.Add((int)(j / 3f));
                        break;
                    }
                    if (mesh.triangles[j + 1] == i)
                    {
                        trianglesIndex.Add((int)(j / 3f));
                        break;
                    }
                    if (mesh.triangles[j + 2] == i)
                    {
                        trianglesIndex.Add((int)(j / 3f));
                        break;
                    }
                }
            }
        }

	#endregion

}
