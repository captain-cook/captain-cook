﻿using UnityEngine;
using System.Collections;

public class MiniRecipeCard : MonoBehaviour {

    
    public SORecipe _soRecipe;

    //reference to the item of the minicard
    public MeshRenderer _recipeImage;
    public TextMesh _title;

    //visual feedback of the minirecipe selection
    private Highlighter m_highlighter;
    private bool m_isActive = false;

    private MiniRecipeManager m_manager;
    private HandController m_hand;

    public bool _Locked = false;

	// Use this for initialization
	void Start () {

        m_manager = transform.parent.GetComponent<MiniRecipeManager>();
        m_highlighter = GetComponent<Highlighter>();
        LoadMiniRecipe();
	}
	

    //load every recipe field depending on the sorecipe
    void LoadMiniRecipe()
    {
        if (_soRecipe != null)
        {
            //set the recipe information
            _title.text = _soRecipe._name;
            //_recipeImage.material = m_manager._recipeCard.CreateMaterial(_soRecipe._image);
        }
    }

    //look if the player close his hand
    void OnTriggerStay(Collider other)
    {
        if (other.collider.tag == "Hand" && m_hand != null && m_hand.GetHandState() == KinectBody.HandState.Closed && _Locked == false)
        {
            m_manager._recipeCard.LoadRecipe(_soRecipe);
            //m_manager._recipeCard.PlayAnimation(true);
            SdAudioManager.Trigger("RecipeBoard_RecipePick", gameObject);
            m_manager.Activate(false);
        }
    }

    //set the card to onhover mode 
    void OnTriggerEnter(Collider other)
    {        
        if (other.collider.tag == "Hand")
        {
            m_isActive = true;
            m_highlighter.Highlight();
            m_hand = other.collider.GetComponent<HandController>();
        }
    }

    //set the card to no-onhover mode 
    void OnTriggerExit(Collider other)
    {        
        if (other.collider.tag == "Hand")
        {
            m_isActive = false;
            m_highlighter.UnHighlight();
            m_hand = null;
        }
    }
}
