﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseVerdict : Phase  {

   public PhaseVerdict()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerVerdict>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerVerdict>();
        
    }
	
    public override void Enter()
    {
        _references._handCamera.enabled = false;
        //_references._lootManager.ThrowLoots();
        
    }

    public override void Exit()
    {
        _references._verdictPanel.DisablePanel();
    }

    public override void EarlyEnter()
    {
        _references._finishPlatManager.gameObject.SetActive(true);
        _references._finishPlatManager.ResetFinishPlats();
        _references._finishPlatManager.SetRecipePrefab();
        _references._recipeSumUp.PlaySumUpAnimation(false);
    }

    public override void EarlyExit()
    {
        //_references._fightingTable.gameObject.SetActive(false);
        _references._finishPlatManager.RemovePrefab();
    }
}
