﻿using UnityEngine;
using System.Collections;

public class SoundOven : ReferencedMonoBehaviour {

    public bool _isOpen;

	// Use this for initialization
	void Start () {

        base.LoadReferences();

	}

    public void PlayOvenOpen()
    {
        if(_isOpen == false)
            SdAudioManager.Trigger("CookBoard_OvenOpen", references._oven.gameObject);
    }

    public void PlayOvenClose()
    {
        if (_isOpen == true)
            SdAudioManager.Trigger("CookBoard_OvenClose", references._oven.gameObject);
    }

    public void PlayOvenTurnOn()
    {
        SdAudioManager.Trigger("CookBoard_OvenTurnOn", references._oven.gameObject);

    }
}
