﻿using UnityEngine;
using System.Collections;


public class OldMap : ReferencedMonoBehaviour {

    
	public OldAnker _anker;
    public OldBoat _boat;
    
    public Camera _mapCamera;
	public Camera _mapCameraZoom;  

    //coordonnate values
    public Vector3 _leftTopWorld;
    public Vector3 _rightBotWorld;
    private float m_sizeWorld;

    public GameObject _sea;

    //store the last "hover" objectif  
    private Objectif m_objectifTargeted;
    public float _objectifRadius = 0.5f;

    private bool m_end = false;


	// Use this for initialization
	void Start () 
    {        
        _sea.SetActive(true);
        m_sizeWorld = _sea.transform.localScale.x;
        base.LoadReferences();
	}

	//get the input on the left map
    public void InputMap(HandControllerUI hand , Vector2 handRatio)
	{
        // grab the anker
        if (!_anker._isDraged && (GetAnkerPositionRelativeToMap() - handRatio).magnitude < 0.1f )
        {
            _anker._highlighter.Highlight();
            if (hand._handController.IsClosingHand())
            {
                _anker._highlighter.UnHighlight();
                _anker.StartDrag(hand);
            }
        }
        else if(!_anker._isDraged && (GetAnkerPositionRelativeToMap() - handRatio).magnitude >= 0.1f )
        {
            _anker._highlighter.UnHighlight();
        }
        //release the anker
        else if (_anker._isDraged && hand._handController.IsOpeningHand())
        {
            //StartCoroutine(references._phase1Manager.WaitBeforeSwitchingToCooking(1.5f));
            SdAudioManager.Trigger("Boat_Sail", SdAudioManager.Audioinstance.gameObject);
            Vector3 handPosition = new Vector3(m_sizeWorld * handRatio.x , 0 , m_sizeWorld * handRatio.y);
            _anker.MoveTo(handRatio, handPosition);
        }
        // drag the anker
        else if (_anker._isDraged)
        {
            Vector3 dragPosition = new Vector3(m_sizeWorld * handRatio.x, 0,  m_sizeWorld * handRatio.y );
            _anker.Drag(dragPosition);
        }
	}

    //get ht einput en the right zoomed map
    public void InputZoomMap(HandControllerUI hand , Vector2 handPosition , float ratio)
	{
        Vector3 correspondingPosition = _boat.transform.position;
        correspondingPosition.y = 0;
        correspondingPosition +=  new Vector3(handPosition.x, 0, handPosition.y) * _mapCameraZoom.orthographicSize * ratio;

        
        //look is we're hover an objectif
        bool objectifFound = false;
        foreach(Objectif obj in _boat._objectifs)
        {
            //Objectif found
            if ((correspondingPosition - obj.transform.position).magnitude < _objectifRadius)
            {
                // if there is a click
                if (hand._handController.IsClosingHand() && !m_end)
                {
                    m_end = true;
                    _boat.MoveTo(obj.transform.position);
                    references._fadinPanel.Fade(false, 7, 10, 3, 2);
                    references._fadinPanel.UpdatePanel("");
                    SdAudioManager.Play("DemoEnd", SdAudioManager.Audioinstance.gameObject);
                }

                // new objectif we enable the visual feedback
                if (obj != m_objectifTargeted)
                {                    
                    m_objectifTargeted = obj;
                    m_objectifTargeted.GetComponent<MeshRenderer>().material.color = Color.red;                    
                }

                objectifFound = true;
                break;
            }
        }

        // no objectif on hover we disable the visual feedback on the previous objectif
        if (!objectifFound && m_objectifTargeted != null)
        {
            m_objectifTargeted.GetComponent<MeshRenderer>().material.color = Color.white;
            m_objectifTargeted = null;
        }
	}

    // get the position of the anker proportionnaly to the map
    Vector2 GetAnkerPositionRelativeToMap()
    {
        Vector2 ratio;
        ratio.x = _anker.transform.localPosition.x  / m_sizeWorld;
        ratio.y = _anker.transform.localPosition.z / m_sizeWorld;
        return ratio;
    }
}
