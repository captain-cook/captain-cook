﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandControllerUI : CustomFixedHandController
{
    public Vector2 _screenPosition;
    public Camera _handCamera;

    public HandController _handController;

    void Awake()
    {
        _handController = GetComponent<HandController>();
    }

    public override void Update()
    {
        // get the coordinate of the hand on the screen 
        _screenPosition = _handCamera.WorldToScreenPoint(transform.position);

        base.Update();
    }

}