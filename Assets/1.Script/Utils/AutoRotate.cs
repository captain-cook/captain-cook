﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour {

	#region Enum

	#endregion

	#region Public

    public float _constantSpeed;

	#endregion

	#region Private

	#endregion

	#region Unity

		void Start ()
		{

		}

		void Update ()
		{
            Vector3 euler = transform.localEulerAngles;
            euler.y += (_constantSpeed * Time.deltaTime);
            transform.localEulerAngles = euler;
		}

	#endregion

	#region Custom

	#endregion
	
}
