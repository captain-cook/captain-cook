﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class AudioDistanceHelper : MonoBehaviour {

    public float _distance;
    public bool _play;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        _distance = Vector3.Distance(transform.position, Camera.main.transform.position);
	}
    protected void OnValidate()
    {
        
        if (_play)
        {
            AkSoundEngine.SetSwitch("SFXBoat", "Rattle", gameObject);
            AkSoundEngine.PostEvent("SFXBoat", gameObject);
        }
        else
        {
            AkSoundEngine.ExecuteActionOnEvent("_3DTest", AkActionOnEventType.AkActionOnEventType_Stop, gameObject);
        }
    }
}
