﻿using UnityEngine;
using System.Collections;

public class Radar : MonoBehaviour {

    private OldBoat m_boat;

	// Use this for initialization
	void Start () {

        m_boat = transform.parent.GetComponent<OldBoat>();

	}

    //detect enemy boat proximity
    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Objectif")
        {
            m_boat._objectifs.Add(other.GetComponent<Objectif>());
            Debug.Log(other.GetComponent<Objectif>());
            Debug.Log(m_boat._objectifs.Count);
        }
    }

    //detect enemy boat proximity
    void OnTriggerExit(Collider other)
    {
        
        if (other.tag == "Objectif")
        {
            Debug.Log("ONExit");
            m_boat._objectifs.Remove(other.GetComponent<Objectif>());
        }
    }
	
}
