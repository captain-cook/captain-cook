﻿using UnityEngine;
using System.Collections;

public class FightingTable : ReferencedMonoBehaviour {

    public Animator _pirateLeft;
    public Animator _pirateRight;

    public int _nextIndex;

    public Animator _rodny;
    public Animator _arnaud;
    public Animator _ming;

    public Animator _sbire1;
    public Animator _sbire2;
    public Animator _sbire3;

	// Use this for initialization
	void Start () {

        base.LoadReferences();
	}
	
	public void InitVerdict()
    {
        StartCoroutine("Eat");
    }

    public void SetPirateFromNextIndex()
    {
        _rodny.gameObject.SetActive(false);
        _arnaud.gameObject.SetActive(false);
        _ming.gameObject.SetActive(false);

        _sbire1.gameObject.SetActive(false);
        _sbire2.gameObject.SetActive(false);
        _sbire3.gameObject.SetActive(false);

        if (_nextIndex == 1)
        {
            _pirateLeft = _ming;
            _pirateRight = _sbire1;
        }
        else if (_nextIndex == 2)
        {
            _pirateLeft = _arnaud;
            _pirateRight = _sbire2;
        }
        else if (_nextIndex == 3)
        {
            _pirateLeft = _rodny;
            _pirateRight = _sbire3;
        }

        _pirateLeft.gameObject.SetActive(true);
        _pirateRight.gameObject.SetActive(true);
    }

    //launch eating animation
    public IEnumerator Eat()
    {
        SdAudioManager.Trigger("Pirate_Common_EatStart", _pirateLeft.gameObject);
        SdAudioManager.Trigger("Pirate_Common_EatStart", _pirateRight.gameObject);

        _pirateLeft.SetTrigger("Eat");
        _pirateRight.SetTrigger("Eat");
        yield return new WaitForSeconds(3);

        _pirateLeft.SetTrigger("EndEat");
        _pirateRight.SetTrigger("EndEat");

        SdAudioManager.Trigger("Pirate_Common_EatEnd", _pirateLeft.gameObject);
        SdAudioManager.Trigger("Pirate_Common_EatEnd", _pirateRight.gameObject);
        SdAudioManager.Trigger("Music_JuryVerdictStart");

        yield return new WaitForSeconds(3);

        SdAudioManager.Trigger("Music_JuryVerdictStop");

        yield return new WaitForSeconds(1);

        references._roundPanel.DisplayPanel();
    }

   
    //player won, launch the happy animation
    public void FeedBackWon()
    {
        _pirateLeft.SetTrigger("Win");
        _pirateRight.SetTrigger("Win");
    }

    //player lost, launch the sad animation
     public void FeedBackLost()
    {
        _pirateLeft.SetTrigger("Lose");
        _pirateRight.SetTrigger("Lose");
    }
}
