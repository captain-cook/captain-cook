Shader "Toon/BumpLerp" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Raw (RGB)", 2D) = "white" {}
		_CookedTex ("Cooked (RGB)", 2D) = "white" {}
		_BurntTex ("Burnt (RGB)", 2D) = "white" {}
		_Ratio ("Ratio", Range (0, 2)) = 0
		_BumpMap ("Normal map", 2D) = "bump" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

CGPROGRAM
#pragma surface surf ToonRamp

sampler2D _Ramp;

// custom lighting function that uses a texture ramp based
// on angle between light direction and normal
#pragma lighting ToonRamp exclude_path:prepass
inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
{
	#ifndef USING_DIRECTIONAL_LIGHT
	lightDir = normalize(lightDir);
	#endif

	half d = dot (s.Normal, lightDir)*0.5 + 0.5;
	half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;

	half4 c;
	c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
	c.a = 0;
	return c;
}


sampler2D _MainTex;
sampler2D _CookedTex;
sampler2D _BurntTex;

float _Ratio;

sampler2D _BumpMap;
float4 _Color;

struct Input {
	float2 uv_MainTex : TEXCOORD0;
	float2 uv_BumpMap;
};

void surf (Input IN, inout SurfaceOutput o) {
	half4 cA = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	half4 cB = tex2D(_CookedTex, IN.uv_MainTex) * _Color;
	half4 cC = tex2D(_BurntTex, IN.uv_MainTex) * _Color;

	half4 color = half4(0,0,0,0);
	if(_Ratio < 1)
	{
		color = lerp(cA, cB, _Ratio);
	}
	else
	{
		color = lerp(cB, cC, _Ratio - 1);
	}

	o.Albedo = color.rgb;
	o.Alpha = color.a;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
}
ENDCG

	}

	Fallback "Toon/Lit"
}
