﻿using UnityEngine;
using System.Collections;
using System;

public class PhaseArrival : Phase  {

   public PhaseArrival()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerArrival>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerArrival>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
    
    }

    public override void Exit()
    {

    }

    public override void EarlyEnter()
    {
        _references._boatMama.StartArrival();
        _references._labelManager.ActivateLabelsUI(false);
    }

    public override void EarlyExit()
    {
        _references._sky.skyIsFog = false;
        SdAudioManager.Trigger("Ambiances_General_Calm_Sunny");       
    }
}
