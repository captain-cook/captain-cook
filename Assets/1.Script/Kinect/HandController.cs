﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HandController : MonoBehaviour
{
    #region Enum

        public enum HandSide { Left, Right }

    #endregion

    #region Public

        public string _boneName = "WristLeft";
        public KinectBody _body;
        public HandSide _side; 
        public float _radius;
        public float _positionHistoryDuration = 1f;
        public int _handStateHistoryCount = 15;
        public int _speedHistoryCount = 10;
        public bool _debugHand = false;

        public Quaternion _rotation;

        public Mesh _open;
        public Mesh _closed;

        public Plot _velocityPlot;

        public HandController _otherHand;

        //public Transform _cube;


    #endregion

    #region Private

        private Transform m_handBone;
        private List<KinectBody.HandState> m_handStateHistory;
        private List<Vector3> m_positionHistory;
        private List<Vector3> m_velocityHistory;
        private List<Vector3> m_speedHistory;
        private List<float> m_positionHistoryTime;
        private KinectBody.HandState m_oldHandState; 
        private KinectBody.HandState m_currentHandState;

    #endregion

    #region Unity

        void Start () {
            m_positionHistory = new List<Vector3>();
            m_velocityHistory = new List<Vector3>();
            m_speedHistory = new List<Vector3>();
            m_positionHistoryTime = new List<float>();
            m_handStateHistory = new List<KinectBody.HandState>();

            m_handBone = _body.transform.SearchChild(_boneName);

            _velocityPlot._historyDuration = _positionHistoryDuration;

            if (_side == HandController.HandSide.Left)
            {
                _otherHand = transform.parent.SearchChild("HandRight").GetComponent<HandController>();
            }
            else
            {
                _otherHand = transform.parent.SearchChild("HandLeft").GetComponent<HandController>();
            }

            FileLogger.Clear("velocity.log");
            FileLogger.Clear("acceleration.log");
	    }
	
	    void Update () {
            _velocityPlot.Update(Time.deltaTime);

            //Get position from Body
            transform.localPosition = m_handBone.position;

            //Hold position and time history
            if(m_positionHistory.Count > 0)
            {
                if(Vector3.Distance(m_positionHistory.Last(), transform.position) > 0.05f) { AddPositionToHistory(); }
            }
            else { AddPositionToHistory(); }

            ClampPositionHistory();

            if (m_positionHistory.Count > 1)
            {
                //Get rotation from Body
                Vector3 up = m_positionHistory.FromLast(1) - m_positionHistory.Last();
                if (up.normalized.y < 0.1f)
                {
                    up = m_positionHistory.Last() - m_positionHistory.FromLast(1);
                }
                Vector3 direction = m_handBone.transform.position - m_handBone.transform.parent.position;
                _rotation = Quaternion.LookRotation(direction, up);
            }

            //Get current hand state
            KinectBody.HandState hs;
            if (_side == HandSide.Left)
            {
                hs = _body.LeftHandState;
            }
            else
            {
                hs = _body.RightHandState;
            }

            //Hold hand state history
            m_handStateHistory.Add(hs);
            if (m_handStateHistory.Count > _handStateHistoryCount) { m_handStateHistory.RemoveFirst(); }

            //use for kick track of the speed of hand
            m_speedHistory.Add(transform.position);
            if (m_speedHistory.Count > _speedHistoryCount)
            {
                m_speedHistory.RemoveFirst();
            }

            //Save old hand state
            m_oldHandState = m_currentHandState;

            //Select smooth hand state
            SelectHandState(hs);
            
            if(_debugHand == true)
            {
                DebugHandState();
            }

            if (m_currentHandState == KinectBody.HandState.Open)
            {
                GetComponent<MeshFilter>().sharedMesh = _open;
            }
            else if (m_currentHandState == KinectBody.HandState.Closed)
            {
                GetComponent<MeshFilter>().sharedMesh = _closed;
            }


            ////Test
            //if(_cube != null)
            //{
            //    //Vector3 average = Vector3.zero;
            //    //for (int i = 0; i < m_positionHistory.Count - 1; i++)
            //    //{
            //    //    Vector3 velocity = m_positionHistory[i + 1] - m_positionHistory[i];
            //    //    average = average + velocity;
            //    //}
            //    //average = average / (m_positionHistory.Count - 1);

            //    int i = m_positionHistory.Count - 1;

            //    Vector3 velocity = (m_positionHistory[i] - m_positionHistory[i - 1]) / (m_positionHistoryTime[i] - m_positionHistoryTime[i - 1]);

            //    _cube.transform.localScale = velocity;
            //    _cube.light.intensity = velocity.magnitude;
            //    _cube.particleSystem.emissionRate = velocity.magnitude * 10;
            //}
            
	    }

        void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, _radius);
        }

    #endregion

    #region Custom
    
        private void AddPositionToHistory()
        {
            if(m_positionHistory.Count > 1)
            {
                float deltaTime = m_positionHistoryTime.Last() - m_positionHistoryTime.FromLast(1);
                Vector3 speed = m_positionHistory.Last() - m_positionHistory.FromLast(1);
                Vector3 velocity = speed / deltaTime;

                _velocityPlot.AddPoint(velocity.z);
            }
            m_positionHistory.Add(transform.position);
            m_positionHistoryTime.Add(Time.time);
        }

        public void Hide()
        {
            renderer.enabled = false;
        }

        public void Show()
        {
            renderer.enabled = true;
        }

        public void ClearPositionHistory()
        {
            m_positionHistory.Clear();
            m_positionHistoryTime.Clear();
            m_velocityHistory.Clear();
        }

        public KinectBody.HandState HandState
        {
            get
            {
                return m_currentHandState;
            }
        }

        public List<Vector3> PositionHistory
        {
            get
            {
                return m_positionHistory;
            }
        }

        public List<Vector3> PositionHistoryWithTime(out List<float> time)
        {
            time = m_positionHistoryTime;
            return m_positionHistory;
        }
        
        //Remove all the history too old
        public void ClampPositionHistory()
        {
            for (int i = 0; i < m_positionHistory.Count; i++)
            {
                float timeKey = m_positionHistoryTime[i];
                if (Time.time - timeKey > _positionHistoryDuration)
                {
                    m_positionHistory.RemoveFirst();
                    m_positionHistoryTime.RemoveFirst();
                    i--;
                }
                else
                {
                    break;
                }
            }
        }

        //Filter the hand state to avoid some tracking issues
        void SelectHandState(KinectBody.HandState hs)
        {
            
            int n = 0;
            //KinectBody.HandState hs = m_handStateHistory.Last();

            for (int i = 1; i < m_handStateHistory.Count; i++)
            {
                if (m_handStateHistory[i] == hs)
                {
                    n++;
                }
            }

            if (n > m_handStateHistory.Count * 0.5f)
            {
                m_currentHandState = hs;
            }
        }

        void DebugHandState()
        {
            Color color = Color.white;
            if (m_currentHandState == KinectBody.HandState.Open)
            {
                color = Color.green;
            }
            else if (m_currentHandState == KinectBody.HandState.Closed)
            {
                color = Color.red;
            }
            else if (m_currentHandState == KinectBody.HandState.Lasso)
            {
                color = Color.blue;
            }

            color.a = 0.5f;
            renderer.material.color = color;
        }

        public bool IsEnterInNewState()
        {
            return m_oldHandState != m_currentHandState;
        }

        public bool IsClosingHand()
        {
            return IsEnterInNewState() && m_currentHandState == KinectBody.HandState.Closed;
        }

        public bool IsOpeningHand()
        {
            return IsEnterInNewState() && m_currentHandState == KinectBody.HandState.Open;
        }

        public bool IsLassingHand()
        {
            return IsEnterInNewState() && m_currentHandState == KinectBody.HandState.Lasso;
        }

        public KinectBody.HandState GetHandState()
        {
            return m_currentHandState;  
        }

        public float GetHandYSpeed()
        {
            float speed = 0;
            
            for (int i = _speedHistoryCount - 1 ; i > 0 ; i--)
            {
                speed += (m_speedHistory[i].y - m_speedHistory[i - 1].y);
            }

            return speed / _speedHistoryCount;
        }

    #endregion

}
