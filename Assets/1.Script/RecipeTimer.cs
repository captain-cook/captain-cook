﻿using UnityEngine;
using System.Collections;

public class RecipeTimer : ReferencedMonoBehaviour {

    private UILabel m_timer;
    private TweenAlpha m_tAlpha;
    private TweenAlpha m_tAlphaBG;
    private TweenScale m_tScale;
    private TweenColor m_tColor;
    private bool m_isDisplayed = false;

   
	// Use this for initialization
	void Awake () {

        base.LoadReferences();
        m_tAlphaBG = transform.parent.GetComponent<TweenAlpha>();
        m_tAlpha = GetComponent<TweenAlpha>();
        m_tScale = GetComponent<TweenScale>();
        m_tColor = GetComponent<TweenColor>();
        m_timer = GetComponent<UILabel>();

        m_tColor.duration = m_tScale.duration;
        
        //enabled = false;
        m_tScale.enabled = false;
        m_tColor.enabled = false;

        enabled = false;
	}

    // Update is called once per frame
    void Update()
    {

        float time = references._recipeManager.GetTimerProgress();
        m_timer.text = "" + ((int)time) + ":" + ((time % 1f) * 100).ToString("00");


    }

    void OnEnable()
    {
        m_isDisplayed = true;
        m_tAlpha.ResetToBeginning();
        m_tAlpha.PlayForward();

        m_tAlphaBG.ResetToBeginning();
        m_tAlphaBG.PlayForward();
    }

    public void FadeOut()
    {
        m_isDisplayed = false;
        m_tAlpha.PlayReverse();
        m_tAlphaBG.PlayReverse();
    }

    public void Disable()
    {
        if (!m_isDisplayed)
        {
            enabled = false;
        }        
    }

    //flash green le text
    public void EndAnim()
    {
        //scale
        transform.localScale = m_tScale.from;
        m_tScale.PlayForward();

        //color
        m_tColor.PlayForward();
    }

    public void AnimIsFinished()
    {
        if (transform.localScale.x > 1.4f)
        {
            //scale
            transform.localScale = m_tScale.to;
            m_tScale.PlayReverse();

            //color
            m_tColor.PlayReverse();
        }        
        
    }
}
