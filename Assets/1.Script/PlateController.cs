﻿using UnityEngine;
using System.Collections;

public class PlateController : ReferencedMonoBehaviour {

	#region Enum

	#endregion

	#region Public

        public AnimationCurve _progress;
        public AnimationCurve _rotation;
        public Transform _source;
        public Transform _destination;

        public int _hitTableKeyIndex = 3;

	#endregion

	#region Private

        private Timer m_timer;
        private float m_startTime;
        private bool m_hitTable = false;

	#endregion

	#region Unity

		void Start ()
		{
            base.LoadReferences();
		}

		void Update ()
		{
            if(m_timer != null)
            {
                transform.position = Vector3.Lerp(_source.position, _destination.position, _progress.Evaluate(Time.time - m_startTime));
                transform.rotation = Quaternion.Euler(0, _rotation.Evaluate(Time.time - m_startTime), 0);

                if(m_hitTable == false && m_timer.Current > _progress.keys[_hitTableKeyIndex].time)
                {
                    SdAudioManager.Trigger("Ingredient_PieFall", gameObject);
                    transform.FindChild("tarte").particleSystem.enableEmission = false;
                    transform.FindChild("FX_Dust").particleSystem.Play();
                    m_hitTable = true;
                }

                if(m_timer.IsElapsedOnce)
                {
                    //SdAudioManager.Trigger("Pirate_Rodny_PlayerEndRecipe", references._rodny.gameObject);
                    
                    m_timer = null;
                }
            }
		}

        public void PlayAnimation()
        {
            m_timer = new Timer(_progress.keys[_progress.length - 1].time);
            m_startTime = Time.time;
            SdAudioManager.Trigger("Music_EndRecipe");
        }

	#endregion

	#region Custom

	#endregion
	
}
