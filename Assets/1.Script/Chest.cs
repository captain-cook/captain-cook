﻿using UnityEngine;
using System.Collections;

public class Chest : MonoBehaviour {

    private Animation m_animation;
    private bool m_isOpen;

	// Use this for initialization
	void Start () {

        m_animation = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {

        //if (Input.GetKeyUp(KeyCode.LeftArrow))
        //{
        //    PlayChest(true);
        //}
        //else if (Input.GetKeyUp(KeyCode.RightArrow))
        //{
        //    PlayChest(false);
        //}

	}

    //open and close the chest
    public void PlayChest(bool open)
    {        
        m_animation.enabled = true;
        if (open && m_isOpen == false)
        {
            m_animation["Chest"].speed = 0.75f;
            m_animation["Chest"].time = 0;
            m_animation.Play();
            m_isOpen = true;
            SdAudioManager.Trigger("CookBoard_CountToolOpen", gameObject);
        }
        else if(open == false && m_isOpen == true)
        {
            m_animation["Chest"].speed = -0.55f;
            m_animation["Chest"].time = 0.8f;
            m_animation.Play();
            m_isOpen = false;
            SdAudioManager.Trigger("CookBoard_CountToolClose", gameObject);
        }
    }

    //public void StopAnimation()
    //{
    //    if (m_isOpen && m_animation["Chest"].time == m_animation["Chest"].length
    //        || !m_isOpen && m_animation["Chest"].time == 0)
    //    {
    //        m_animation.enabled = false;
    //    }
        
    //}
}
