﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandControllerBasket : CustomHandController
{

    [HideInInspector]
    public GameObject _objectGrabbed;

    [HideInInspector]
    private HandController m_handController;

    private Highlighter m_hoverIngredient;

    private GameObject m_preselectedObject;

	// Use this for initialization
	void Awake () {
        base.LoadReferences();

        m_handController = GetComponent<HandController>();     

	}
	
	// Update is called once per frame
	void Update ()
    {
        //Preselect
        if (_objectGrabbed == null && m_handController.HandState != KinectBody.HandState.Closed)
        {
            PreselectInteractiveObject();
        }

        //Grab
        else if (_objectGrabbed == null && m_handController.HandState == KinectBody.HandState.Closed && m_preselectedObject != null)
        {
            GrabPreselectedObject();
        }

        //Update object
        if (_objectGrabbed != null)
        {
            MoveGrabbedObject();
        }

        //Release
        if (_objectGrabbed != null && m_handController.HandState == KinectBody.HandState.Open)
        {
            ReleaseGrabbedObject();
        }
	}

    void OnDisable()
    {
        //Release
        if (_objectGrabbed != null)
        {
            ReleaseGrabbedObject();
        }
    }

    bool IsGrabbable(GameObject go)
    {
        return go.tag == "Ingredient" || go.tag == "Murene";
    }

    void PreselectInteractiveObject()
    {
        //Unhighlight previous
        if (m_preselectedObject != null)
        {
            if (m_preselectedObject.GetComponent<Highlighter>() != null)
            {
                m_preselectedObject.GetComponent<Highlighter>().UnHighlight();
            }
            m_preselectedObject = null;
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, m_handController._radius);
        List<GameObject> gameObjects = new List<GameObject>();

        float distance = float.MaxValue;

        //Recuperer l'objet le plus proche
        for (int i = 0; i < colliders.Length; i++)
        {
            if (IsGrabbable(colliders[i].gameObject) == true)
            {
                float d = Vector3.Distance(transform.position, colliders[i].transform.position);
                if (d < distance)
                {
                    distance = d;
                    m_preselectedObject = colliders[i].gameObject;
                }
            }
        }

        //Highlight un objet si il est dans le range
        if (m_preselectedObject != null)
        {
            if (m_handController._otherHand.GetComponent<HandControllerCooking>()._objectGrabbed != m_preselectedObject && m_preselectedObject.GetComponent<Highlighter>() != null)
            {
                m_preselectedObject.GetComponent<Highlighter>().Highlight();
            }
        }
    }

    void GrabPreselectedObject()
    {
        _objectGrabbed = m_preselectedObject;

        m_preselectedObject = null;

        references._basketManager.UnlinkIngredient(_objectGrabbed);

        if (_objectGrabbed.GetComponent<Highlighter>() != null)
        {
            _objectGrabbed.GetComponent<Highlighter>().UnHighlight();
        }

        //Hide idle hand cursor
        m_handController.Hide();
    }

    void MoveGrabbedObject()
    {
        if (_objectGrabbed.rigidbody != null)
        {
            Vector3 position = transform.position;
            Quaternion rotation = m_handController._rotation;

            if (_objectGrabbed.rigidbody != null && _objectGrabbed.rigidbody.isKinematic == false)
            {
                _objectGrabbed.rigidbody.velocity = Vector3.zero;
                _objectGrabbed.rigidbody.angularVelocity = Vector3.zero;
            }

            _objectGrabbed.transform.rotation = rotation;
            _objectGrabbed.transform.position = position;

            //Block the tool to the table
            if (_objectGrabbed.collider.bounds.min.y < references._table.collider.bounds.max.y)
            {
                float y = _objectGrabbed.transform.position.y + (references._table.collider.bounds.max.y - _objectGrabbed.collider.bounds.min.y) - 0.01f;
                _objectGrabbed.transform.position = new Vector3(_objectGrabbed.transform.position.x, y, _objectGrabbed.transform.position.z);
            }

        }

    }

    void ReleaseGrabbedObject()
    {
        //Show idle hand cursor
        m_handController.Show();

        if(_objectGrabbed.GetComponent<Ingredient>()._inBasket == true)
        {
            references._basketManager.LinkIngredient(_objectGrabbed);
        }

        _objectGrabbed = null;
    }
}
