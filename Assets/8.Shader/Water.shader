Shader "Custom/Water" {
		Properties {
        _Color ("Main Color", Color) = (1,1,1,0.5)
        _WaterTex1 ("Water Texture 1", 2D) = "white" { }
			_WaterTex2 ("Water Texture 2", 2D) = "white" { }
			_WaterTexLerpLevel ("Water Texture Lerp Level", Range (0, 1)) = 0

			_HeightMap1 ("Height 1", 2D) = "white" { }
			_HeightMap2 ("Height 2", 2D) = "white" { }
			_HeightMapLerpLevel ("Height Map Lerp Level", Range (0, 1)) = 0

			_HeightFactor ("Height Factor", Float) = 1
			_WaterSpeed ("Water Speed", Float) = 1
			_WaveSpeed ("Wave Speed", Float) = 1

			_RadiusWaves ("Radius Waves", Float) = 1
    }
    SubShader {
        Pass {

            CGPROGRAM
						#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

						float4 _Color;

						sampler2D _WaterTex1;
						float4 _WaterTex1_ST;
						sampler2D _WaterTex2;
						float _WaterTexLerpLevel;

						sampler2D _HeightMap1;
						sampler2D _HeightMap2;
						float _HeightMapLerpLevel;

						float _HeightFactor;
						float _WaterSpeed;
						float _WaveSpeed;
						float _RadiusWaves;

            struct v2f {
                float4 pos : SV_POSITION;
                fixed4 color : COLOR0;
								float2 uv: TEXCOORD0;
								float2 uvHeight: TEXCOORD1;
            };

            v2f vert (appdata_base v)
            {
                v2f o;
                o.pos = mul (UNITY_MATRIX_MVP, v.vertex);

								//Offset UV Texture
								o.uv = v.texcoord.xy * _WaterTex1_ST.xy + _WaterTex1_ST.zw;
								o.uv.x += (_Time.w * _WaterSpeed);

								//Offset UV HeightMap
								o.uvHeight = v.texcoord.xy;
								float2 dist = o.uvHeight - float2(0.5, 0.5);
								float distanceFromMiddle = sqrt(pow(dist.x, 2) + pow(dist.y, 2));
								o.uvHeight.x += (_Time.w * _WaveSpeed);

								distanceFromMiddle = min(distanceFromMiddle, _RadiusWaves);
								float radiusRatio = 1 - (distanceFromMiddle / _RadiusWaves);


								//Apply Height
								float height1 = tex2D(_HeightMap1, o.uvHeight).r;
								float height2 = tex2D(_HeightMap2, o.uvHeight).r;
								float height = lerp(height1, height2, _HeightMapLerpLevel);


								o.pos.y += (height * _HeightFactor * radiusRatio);

								o.color = _Color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
								float4 c1 = tex2D(_WaterTex1, i.uv);
								float4 c2 = tex2D(_WaterTex2, i.uv);
								float4 c = lerp(c1, c2, _WaterTexLerpLevel);
                return c * i.color;
            }
            ENDCG

        }
    }

		Fallback "Diffuse"
}
