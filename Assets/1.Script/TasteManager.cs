﻿using UnityEngine;
using System.Collections;

public class TasteManager : MonoBehaviour {

    public SOTaste[] _SOTastes;

    //return a SOtaste from the list
    public SOTaste GetRandomTaste()
    {
        int tasteIndex = Random.Range(0 , _SOTastes.Length);
        return _SOTastes[tasteIndex];
    }
	
}
