﻿using UnityEngine;
using System.Collections;

public class SdRecipeBoard : SdItems
{
    public override void Execute(string action, GameObject source = null)
    {
        switch (action)
        {
            case "Open":
            case "Close":
                SdAudioManager.SetSwitch("RecipeBoard_State", action, source);
                SdAudioManager.Play("RecipeBoard", source);
                break;
            case "RecipePick":
                SdAudioManager.SetSwitch("Recipe", "Change", source);
                SdAudioManager.Play("Recipe", source);
                break;
            default:
                break;
        }
    }
}


