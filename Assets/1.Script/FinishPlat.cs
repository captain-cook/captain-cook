﻿using UnityEngine;
using System.Collections;

public class FinishPlat : MonoBehaviour {

    private TweenPosition m_tPosition;
    private TweenScale m_tScale;

    public ParticleSystem _particle;

	// Use this for initialization
	void Awake () {

        //renderer.enabled = false;               

        m_tPosition = GetComponent<TweenPosition>();
        m_tScale = GetComponent<TweenScale>();
        m_tScale.duration = m_tPosition.duration;

        m_tPosition.enabled = false;
        m_tScale.enabled = false;

        m_tPosition.ResetToBeginning();
        m_tScale.ResetToBeginning();

	}

    void Start()
    {
        Init();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Init()
    {
        MeshRenderer rend = GetComponentInChildren<MeshRenderer>();
        if(rend != null)
        {
            rend.enabled = true;
        }
        _particle.Emit(60);
        StartCoroutine("WaitBeforePop");
    }

    IEnumerator WaitBeforePop()
    {
        yield return new WaitForSeconds(0.3f);
        MeshRenderer rend = GetComponentInChildren<MeshRenderer>();
        if (rend != null)
        {
            rend.enabled = true;
        }
        //renderer.enabled = true;        
    }

    public void Push()
    {
        m_tPosition.ResetToBeginning();
        m_tPosition.PlayForward();
        m_tScale.ResetToBeginning();
        m_tScale.PlayForward();
    }

    public void Reset()
    {
        transform.localPosition = m_tPosition.from;
        transform.localScale = m_tScale.from;
    }
}
