﻿using UnityEngine;
using System.Collections;

public static class MathPlus {

    public static float Map(float value, float min1, float max1, float min2, float max2)
    {
        return (value - min1) / (max1 - min1) * (max2 - min2) + min2;
    }

    public static int Floor(float val)
    {
        return (int)Mathf.Floor(val);
    }

    public static bool IsInRange(float value, float min, float max)
    {
        return (value >= min && value <= max);
    }

    public static bool IsLinePlaneIntersection(Vector3 a, Vector3 b, Plane plane, out Vector3 intersection)
    {
        bool ret = false;
        intersection = Vector3.zero;

        if(plane.SameSide(a, b) == false)
        {
            Ray ab = new Ray(a, b - a);
            float distance = 0;
            if(plane.Raycast(ab, out distance) == true)
            {
                intersection = ab.GetPoint(distance);
                ret = true;
            }
        }
        return ret;
    }

    public static bool IsLinePlaneIntersection(Vector3 a, Vector3 b, Plane plane, out Vector3 intersection, out bool sideA, out bool sideB, out float distanceFromPlane)
    {
        bool ret = false;
        intersection = Vector3.zero;
        distanceFromPlane = 0;

        sideA = plane.GetSide(a);
        sideB = plane.GetSide(b);

        if (sideA != sideB)
        {
            Ray ab = new Ray(a, b - a);
            if (plane.Raycast(ab, out distanceFromPlane) == true)
            {
                intersection = ab.GetPoint(distanceFromPlane);
                ret = true;
            }
        }
        return ret;
    }

}
