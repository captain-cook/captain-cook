﻿using UnityEngine;
using System.Collections;

public class SOIngredient : ScriptableObject {

    public string _name;

    public Sprite _image;

    public GameObject _prefab;
}
