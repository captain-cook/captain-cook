﻿using UnityEngine;
using System.Collections;

public class Ingredient : ReferencedMonoBehaviour {

    public enum SliceType { Soft, Hard, Egg, Coco }

    public bool _inBasket = true;
    public int _indexInSteps;
    public bool _forceInWorkspace;

    public SliceType _sliceType;
    public GameObject _fxDropsPrefab;

    public AnimationCurve _bumpCurve;
    public Timer _bumpTimer;
    public Vector3 _bumpStartPosition;

    public float _startCuttingTime;

    public bool _couldBeGrabbed = true;

    public bool _soundOnCollision;

    private Bezier m_toWok;
    private Timer m_timerToWok;

    private Vector3 m_startPositionToCuttingBoard;
    private Timer m_timerToCuttingBoard;

    void Awake()
    {
        base.LoadReferences();
    }

    void Update()
    {
        if (m_timerToWok != null)
        {
            transform.position = m_toWok.GetPosition(m_timerToWok.CurrentNormalized);
            if (m_timerToWok.IsElapsedLoop == true)
            {
                m_timerToWok = null;
                //TEMPORAIRE
                Destroy(gameObject);
                //gameObject.SetActive(false);
            }
        }

        if (m_timerToCuttingBoard != null)
        {
            transform.position = Vector3.Lerp(m_startPositionToCuttingBoard, references._cuttingBoard, m_timerToWok.CurrentNormalized);
            if(references._workspace.InWorkspace(transform.position) == true)
            {
                _forceInWorkspace = true;
            }
            if(m_timerToCuttingBoard == null)
            {
                m_timerToCuttingBoard = null;
            }
        }

        if (IsOutOfTriggerBasket() == true && _inBasket == true)
        {
            if(GetComponent<BreakableObject>() != null)
            {
                SdAudioManager.Trigger("Pirate_" + references._binomialManager.GetPirateName() + "_IngredientBreak", references._fightingTable._pirateLeft.gameObject);
            }
            references._phaseManager.SwitchPhase("PhaseCooking");
            transform.localScale /= references._basketManager._ratioInitialize;
            references._recipeManager.SetStep(GetComponent<Ingredient>());

            _inBasket = false;
        }

        if(_bumpTimer != null)
        {
            if(_bumpTimer.IsTimeElapsedOnce(_startCuttingTime) == true)
            {
                OnStartCutting();
            }

            if (references._knife.GetComponent<Tool>()._inSlot == true)
            {
                _bumpTimer.ForceToElapse();
            }

            if(_bumpTimer.IsElapsedOnce == true)
            {
                OnStopCutting();
            }

            if (_bumpTimer.IsElapsedLoop == true)
            {
                collider.enabled = true;
                _bumpTimer = null;
            }
            else
            {
                Vector3 position = _bumpStartPosition;
                position.y += _bumpCurve.Evaluate(_bumpTimer.Current);
                transform.position = position;
            }
        }
    }

    void LateUpdate()
    {
        if(_forceInWorkspace == true)
        {
            Vector3 position = transform.position;
            if (position.x < references._workspace.MinX) position.x = references._workspace.MinX;
            if (position.x > references._workspace.MaxX) position.x = references._workspace.MaxX;
            if (position.z < references._workspace.MinZ) position.z = references._workspace.MinZ;
            if (position.z > references._workspace.MaxZ) position.z = references._workspace.MaxZ;
            transform.position = position;
        }

    }

    bool IsOutOfTriggerBasket()
    {
        Vector3 basketPosition = references._basketTrigger.transform.position;
        Vector3 projection = Vector3.Project(transform.position - basketPosition, Vector3.up);
        Vector3 closestPoint = basketPosition + projection;
        if (Vector3.Distance(closestPoint, transform.position) <= references._basketTrigger.radius)
        {
            return false;
        }
        return true;
    }

    /*void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Basket" && _inBasket == true)
        {
            Debug.Log(gameObject.name);
            references._phaseManager.SwitchPhase("PhaseCooking");
            transform.localScale /= references._basketManager._ratioInitialize;
            references._recipeManager.SetStep(GetComponent<Ingredient>());

            _inBasket = false;
        }
    }*/

    void OnCollisionEnter(Collision col)
    {
        if (_soundOnCollision == true && col.gameObject.tag == "Table")
        {
            SdAudioManager.Trigger("Ingredient_Fall", gameObject);
            _soundOnCollision = false;
        }
    }

    void OnStartCutting()
    {
        references._knife._enabled = true;
    }

    void OnStopCutting()
    {
        references._knife.ResetSlicer();
        references._knife._enabled = false;

        references._knife.GetComponent<Tool>()._couldBeGrabbed = false;
    }

    public void Bump()
    {
        if(_inBasket == false)
        {
            _bumpTimer = new Timer(_bumpCurve.keys[_bumpCurve.length - 1].time);
            collider.enabled = false;
            _bumpStartPosition = transform.position;

            references._knife.GetComponent<Tool>()._couldBeGrabbed = false;
            references._knife.GetComponent<Tool>()._couldBeReleased = false;
            references._knife._enabled = false;

            _couldBeGrabbed = false;
        }
    }

    public void ToWok(Vector3 wokPosition, float time, float upMultiplier)
    {
        m_toWok = new Bezier(transform.position, transform.position + (Vector3.up * upMultiplier), wokPosition + (Vector3.up * upMultiplier), wokPosition);
        m_timerToWok = new Timer(time);
        _forceInWorkspace = false;
    }

    public void ToCuttingBoard()
    {
        m_startPositionToCuttingBoard = transform.position;
        m_timerToCuttingBoard = new Timer(1f);
    }
	
}
