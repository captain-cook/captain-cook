﻿using UnityEngine;
using System.Collections;

public class PhaseBinomial : Phase {

	public PhaseBinomial()
    {
        _camPosition = PhaseManager.CamPosition.Front;
        _customHandControllerLeft = _references._handLeft.GetComponent<HandControllerBinomial>();
        _customHandControllerRight = _references._handRight.GetComponent<HandControllerBinomial>();
        
    }
	
    public override void Enter()
    {
        base.Enter();
                
        _references._handCamera.enabled = false;
        _references._binomialSelectionButtons.animation.Play("Support_ButtonIn");
        
        if (_references._binomialManager._numPhase == 0)
        {
            SdAudioManager.Trigger("Music_WaitingForRound_Start");
        }
        else{
            SdAudioManager.Trigger("Music_WaitingForRound_Resume");

        }
        _references._starterRound.DisplayStarterRound();

        SelectionButton[] buttons = _references._binomialSelectionButtons.GetComponentsInChildren<SelectionButton>();
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i]._enabled = true;
        }
        
    }

    public override void Exit()
    {
        _references._binomialManager.ResetSetpanels();
        _references._binomialSelectionButtons.SetActive(false);
        _references._binomialManager._binomialToDisable.gameObject.SetActive(false);
        _references._binomialManager._buttonToDisable.transform.parent.gameObject.SetActive(false);

        _references._binomialSelectionButtons.animation.Play("Support_ButtonOut");

        if (_references._binomialManager._numPhase == 3)
        {
            SdAudioManager.Trigger("Music_WaitingForRound_Stop");
        }
        else
        {
            SdAudioManager.Trigger("Music_WaitingForRound_Pause");
        }

    }

    public override void EarlyEnter()
    {
        _references._binomialManager._numBinomialActive = 0;
        _references._binomialManager.SetTastes();
        _references._binomialManager.ResetSetpanels();
        _references._binomialSelectionButtons.SetActive(true);        
        _references._binomialManager.DisableBinomial();
    }

    public override void EarlyExit()
    {
    }
}
