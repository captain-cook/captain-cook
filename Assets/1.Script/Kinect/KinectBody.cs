﻿using UnityEngine;
using System.Collections;

public class KinectBody : MonoBehaviour {

    public enum HandState { Unknown, NotTracked, Open, Closed, Lasso }
    public HandState LeftHandState;
    public HandState RightHandState;

    private bool m_dirty;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        m_dirty = false;
	}

    public void TriggerDirty()
    {
        m_dirty = true;
    }

    public bool IsDirty
    {
        get
        {
            return m_dirty;
        }
    }
}
